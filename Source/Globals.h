/*
 * Globals.h
 *
 *  Created on: Dec 19, 2010
 *      Author: Administrator
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <string>

class QWidget;

//! simSCADA open database filter
extern std::string gSsOpenDbFilter;

const std::string DUAL_ALIAS_TAG = "_dual";
const std::string DUAL_NAME_TAG = " Dual";

const int AGENT_TCP_TIMEOUT = 3000;

QWidget*
MsgBoxParent();

#endif /* GLOBALS_H_ */
