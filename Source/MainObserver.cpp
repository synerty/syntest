/*
 * MainAutoTestObserver.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainObserver.h"

#include "Main.h"

MainObserver::MainObserver()
{
}
// ----------------------------------------------------------------------------

MainObserver::~MainObserver()
{
}
// ----------------------------------------------------------------------------

void
MainObserver::mainUiModeChanged(main_ui::UiModeE oldUiMode, main_ui::UiModeE newUiMode)
{
}
// ----------------------------------------------------------------------------

