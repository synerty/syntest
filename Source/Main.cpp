/*
 * main.cpp
 *
 *  Created on: Feb 4, 2010
 *      Author: Administrator
 */

#include "Main.h"

#include <iostream>
#include <stdexcept>
#include <string>

#if defined(__WIN32__) && !defined(DEBUG)
#include <wincon.h>
#endif

#include <boost/format.hpp>

#include <QtGui/qapplication.h>
#include <QtGui/qmessagebox.h>

#include "SettingsUi.h"

#include "MainSimModel.h"
#include "MainSynModel.h"
#include "PointControl.h"
#include "Proactor.h"

#include "MainUi.h"

Q_DECLARE_METATYPE(std::string)

// ----------------------------------------------------------------------------
//                         main - Application Entry
// ----------------------------------------------------------------------------

class RsApplication : public QApplication
{
  public:
    RsApplication(int &argc, char **argv) :
        QApplication(argc, argv)
    {
    }

    bool
    notify(QObject* obj, QEvent* evt)
    {
      try
      {
        return QApplication::notify(obj, evt);
      }
      catch (std::exception& e)
      {
        QString msg = "An application exception has occured :\n";
        msg += e.what();
        QMessageBox::critical(NULL, "Application Exception Occured", msg);
        return false;
      }

      return true;
    }
};

int main(int argc, char *argv[])
{
#if defined(__WIN32__) && !defined(DEBUG)
  FreeConsole();
#endif

  RsApplication a(argc, argv);

  QApplication::setApplicationName("SynTEST");
  QApplication::setApplicationVersion("1.0.0");
  QApplication::setOrganizationName("Synerty Pty Ltd");
  QApplication::setOrganizationDomain("synerty.com.au");

  try
  {
    Main::Inst().Run();
  }
  catch (std::exception& e)
  {
    QString msg = "An application exception has occured :\n";
    msg += e.what();
    QMessageBox::critical(NULL, "Application Exception Occured", msg);
  }

  return a.exec();
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
//                                 Main class
// ----------------------------------------------------------------------------

// Excplitit instantiation
#include "Observable.ini"
template class Observable< MainObserver > ;

// Initialise static variables
boost::shared_ptr< Main > Main::sInst = boost::shared_ptr< Main >(new Main());

// ----------------------------------------------------------------------------

Main::Main() :
    mUiMode(main_ui::blankUiMode), //
    mProactor(new Proactor()), //
    mWakeQtTimer(mProactor->ioService())
{
}
// ----------------------------------------------------------------------------

Main&
Main::Inst()
{
  return *sInst;
}
// ----------------------------------------------------------------------------

Main::~Main()
{
}
// ----------------------------------------------------------------------------

void
Main::Run()
{
  assert(mProactor.get());
  mProactor->run();
  wakeQtEventLoop();

  mGui.reset(new MainUi(*this));
  mGui->show();
  defaultUiMode();

  while (!MainSynModel::Inst().isOrmLoaded())
  {
    try
    {
      std::string synDbFileName = Settings().synTestModelFileName();
      if (synDbFileName.empty())
        mGui->openSynTestDatabase();
      else
        MainSynModel::Inst().loadOrmModel(synDbFileName);
    }
    catch (std::exception& e)
    {
    }
  }

  while (!MainObjectModel::Inst().isOrmLoaded())
  {
    try
    {
      std::string simDbFileName = Settings().simScada().DbFilename;
      if (simDbFileName.empty())
        mGui->openSimScadaDatabase();
      else
        MainObjectModel::Inst().loadOrmModel(simDbFileName);
    }
    catch (std::exception& e)
    {
    }
  }

}
// ----------------------------------------------------------------------------

void
Main::openSettings()
{
  SettingsUi().run();
}
// ----------------------------------------------------------------------------

void
Main::exitApplication()
{
  mProactor->terminate();

// Tell QT to delete the object, it probably is already
  mGui->deleteLater();
  mGui.release();
  QApplication::exit(0);
  sInst.reset();
}
// ----------------------------------------------------------------------------

void
Main::mainGuiClosing()
{
  exitApplication();
}
// ----------------------------------------------------------------------------

// ---------------
// Glaboal UI related interface

void
Main::selectUiMode(main_ui::UiModeE uiMode,
    QWidget* ui,
    boost::shared_ptr< MainUiCenter > newDisplay)
{
  if (mCenterDisplay)
    mCenterDisplay->mainUiCenterClose();

  mCenterDisplay = newDisplay;
  main_ui::UiModeE oldUiMode = uiMode;
  mUiMode = uiMode;
  mGui->setCentralWidget(ui);
  notifyOfMainUiModeChange(oldUiMode);
}
// ----------------------------------------------------------------------------

void
Main::defaultUiMode()
{
  PointControl::launch();
}
// ----------------------------------------------------------------------------

// ---------------
// Glaboal interface

QWidget*
Main::MsgBoxParent()
{
  assert(mGui.get());
  return mGui.get();
}
// ----------------------------------------------------------------------------

ProactorPtrT
Main::proactor()
{
  assert(mProactor.get());
  return mProactor;
}
// ----------------------------------------------------------------------------

void
Main::wakeQtEventLoop()
{
  if (!(mProactor.get() && mProactor->isRunning()))
    return;

  QMetaObject::invokeMethod(this, "wakeQtEventLoopSlot");

  mWakeQtTimer.expires_from_now(boost::posix_time::milliseconds(100));
  mWakeQtTimer.async_wait(bind(&Main::wakeQtEventLoop, shared_from_this()));
}
// ----------------------------------------------------------------------------

void
Main::wakeQtEventLoopSlot()
{
  if (mGui.get())
    mGui->repaint();
  QApplication::processEvents();
}
// ----------------------------------------------------------------------------

void
Main::notifyOfMainUiModeChange(main_ui::UiModeE oldUiMode)
{
// Some observers remove them selves during the notification
  ObserverPtrsT observers;
  getObserverPtrs(observers);

  for (ObserverPtrsT::iterator itr = observers.begin(); itr != observers.end();
      ++itr)
    (*itr)->mainUiModeChanged(oldUiMode, mUiMode);
}
// ----------------------------------------------------------------------------

// ---------------
// Private functions

// ----------------------------------------------------------------------------

