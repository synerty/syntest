/*
 * MainDeclarations.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef MAINDECLARATIONS_H_
#define MAINDECLARATIONS_H_


class MainUi;
class Main;
class MainObserver;

namespace main_ui
{

  enum UiModeE
  {
    blankUiMode,
    pointControlUiMode,
    autoTestUiMode
  };

} /* namespace main_ui*/

#endif /* MAINDECLARATIONS_H_ */
