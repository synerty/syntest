/*
 * PointControl.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "PointControl.h"

#include <assert.h>

#include "Main.h"
#include "MainSimControl.h"
#include "MainSimModel.h"
#include "PointControlUi.h"
#include "Settings.h"
#include "ObjectTreeFilterUi.h"

PointControl::PointControl() :
    mInitialising(false)
{
}
// ----------------------------------------------------------------------------

PointControl::~PointControl()
{
  resetSelection();
  MainSim::Inst().disconnectFromSim();
}
// ----------------------------------------------------------------------------

void
PointControl::launch()
{
  PtrT ptr(new PointControl);
  boost::shared_ptr< MainObserver > basePtr(ptr);
  Main::Inst().addObserver(basePtr);
  ptr->run();
}
/// -------------------------------------------------------------------------

void
PointControl::run()
{
  mInitialising = true;

  mUi.reset(new PointControlUi(*this));
  boost::shared_ptr<PointControlUi> baseUi(mUi);
  MainSim::Inst().addObserver(baseUi);

  simOrmModelChanged(MainObjectModel::Inst().ormModel());

  Main::Inst().selectUiMode(main_ui::pointControlUiMode, mUi.get(), shared_from_this());

  mInitialising = false;
}
// ----------------------------------------------------------------------------

// ---------------
// MainUiCenter implementation

void
PointControl::mainUiCenterClose()
{
  // No special cleanup required
}
// ----------------------------------------------------------------------------

// ---------------
// MainObserver implementation

void
PointControl::mainUiModeChanged(main_ui::UiModeE oldUiMode,
    main_ui::UiModeE newUiMode)
{
}
// ----------------------------------------------------------------------------

// ---------------
// MainSimModelObserver implementation

void
PointControl::simOrmModelChanged(boost::weak_ptr< db_simscada::SimScadaModel > ormModel)
{
  resetSelection();

  mSimOrmModel = ormModel.lock();

  if (mSimOrmModel.get())
  {
    ObjectTreeFilterSettings filter = Settings().objectTreeFilter();
    mPlantModel.reset(new PlantModel(mSimOrmModel, filter));
    mSsQtModel.reset(new SsQtModel(mPlantModel, filter));
  }
  else
  {
    mPlantModel.reset();
    mSsQtModel.reset();
  }

  mUi->objectQtModelChanged(mSsQtModel);
}
// ----------------------------------------------------------------------------

ObjectTreeFilterSettings
PointControl::objectTreeFilter()
{
  return Settings().objectTreeFilter();
}
// ----------------------------------------------------------------------------

void
PointControl::objectFilterChange()
{
  ObjectTreeFilterSettings filter = Settings().objectTreeFilter();

  ObjectTreeFilterUi ui;
  ui.SetFilter(filter);

  if (ui.exec() == ObjectTreeFilterUi::Rejected)
    return;

  filter = ui.Filter();

  Settings().setObjectTreeFilter(filter);

  objectTreeFilterChanged(filter);
}
// ----------------------------------------------------------------------------

// ---------------
// PointControl implementation

void
PointControl::objectTreeFilterChanged(const ObjectTreeFilterSettings& filter)
{
  mUi->objectTreeFilterChanged(filter);
  resetSelection();
  mPlantModel.reset(new PlantModel(mSimOrmModel, filter));
  mSsQtModel.reset(new SsQtModel(mPlantModel, filter));
  mUi->objectQtModelChanged(mSsQtModel);
}
// ----------------------------------------------------------------------------

boost::shared_ptr< const PlantModel >
PointControl::plantModel()
{
  return mPlantModel;
}
// ----------------------------------------------------------------------------

void
PointControl::qtModelIndexSelected(const QModelIndex& index)
{
  assert(mSsQtModel.get() != NULL);
  PlantModel::ObjectCptr obj = mSsQtModel->plantModelObject(index);

  MainObjectModelObserver::SimObjectsSelectedT objects;

  switch (obj->type)
  {
    case PlantModel::Object::Protocol:
    case PlantModel::Object::Port:
    case PlantModel::Object::Channel:
    case PlantModel::Object::Rtu:
    case PlantModel::Object::DataGroup:
    case PlantModel::Object::Point:
      objects.push_back(obj->simObject);
      break;

    case PlantModel::Object::PointGroup:
      for (PlantModel::ObjectsT::const_iterator itr = obj->children.begin(); //
      itr != obj->children.end(); //
          ++itr)
      {
        PlantModel::ObjectPtr child = *itr;
        if (child->type == PlantModel::Object::Point)
          objects.push_back(child->simObject);
      }
      break;

    case PlantModel::Object::UnknownType:
      break;
  }

  mUi->simObjectsSelected(objects);
  MainSim::Inst().simObjectsSelected(objects);
}
// ----------------------------------------------------------------------------

void
PointControl::resetSelection()
{
  // Reset the selected objects
  MainObjectModelObserver::SimObjectsSelectedT objects;
  MainSim::Inst().simObjectsSelected(objects);

  assert(mUi.get());
  mUi->simObjectsSelected(objects);
}
// ----------------------------------------------------------------------------
