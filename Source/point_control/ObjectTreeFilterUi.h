#ifndef OBJECTTREEFILTER_H
#define OBJECTTREEFILTER_H

#include <QtGui/QDialog>

#include "ui_ObjectTreeFilterUi.h"

#include "ObjectTreeFilterSettings.h"

class ObjectTreeFilterUi : public QDialog
{
  Q_OBJECT

  public:
    ObjectTreeFilterUi();
    virtual
    ~ObjectTreeFilterUi();

  public:
    ObjectTreeFilterSettings
    Filter();

    void
    SetFilter(const ObjectTreeFilterSettings& filter);

  private:
    Ui::ObjectTreeFilterUiClass ui;
};

#endif // OBJECTTREEFILTER_H
