#include "AnaPointUi.h"

#include "MainSimControl.h"

AnaPointUi::AnaPointUi(QWidget *panel, db_simscada::sim::ObjectPtrT simObj)
: mSimObj(simObj)
{
  ui.setupUi(panel);

  connect(ui.btnSetDefault, SIGNAL(clicked()), this, SLOT(SetNormalValue()));
  connect(ui.btnSetRaw, SIGNAL(clicked()), this, SLOT(SetRawValue()));
  connect(ui.btnSetEng, SIGNAL(clicked()), this, SLOT(SetEngValue()));
}

AnaPointUi::~AnaPointUi()
{

}

void
AnaPointUi::setData(const sim_dll::Server::AnaPointInfo& data)
{
  using namespace sim_dll;

  ui.txtRawValue->setValue(data.rawValue);
  ui.txtEngValue->setValue(data.value);
  ui.txtNormalValue->setValue(data.defaultValue);
}

void
AnaPointUi::SetRawValue()
{
  MainSim::Inst().setAnaRawValue(mSimObj, ui.txtRawValue->value());
}

void
AnaPointUi::SetEngValue()
{
  MainSim::Inst().setAnaEngValue(mSimObj, ui.txtEngValue->value());
}

void
AnaPointUi::SetNormalValue()
{
  MainSim::Inst().setAnaEngValue(mSimObj, ui.txtNormalValue->value());
}
