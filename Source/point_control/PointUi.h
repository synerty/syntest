#ifndef _POINT_UI_H_
#define _POINT_UI_H_

#include <QtGui/QWidget>
#include "ui_PointUi.h"

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <simscada/DbSimscadaDeclaration.h>

#include "MainSimControlObserver.h"

class DigPointUi;
class AnaPointUi;
class CtrlPointUi;

class PointUi : public QWidget,
    public boost::enable_shared_from_this< PointUi >,
    public MainSimObserver
{
  Q_OBJECT

  public:
    PointUi(QWidget* parent, db_simscada::sim::ObjectPtrT simObj);
    virtual
    ~PointUi();

  private:
    // ---------------
    // MainSimObserver interface

    void
    objectUpdateReceived(ObjectUpdate info);

    void
    simulatorDisconnected();

  private:
    Ui::PointUiClass ui;

    db_simscada::sim::ObjectPtrT mSimObj;

    boost::shared_ptr< DigPointUi > mDigPointUi;
    boost::shared_ptr< AnaPointUi > mAnaPointUi;
    boost::shared_ptr< CtrlPointUi > mCtrlPointUi;
};

#endif // _POINT_UI_H_
