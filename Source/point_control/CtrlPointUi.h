#ifndef CTRLPOINTUI_H
#define CTRLPOINTUI_H

#include <boost/shared_ptr.hpp>

#include <QtGui/QWidget>

#include <simscada/DbSimscadaDeclaration.h>

#include "ui_CtrlPointUi.h"

#include "SimDllDeclaration.h"

class CtrlPointUi : public QObject
{
  Q_OBJECT

  public:
    CtrlPointUi(QWidget* panel, db_simscada::sim::ObjectPtrT simObj);
    virtual
    ~CtrlPointUi();

  public:
    void
    setData(const sim_dll::Server::CtrlPointInfo& data);

  public slots:
    void
    resetLastRecieved();

  private:
    void initData();

    Ui::CtrlPointUiClass ui;

    db_simscada::sim::CtrlPtPtrT mCtrlPt;
};

#endif // CTRLPOINTUI_H
