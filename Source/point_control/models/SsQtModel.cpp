/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "SsQtModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimCtrlLink.h>
#include <simscada/sim/SimLinkedPt.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "ObjectTreeFilterSettings.h"

using namespace db_simscada;
using namespace db_simscada::sim;

//---------------------------------------------------------------------------
//                           SsQtModel private
//---------------------------------------------------------------------------

namespace SsQtModelPrivate
{
  struct Node
  {
      SsQtModelPrivate::Node* parent;
      int row;
      PlantModel::ObjectCptr object;
      typedef std::vector< SsQtModelPrivate::Node* > ChildrenT;
      ChildrenT children;

      Node() :
          parent(NULL), row(0)
      {
      }
  };

  bool
  SortByAlias(Node* o1, Node* o2);

  bool
  SortByName(Node* o1, Node* o2);

  bool
  SortByAlias(Node* o1, Node* o2)
  {
    assert(o1);
    assert(o2);
    assert(o1->object.get());
    assert(o2->object.get());

    if (o1->object->groupOrder != o2->object->groupOrder)
      return o1->object->groupOrder < o2->object->groupOrder;

    boost::shared_ptr< db_simscada::sim::Object > simO1 = o1->object->simObject;
    boost::shared_ptr< db_simscada::sim::Object > simO2 = o2->object->simObject;

    // Not all objects have simObjects
    // FIXME assert(simO1.get() == simO2.get());
    if (simO1.get() == NULL || simO2.get() == NULL)
      return SortByName(o1, o2);

    return (simO1->alias() < simO2->alias());
  }

  bool
  SortByName(Node* o1, Node* o2)
  {
    assert(o1);
    assert(o2);
    assert(o1->object.get());
    assert(o2->object.get());

    if (o1->object->groupOrder != o2->object->groupOrder)
      return o1->object->groupOrder < o2->object->groupOrder;

    std::string name1 =
        o1->object->simObject.get() == NULL ? o1->object->name :
            o1->object->simObject->name();

    std::string name2 =
        o2->object->simObject.get() == NULL ? o2->object->name :
            o2->object->simObject->name();

    return (name1 < name2);
  }

  void
  testModel(QAbstractItemModel* model, QModelIndex parent = QModelIndex())
  {
    //    qDebug("Testing parent row=%d, col=%d, alias=%s",
    //        parent.row(),
    //        parent.column(),
    //        parent.isValid() ? model->data(parent, Qt::DisplayRole).toString().toStdString().c_str()
    //            : "");

    int rowCount = model->rowCount(parent);
    int colCount = model->columnCount(parent);

    for (int row = 0; row < rowCount; row++)
    {
      // Test the data
      for (int col = 0; col < colCount; col++)
      {

        QModelIndex index = model->index(row, col, parent);

        // Test the parent
        QModelIndex testParent = model->parent(index);
        if (testParent != parent)
          assert(testParent == parent);

        QVariant disp = model->data(index, Qt::DisplayRole);
        QVariant forCol = model->data(index, Qt::ForegroundRole);
        QVariant toolTip = model->data(index, Qt::ToolTipRole);
      }

      // Test the subtree
      QModelIndex index = model->index(row, 0, parent);
      testModel(model, index);

    }
  }
}

//---------------------------------------------------------------------------
//                       SsQtModel Implementation
//---------------------------------------------------------------------------

SsQtModel::SsQtModel(boost::shared_ptr< const PlantModel > plantModel,
    const ObjectTreeFilterSettings& filter) :
    mPlantModel(plantModel), //
    mFilter(filter), //
    mRootNode(NULL)
{
  setupModel();

  Sort();

#ifdef DEBUG
  SsQtModelPrivate::testModel(this);
  qDebug("Model tested, no errors found");
#endif

} //---------------------------------------------------------------------------

SsQtModel::~SsQtModel()
{
}
//---------------------------------------------------------------------------

PlantModel::ObjectCptr
SsQtModel::plantModelObject(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  return Node(index).object;
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
SsQtModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColAlias:
      return "Alias";
    case ColName:
      return "Name";
    default:
      assert(false);
      break;
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
SsQtModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast<uintptr_t> (parent.internalPointer()));

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  SsQtModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
SsQtModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast<uintptr_t> (index.internalPointer()));

  assert(index.isValid());

  SsQtModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->object == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
SsQtModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast<uintptr_t> (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
SsQtModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

QVariant
SsQtModel::data(const QModelIndex& index, int role) const
{
  // We only provide certain data.
  if (!index.isValid() //
      || (role != Qt::DisplayRole && role != Qt::ToolTipRole
          && role != Qt::ForegroundRole) //
  || !(0 <= index.column() && index.column() < ColCount))
  {
    return QVariant();
  }

  PlantModel::ObjectCptr obj = plantModelObject(index);
  assert(obj.get() != NULL);

  if (role == Qt::ForegroundRole)
  {
    if (obj->simObject.get() == NULL
    || (index.column() == ColName //
    && (toObjectType(obj->simObject) == db_simscada::ControlObject //
    || toObjectType(obj->simObject) == db_simscada::DigitalObject //
    || toObjectType(obj->simObject) == db_simscada::AnalogueObject//
    || toObjectType(obj->simObject) == db_simscada::AccumulatorObject)))
    {
      return QVariant();
    }

    // Make the rest of the labels ligher coloured
    return QBrush(QColor("DarkBlue"));
  }

  switch (index.column())
  {
    case ColAlias:
      if (obj->simObject.get() == NULL)
        return QVariant();

      return QString(std::string(obj->simObject->alias()).c_str());

    case ColName:
      if (!obj->name.empty())
        return QString(obj->name.c_str());

      // You can actually have objects with no names
      assert(obj->simObject.get() != NULL);
      return QString(std::string(obj->simObject->name()).c_str());

    case ColCount:
      assert(false);
      break;
  }

  assert(false);
  return QVariant();
}
//---------------------------------------------------------------------------

SsQtModelPrivate::Node&
SsQtModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  SsQtModelPrivate::Node * obj = static_cast< SsQtModelPrivate::Node* >(index.internalPointer());
  obj = dynamic_cast< SsQtModelPrivate::Node* >(obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

void
SsQtModel::setupModel()
{
  // Setup root node
  mRootNode = &mNodes[mPlantModel->root().get()];

  for (PlantModel::ObjectsT::const_iterator itr = mPlantModel->root()->children.begin(); //
  itr != mPlantModel->root()->children.end(); //
      ++itr)
    setupModel(*itr);
}
//---------------------------------------------------------------------------

void
SsQtModel::setupModel(const PlantModel::ObjectCptr& modelNode)
{
  // NOTE, We use the pointers to the PlantModel::Object as a unique ID.
  PlantModel::ObjectCptr objParent = modelNode->parent.lock();
  PlantModel::ObjectCptr obj = modelNode;

  SsQtModelPrivate::Node* parentNode = &mNodes[objParent.get()];
  SsQtModelPrivate::Node* node = &mNodes[obj.get()];

  // Set the parent
  parentNode->children.push_back(node);
  node->parent = parentNode;
  // node->row = row;// Left for sort
  node->object = obj;

  // Iterate though the map and store the child indexes
  for (PlantModel::ObjectsT::const_iterator itr = modelNode->children.begin(); //
  itr != modelNode->children.end(); //
      ++itr)
    setupModel(*itr);
}
//---------------------------------------------------------------------------

void
SsQtModel::Sort()
{
  SsQtModelPrivate::SortFunctionT sortFunc = NULL;
  switch (mFilter.Sort)
  {
    case ObjectTreeFilterSettings::SortByAlias:
      sortFunc = &SsQtModelPrivate::SortByAlias;
      break;

    case ObjectTreeFilterSettings::SortByName:
      sortFunc = &SsQtModelPrivate::SortByName;
      break;

    case ObjectTreeFilterSettings::SortEnumCount:
      break;
  }
  assert(sortFunc);

  Sort(mRootNode, sortFunc);
}
//---------------------------------------------------------------------------

void
SsQtModel::Sort(SsQtModelPrivate::Node* node,
    SsQtModelPrivate::SortFunctionT sortFunc)
{
  std::sort(node->children.begin(), node->children.end(), sortFunc);

  int row = 0; // we need to update the row since we've changed the order
  for (SsQtModelPrivate::Node::ChildrenT::iterator itr = node->children.begin(); //
  itr != node->children.end(); //
      ++itr)
  {
    (*itr)->row = row++;
    Sort(*itr, sortFunc);
  }

}
//---------------------------------------------------------------------------
