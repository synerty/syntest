/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _U_OBJECT_MODEL_H_
#define _U_OBJECT_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <vector>
#include <set>

#include <boost/shared_ptr.hpp>

// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

#include <simscada/DbSimscadaDeclaration.h>

#include "PlantModel.h"
#include "ObjectTreeFilterSettings.h"

class PlantModel;
class ObjectTreeFilterSettings;

namespace SsQtModelPrivate
{
  class Node;

  typedef bool
  (*SortFunctionT)(Node*, Node*);

}

class SsQtModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColName,
      ColAlias,
      ColCount
    };

  public:
    SsQtModel(boost::shared_ptr<const PlantModel> plantModel,
        const ObjectTreeFilterSettings& filter);
    virtual
    ~SsQtModel();

  public:

    PlantModel::ObjectCptr
    plantModelObject(const QModelIndex& index) const;

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

  private:
    // ---------------
    // ObjectModel methods

    void
    setupModel();

    void
    setupModel(const PlantModel::ObjectCptr& modelNode);

    void
    Sort();

    void
        Sort(SsQtModelPrivate::Node* node,
            SsQtModelPrivate::SortFunctionT sortFunc);

    SsQtModelPrivate::Node&
    Node(const QModelIndex& index) const;

    boost::shared_ptr<const PlantModel> mPlantModel;
    const ObjectTreeFilterSettings& mFilter;

    typedef std::map<const PlantModel::Object*, SsQtModelPrivate::Node>
        NodeMapT;
    NodeMapT mNodes;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    SsQtModelPrivate::Node* mRootNode;

};
// ----------------------------------------------------------------------------


#endif /* _U_OBJECT_MODEL_H_ */
