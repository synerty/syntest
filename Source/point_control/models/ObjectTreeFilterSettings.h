/*
 * ObjectTreeFilterSettings.h
 *
 *  Created on: Dec 20, 2010
 *      Author: Administrator
 */

#ifndef _OBJECT_TREE_FILTER_H_
#define _OBJECT_TREE_FILTER_H_

#include <string>

/*! ObjectTreeFilter Brief Description
 * Long comment about the class
 *
 */
struct ObjectTreeFilterSettings
{
    enum FilterE
    {
      Standard,
      ManualTesting
    };

    enum SortE
    {
      SortByName,
      SortByAlias,
      SortEnumCount
    };

    FilterE Filter;
    SortE Sort;

    ObjectTreeFilterSettings();

    std::string
    FilterStr() const;

    static std::string
    EnumToString(SortE sort);
};

#endif /* _OBJECT_TREE_FILTER_H_ */
