/*
 * ObjectTreeFilter.cpp
 *
 *  Created on: Dec 20, 2010
 *      Author: Administrator
 */

#include "ObjectTreeFilterSettings.h"
#include <assert.h>

ObjectTreeFilterSettings::ObjectTreeFilterSettings() :
  Filter(Standard), //
      Sort(SortByName)
{
}

std::string
ObjectTreeFilterSettings::FilterStr() const
{
  switch (Filter)
  {
    case Standard:
      return "Standard";

    case ManualTesting:
      return "Manual Testing";
  }

  assert(false);
  return "ERROR";
}

std::string
ObjectTreeFilterSettings::EnumToString(SortE sort)
{
  switch (sort)
  {
    case SortByName:
      return "By Name";
    case SortByAlias:
      return "By Alias";
    case SortEnumCount:
      break;
  }

  assert(false);
  return "ERROR";
}
