/*
 * PlantModel.cpp
 *
 *  Created on: Dec 28, 2010
 *      Author: Administrator
 */

#include "PlantModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <boost/tokenizer.hpp>
#include <boost/format.hpp>

// TODO, remove this infavour of non Qt progress code
// Something will have to listen to this and report the progress via Qt
// Qt code should not be in here.
#include <QtGui/qprogressdialog.h>

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimCtrlLink.h>
#include <simscada/sim/SimCtrlPt.h>
#include <simscada/sim/SimTelePt.h>
#include <simscada/sim/SimRtu.h>
#include <simscada/sim/SimDataGroup.h>
#include <simscada/sim/SimLinkedPt.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "ObjectTreeFilterSettings.h"
#include "Macros.h"
#include "Globals.h"

// Disable debug code here
#undef FUNCTION_START
#define FUNCTION_START(DESC)
#undef FUNCTION_END
#define FUNCTION_END(DESC)


using namespace db_simscada;
using namespace db_simscada::sim;

PlantModel::PlantModel(db_simscada::SimScadaModelPtrT ormModel,
    const ObjectTreeFilterSettings& filter) :
    mOrmModel(ormModel), //
    mFilter(filter), //
    mRoot(new Object())
{
  FUNCTION_START(__PRETTY_FUNCTION__)
    assert(mOrmModel.get() != NULL);

    switch (mFilter.Filter)
    {
      case ObjectTreeFilterSettings::Standard:
        setupStandardObjectModel();
        break;

      case ObjectTreeFilterSettings::ManualTesting:
        setupManualTestingObjectModel();
        break;

      default:
        assert(false);
        break;
    }

    // We don't need this anymore
    mOrmModel.reset();
  FUNCTION_END(__PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------

PlantModel::~PlantModel()
{
  FUNCTION_START(__PRETTY_FUNCTION__)
  // Smart pointers ;-)
  FUNCTION_END(__PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------

PlantModel::ObjectPtr
PlantModel::root()
{
  return mRoot;
}
// ----------------------------------------------------------------------------

const PlantModel::ObjectPtr
PlantModel::root() const
{
  return mRoot;
}
// ----------------------------------------------------------------------------

void
PlantModel::setupStandardObjectModel()
{
  FUNCTION_START(__PRETTY_FUNCTION__)

    QProgressDialog progressDialog(MsgBoxParent());
    progressDialog.setMinimumDuration(0);
    progressDialog.setLabel(new QLabel("Generating simulator browser tree"));
    progressDialog.setAutoClose(false);
    progressDialog.setCancelButton(NULL);
    progressDialog.setModal(true);
    progressDialog.setRange(0, mOrmModel->SimObjectsMap().size());
    progressDialog.show();

    // Used to store the point groups while the model is being constructed
    typedef std::map< db_simscada::sim::Object::PtrT, ObjectPtr > NodeMapT;
    NodeMapT objMap;

    // Setup root node
    objMap[db_simscada::sim::Object::PtrT()] = mRoot;

    // ---------------
    // Init the child no map from simObjects

    // Iterate though the map and store the child indexes
    for (db_simscada::SimScadaModel::SimObjectMapT::const_iterator itr = mOrmModel->SimObjectsMap().begin(); //
    itr != mOrmModel->SimObjectsMap().end(); //
        ++itr)
    {
      progressDialog.setValue(progressDialog.value() + 1);

      db_simscada::sim::Object::PtrT obj = itr->second;
      db_simscada::sim::Object::PtrT parentObj;
      if (!obj->isParentNull())
        parentObj = obj->parent();

      // A protocol with no children, don't load
      if (db_simscada::toObjectType(obj) == db_simscada::ProtocolObject
          && obj->isObjectParentsEmpty())
        continue;

      ObjectPtr& parentNode = objMap[parentObj];
      ObjectPtr& node = objMap[obj];

      if (parentNode.get() == NULL)
        parentNode.reset(new Object());

      if (node.get() == NULL)
        node.reset(new Object());

      // Set the parent
      parentNode->children.push_back(node);
      node->parent = parentNode;
      node->simObject = obj;

      switch (db_simscada::toObjectType(obj))
      {
        case db_simscada::ProtocolObject:
          node->type = Object::Protocol;
          break;

        case db_simscada::DataPortObject:
          node->type = Object::Port;
          break;

        case db_simscada::ChannelObject:
          node->type = Object::Channel;
          break;

        case db_simscada::RtuObject:
          node->type = Object::Rtu;
          break;

        case db_simscada::DataGroupObject:
          node->type = Object::DataGroup;
          break;

        case db_simscada::ControlObject:
        case db_simscada::AnalogueObject:
        case db_simscada::AccumulatorObject:
        case db_simscada::DigitalObject:
          node->type = Object::Point;
          break;

        case db_simscada::ObjectTypeCount:
        case db_simscada::ObjectTypeUnset:
          assert(false);
          break;
      }
    }
    progressDialog.setValue(progressDialog.maximum());

    progressDialog.close();

  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

/*! Setup Manual Testing Object Model
 * We will setup a tree that looks like this :
 * RTU ->
 *      Point Group (plant) ->
 *              Points ->
 *
 */
void
PlantModel::setupManualTestingObjectModel()
{
  FUNCTION_START(__PRETTY_FUNCTION__)

    const size_t telePntCount = mOrmModel->SimTelePts().size();
    size_t pntGroupCount = mOrmModel->SimDigPts().size()
        + mOrmModel->SimAnaPts().size(); // An estimate

    QProgressDialog progressDialog(MsgBoxParent());
    progressDialog.setMinimumDuration(0);
    progressDialog.setLabel(new QLabel("Generating simulator browser tree"));
    progressDialog.setAutoClose(false);
    progressDialog.setCancelButton(NULL);
    progressDialog.setModal(true);
    progressDialog.setRange(0, telePntCount + pntGroupCount * 2);
    progressDialog.show();

// object : pointGroupId
    std::map< db_simscada::sim::Object::PtrT, int > pointGroupsBySsObj;

    int nextPointGroupId = 0;

// Group the points into plants
    const TelePtListT& telePts = mOrmModel->SimTelePts();
    for (TelePtListT::const_iterator itr = telePts.begin();
        itr != telePts.end(); ++itr)
    {
      progressDialog.setValue(progressDialog.value() + 1);

      db_simscada::sim::Object::PtrT obj = *itr;
      assert(obj.get() != NULL);

      int pointGroupId = nextPointGroupId++;

      if (pointGroupsBySsObj.find(obj) != pointGroupsBySsObj.end())
        continue;

      switch (toObjectType(obj))
      {
        case db_simscada::AccumulatorObject:
        case db_simscada::AnalogueObject:
        case db_simscada::DigitalObject:
        {
          bool foundPointGroupId = false;
          std::set< TelePtPtrT > visited;
          followLinkedPts(TelePt::downcast(obj),
              pointGroupsBySsObj,
              pointGroupId,
              foundPointGroupId,
              visited);
        }
          break;

        case db_simscada::ControlObject:
          followCtrlLinks(CtrlPt::downcast(obj),
              pointGroupsBySsObj,
              pointGroupId);
          break;

        default:
          break;
      }
    }

    // Update the progress range
    pntGroupCount = pointGroupsBySsObj.size();
    progressDialog.setRange(0, telePntCount + pntGroupCount * 2);

// Next get the point we want to use as the main point, It will be :
    typedef std::multimap< int, db_simscada::sim::Object::PtrT > IntObjMmT;
    IntObjMmT pointGroupsById;

    for (std::map< db_simscada::sim::Object::PtrT, int >::iterator itr = pointGroupsBySsObj.begin(); //
    itr != pointGroupsBySsObj.end(); //
        ++itr)
    {
      progressDialog.setValue(progressDialog.value() + 1);
      pointGroupsById.insert(std::make_pair(itr->second, itr->first));
    }

// Scope our variables

    {
      // To know when we move onto the next node
      int workingWithPlant = -1;
      // To know what we have picked already
      db_simscada::sim::Object::PtrT pickedObj;
      // The other objs in the plant
      std::vector< db_simscada::sim::Object::PtrT > otherObjs;
      // So we know when we're one before the end
      IntObjMmT::size_type index = 0;
      // Already added RTUs
      std::map< db_simscada::sim::Object::PtrT, ObjectPtr > rtusMap;

      // Initialise the RTUs structure
      setupRtus(rtusMap);

      for (IntObjMmT::iterator itr = pointGroupsById.begin(); //
      itr != pointGroupsById.end(); //
          ++itr)
      {
        progressDialog.setValue(progressDialog.value() + 1);

        index++;
        // If we're onto a new plant Id, or onto the last iteration
        if (workingWithPlant != itr->first || index == pointGroupsById.size())
        {
          // If we need to assign the picked obj for the last plant
          if (pickedObj.get() != NULL)
            setupPointGroup(pickedObj, otherObjs, rtusMap);

          workingWithPlant = itr->first;
          pickedObj.reset();
          otherObjs.clear();
        }

        if (pickedObj == NULL)
        {
          pickedObj = itr->second;
        }

        // Prefer scan points
        else if (toObjectType(pickedObj) == db_simscada::ControlObject // Ctrl Pt
        && toObjectType(itr->second) != db_simscada::ControlObject) // Scan Pt
        {
          otherObjs.push_back(pickedObj);
          pickedObj = itr->second;
        }

        // If we already have a scan, don't evanulate more steps for ctrl points
        else if (toObjectType(pickedObj) != db_simscada::ControlObject // Scan pt
        && toObjectType(itr->second) == db_simscada::ControlObject) // Ctrl Pt
        {
          otherObjs.push_back(itr->second);
        }

        // Prefer shorter aliases or sort alphabetically
        else if (itr->second->alias().size() < pickedObj->alias().size() //
            || (itr->second->alias().size() == pickedObj->alias().size()
                && itr->second->alias() < pickedObj->alias()))
        {
          otherObjs.push_back(pickedObj);
          pickedObj = itr->second;
        }
        else
        {
          otherObjs.push_back(itr->second);
        }
      }

      progressDialog.setValue(progressDialog.maximum());

      if (pickedObj != NULL)
        setupPointGroup(pickedObj, otherObjs, rtusMap);
    }
// DONE

    progressDialog.close();

  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

void
PlantModel::followCtrlLinks(CtrlPtPtrT obj,
    std::map< db_simscada::sim::Object::PtrT, int >& groupMap
    ,
    int& pointGroupId)
{
  FUNCTION_START(__PRETTY_FUNCTION__)
    CtrlLinkPtrT ctrlLink = mOrmModel->SimCtrlLink(obj->alias());

    if (ctrlLink.get())
    {
// First get the parents
      db_simscada::sim::TelePtPtrT linkedObj = ctrlLink->scanPoint();
      assert(linkedObj != NULL);
      bool foundPointGroupId = false;
      std::set< TelePtPtrT > visited;
      followLinkedPt(linkedObj,
          groupMap,
          pointGroupId,
          foundPointGroupId,
          visited);
    }
    groupMap.insert(std::make_pair(obj, pointGroupId));
  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

void
PlantModel::followLinkedPts(TelePtPtrT obj,
    std::map< sim::ObjectPtrT, int >& groupMap
    ,
    int& pointGroupId
    ,
    bool& foundPointGroupId
    ,
    std::set< TelePtPtrT >& visited)
{
  FUNCTION_START(__PRETTY_FUNCTION__)
    assert(obj.get());

// Check if we've visited this point before
    if (visited.find(obj) != visited.end())
      return;

    visited.insert(obj);

    LinkedPtPtrT linkedPt = mOrmModel->SimLinkedPt(obj->alias());
    if (linkedPt.get())
    {
      TelePtPtrT linkedObj = linkedPt->linkedAlias();
      assert(linkedObj.get() != NULL);

      followLinkedPt(linkedObj,
          groupMap,
          pointGroupId,
          foundPointGroupId,
          visited);
    }

    groupMap.insert(std::make_pair(obj, pointGroupId));
  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

void
PlantModel::followLinkedPt(TelePtPtrT obj,
    std::map< db_simscada::sim::Object::PtrT, int >& groupMap
    ,
    int& pointGroupId
    ,
    bool& foundPointGroupId
    ,
    std::set< TelePtPtrT >& visited)
{
  FUNCTION_START(__PRETTY_FUNCTION__)
    assert(obj.get());

// Check if we've already got a plant ID and if its the same
    std::map< db_simscada::sim::Object::PtrT, int >::iterator gmItr = groupMap.find(obj);
    if (gmItr != groupMap.end())
    {
      // Check if we've already
      if (foundPointGroupId && gmItr->second != pointGroupId)
      {
        qWarning("Found plant already for object %s, old plant %d, new plant %d",
            obj->alias().c_str(),
            gmItr->second,
            pointGroupId);
        groupMap[obj] = pointGroupId;
      }
      else if (!foundPointGroupId)
      {
        pointGroupId = gmItr->second;
        foundPointGroupId = true;
      }
    }

    followLinkedPts(obj, groupMap, pointGroupId, foundPointGroupId, visited);
  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

std::string
PlantModel::stripDualAlias(std::string alias)
{
  FUNCTION_START(__PRETTY_FUNCTION__)
// Strip off the dual tag so we can group the duplucated RTUs
    if (alias.size() > DUAL_ALIAS_TAG.size())
    {
      if (alias.substr(alias.size() - DUAL_ALIAS_TAG.size()) == DUAL_ALIAS_TAG)
      {
        alias = alias.substr(0, alias.size() - DUAL_ALIAS_TAG.size());
      }
    }

  FUNCTION_END(__PRETTY_FUNCTION__)
  return alias;
}
//---------------------------------------------------------------------------

std::string
PlantModel::stripDualName(std::string name)
{
  FUNCTION_START(__PRETTY_FUNCTION__)
// Strip off the dual tag so we can group the duplucated RTUs
    if (name.size() > DUAL_NAME_TAG.size())
    {
      if (name.substr(name.size() - DUAL_NAME_TAG.size()) == DUAL_NAME_TAG)
      {
        name = name.substr(0, name.size() - DUAL_NAME_TAG.size());
      }
    }

    return name;

  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

void
PlantModel::setupRtus(std::map< db_simscada::sim::Object::PtrT, ObjectPtr >& rtusMap)
{
  FUNCTION_START(__PRETTY_FUNCTION__)
    typedef std::map< std::string, ObjectPtr > RtuObjAliasMapT;
    RtuObjAliasMapT psuedoRtuObjByAlias;

    RtuObjAliasMapT rtusGroupObjByAlias;

    std::map< db_simscada::sim::Object::PtrT, ObjectPtr > rtuObjBySimObj;

// Create the RTU objects if required
// And add the real rtus under the object.
    const RtuListT& simRtus = mOrmModel->SimRtus();
    for (RtuListT::const_iterator rtuItr = simRtus.begin(); //
    rtuItr != simRtus.end(); //
        ++rtuItr)
    {
      const RtuPtrT& rtuSimObj = (*rtuItr);

      std::string rtuAlias = stripDualAlias(rtuSimObj->alias());

      ObjectPtr& psuedoRtuObj = psuedoRtuObjByAlias[rtuAlias];
      ObjectPtr& rtuGroupObj = rtusGroupObjByAlias[rtuAlias];

      if (psuedoRtuObj.get() == NULL)
      {
        // Add our Psudo RTU
        psuedoRtuObj.reset(new Object());
        psuedoRtuObj->name = stripDualName(rtuSimObj->name());
        psuedoRtuObj->groupOrder = MtgoRtu;
        psuedoRtuObj->type = Object::UnknownType;
        psuedoRtuObj->parent = mRoot;
        mRoot->children.push_back(psuedoRtuObj);

        // Add the RTUs group, This holds the real RTUs
        assert(rtuGroupObj.get() == NULL);
        rtuGroupObj.reset(new Object());
        rtuGroupObj->name = "Rtus";
        rtuGroupObj->groupOrder = MtgoRtu;
        rtuGroupObj->type = Object::UnknownType;
        rtuGroupObj->parent = psuedoRtuObj;
        psuedoRtuObj->children.push_back(rtuGroupObj);

      }

      // Add the actual RTU
      ObjectPtr rtuObj(new Object());
      rtuObj->simObject = rtuSimObj;
      rtuObj->groupOrder = MtgoRtu;
      rtuObj->type = Object::Rtu;
      rtuObj->parent = rtuGroupObj;
      rtuGroupObj->children.push_back(rtuObj);
      rtusMap.insert(std::make_pair(rtuSimObj, psuedoRtuObj));
      rtuObjBySimObj.insert(std::make_pair(rtuSimObj, rtuObj));

      // Add the channel under the RTU
      db_simscada::sim::Object::PtrT chanSimObj = (*rtuItr)->parent();
      assert(chanSimObj.get() != NULL);
      ObjectPtr channelObj(new Object());
      channelObj->simObject = chanSimObj;
      channelObj->groupOrder = MtgoChannel;
      channelObj->type = Object::Channel;
      channelObj->parent = rtuObj;
      rtuObj->children.push_back(channelObj);

      // Add the dataport under the RTU
      db_simscada::sim::Object::PtrT portSimObj = chanSimObj->parent();
      assert(portSimObj.get() != NULL);
      ObjectPtr portObj(new Object());
      portObj->simObject = portSimObj;
      portObj->groupOrder = MtgoPort;
      portObj->type = Object::Port;
      portObj->parent = rtuObj;
      rtuObj->children.push_back(portObj);
    }

// Add the data groups under the real rtu object
    const DataGroupListT& dataGroups = mOrmModel->SimDataGroups();
    for (DataGroupListT::const_iterator simOjbItr = dataGroups.begin(); //
    simOjbItr != dataGroups.end(); //
        ++simOjbItr)
    {
      const DataGroupPtrT& dataGroupSimObj = *simOjbItr;

      assert(dataGroupSimObj->parent().get() != NULL);
      ObjectPtr& rtuObj = rtuObjBySimObj[dataGroupSimObj->parent()];

      // Add the dataport under the RTU
      ObjectPtr dataGroupObj(new Object());
      dataGroupObj->simObject = dataGroupSimObj;
      dataGroupObj->groupOrder = MtgoDataGroup;
      dataGroupObj->parent = rtuObj;
      rtuObj->children.push_back(dataGroupObj);
    }
    FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

void
PlantModel::setupPointGroup(db_simscada::sim::Object::PtrT pickedObj,
    std::vector< db_simscada::sim::Object::PtrT >& otherObjs,
    const std::map< db_simscada::sim::Object::PtrT, ObjectPtr >& rtusMap)
{
  FUNCTION_START(__PRETTY_FUNCTION__)

    typedef std::set< ObjectPtr > RtuObjsSetT;
    RtuObjsSetT rtuObjs;

// Create/insert the point group.
    std::string pointGroupName;
    db_simscada::sim::Object::ListT pointGroupPoints;

// Add the picked object to otherObjs
    // DEFINE THE NAME OF THE POINT GROUP
    if (pickedObj->name().empty())
      pointGroupName = pickedObj->alias();
    else
      pointGroupName = pickedObj->name();

    otherObjs.push_back(pickedObj);

// Insert the other objects in the plant
    for (std::vector< db_simscada::sim::Object::PtrT >::iterator itr = otherObjs.begin(); //
    itr != otherObjs.end(); //
        ++itr)
    {
      db_simscada::sim::Object::PtrT simObj = *itr;
      pointGroupPoints.push_back(simObj);

      // Set the RTU.
      assert(simObj->parent().get() != NULL); // DATA GROUP
      assert(simObj->parent()->parent().get() != NULL); // RTU
      const db_simscada::sim::Object::PtrT& rtuSimObj = simObj->parent()->parent();

      std::map< db_simscada::sim::Object::PtrT, ObjectPtr >::const_iterator rtuObjItr = rtusMap.find(rtuSimObj);
      assert(rtuObjItr != rtusMap.end());
      rtuObjs.insert(rtuObjItr->second);
    }

    if (rtuObjs.size() != 1)
    {
      boost::format fmt(" (In %d RTUs)");
      fmt % rtuObjs.size();
      pointGroupName += fmt.str();
    }

// Duplicate the plant for each RTU
    for (RtuObjsSetT::iterator itr = rtuObjs.begin(); //
    itr != rtuObjs.end(); //
        ++itr)
    {
      const ObjectPtr& rtuObj = *itr;

      // Create the point group
      ObjectPtr pg(new Object());
      pg->name = pointGroupName;
      pg->groupOrder = MtgoPointGroup;
      pg->type = Object::PointGroup;
      pg->parent = rtuObj;
      rtuObj->children.push_back(pg);

      // Create the objects for the point group
      for (db_simscada::sim::Object::ListT::iterator pointItr = pointGroupPoints.begin(); //
      pointItr != pointGroupPoints.end(); //
          ++pointItr)
      {
        ObjectPtr point(new Object());
        point->simObject = *pointItr;
        point->groupOrder = MtgoPoint;
        point->type = Object::Point;
        point->parent = pg;
        pg->children.push_back(point);
      }

    }

  FUNCTION_END(__PRETTY_FUNCTION__)
}
//---------------------------------------------------------------------------

//void
//PlantModel::SetupPlants()
//{

/* Abandoned attempt to derrive plant name and site from grouping names.
 *
 // Create a tree structure of the point
 // top = = PlantNameMap [token :
 class PlantNameMapT;
 typedef boost::shared_ptr<PlantNameMapT> PlantNameMapPtr;
 typedef std::pair<PointGroupPtr, PlantNameMapPtr> PlantNameMapValT;

 class PlantNameMapT : public std::map<std::string, PlantNameMapValT>
 {
 };

 void
 AddToPlant(PlantNameMapValT& val, Plant& plant);

 void
 PlantModel::SetupPlants()
 {

 // Construct a tree of the tokenised point names

 PlantNameMapValT rootVal;
 rootVal.second.reset(new PlantNameMapT());

 for (TmpPointGroupMapT::iterator itr = mTmpPointGroup.begin(); //
 itr != mTmpPointGroup.end(); //
 ++itr)
 {
 assert(!itr->first.empty());
 PlantNameMapValT& lastVal = rootVal;

 typedef boost::char_separator<char> SepT;
 typedef boost::tokenizer<SepT> TokT;
 SepT sep(" ");
 TokT tokens(itr->first, sep);

 for (TokT::iterator tokItr = tokens.begin(); //
 tokItr != tokens.end(); //
 ++tokItr)
 {
 // This will create a new node if required
 lastVal = (*lastVal.second)[*tokItr];

 // Create this nodes map if required
 if (lastVal.second.get() == NULL)
 lastVal.second.reset(new PlantNameMapT());
 }
 // At this point we should be at a unique node
 assert(lastVal.first.get() == NULL);

 // Assign the value to this node
 lastVal.first = itr->second;
 }

 PlantNameMapValT& lastVal = rootVal;
 std::string plantName;
 while (!rootVal.second->empty())
 {
 if (lastVal.first.get() != NULL)
 {
 PlantPtr plant(new Plant());
 plant->name = plantName;

 AddToPlant(lastVal, *plant);

 // TODO, Delete this node

 // Reset to root and continue
 lastVal = rootVal;
 plantName.clear();
 continue;
 }

 for (PlantNameMapT::iterator itr = lastVal.second->begin(); //
 itr != lastVal.second->end(); //
 ++itr)
 {
 // Itr->second PlantNameMapValT
 if (itr->second.first.get() != NULL // Point Group
 || itr->second.second->size() == 1 // The sub map
 )
 {
 PlantPtr plant(new Plant());
 plant->name = plantName;

 AddToPlant(lastVal, *plant);

 // TODO, Delete this node

 // Reset to root and continue
 lastVal = rootVal;
 plantName.clear();
 continue;
 }

 }
 plantName += lastVal.second->begin()->first;
 lastVal = *lastVal.second->begin()->second;
 }
 }
 //---------------------------------------------------------------------------


 void
 AddToPlant(PlantNameMapValT& val, Plant& plant)
 {
 // Need to recursivly add the point objects to the plant
 }
 //---------------------------------------------------------------------------
 */
