/*
 * PlantModel.h
 *
 *  Created on: Dec 28, 2010
 *      Author: Administrator
 */

#ifndef PLANTMODEL_H_
#define PLANTMODEL_H_

#include <string>
#include <vector>
#include <map>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include <simscada/DbSimscadaDeclaration.h>

#include "ObjectTreeFilterSettings.h"

class ObjectTreeFilterSettings;

/*! Plant Model
 * Contains the structures to describe the Site->Plant->Point Group model
 */
class PlantModel
{
  public:

    struct Object;

    typedef boost::shared_ptr<const Object> ObjectCptr;
    typedef boost::shared_ptr<Object> ObjectPtr;
    typedef boost::weak_ptr<Object> ObjectWptr;
    typedef std::vector<ObjectPtr> ObjectsT;

    struct Object
    {
        enum TypeE
        {
          UnknownType,
          Protocol,
          Port,
          Channel,
          Rtu,
          DataGroup,
          PointGroup,
          Point
        };

        std::string name;
        ObjectWptr parent;
        ObjectsT children;
        db_simscada::sim::ObjectPtrT simObject;

        int groupOrder;
        TypeE type;

        Object() :
          groupOrder(0), type(UnknownType)
        {
        }
    };

  public:
    PlantModel(db_simscada::SimScadaModelPtrT ormModel,
        const ObjectTreeFilterSettings& filter);
    virtual
    ~PlantModel();

  public:
    ObjectPtr
    root();

    const ObjectPtr
    root() const;

  private:
    // ---------------
    // Standard model

    void
    setupStandardObjectModel();

  private:
    // ---------------
    // ""Other""  model

    void
    setupManualTestingObjectModel();

    void
    followCtrlLinks(db_simscada::sim::CtrlPtPtrT obj,
        std::map<db_simscada::sim::ObjectPtrT, int>& groupMap,
        int& plantId);

    void
    followLinkedPts(db_simscada::sim::TelePtPtrT obj,
        std::map<db_simscada::sim::ObjectPtrT, int>& groupMap,
        int& plantId,
        bool& foundPlantId,
        std::set<db_simscada::sim::TelePtPtrT >& visited);

    void
    followLinkedPt(db_simscada::sim::TelePtPtrT obj,
        std::map<db_simscada::sim::ObjectPtrT, int>& groupMap,
        int& plantId,
        bool& foundPlantId,
        std::set<db_simscada::sim::TelePtPtrT >& visited);

    void
    setupRtus(std::map<db_simscada::sim::ObjectPtrT, ObjectPtr>& rtusMap);

    void
    setupPointGroup(db_simscada::sim::ObjectPtrT pickedObj,
        std::vector<db_simscada::sim::ObjectPtrT >& otherObjs,
        const std::map<db_simscada::sim::ObjectPtrT, ObjectPtr>& rtusMap);

    std::string
    stripDualAlias(std::string alias);

    std::string
    stripDualName(std::string name);

  private:

    enum ManualTestingGroupingOrderE
    {
      MtgoPort,
      MtgoChannel,
      MtgoRtu,
      MtgoDataGroup,
      MtgoPointGroup,
      MtgoPoint
    };

    db_simscada::SimScadaModelPtrT mOrmModel;
    const ObjectTreeFilterSettings& mFilter;
    ObjectPtr mRoot;

};

#endif /* PLANTMODEL_H_ */
