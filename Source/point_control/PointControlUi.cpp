#include "PointControlUi.h"

#include <auto_ptr.h>
#include <algorithm>

#include <QtGui/qstandarditemmodel.h>
#include <QtGui/qmessagebox.h>

#include <simscada/sim/SimObject.h>

#include "Globals.h"
#include "MainSimControl.h"
#include "MainSimModel.h"
#include "ObjectTreeFilterSettings.h"
#include "SsQtModel.h"
#include "PointUi.h"
#include "PointControl.h"

bool
SortSimObjWptrByAlias(boost::weak_ptr< db_simscada::sim::Object > const& w1,
    boost::weak_ptr< db_simscada::sim::Object > const& w2)
{
  db_simscada::sim::Object::PtrT o1 = w1.lock();
  db_simscada::sim::Object::PtrT o2 = w2.lock();

  if (o1.get() == NULL && o2.get() != NULL)
    return true;

  if (o1.get() != NULL && o2.get() == NULL)
    return false;

  if (o1.get() == NULL && o2.get() == NULL)
    return false;

  return (o1->alias() < o2->alias());
}

PointControlUi::PointControlUi(PointControl& uc) :
    mUc(uc)
{
  ui.setupUi(this);

  connectSlots();

  // Init observer
  simConnectionStateChanged(Disconnected);
  objectTreeFilterChanged(mUc.objectTreeFilter());
}
// ----------------------------------------------------------------------------

PointControlUi::~PointControlUi()
{

}
// ----------------------------------------------------------------------------

void
PointControlUi::simObjectsSelected(MainObjectModelObserver::SimObjectsSelectedT objects)
{
  clearPointUis();

  // Sort by name
  std::sort(objects.begin(), objects.end(), &SortSimObjWptrByAlias);

  for (MainObjectModelObserver::SimObjectsSelectedT::iterator itr = objects.begin(); //
  itr != objects.end(); //
      ++itr)
  {
    db_simscada::sim::Object::PtrT simObj = itr->lock();
    assert(simObj.get() != NULL);

    boost::shared_ptr< PointUi > pointUi(new PointUi(ui.wgtPointControl,
        simObj));
    mPointUis.push_back(pointUi);
    ui.wgtPointControl->layout()->addWidget(pointUi.get());
    MainSim::Inst().addObserver(pointUi);
  }
}
// ----------------------------------------------------------------------------

void
PointControlUi::objectQtModelChanged(boost::weak_ptr< SsQtModel > qtModel)
{
  boost::shared_ptr< SsQtModel > qtModelPtr = qtModel.lock();
  if (qtModelPtr.get())
  {
    ui.treObjects->setModel(qtModelPtr.get());
    ui.treObjects->setColumnWidth(0, 350);
  }
  else
  {
    ui.treObjects->setModel(new QStandardItemModel(ui.treObjects));
  }
}
// ----------------------------------------------------------------------------

void
PointControlUi::objectTreeFilterChanged(const ObjectTreeFilterSettings& filter)
{
  ui.btnFilterObjectTree->setText(QString(("Filter [" + filter.FilterStr() + "]").c_str()));
}
// ----------------------------------------------------------------------------

// ---------------
// PointControlUi stuff

void
PointControlUi::clearPointUis()
{
  for (PointUisT::iterator itr = mPointUis.begin(); //
  itr != mPointUis.end(); //
      ++itr)
  {
    boost::shared_ptr< PointUi > pointUi = *itr;
    pointUi->setVisible(false);
    pointUi->deleteLater();
  }

  mPointUis.clear();

}
// ----------------------------------------------------------------------------

void
PointControlUi::objectActivated(const QModelIndex& index)
{
  if (!index.isValid())
    return;

  mUc.qtModelIndexSelected(index);
}
// ----------------------------------------------------------------------------

// ---------------
// MainSimObserver implentation

void
PointControlUi::simConnectionStateChanged(ConnectionStateE state)
{
  switch (state)
  {
    case Connecting:
      ui.txtSimAddress->setText("Connecting to simSCADA ...");
      break;

    case Connected:
      ui.txtSimAddress->setText("Connected to simSCADA");
      break;

    case Disconnected:
      ui.txtSimAddress->setText("Not Connected to simSCADA");
      break;

    case ConnectionAttemptFailed:
      ui.txtSimAddress->setText("Connection attempt to simSCADA failed");
      QMessageBox::critical(MsgBoxParent(),
          "Connection Attempt Fail",
          "The connection attempt to simSCADA has failed.\n"
              "Please check the log for details");
      break;

    case Broken:
      ui.txtSimAddress->setText("Connected to simSCADA Broken");
      QMessageBox::critical(MsgBoxParent(),
          "Connection Broken",
          "The connection to simSCADA has been broken.\n"
              "Please check the log for details");
      break;
  }

  ui.btnSimConnToggle->setEnabled(state != Connecting);
  ui.btnSimConnToggle->setChecked(state == Connected);
  std::string str = (
      state == Connecting ?
          "Connecting ..." :
          (state == Connected ? "Disconnet" : "Connect"));
  ui.btnSimConnToggle->setText(str.c_str());
}
// ----------------------------------------------------------------------------

void
PointControlUi::simDllConnectionToggleClicked(bool checked)
{
  if (checked)
    MainSim::Inst().connectToSim();
  else
    MainSim::Inst().disconnectFromSim();

  // Reject the checked change
  ui.btnSimConnToggle->setChecked(!checked);
}
/// ---------------------------------------------------------------------

void
PointControlUi::connectSlots()
{

  connect(ui.treObjects,
      SIGNAL(clicked(const QModelIndex&)),
      this,
      SLOT(objectActivated(const QModelIndex&)));

  connect(ui.btnFilterObjectTree,
      SIGNAL(clicked()),
      &mUc,
      SLOT(objectFilterChange()));

  connect(ui.btnSimConnToggle,
      SIGNAL(clicked(bool)),
      this,
      SLOT(simDllConnectionToggleClicked(bool)));
}
// ----------------------------------------------------------------------------

