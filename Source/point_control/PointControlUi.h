#ifndef POINTCONTROL_H
#define POINTCONTROL_H

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <QtGui/qwidget.h>
#include "ui_PointControlUi.h"

#include "MainSimModelObserver.h"
#include "MainSimControlObserver.h"

#include "ObjectTreeFilterSettings.h"

class PointUi;
class PointControl;

class PointControlUi : public QWidget, public boost::enable_shared_from_this<
    PointControlUi >, public MainSimObserver
{
  Q_OBJECT

  public:
    PointControlUi(PointControl& uc);
    virtual
    ~PointControlUi();

  public:
    void
    simObjectsSelected(MainObjectModelObserver::SimObjectsSelectedT objects);

    void
    objectQtModelChanged(boost::weak_ptr< SsQtModel > qtModel);

    void
    objectTreeFilterChanged(const ObjectTreeFilterSettings& filter);

  private:
    // ---------------
    // MainSimObserver implentation

    void
    simConnectionStateChanged(ConnectionStateE state);

  public slots:
    void
    objectActivated(const QModelIndex& index);

    void
    simDllConnectionToggleClicked(bool checked);

  private:
    void
    clearPointUis();

    void
    connectSlots();

    PointControl& mUc;

    Ui::PointControlUiClass ui;

    typedef std::vector< boost::shared_ptr< PointUi > > PointUisT;
    PointUisT mPointUis;
};

#endif // POINTCONTROL_H
