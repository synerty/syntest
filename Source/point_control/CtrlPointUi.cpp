#include "CtrlPointUi.h"

#include <assert.h>

#include <QtCore/qdatetime.h>

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimCtrlPt.h>
#include <simscada/sim/SimCtrlLink.h>
#include <simscada/sim/SimCtrlDeviceType.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "MainSimModel.h"

using namespace db_simscada;
using namespace db_simscada::sim;

CtrlPointUi::CtrlPointUi(QWidget *parent, db_simscada::sim::ObjectPtrT simObj) :
  mCtrlPt(CtrlPt::downcast(simObj))
{
  ui.setupUi(parent);

  initData();

  connect(ui.btnResetLastReceived,
      SIGNAL(clicked()),
      this,
      SLOT(resetLastRecieved()));
}

CtrlPointUi::~CtrlPointUi()
{

}

void
CtrlPointUi::setData(const sim_dll::Server::CtrlPointInfo& data)
{
  using namespace sim_dll;

  ui.txtLastReceived->setText(QDateTime().toString());
  ui.txtLastReceived->setStyleSheet("background-color: rgb(0, 150, 0);");
}

void
CtrlPointUi::initData()
{
  db_simscada::SimScadaModelPtrT ormModel = MainObjectModel::Inst().ormModel();
  assert(ormModel.get() != NULL);

  CtrlLinkPtrT ctrlLink = ormModel->SimCtrlLink(mCtrlPt->alias());
  assert(mCtrlPt.get());

  std::string backInd = toObjectTypeName(ctrlLink->scanPoint()) //
      + " - " + ctrlLink->scanPoint()->alias();

  std::string backIndValue = ctrlLink->stateTo();

  std::string deviceType;
  if (!mCtrlPt->isDeviceTypeNull())
    deviceType = mCtrlPt->deviceType()->id();

  ui .txtDeviceType->setText(deviceType.c_str());
  ui.txtLastReceived->setText("");
  ui.txtBackIndAlias->setText(backInd.c_str());
  ui.txtBackIndValue->setText(backIndValue.c_str());
}

void
CtrlPointUi::resetLastRecieved()
{
  ui.txtLastReceived->setText("");
  ui.txtLastReceived->setStyleSheet("");
}
