#ifndef ANAPOINTUI_H
#define ANAPOINTUI_H

#include <boost/shared_ptr.hpp>

#include <QtCore/qobject.h>

#include <simscada/DbSimscadaDeclaration.h>

#include "ui_AnaPointUi.h"

#include "SimDllDeclaration.h"

class AnaPointUi : public QObject
{
  Q_OBJECT

  public:
    AnaPointUi(QWidget* panel, db_simscada::sim::ObjectPtrT simObj);
    virtual
    ~AnaPointUi();

  public:
    void
    setData(const sim_dll::Server::AnaPointInfo& data);

  public slots:
    void
    SetRawValue();

    void
    SetEngValue();

    void
    SetNormalValue();

  private:
    Ui::AnaPointUiClass ui;

    db_simscada::sim::ObjectPtrT mSimObj;
};

#endif // ANAPOINTUI_H
