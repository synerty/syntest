#include "PointUi.h"

#include <simscada/sim/SimObject.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "MainSimControl.h"
#include "DigPointUi.h"
#include "AnaPointUi.h"
#include "CtrlPointUi.h"

#include "Macros.h"

using namespace db_simscada;
using namespace db_simscada::sim;

PointUi::PointUi(QWidget* parent, db_simscada::sim::ObjectPtrT simObj) :
    QWidget(parent), mSimObj(simObj)
{
  ui.setupUi(this);

  std::string title = toObjectTypeName(simObj) + " - " + simObj->alias();
  ui.grpPoint->setTitle(title.c_str());
  ui.txtAlias->setText(simObj->alias().c_str());
  ui.txtName->setText(simObj->name().c_str());
  ui.txtObjectType->setText(toObjectTypeName(simObj).c_str());

  // Disable displaying of object type and name.
  // Code was added here because I didn't want to remove the feature just yet
  ui.txtAlias->setVisible(false);
  ui.lblAliasLabel->setVisible(false);
  ui.txtObjectType->setVisible(false);
  ui.lblObjectTypeLabel->setVisible(false);

  // Make sure the details panel is enabled
  ui.wgtPointDetails->setVisible(true);
  ui.lblSimDisconnected->setVisible(false);

  switch (toObjectType(simObj))
  {
    case db_simscada::DigitalObject:
      mDigPointUi.reset(new DigPointUi(ui.wgtPointDetails, simObj));
      break;

    case db_simscada::AnalogueObject:
      mAnaPointUi.reset(new AnaPointUi(ui.wgtPointDetails, simObj));
      break;

    case db_simscada::AccumulatorObject:
      // TODO Accumulators
//      mAnaPointUi.reset(new AnaPointUi(ui.wgtPointDetails, simObj));
      break;

    case db_simscada::ControlObject:
      mCtrlPointUi.reset(new CtrlPointUi(ui.wgtPointDetails, simObj));
      break;

    case db_simscada::ProtocolObject:
    case db_simscada::DataPortObject:
    case db_simscada::ChannelObject:
    case db_simscada::RtuObject:
    case db_simscada::DataGroupObject:
      break;

    default:
      // Unknown point type
      assert(false);
      break;
  }
}
// ----------------------------------------------------------------------------

PointUi::~PointUi()
{

}
// ----------------------------------------------------------------------------

// ---------------
// MainSimObserver interface

void
PointUi::objectUpdateReceived(ObjectUpdate info)
{
  using namespace sim_dll::Server;

  if (info.alias != mSimObj->alias())
    return;

  switch (info.msgType)
  {
    case DigUpdate:
    case DigSetValAckMsg:
    {
      assert(mDigPointUi.get() != NULL);
      DigPointInfo* updateData = boost::get< DigPointInfo >(&info.pointInfo);
      assert(updateData != NULL);
      mDigPointUi->setData(*updateData);
      break;
    }

    case AnaUpdate:
    case AnaSetValAckMsg:
    {
      assert(mAnaPointUi.get() != NULL);
      AnaPointInfo* updateData = boost::get< AnaPointInfo >(&info.pointInfo);
      assert(updateData != NULL);
      mAnaPointUi->setData(*updateData);
      break;
    }

    case CtrlUpdate:
    {
      assert(mCtrlPointUi.get() != NULL);
      CtrlPointInfo* updateData = boost::get< CtrlPointInfo >(&info.pointInfo);
      assert(updateData != NULL);
      mCtrlPointUi->setData(*updateData);
      break;
    }

    THROW_UNHANDLED_CASE_EXCEPTION
  }

}
// ----------------------------------------------------------------------------

void
PointUi::simulatorDisconnected()
{
  ui.wgtPointDetails->setVisible(false);
  ui.lblSimDisconnected->setVisible(true);
}
// ----------------------------------------------------------------------------
