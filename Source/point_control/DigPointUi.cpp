#include "DigPointUi.h"

#include <boost/algorithm/string.hpp>

#include "MainSimControl.h"

DigPointUi::DigPointUi(QWidget *panel, db_simscada::sim::ObjectPtrT simObj) :
  mSimObj(simObj)
{
  ui.setupUi(panel);

  connect(ui.optStateA, SIGNAL(clicked()), this, SLOT(setStateA()));
  connect(ui.optStateB, SIGNAL(clicked()), this, SLOT(setStateB()));
  connect(ui.optStateC, SIGNAL(clicked()), this, SLOT(setStateC()));
  connect(ui.optStateD, SIGNAL(clicked()), this, SLOT(setStateD()));
}

DigPointUi::~DigPointUi()
{

}

void
DigPointUi::setData(const sim_dll::Server::DigPointInfo& data)
{
  using namespace sim_dll;
  ui.optStateA->setText(QString(("A - " + data.textA).c_str()));
  ui.optStateB->setText(QString(("B - " + data.textB).c_str()));
  ui.optStateC->setText(QString(("C - " + data.textC).c_str()));
  ui.optStateD->setText(QString(("D - " + data.textD).c_str()));

  if (data.rawC == 0 && data.rawD == 0)
  {
    ui.optStateC->setVisible(false);
    ui.optStateD->setVisible(false);
  }

  switch (data.value)
  {
    case A:
      ui.optStateA->setChecked(true);
      break;

    case B:
      ui.optStateB->setChecked(true);
      break;

    case C:
      ui.optStateC->setChecked(true);
      break;

    case D:
      ui.optStateD->setChecked(true);
      break;

    case DigStateUnset:
      break;
  }

  colourOption(ui.optStateA, data.textA);
  colourOption(ui.optStateB, data.textB);
}

void
DigPointUi::colourOption(QRadioButton* option, const std::string& text)
{

  if (boost::icontains(text, std::string("open")))
    option->setStyleSheet("background-color: rgb(0, 150, 0);");

  else if (boost::icontains(text, std::string("close")))
    option->setStyleSheet("background-color: rgb(150, 0, 0);");
}

void
DigPointUi::setStateA()
{
  MainSim::Inst().setDigState(mSimObj, sim_dll::A);
}

void
DigPointUi::setStateB()
{
  MainSim::Inst().setDigState(mSimObj, sim_dll::B);
}

void
DigPointUi::setStateC()
{
  MainSim::Inst().setDigState(mSimObj, sim_dll::C);
}

void
DigPointUi::setStateD()
{
  MainSim::Inst().setDigState(mSimObj, sim_dll::D);
}
