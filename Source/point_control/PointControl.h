/*
 * PointControl.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef POINTCONTROL_H_
#define POINTCONTROL_H_

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include "SsQtModel.h"
#include "PlantModel.h"
#include "ObjectTreeFilterSettings.h"

#include "MainObserver.h"
#include "MainSimModelObserver.h"
#include "MainUiCenter.h"

class PointControlUi;

class SsQtModel;
class PlantModel;
class QModelIndex;

/*! PointControl Brief Description
 * Long comment about the class
 *
 */
class PointControl : public QObject,
    public MainObserver,
    public MainUiCenter,
    MainObjectModelObserver,
    public boost::enable_shared_from_this< PointControl >
{
  Q_OBJECT

  public:
    typedef boost::shared_ptr< PointControl > PtrT;

  public:
    PointControl();
    virtual
    ~PointControl();

    static void
    launch();

  public:
    void
    run();

  public slots:
    void
    objectFilterChange();

    void
    qtModelIndexSelected(const QModelIndex& index);

  public:
    // ---------------
    // Methods for getting the models

    db_simscada::SimScadaModelPtrT
    ormModel();

    boost::shared_ptr< const PlantModel >
    plantModel();

    ObjectTreeFilterSettings
    objectTreeFilter();

  private:
    // ---------------
    // MainUiCenter implementation

    void
    mainUiCenterClose();

  private:
    // ---------------
    // MainObserver implementation

    void
    mainUiModeChanged(main_ui::UiModeE oldUiMode, main_ui::UiModeE newUiMode);

  private:
    // ---------------
    // MainSimModelObserver implementation

    void
    simOrmModelChanged(boost::weak_ptr< db_simscada::SimScadaModel > ormModel);

  private:

    void
    objectTreeFilterChanged(const ObjectTreeFilterSettings& filter);

    void
    resetSelection();

  private:
    boost::shared_ptr< PointControlUi > mUi;

    db_simscada::SimScadaModelPtrT mSimOrmModel;
    boost::shared_ptr< SsQtModel > mSsQtModel;
    boost::shared_ptr< PlantModel > mPlantModel;

    bool mInitialising;

};

#endif /* POINTCONTROL_H_ */
