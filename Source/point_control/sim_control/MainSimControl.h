/*
 * MainSim.h
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#ifndef MAINSIM_H_
#define MAINSIM_H_

#include "MainSimModelObserver.h"

#include <set>
#include <vector>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include <QtCore/qobject.h>

#include <simscada/DbSimscadaDeclaration.h>

// Needed for the notify function :-|
#include "MainSimControlObserver.h"
#include "MainSimControlDispatcherAdaptor.h"

#include "SimDllDeclaration.h"
#include "Observable.h"

/*! MainSim Brief Description
 * Long comment about the class
 *
 */
class MainSim : public QObject,
    public Observable< MainSimObserver >,
    MainObjectModelObserver
{
  Q_OBJECT

  public:
    static MainSim&
    Inst();

    virtual
    ~MainSim();

  public:
    // ---------------
    // Misc functions

    virtual void
    simObjectsSelected(SimObjectsSelectedT objects);

  public:
    // ---------------
    // Functions to query simSCADA

    void
    requestObjectUpdate(db_simscada::sim::ObjectPtrT simObject);

    void
    registerForUpdates(db_simscada::sim::ObjectPtrT simObject);

    void
    deregisterForUpdates(db_simscada::sim::ObjectPtrT simObject);

  public:
    // ---------------
    // Functions to control simSCADA

    void
    setAnaRawValue(db_simscada::sim::ObjectPtrT simObject, double value);

    void
    setAnaEngValue(db_simscada::sim::ObjectPtrT simObject, double value);

    void
    setDigState(db_simscada::sim::ObjectPtrT simObject,
        sim_dll::DigStateE state);

  public slots:
    void
    connectToSim();

    void
    disconnectFromSim();

  public slots:
    // ---------------
    // Interface used ASIO

    void
    connectionBroken();

  public:
    void
    notifyOfObjectUpdateReceived(MainSimObserver::ObjectUpdate update);

    void
    notifyOfConnectionStateChanged(MainSimObserver::ConnectionStateE state);

    void
    notifyOfSimulatorDisconnected();

  private:
    // ---------------
    // MainObjectModelObserver interface

    virtual void
    simOrmModelChanged(boost::weak_ptr< db_simscada::SimScadaModel > ormModel);

  private:

    typedef std::set< MainSimObserver* > ObserversT;
    ObserversT mObservers;

  private:
    db_simscada::SimScadaModelPtrT mSimScadaModel;

    boost::shared_ptr< MainSimControlDispatcherAdaptor > mAdaptor;

    std::vector< std::string > mRegisteredAliases;

  private:
    // ---------------
    // Singleton stuff
    MainSim();

    static MainSim sInst;

};

#endif /* MAINSIM_H_ */
