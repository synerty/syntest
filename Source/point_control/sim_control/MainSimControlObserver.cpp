/*
 * MainSimObserver.cpp
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#include "MainSimControlObserver.h"
#include "MainSimControl.h"

MainSimObserver::MainSimObserver()
{
}
// ----------------------------------------------------------------------------

MainSimObserver::~MainSimObserver()
{
}
// ----------------------------------------------------------------------------

void
MainSimObserver::simConnectionStateChanged(ConnectionStateE state)
{
}
// ----------------------------------------------------------------------------

void
MainSimObserver::objectUpdateReceived(ObjectUpdate info)
{
}
// ----------------------------------------------------------------------------

void
MainSimObserver::simulatorDisconnected()
{
}
// ----------------------------------------------------------------------------
