/*
 * MainSimControlDispatcherAdaptor.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef MAINSIMCONTROLDISPATCHEROBSERVER_H_
#define MAINSIMCONTROLDISPATCHEROBSERVER_H_

#include <list>

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <QtCore/qobject.h>

#include "TcpClientDeclaration.h"
#include "SimDllDeclaration.h"
#include "SimDllDispatcherObserver.h"

namespace sim_dll
{
  class SimDllDispatcher;
} /* namespace sim_dll */

/*! MainSimControlDispatcherAdaptor Brief Description
 * Long comment about the class
 *
 */
class MainSimControlDispatcherAdaptor : public QObject,
    public sim_dll::SimDllDispatcherObserver,
    public boost::enable_shared_from_this< MainSimControlDispatcherAdaptor >
{
  Q_OBJECT

  public:
    MainSimControlDispatcherAdaptor();
    virtual
    ~MainSimControlDispatcherAdaptor();

  public:
    boost::shared_ptr< sim_dll::SimDllDispatcher >
    dispatcher();

    void
    connectSim();

    void
    disconnectSim();

    bool
    isSimConnected();

  public slots:
    void
    messageQueuedSlot();

    void
    connectionStateChangedSlot(int intState);

    void
    simulatorDisconnectedSlot();

  signals:
    void
    messageQueuedSignal();

    void
    connectionStateChangedSignal(int intState);

    void
    simulatorDisconnectedSignal();

  private:
    // ---------------
    // SimDllDispatcherInterface interface implementation
    virtual void
    postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
        std::string errorMessage);

    virtual void
    postedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message);

    virtual void
    postedSimulatorDisconnected();

  private:
    typedef boost::lock_guard< boost::mutex > LockerT;

    boost::shared_ptr< sim_dll::SimDllDispatcher > mDispatcher;

    std::list< sim_dll::Server::Msg > mMessageQueue;
    boost::mutex mMutex;
};

#endif /* MAINSIMCONTROLDISPATCHEROBSERVER_H_ */
