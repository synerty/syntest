/*
 * MainSimObserver.h
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#ifndef MAINSIMOBSERVER_H_
#define MAINSIMOBSERVER_H_

#include <stdint.h>
#include <string>

#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>

#include <simscada/DbSimscadaDeclaration.h>

#include "SimDllDeclaration.h"

/*! Main Sim Observer
 *  Notifies of changes with in simSCADA.
 *
 */
class MainSimObserver
{
  public:
    typedef sim_dll::DigStateE DigStateE;
    typedef sim_dll::Server::CtrlTypeE CtrlTypeE;
    typedef sim_dll::Server::Msg ObjectUpdate;

  public:
    enum ConnectionStateE
    {
      Connecting,
      Connected,
      Disconnected,
      Broken,
      ConnectionAttemptFailed
    };

  public:
    MainSimObserver();

    virtual
    ~MainSimObserver();

  public:

    virtual void
    simConnectionStateChanged(ConnectionStateE state);

    virtual void
    objectUpdateReceived(ObjectUpdate info);

    virtual void
    simulatorDisconnected();

};

#endif /* MAINSIMOBSERVER_H_ */
