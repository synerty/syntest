/*
 * MainSim.cpp
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#include "MainSimControl.h"

#include <QtGui/qmessagebox.h>

#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimObject.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "Globals.h"
#include "SimDllDispatcher.h"
#include "MainSimControlDispatcherAdaptor.h"

using namespace db_simscada;
using namespace sim;

// ----------------------------------------------------------------------------
//                          MainSim implementation
// ----------------------------------------------------------------------------

// Excplitit instantiation
#include "Observable.ini"
template class Observable< MainSimObserver > ;

// Initialise static variables
// After MainObjectModel::sInst is initialised
MainSim MainSim::sInst __attribute__((init_priority(65001)));

MainSim::MainSim()
{
}
// ----------------------------------------------------------------------------

MainSim&
MainSim::Inst()
{
  if (!sInst.mAdaptor.get())
  {
    sInst.mAdaptor.reset(new MainSimControlDispatcherAdaptor());
    sInst.mAdaptor->dispatcher()->addObserver(sInst.mAdaptor);
  }

  return sInst;
}
// ----------------------------------------------------------------------------

MainSim::~MainSim()
{
}
// ----------------------------------------------------------------------------

void
MainSim::requestObjectUpdate(db_simscada::sim::Object::PtrT simObject)
{
  if (mAdaptor.get() == NULL || simObject.get() == NULL)
    return;

  mAdaptor->dispatcher()->requestPointInfo(simObject);
}
// ----------------------------------------------------------------------------

void
MainSim::registerForUpdates(db_simscada::sim::ObjectPtrT simObject)
{
  if (mAdaptor.get() == NULL || simObject.get() == NULL)
    return;

  mAdaptor->dispatcher()->registerForUpdates(simObject);
}
// ----------------------------------------------------------------------------

void
MainSim::deregisterForUpdates(db_simscada::sim::ObjectPtrT simObject)
{
  if (mAdaptor.get() == NULL || simObject.get() == NULL)
    return;

  mAdaptor->dispatcher()->deregisterForUpdates(simObject->alias());
}
// ----------------------------------------------------------------------------

void
MainSim::setAnaRawValue(db_simscada::sim::ObjectPtrT simObject, double value)
{
  if (mAdaptor.get() == NULL || simObject.get() == NULL)
    return;

  mAdaptor->dispatcher()->setAnaRawValue(simObject, value);
}
// ----------------------------------------------------------------------------

void
MainSim::setAnaEngValue(db_simscada::sim::ObjectPtrT simObject, double value)
{
  if (mAdaptor.get() == NULL || simObject.get() == NULL)
    return;

  mAdaptor->dispatcher()->setAnaEngValue(simObject, value);
}
// ----------------------------------------------------------------------------

void
MainSim::setDigState(db_simscada::sim::ObjectPtrT simObject,
    sim_dll::DigStateE state)
{
  if (mAdaptor.get() == NULL || simObject.get() == NULL)
    return;

  mAdaptor->dispatcher()->setDigState(simObject, state);
}
// ----------------------------------------------------------------------------

void
MainSim::notifyOfObjectUpdateReceived(MainSimObserver::ObjectUpdate update)
{
  // Skip notifications of registrations
  if (update.msgType == sim_dll::Server::RegPointSucceeded)
  {
    return;
  }
  else if (update.msgType == sim_dll::Server::RegPointFailed)
  {
    boost::format info("Failed to register '%s' for update notifications in simulator.");
    info % update.alias;
    QMessageBox::critical(MsgBoxParent(),
        "Simulator Registration Failed",
        info.str().c_str());
    return;
  }

  ObserverPtrsT observers;
  getObserverPtrs(observers);

  for (ObserverPtrsT::iterator itr = observers.begin(); //
  itr != observers.end(); //
      ++itr)
  {
    (*itr)->objectUpdateReceived(update);
  }
}
// ----------------------------------------------------------------------------

void
MainSim::connectToSim()
{
  mAdaptor->connectSim();
}
// ----------------------------------------------------------------------------

void
MainSim::disconnectFromSim()
{
  mAdaptor->disconnectSim();
}
// ----------------------------------------------------------------------------

void
MainSim::connectionBroken()
{
  disconnectFromSim();
  notifyOfConnectionStateChanged(MainSimObserver::Broken);
}
// ----------------------------------------------------------------------------

// ---------------
// MainObjectModelObserver interface

void
MainSim::simOrmModelChanged(boost::weak_ptr< db_simscada::SimScadaModel > ormModel)
{
}
// ----------------------------------------------------------------------------

// #################################################
// NOTE, This function should be in PointControl.cpp
void
MainSim::simObjectsSelected(SimObjectsSelectedT objects)
{
  if (mAdaptor.get() == NULL)
  {
    mRegisteredAliases.clear();
    return;
  }

  if (!mAdaptor->isSimConnected())
  {
    mRegisteredAliases.clear();
    notifyOfSimulatorDisconnected();
    return;
  }

  for (std::vector< std::string >::const_iterator itr = mRegisteredAliases.begin(); //
  itr != mRegisteredAliases.end(); //
      ++itr)
  {
    mAdaptor->dispatcher()->deregisterForUpdates(*itr);
  }

  mRegisteredAliases.clear();

  for (SimObjectsSelectedT::const_iterator itr = objects.begin(); //
  itr != objects.end(); //
      ++itr)
  {
    db_simscada::sim::Object::PtrT obj = itr->lock();

    if (obj.get() == NULL)
      continue;

    switch (toObjectType(obj))
    {
      case AccumulatorObject:
        // TODO Accumulators
        break;

      case AnalogueObject:
      case DigitalObject:
      case ControlObject:
        mAdaptor->dispatcher()->registerForUpdates(obj);
        mRegisteredAliases.push_back(obj->alias());
        break;

      case ProtocolObject:
      case DataPortObject:
      case ChannelObject:
      case RtuObject:
      case DataGroupObject:
      case ObjectTypeCount:
      case ObjectTypeUnset:
        break;
    }
  }

}
// ----------------------------------------------------------------------------

// ---------------
// Public functions

void
MainSim::notifyOfConnectionStateChanged(MainSimObserver::ConnectionStateE state)
{
  ObserverPtrsT observers;
  getObserverPtrs(observers);

  for (ObserverPtrsT::iterator itr = observers.begin(); //
  itr != observers.end(); //
      ++itr)
  {
    (*itr)->simConnectionStateChanged(state);
  }
}
// ----------------------------------------------------------------------------

void
MainSim::notifyOfSimulatorDisconnected()
{
  notifyOfSomthing(&MainSimObserver::simulatorDisconnected);
}
// ----------------------------------------------------------------------------

// ---------------
// Private functions
