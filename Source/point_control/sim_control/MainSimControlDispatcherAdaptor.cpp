/*
 * MainSimControlDispatcherAdaptor.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "MainSimControlDispatcherAdaptor.h"

#include <QtCore/qglobal.h>
#include <QtGui/qapplication.h>

#include "Main.h"
#include "MainSimControl.h"
#include "SimDllDispatcher.h"

#include "SimScadaSettings.h"
#include "Settings.h"
#include "Macros.h"
#include "Proactor.h"

MainSimControlDispatcherAdaptor::MainSimControlDispatcherAdaptor()
{
  moveToThread(QApplication::instance()->thread());

  SimScadaSettings settings = Settings().simScada();

  mDispatcher.reset(new sim_dll::SimDllDispatcher(Main::Inst().proactor(),
      settings.Host,
      25610));

  connect(this, SIGNAL(messageQueuedSignal()), this, SLOT(messageQueuedSlot()));
  connect(this,
      SIGNAL(connectionStateChangedSignal(int)),
      this,
      SLOT(connectionStateChangedSlot(int)),
      Qt::BlockingQueuedConnection);
  connect(this,
      SIGNAL(simulatorDisconnectedSignal()),
      this,
      SLOT(simulatorDisconnectedSlot()),
      Qt::BlockingQueuedConnection);
}

MainSimControlDispatcherAdaptor::~MainSimControlDispatcherAdaptor()
{
  qDebug("Destructing %s", __PRETTY_FUNCTION__);
}

boost::shared_ptr< sim_dll::SimDllDispatcher >
MainSimControlDispatcherAdaptor::dispatcher()
{
  assert(mDispatcher.get());
  return mDispatcher;
}

void
MainSimControlDispatcherAdaptor::connectSim()
{
  Main::Inst().proactor()->ioService().post( //
  boost::bind(&sim_dll::SimDllDispatcher::postedConnect, mDispatcher));
}

void
MainSimControlDispatcherAdaptor::disconnectSim()
{
  Main::Inst().proactor()->ioService().post( //
  boost::bind(&sim_dll::SimDllDispatcher::postedDisconnect, mDispatcher));
}

bool
MainSimControlDispatcherAdaptor::isSimConnected()
{
  boost::shared_ptr< sim_dll::SimDllDispatcher > dispatcher = mDispatcher;
  return dispatcher.get() ? dispatcher->isConnected() : false;
}

void
MainSimControlDispatcherAdaptor::postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
    std::string errorMessage)
{
  int intState = newState;
  emit connectionStateChangedSignal(intState);
}

void
MainSimControlDispatcherAdaptor::postedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message)
{
  // Scope the locker
  {
    LockerT lock(mMutex);
    mMessageQueue.push_back(message);
  }
  emit messageQueuedSignal();
}

void
MainSimControlDispatcherAdaptor::postedSimulatorDisconnected()
{
  emit simulatorDisconnectedSignal();
}

void
MainSimControlDispatcherAdaptor::connectionStateChangedSlot(int intState)
{
  tcp_client::ConnectionStateE newState = static_cast< tcp_client::ConnectionStateE >(intState);
  switch (newState)
  {
    case tcp_client::connectingState:
      MainSim::Inst().notifyOfConnectionStateChanged(MainSimObserver::Connecting);
      break;

    case tcp_client::connectedState:
      MainSim::Inst().notifyOfConnectionStateChanged(MainSimObserver::Connected);
      break;

    case tcp_client::connectionAttemptFailedState:
      MainSim::Inst().notifyOfConnectionStateChanged(MainSimObserver::ConnectionAttemptFailed);
      break;

    case tcp_client::disconnectedState:
      MainSim::Inst().notifyOfConnectionStateChanged(MainSimObserver::Disconnected);
      break;

    case tcp_client::connectionBrokenState:
      MainSim::Inst().connectionBroken();
      break;

    THROW_UNHANDLED_CASE_EXCEPTION
  }
}

void
MainSimControlDispatcherAdaptor::simulatorDisconnectedSlot()
{
  MainSim::Inst().notifyOfSimulatorDisconnected();
}

void
MainSimControlDispatcherAdaptor::messageQueuedSlot()
{
  LockerT lock(mMutex);

  while (!mMessageQueue.empty())
  {
    MainSim::Inst().notifyOfObjectUpdateReceived(mMessageQueue.front());
    mMessageQueue.pop_front();
  }
}
