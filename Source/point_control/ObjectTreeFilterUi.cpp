#include "ObjectTreeFilterUi.h"

#include <assert.h>

#include "Globals.h"

ObjectTreeFilterUi::ObjectTreeFilterUi() :
  QDialog(MsgBoxParent())
{
  ui.setupUi(this);
  connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

  for (int i = 0; i < ObjectTreeFilterSettings::SortEnumCount; i++)
    ui.cboSortBy->addItem(QString(ObjectTreeFilterSettings::EnumToString(static_cast<ObjectTreeFilterSettings::SortE> (i)).c_str()));

}
// ----------------------------------------------------------------------------

ObjectTreeFilterUi::~ObjectTreeFilterUi()
{

}
// ----------------------------------------------------------------------------

ObjectTreeFilterSettings
ObjectTreeFilterUi::Filter()
{
  ObjectTreeFilterSettings filter;

  if (ui.tabWidget->currentWidget() == ui.tabStandard)
    filter.Filter = ObjectTreeFilterSettings::Standard;

  else if (ui.tabWidget->currentWidget() == ui.tabManualTesting)
    filter.Filter = ObjectTreeFilterSettings::ManualTesting;

else    assert(false);

    filter.Sort = static_cast<ObjectTreeFilterSettings::SortE>(ui.cboSortBy->currentIndex());

    return filter;
  } // ----------------------------------------------------------------------------

void
ObjectTreeFilterUi::SetFilter(const ObjectTreeFilterSettings& filter)
{
  switch (filter.Filter)
  {
    case ObjectTreeFilterSettings::Standard:
      ui.tabWidget->setCurrentWidget(ui.tabStandard);
      break;

    case ObjectTreeFilterSettings::ManualTesting:
      ui.tabWidget->setCurrentWidget(ui.tabManualTesting);
      break;

    default:
      assert(false);
      break;
  }

  ui.cboSortBy->setCurrentIndex(filter.Sort);
}
// ----------------------------------------------------------------------------
