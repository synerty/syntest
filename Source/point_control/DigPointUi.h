#ifndef DIGPOINTUI_H
#define DIGPOINTUI_H

#include <boost/shared_ptr.hpp>

#include <QtCore/qobject.h>

#include <simscada/DbSimscadaDeclaration.h>

#include "ui_DigPointUi.h"

#include "SimDllDeclaration.h"

class DigPointUi : public QObject
{
  Q_OBJECT

  public:
    DigPointUi(QWidget* panel, db_simscada::sim::ObjectPtrT simObj);
    virtual
    ~DigPointUi();

  public:
    void
    setData(const sim_dll::Server::DigPointInfo& data);

  public slots:
    void
    setStateA();

    void
    setStateB();

    void
    setStateC();

    void
    setStateD();

  private:
    void
    colourOption(QRadioButton* option, const std::string& text);
    Ui::DigPointUiClass ui;

    db_simscada::sim::ObjectPtrT mSimObj;
};

#endif // DIGPOINTUI_H
