/*
 * QtModelTest.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "QtModelTest.h"

#include <assert.h>

#include "QtModelUserRoles.h"

QtModelTest::QtModelTest(QAbstractItemModel* model) :
  mModel(model)
{
}

QtModelTest::~QtModelTest()
{
}

void
QtModelTest::run()
{
  testModel();
}

void
QtModelTest::testModel(QModelIndex parent)
{
//  qDebug("Testing parent row=%d, col=%d, alias=%s",
//      parent.row(),
//      parent.column(),
//      parent.isValid() ? mModel->data(parent, Qt::DisplayRole).toString().toStdString().c_str()
//          : "");

  int rowCount = mModel->rowCount(parent);
  int colCount = mModel->columnCount(parent);

  for (int row = 0; row < rowCount; row++)
  {
    // Test the data
    for (int col = 0; col < colCount; col++)
    {

      QModelIndex index = mModel->index(row, col, parent);

      // Test the parent
      QModelIndex testParent = mModel->parent(index);
      if (testParent != parent)
        assert(testParent == parent);

      QVariant disp = mModel->data(index, Qt::DisplayRole);
      QVariant edit = mModel->data(index, Qt::EditRole);
      QVariant checkState = mModel->data(index, Qt::CheckStateRole);
      QVariant forCol = mModel->data(index, Qt::ForegroundRole);
      QVariant backCol = mModel->data(index, Qt::BackgroundRole);
      QVariant toolTip = mModel->data(index, Qt::ToolTipRole);
      QVariant font = mModel->data(index, Qt::FontRole);
      QVariant options = mModel->data(index, Qt::ComboOptionsRole);
      Qt::ItemFlags flats = mModel->flags(index);
    }

    // Test the subtree
    QModelIndex index = mModel->index(row, 0, parent);
    testModel(index);

  }
}
