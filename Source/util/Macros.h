/*
 * Maros.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <boost/format.hpp>
#include <QtCore/qglobal.h>

// Throw an exception if a switch doesn't handle something and it should
// have handled everything.
#define THROW_UNHANDLED_CASE_EXCEPTION \
  default: \
  { \
      throw std::runtime_error(std::string("Unhandled switch value ") \
              + "Function: " + __PRETTY_FUNCTION__ + "\nFile :" + __FILE__); \
  } \
  break;

// Throw an exception if an if statement doesn't handle something and it should
// have handled everything.
#define THROW_UNHANDLED_IF_EXCEPTION \
  { \
      throw std::runtime_error(std::string("Unhandled if statement condition ") \
              + __PRETTY_FUNCTION__); \
  }

// Function start and function end try/catch blocks
// An attempt to add stack tracing to exceptions

#ifdef DEBUG

//#define DEBUG_FUNC void
//#define DEBUG_FUNC qDebug

#define FUNCTION_START( DESC )  \
  \
  qDebug("Entering %s", DESC); \
  try { \
  // END #define FUNCTION_START

#define FUNCTION_END( DESC )  \
  \
  qDebug("Exiting %s", DESC); \
  } \
  catch (...) \
  { \
    qDebug("Exception Stacktrace %s", DESC); \
    throw; \
  } \
  // END #define FUNCTION_END

template<typename ClassT>
  struct DebugClassMixin
  {
      DebugClassMixin()
      {
        qDebug("Constructing class %s", typeid(ClassT).name());
      }

      virtual
      ~DebugClassMixin()
      {
        qDebug("Destructing class %s", typeid(ClassT).name());
      }
  };

#define DEBUG_CLASS_MIXIN( CLASS_TYPE ) \
  ,  DebugClassMixin< CLASS_TYPE > \
  // END #define DEBUG_CLASS_MIXIN

#define DEBUG_CLASS_BASE( CLASS_TYPE ) \
  :  DebugClassMixin< CLASS_TYPE > \
    // END #define DEBUG_CLASS_BASE

#else
#define FUNCTION_START( DESC )
#define FUNCTION_END( DESC )

template<typename ClassT>
struct DebugClassMixin
{
};

#define DEBUG_CLASS_MIXIN( CLASS_TYPE )
#define DEBUG_CLASS_BASE( CLASS_TYPE )
#endif

#endif /* MACROS_H_ */
