/*
 * Progress.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Administrator
 */

#include "Progress.h"

Progress::Progress()
{
}

Progress::~Progress()
{
}

void
Progress::SetMax(uint32_t max)
{
}
// ----------------------------------------------------------------------------

void
Progress::Reset()
{
}
// ----------------------------------------------------------------------------

void
Progress::Incriment(uint32_t incriment)
{
}
// ----------------------------------------------------------------------------

void
Progress::SetDescription(const std::string desc)
{
}
// ----------------------------------------------------------------------------

ProgressPtrT
Progress::AddSubProgress(ProgressPtrT progress)
{
  if (!progress.get())
    progress.reset(new Progress());

  mSubProgresses.push_back(progress);
  return progress;
}
// ----------------------------------------------------------------------------

