/*
 * UiSender.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _UI_MESSAGE_H_
#define _UI_MESSAGE_H_

#include <QtCore/qobject.h>
#include <QtCore/qstring.h>

#include <string>

/*! autotest::exec::processor::sim_dispatcher_proxy::UiSender Brief Description
 * Long comment about the class
 *
 */
class UiMessage : public QObject
{
  Q_OBJECT

  public:
    UiMessage();
    virtual
    ~UiMessage();

  public:
/// ---------------
/// Message box stuff

    enum MessageTypeE
    {
      criticalMessage,
      warningMessage,
      infomationMessage
    };

    void
    showMessage(const MessageTypeE type,
        const std::string title,
        const std::string message) const;

  public slots:
    void
    __showMessageSlot(const int type,
        const QString title,
        const QString message) const;

  signals:

    void
    __showMessageSignal(const int type,
        const QString title,
        const QString message) const;

};

#endif /* UISENDER_H_ */
