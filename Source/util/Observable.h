/*
 * Observable.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _OBSERVABLE_H_
#define _OBSERVABLE_H_

#include <set>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/thread/mutex.hpp>

/*! Observable patterh
 * This class is responsible for processing the tests
 *
 */
template<typename ObserverT>
  class Observable
  {
    protected:
      typedef boost::weak_ptr< ObserverT > ObserverWptrT;
      typedef boost::shared_ptr< ObserverT > ObserverPtrT;
      typedef std::set< ObserverWptrT > ObserversT;
      typedef std::vector< ObserverPtrT > ObserverPtrsT;
      typedef void
      (ObserverT::*ProcObserveFuncT)();

    public:
      virtual
      ~Observable();

    public:
      /// ---------------
      /// Observerable Stuff

      void
      addObserver(ObserverPtrT observer);

      void
      removeObserver(ObserverPtrT observer);

      void
      removeObserver(const ObserverT* observer);



    protected:
      /// ---------------
      /// Observerable Stuff

      void
      notifyOfSomthing(ProcObserveFuncT func);

      void
      getObserverPtrs(ObserverPtrsT& observerPtrs);

    private:
      /// ---------------
      /// Observerable Stuff

      ObserversT mObservers;
      boost::mutex mMutex;

  };

#endif /* _OBSERVABLE_H_ */
