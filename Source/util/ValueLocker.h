/*
 * BoolLocker.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _VALUE_LOCKER_H_
#define _VALUE_LOCKER_H_

#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/locks.hpp>

/*! BoolLocker Brief Description
 * Long comment about the class
 *
 */
template<typename TypeT>
  class ValueLocker
  {
    public:
      ValueLocker(const TypeT initialValue = false);
      virtual
      ~ValueLocker();

    public:
      typedef boost::unique_lock< boost::shared_mutex > ExclusiveLockT;

    public:
      /// ---------------
      /// Mutex stuff

      bool
      value() const;

      void
      setValue(const bool val);

      ExclusiveLockT
      lock();

    private:
      mutable boost::shared_mutex mMutex;
      TypeT mValue;

  };

typedef typename ValueLocker< bool > BoolLocker;

#include "ValueLocker.ini"

#endif /* _VALUE_LOCKER_H_ */
