/*
 * UiProgress.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef UIPROGRESS_H_
#define UIPROGRESS_H_

#include "Progress.h"

// TODO implement UiProgress

/*! UiProgress Brief Description
 * Long comment about the class
 *
 */
class UiProgress : public Progress
{
  public:
    UiProgress();
    virtual
    ~UiProgress();
};

#endif /* UIPROGRESS_H_ */
