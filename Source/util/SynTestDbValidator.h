/*
 * SynTestDbValidator.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _SYN_TEST_DB_VALIDATOR_H_
#define _SYN_TEST_DB_VALIDATOR_H_

#include <simscada/DbSimscadaDeclaration.h>
#include <syntest/DbSyntestDeclaration.h>

/*! SynTEST DB Validator
 * Validate the data in the SynTEST data model matches what is configured in
 * simSCADA
 *
 */
class SynTestDbValidator
{
  public:
    SynTestDbValidator(db_syntest::SynTestModelPtrT synModel,
        db_simscada::SimScadaModelPtrT simModel);
    virtual
    ~SynTestDbValidator();

  public:
    void
    run();

  private:
    db_syntest::SynTestModelPtrT mSynModel;
    db_simscada::SimScadaModelPtrT mSimModel;
};

#endif /* SYNTESTDBVALIDATOR_H_ */
