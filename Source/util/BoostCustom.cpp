// Std C++ Headers
#include <stdexcept>

// Boost Headers
#include <boost/format.hpp>


namespace boost
{
  void
  assertion_failed(char const * expr,
      char const * function,
      char const * file,
      long line)
  {
    boost::format info("Assertion (%s) failed at %s:%d - %s");
    info % expr;
    info % file;
    info % line;
    info % function;
    throw std::runtime_error(info.str());

  }
}
