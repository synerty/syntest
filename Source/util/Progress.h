/*
 * Progress.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Administrator
 */

#ifndef PROGRESS_H_
#define PROGRESS_H_

#include <stdint.h>
#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

class Progress;
typedef boost::shared_ptr< Progress > ProgressPtrT;

/*! Progress Brief Description
 * Long comment about the class
 *
 */
class Progress
{
  public:
    Progress();

    virtual
    ~Progress();

  public:
    void
    SetMax(uint32_t max);

    void
    Reset();

    void
    Incriment(uint32_t incriment = 1);

    void
    SetDescription(const std::string desc);

    ProgressPtrT
    AddSubProgress(ProgressPtrT progress = ProgressPtrT());

  private:
    typedef std::vector< ProgressPtrT > ProgressesT;
    ProgressesT mSubProgresses;
};

#endif /* PROGRESS_H_ */
