/*
 * OrmModelThread.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef ORMMODELTHREAD_H_
#define ORMMODELTHREAD_H_

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/format.hpp>

#include <QtCore/qthread.h>
#include <QtGui/qapplication.h>
#include <QtGui/qprogressdialog.h>
#include <QtGui/qpushbutton.h>
#include <QtGui/qlabel.h>

#include <orm/v1/Model.h>

#include "Globals.h"

/*! OrmModelThread Brief Description
 * Long comment about the class
 *
 */
template<typename ModelT>
  class OrmModelThread : public QThread
  {
    public:
      typedef typename boost::shared_ptr< ModelT > ModelPtrT;

      typedef bool
      (ModelT::*ModelFuncT)();

      OrmModelThread(ModelPtrT model, ModelFuncT func) :
          mModel(model), mFunc(func), mResult(false)
      {
      }

      virtual
      ~OrmModelThread()
      {
      }

    public:

      std::string
      exceptionMessage()
      {
        return mExceptMessage;
      }

      bool
      isExcepted()
      {
        return !mExceptMessage.empty();
      }

      bool
      functionResult()
      {
        return mResult;
      }

    public:
      void
      run()
      {
        try
        {
          ModelT& model = *mModel;
          mResult = (model.*mFunc)();
        }
        catch (std::exception& e)
        {
          mExceptMessage = e.what();
        }
      }

    private:
      ModelPtrT mModel;
      ModelFuncT mFunc;
      bool mResult;
      std::string mExceptMessage;
  };

/*! OrmModelThreadUnloader Brief Description
 * Long comment about the class
 *
 */
template<typename ModelT>
  class OrmModelThreadUnloader : public QThread
  {
    public:
      typedef boost::shared_ptr< ModelT > ModelPtrT;
      typedef OrmModelThreadUnloader< ModelT > ThisT;
      typedef typename boost::shared_ptr< ThisT > PtrT;

      OrmModelThreadUnloader(ModelPtrT model) :
          mModel(model)
      {
      }

      virtual
      ~OrmModelThreadUnloader()
      {
      }

    public:
      void
      run()
      {
        mModel.reset();
      }

    private:
      ModelPtrT mModel;
  };

template<typename ModelT>
  void
  unloadModel(boost::shared_ptr< ModelT >& model, std::string modelTitle)
  {
    QString title = QString(modelTitle.c_str());
    title += "\nUnloading database";

    QProgressDialog progressDialog(MsgBoxParent());
    progressDialog.setMinimumDuration(0);
    progressDialog.setLabel(new QLabel(title));
    progressDialog.setAutoClose(false);
    progressDialog.setCancelButton(NULL);
    progressDialog.setModal(true);
    progressDialog.show();

    typedef OrmModelThreadUnloader< ModelT > ThreadT;
    ThreadT thread(model);
    model.reset(new ModelT());
    thread.start();

    while (thread.isRunning())
      QApplication::processEvents();

    progressDialog.close();
  }

template<typename ModelT>
  void
  loadModel(boost::shared_ptr< ModelT > model, std::string modelTitle)
  {

    qDebug("Connecting to %s database", modelTitle.c_str());
    // Open
    {
      QString title = QString(modelTitle.c_str());
      title += "\nConnecting to database";

      QProgressDialog progressDialog(MsgBoxParent());
      progressDialog.setMinimumDuration(0);
      progressDialog.setLabel(new QLabel(title));
      progressDialog.setModal(true);
      progressDialog.setAutoClose(false);
      progressDialog.setCancelButton(NULL);
      progressDialog.show();

      OrmModelThread< ModelT > thread(model, &ModelT::openDb);
      thread.start();

      while (thread.isRunning())
      {
        QApplication::processEvents();
        if (progressDialog.wasCanceled())
        {
          thread.terminate();
          unloadModel(model, modelTitle);

          boost::format fmt("Can not connect to database %s\n"
              "Operation was canceled by user");
          fmt % modelTitle;
          throw std::runtime_error(fmt.str());
        }
      }

      if (thread.isExcepted() || !thread.functionResult())
      {
        boost::format fmt("Can not connect to database %s\n%s%s");
        fmt % modelTitle;
        fmt % ""; //model.Database;
        fmt % (thread.isExcepted() ? "\n" + thread.exceptionMessage() : "");
        model.reset(new ModelT());
        throw std::runtime_error(fmt.str());
      }

      progressDialog.close();
    }

    qDebug("Loading %s model", modelTitle.c_str());
    // Load
    {
      QString title = QString(modelTitle.c_str());
      title += "\nLoading database model";

      QProgressDialog progressDialog(MsgBoxParent());
      progressDialog.setMinimumDuration(0);
      progressDialog.setLabel(new QLabel(title));
      progressDialog.setModal(true);
      progressDialog.setAutoClose(false);
      progressDialog.setCancelButton(NULL);
      progressDialog.show();

      OrmModelThread< ModelT > thread(model, &ModelT::load);
      thread.start();

      while (thread.isRunning())
      {
        QApplication::processEvents();
        if (progressDialog.wasCanceled())
        {
          thread.terminate();
          unloadModel(model, modelTitle);

          boost::format fmt("Can not load model %s\n"
              "Operation was canceled by user");
          fmt % modelTitle;
          throw std::runtime_error(fmt.str());
        }
      }

      if (thread.isExcepted() || !thread.functionResult())
      {
        boost::format fmt("Can not load model %s%s");
        fmt % modelTitle;
        fmt % (thread.isExcepted() ? "\n" + thread.exceptionMessage() : "");
        model.reset(new ModelT());
        throw std::runtime_error(fmt.str());
      }

      progressDialog.close();
    }

    qDebug("Loading %s successfull", modelTitle.c_str());
  }
#endif /* ORMMODELTHREAD_H_ */
