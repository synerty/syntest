/*
 * ObservableProgress.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Administrator
 */

#include "ObservableProgress.h"

#include "Progress.h"

ObservableProgress::ObservableProgress()
{
}

ObservableProgress::~ObservableProgress()
{
}
// ----------------------------------------------------------------------------

void
ObservableProgress::AddProgressObserver(Progress* progress)
{
  mProgressObservers.insert(progress);
}
// ----------------------------------------------------------------------------

void
ObservableProgress::RemoveProgressObserver(Progress* progress)
{
  mProgressObservers.erase(progress);
}
// ----------------------------------------------------------------------------

void
ObservableProgress::SetProgressMax(uint32_t max)
{
  for (std::set<Progress*>::iterator itr = mProgressObservers.begin();//
  itr != mProgressObservers.end(); //
  ++itr)
  {
    (*itr)->SetMax(max);
  }
}
// ----------------------------------------------------------------------------

void
ObservableProgress::ProgressReset()
{
  for (std::set<Progress*>::iterator itr = mProgressObservers.begin();//
  itr != mProgressObservers.end(); //
  ++itr)
  {
    (*itr)->Reset();
  }
}
// ----------------------------------------------------------------------------

void
ObservableProgress::ProgressIncrement(uint32_t incriment)
{
  for (std::set<Progress*>::iterator itr = mProgressObservers.begin();//
  itr != mProgressObservers.end(); //
  ++itr)
  {
    (*itr)->Incriment(incriment);
  }
}
// ----------------------------------------------------------------------------

void
ObservableProgress::SetProgressDescription(const std::string desc)
{
  for (std::set<Progress*>::iterator itr = mProgressObservers.begin();//
  itr != mProgressObservers.end(); //
  ++itr)
  {
    (*itr)->SetDescription(desc);
  }
}
// ----------------------------------------------------------------------------

