/*
 * UiSender.cpp
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "UiMessage.h"

#include <QtCore/qthread.h>
#include <QtGui/qapplication.h>
#include <QtGui/qmessagebox.h>

#include "Macros.h"
#include "Globals.h"

UiMessage::UiMessage()
{
  moveToThread(QApplication::instance()->thread());
  connect(this,
      SIGNAL(__showMessageSignal(int, QString, QString)),
      this,
      SLOT(__showMessageSlot(int, QString, QString)),
      Qt::BlockingQueuedConnection);
}
/// ---------------------------------------------------------------------------

UiMessage::~UiMessage()
{
}
/// ---------------------------------------------------------------------------

/// ---------------
/// Message box stuff

void
UiMessage::showMessage(const MessageTypeE type,
    const std::string title,
    const std::string message) const
{
  int typeInt = type;

  if (QApplication::instance()->thread() == QThread::currentThread())
    __showMessageSlot(typeInt, title.c_str(), message.c_str());
  else
    emit __showMessageSignal(typeInt, title.c_str(), message.c_str());
}
/// ---------------------------------------------------------------------------

void
UiMessage::__showMessageSlot(const int type,
    const QString title,
    const QString message) const
{
  switch (type)
  {
    case criticalMessage:
      QMessageBox::critical(MsgBoxParent(), title, message);
      break;

    case warningMessage:
      QMessageBox::warning(MsgBoxParent(), title, message);
      break;

    case infomationMessage:
      QMessageBox::information(MsgBoxParent(), title, message);
      break;

    THROW_UNHANDLED_CASE_EXCEPTION
  }

}
/// ---------------------------------------------------------------------------

