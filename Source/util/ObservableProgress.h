/*
 * ObservableProgress.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Administrator
 */

#ifndef OBSERVABLEPROGRESS_H_
#define OBSERVABLEPROGRESS_H_

#include <stdint.h>
#include <string>
#include <set>

class Progress;

// FIXME - Update to use weak pointers

/*! ObservableProgress Brief Description
 * Long comment about the class
 *
 */
class ObservableProgress
{
  public:
    ObservableProgress();
    virtual
    ~ObservableProgress();

  public:
    void
    AddProgressObserver(Progress* progress);

    void
    RemoveProgressObserver(Progress* progress);

  public:
    void
    SetProgressMax(uint32_t max);

    void
    ProgressReset();

    void
    ProgressIncrement(uint32_t incriment = 1);

    void
    SetProgressDescription(const std::string desc);

  private:
    std::set<Progress*> mProgressObservers;
};

#endif /* OBSERVABLEPROGRESS_H_ */
