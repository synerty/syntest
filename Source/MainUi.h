#ifndef _MAIN_UI_H_
#define _MAIN_UI_H_

#include <boost/shared_ptr.hpp>

#include <QtGui/QMainWindow>
#include "ui_MainUi.h"

class Main;
class QLabel;

class MainUi : public QMainWindow
{
  Q_OBJECT

  public:
    MainUi(Main& uc);
    virtual
    ~MainUi();

  public:
    // ---------------
    // Interface used by Main

    void setCentralWidget(QWidget* widget);

  public slots:
    void
    openSynTestDatabase();

    void
    openSimScadaDatabase();

  protected:
    // ---------------
    // QMainWindow events

     void closeEvent(QCloseEvent *);

  private:
    // Functions
    void
    connectSlots();

    // Members
    Ui::MainUiClass ui;

    // status bar members
    QLabel* mSimConnStateWgt;

    Main& mUc;
};

#endif // _MAIN_UI_H_
