#include "MainUi.h"

#include <boost/format.hpp>

#include <QtGui/qfiledialog.h>
#include <QtGui/qlabel.h>
#include <QtGui/qmessagebox.h>

#include "Globals.h"
#include "Main.h"
#include "MainSimControl.h"
#include "MainAutoTest.h"
#include "MainSynModel.h"
#include "MainSimModel.h"

#include "Settings.h"
#include "SimScadaSettings.h"

MainUi::MainUi(Main& uc) :
    mSimConnStateWgt(NULL), //
    mUc(uc)
{
  ui.setupUi(this);

  // ----------
  // Add status bar widgets

  mSimConnStateWgt = new QLabel(this);
  ui.statusbar->addPermanentWidget(mSimConnStateWgt);

  connectSlots();
}
// ----------------------------------------------------------------------------

MainUi::~MainUi()
{
}
// ----------------------------------------------------------------------------

void
MainUi::setCentralWidget(QWidget* widget)
{
  QMainWindow::setCentralWidget(widget);
}
// ----------------------------------------------------------------------------

void
MainUi::connectSlots()
{

  // Menu items

  // ----------
  // File menu

  connect(ui.actionSettings,
      SIGNAL(triggered()),
      &Main::Inst(),
      SLOT(openSettings()));

  connect(ui.actionExit,
      SIGNAL(triggered()),
      &Main::Inst(),
      SLOT(exitApplication()));

  // ----------
  // simSCADA Database menu

  connect(ui.actSimDbOpen,
      SIGNAL(triggered()),
      this,
      SLOT(openSimScadaDatabase()));

  // ----------
  // SynTEST Database menu

  connect(ui.actSynDbNew,
      SIGNAL(triggered()),
      &MainSynModel::Inst(),
      SLOT(newDatabase()));

  connect(ui.actSynDbOpen,
      SIGNAL(triggered()),
      this,
      SLOT(openSynTestDatabase()));

  connect(ui.actSynDbInitialise,
      SIGNAL(triggered()),
      &MainSynModel::Inst(),
      SLOT(initialise()));

  // ----------
  // Test menu

  connect(ui.actTestNew,
      SIGNAL(triggered()),
      &MainAutoTest::Inst(),
      SLOT(newTest()));

  connect(ui.actTestExecute,
      SIGNAL(triggered()),
      &MainAutoTest::Inst(),
      SLOT(executeTest()));

  connect(ui.actTestResults,
      SIGNAL(triggered()),
      &MainAutoTest::Inst(),
      SLOT(displayTestResults()));

  connect(ui.actTestExport,
      SIGNAL(triggered()),
      &MainAutoTest::Inst(),
      SLOT(exportTestResults()));
}
// ----------------------------------------------------------------------------

void
MainUi::openSynTestDatabase()
{
  QString fileName(Settings().synTestModelFileName().c_str());
  QString filePath = QDir(fileName).path();

  QFileDialog dlg(MsgBoxParent());
  dlg.setFilter(gSsOpenDbFilter.c_str());
  dlg.setWindowTitle("Open SynTEST Database");
  dlg.setConfirmOverwrite(false);
  dlg.setFileMode(QFileDialog::AnyFile);
  dlg.setDirectory(QApplication::applicationDirPath());
  dlg.setViewMode(QFileDialog::List);

  QStringList fileNames;
  if (dlg.exec())
      fileNames = dlg.selectedFiles();

  if (fileNames.isEmpty())
    return;

  fileName = fileNames.front();

  // File does exist
  if (QFile(fileName).exists())
  {
    try
    {
      MainSynModel::Inst().loadOrmModel(fileName.toStdString());
    }
    catch (std::exception& e)
    {
      boost::format info("Failed to open or load SynTEST database.\n%s");
      info % e.what();
      QMessageBox::critical(MsgBoxParent(),
          "Open SynTEST Database Failed",
          info.str().c_str());
    }
    return;
  }

  // File doesn't exist
  QMessageBox::StandardButton btn = QMessageBox::question(MsgBoxParent(),
      "Create SynTEST Databast?",
      "The file selected does not exist.\n"
          "Do you want to create a new SynTEST database?",
      QMessageBox::Yes | QMessageBox::No,
      QMessageBox::Yes);

  if (btn != QMessageBox::Yes)
    return;

  MainSynModel::Inst().newDatabaseWithFile(fileName.toStdString());
}
// ----------------------------------------------------------------------------

void
MainUi::openSimScadaDatabase()
{
  SimScadaSettings settings = Settings().simScada();
  std::string fileName = QFileDialog::getOpenFileName(MsgBoxParent(),
      "Select simSCADA database",
      settings.DbFilename.c_str(),
      gSsOpenDbFilter.c_str()).toStdString();

  if (fileName.empty())
    return;

  try
  {
    MainObjectModel::Inst().loadOrmModel(fileName);
  }
  catch (std::exception& e)
  {
    boost::format info("Failed to open or load simSCADA database.\n%s");
    info % e.what();
    QMessageBox::critical(MsgBoxParent(),
        "Open simSCADA Database Failed",
        info.str().c_str());
  }
}
// ----------------------------------------------------------------------------

void
MainUi::closeEvent(QCloseEvent *)
{
  Main::Inst().mainGuiClosing();
}
// ----------------------------------------------------------------------------
