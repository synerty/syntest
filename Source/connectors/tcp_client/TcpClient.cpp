/*
 * TcpClient.cpp
 *
 *  Created on: Dec 23, 2010
 *      Author: Administrator
 */

#include "TcpClient.h"

#include <QtCore/qglobal.h>

#include <iostream>
#include <stdexcept>

#include <boost/format.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

// For qWarning, etc
#include <QtCore/qglobal.h>

#include "Proactor.h"

#include "TcpClientFeedback.h"

using boost::asio::ip::tcp;
using namespace boost::asio;
using boost::system::error_code;

namespace tcp_client
{
  TcpClient::TcpClient(boost::shared_ptr< TcpClientFeedback > dispatcher,
      boost::shared_ptr< Proactor > proactor,
      const std::string& host,
      const int port) :
      mFeedback(dispatcher), //
      mProactor(proactor), //
      mHost(host), //
      mPort(port), //
      mSocket(proactor->ioService()), //
      mHasBeenShutdown(false), //
      mSendStrand(proactor->ioService())
  {
  }

  TcpClient::~TcpClient()
  {
    qDebug("Destructing %s", __PRETTY_FUNCTION__);
    strandedDisconnect(true);
    qDebug("Exiting %s", __PRETTY_FUNCTION__);
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::connect()
  {
    boost::shared_ptr< TcpClientFeedback > feedback = getFeedback();
    if (feedback.get())
      feedback->connectionStateChanged(connectingState, "");

    mSendStrand.post(boost::bind(&TcpClient::strandedConnect,
        shared_from_this()));
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::strandedConnect()
  {
    boost::shared_ptr< TcpClientFeedback > feedback = getFeedback();

    mHasBeenShutdown = true;
    try
    {
      tcp::endpoint endPoint(ip::address_v4::from_string(mHost), mPort);

      error_code error = error::host_not_found;
      mSocket.connect(endPoint, error);

      if (error)
      {
        try
        {
          mSocket.shutdown(boost::asio::socket_base::shutdown_both);
          mSocket.close();

        }
        catch (...)
        {
        }
        boost::format info("Can not connect to agent\n%s");
        info % error.message();

        if (feedback.get())
        {
          mProactor->ioService().post( //
          boost::bind(&TcpClientFeedback::connectionStateChanged,
              feedback,
              connectionAttemptFailedState,
              info.str()));
        }

        return;
      }

      mHasBeenShutdown = false;
      startRead();

      if (feedback.get())
      {
        mProactor->ioService().post( //
        boost::bind(&TcpClientFeedback::connectionStateChanged,
            feedback,
            connectedState,
            ""));
      }

    }
    catch (std::exception& e)
    {
      boost::format info("An exception occured while connecting to the agentt\n%s");
      info % e.what();

      if (feedback.get())
      {
        mProactor->ioService().post( //
        boost::bind(&TcpClientFeedback::connectionStateChanged,
            feedback,
            connectionAttemptFailedState,
            info.str()));
      }
    }
    // SUCCESS
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::disconnect(bool quite)
  {
    mSendStrand.post(bind(&TcpClient::strandedDisconnect,
        shared_from_this(),
        quite,
        boost::system::error_code()));
  }
// ----------------------------------------------------------------------------

  void
  TcpClient::strandedDisconnect(const bool quite,
      const boost::system::error_code err)
  {
    if (mHasBeenShutdown)
      return;

    mHasBeenShutdown = true;

    boost::shared_ptr< TcpClientFeedback > feedback = getFeedback();

    if (mSocket.is_open())
    {
      try
      {
        mSocket.shutdown(boost::asio::socket_base::shutdown_both);
        mSocket.close();
      }
      catch (...)
      {
      }
    }

    if (quite || !feedback.get())
      return;

    if (!err)
    {
      qDebug("Disconnected from agent, %s", err.message().c_str());
      mProactor->ioService().post( //
      boost::bind(&TcpClientFeedback::connectionStateChanged,
          feedback,
          disconnectedState,
          ""));
      return;
    }

    if (err == boost::asio::error::eof)
      qCritical("Connection to the agent broken, %s", err.message().c_str());
    else
      qWarning("Connection to the agent closed, %s", err.message().c_str());

    mProactor->ioService().post( //
    boost::bind(&TcpClientFeedback::connectionStateChanged,
        feedback,
        connectionBrokenState,
        err.message()));

  }
// ----------------------------------------------------------------------------

  void
  TcpClient::startRead()
  {
    if (mHasBeenShutdown)
      return;

    async_read_until(mSocket,
        mInBuffer,
        '\0',
        boost::bind(&TcpClient::readComplete,
            shared_from_this(),
            placeholders::error,
            placeholders::bytes_transferred));
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::readComplete(const error_code& error, size_t bytes_transferred)
  {
    if (mHasBeenShutdown)
      return;

    boost::shared_ptr< TcpClientFeedback > feedback = getFeedback();

    if (!isOk(error))
      return;

    if (bytes_transferred == 0)
    {
      startRead();
      return;
    }

    std::istream is(&mInBuffer);
    std::istreambuf_iterator< char > lItr(is.rdbuf());
    std::istreambuf_iterator< char > lItrEnd;
    std::string data(lItr, lItrEnd);
    mInString += data;

    // This should NEVER happen
    if (mInString.empty() || mInString[mInString.size() - 1] != '\0')
    {
      startRead();
      return;
    }

    // Tokenise our message
    std::string msg;
    for (std::string::const_iterator itr = mInString.begin(); //
    itr != mInString.end(); //
        ++itr)
    {
      msg.push_back(*itr);
      if (*itr == '\0')
      {
        qDebug("Recieved : |%s|", msg.c_str());

        if (!mHasBeenShutdown && feedback.get())
        {
          mProactor->ioService().post(bind(&TcpClientFeedback::messageRecieved,
              feedback,
              msg));
        }

        msg.clear();
      }
    }

    mInString.clear();
    startRead();
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::send(const std::string& msg)
  {
    if (mHasBeenShutdown)
      return;

    mProactor->ioService().post(mSendStrand.wrap(bind(&TcpClient::sendImpl,
        shared_from_this(),
        msg)));
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::sendImpl(const std::string& msg)
  {
    bool writeInProgress = !mSendQue.empty();

    mSendQue.push_back(msg + '\0');

    if (!writeInProgress)
      startWrite();
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::startWrite()
  {
    if (mHasBeenShutdown)
      return;

    mProactor->ioService().post(mSendStrand.wrap(bind(&TcpClient::startWriteImpl,
        shared_from_this())));
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::startWriteImpl()
  {
    if (mHasBeenShutdown || mSendQue.empty())
      return;

    async_write(mSocket,
        buffer(mSendQue.front()),
        mSendStrand.wrap(boost::bind(&TcpClient::writeComplete,
            shared_from_this(),
            placeholders::error,
            placeholders::bytes_transferred)));
  }
  // ----------------------------------------------------------------------------

  void
  TcpClient::writeComplete(const error_code& err, size_t /*bytes_transferred*/)
  {
    if (!isOk(err))
      return;

    // Disabled due to shear volumen of downloaded data to agent :-(
    // qDebug("Write complete, |%s|", mSendQue.front().c_str());

    mSendQue.pop_front();

    if (!mSendQue.empty())
      startWrite();
  }
  // ----------------------------------------------------------------------------

  bool
  TcpClient::isOk(const error_code& err)
  {
    if (err)
    {
      mSendStrand.post(bind(&TcpClient::strandedDisconnect,
          shared_from_this(),
          false,
          err));

      return false;
    }

    return true;
  }
  // ----------------------------------------------------------------------------

  boost::shared_ptr< TcpClientFeedback >
  TcpClient::getFeedback()
  {
    boost::shared_ptr< TcpClientFeedback > feedback = mFeedback.lock();
    return feedback;
  }
// ----------------------------------------------------------------------------
}
