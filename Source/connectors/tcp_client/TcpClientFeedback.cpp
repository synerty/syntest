/*
 * TcpClientFeedback.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "TcpClientFeedback.h"

#include <QtCore/qglobal.h>

namespace tcp_client
{

  TcpClientFeedback::TcpClientFeedback()
  {
  }

  TcpClientFeedback::~TcpClientFeedback()
  {
    qDebug("Destructing %s", __PRETTY_FUNCTION__);
  }

} /* namespace tcp_client */
