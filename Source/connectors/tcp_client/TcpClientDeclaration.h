/*
 * TcpClientDeclaration.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef TCPCLIENTDECLARATION_H_
#define TCPCLIENTDECLARATION_H_

namespace tcp_client {
  // ---------------
  // Tcp Client declarations

  enum ConnectionStateE
  {
    //! Attempting to connect
    connectingState,

    //! Connected
    connectedState,

    //! Failed to connect
    connectionAttemptFailedState,

    //! DisconnectedS
    disconnectedState,

    //! Disconnected because of an error
    connectionBrokenState
  };

};


#endif /* TCPCLIENTDECLARATION_H_ */
