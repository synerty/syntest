/*
 * TcpClient.h
 *
 *  Created on: Dec 23, 2010
 *      Author: Administrator
 */

#ifndef _TCP_CLIENT_H_
#define _TCP_CLIENT_H_

#include <string>
#include <deque>

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>

#include "Macros.h"

class Proactor;

namespace tcp_client
{
  class TcpClientFeedback;

  /*! TcpClient Brief Description
   * Long comment about the class
   *
   */
  class TcpClient : public boost::enable_shared_from_this< TcpClient >
      DEBUG_CLASS_MIXIN(TcpClient)
  {

    public:
      TcpClient(boost::shared_ptr< TcpClientFeedback > dispatcher,
          boost::shared_ptr< Proactor > proactor,
          const std::string& host,
          const int port);

      virtual
      ~TcpClient();

    public:

      void
      setDispatcher();

      void
      connect();

      void
      disconnect(bool quite = false);

    public:

      void
      send(const std::string& msg);

    private:

      void
      strandedConnect();

      void
      strandedDisconnect(const bool quite,
          const boost::system::error_code err = boost::system::error_code());

      void
      startRead();

      void
      readComplete(const boost::system::error_code& error,
          size_t bytes_transferred);

      void
      sendImpl(const std::string& msg);

      void
      startWrite();

      void
      startWriteImpl();

      void
      writeComplete(const boost::system::error_code& error,
          size_t /*bytes_transferred*/);

      bool
      isOk(const boost::system::error_code& error);

      boost::shared_ptr< TcpClientFeedback >
      getFeedback();

      boost::weak_ptr< TcpClientFeedback > mFeedback;
      boost::shared_ptr< Proactor > mProactor;
      const std::string mHost;
      const int mPort;

      boost::asio::ip::tcp::socket mSocket;
      bool mHasBeenShutdown;

      boost::asio::streambuf mInBuffer;
      std::string mInString;

      std::deque< std::string > mSendQue;
      boost::asio::strand mSendStrand;
  };
}

#endif /* _TCP_CLIENT_H_ */
