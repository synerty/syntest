/*
 * TcpClientFeedback.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef TCPCLIENTFEEDBACK_H_
#define TCPCLIENTFEEDBACK_H_

#include <string>

#include <boost/shared_ptr.hpp>

#include "TcpClientDeclaration.h"

#include "Macros.h"

namespace tcp_client
{
  /*! TcpClientFeedback Brief Description
   * Long comment about the class
   *
   */
  class TcpClientFeedback;
  typedef boost::shared_ptr< TcpClientFeedback > TcpClientFeedbackPtrT;

  class TcpClientFeedback //
  DEBUG_CLASS_BASE(TcpClientFeedback)
  {
    public:

      TcpClientFeedback();

      virtual
      ~TcpClientFeedback();

      virtual void
      messageRecieved(std::string message) = 0;

      virtual void
      connectionStateChanged(tcp_client::ConnectionStateE state,
          std::string errorMessage) = 0;
  };

} /* namespace tcp_client */
#endif /* TCPCLIENTFEEDBACK_H_ */
