/*
 * ProtocolData.h
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#ifndef _SIM_DLL_DATA_H_
#define _SIM_DLL_DATA_H_

#include <string>
#include <vector>
#include <map>
#include <stdint.h>

#include <boost/config/warning_disable.hpp>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/variant.hpp>

#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>

#include <boost/spirit/include/qi_symbols.hpp>
#include <boost/spirit/include/karma.hpp>

#include "SimDllDeclaration.h"

namespace sim_dll
{
  // ---------------
  // Dig info struct

  struct DigStateReadSym : public boost::spirit::qi::symbols<const char,
      DigStateE>
  {
      DigStateReadSym();
  };

  struct DigStateWriteSym : public boost::spirit::karma::symbols<DigStateE,
      const char*>
  {
      DigStateWriteSym();
  };
}

namespace sim_dll
{
  namespace Client
  {
    struct MsgTypeReadSym : public boost::spirit::qi::symbols<const char,
        MsgTypeE>
    {
        MsgTypeReadSym();
    };

    struct MsgTypeWriteSym : public boost::spirit::karma::symbols<MsgTypeE,
        const char>
    {
        MsgTypeWriteSym();
    };

    typedef boost::variant<double, int, std::string>
        MsgDataVariantT;

  }
}

// Adapt the struts to become first class fusion tuples.
// This means spirit::qi can insert the data directly into theses structs
// more or less.
BOOST_FUSION_ADAPT_STRUCT(
    sim_dll::Client::Msg,
    (sim_dll::Client::MsgTypeE, msgType)
    (std::string, alias)
    (sim_dll::Client::MsgDataVariantT, data)
)

namespace sim_dll
{
  namespace Server
  {
    struct MsgTypeReadSym : public boost::spirit::qi::symbols<const char,
        MsgTypeE>
    {
        MsgTypeReadSym();
    };

    struct MsgTypeWriteSym : public boost::spirit::karma::symbols<MsgTypeE,
        const char>
    {
        MsgTypeWriteSym();
    };

    struct CtrlTypeReadSym : public boost::spirit::qi::symbols<const char,
        CtrlTypeE>
    {
        CtrlTypeReadSym();
    };

    struct CtrlTypeWriteSym : public boost::spirit::karma::symbols<CtrlTypeE,
        const char*>
    {
        CtrlTypeWriteSym();
    };

  }

}

// Adapt the struts to become first class fusion tuples.
// This means spirit::qi can insert the data directly into theses structs
// more or less.
BOOST_FUSION_ADAPT_STRUCT(
    sim_dll::Server::DigPointInfo,
    (std::string, textA)
    (std::string, textB)
    (std::string, textC)
    (std::string, textD)
    (uint8_t, rawA)
    (uint8_t, rawB)
    (uint8_t, rawC)
    (uint8_t, rawD)
    (std::string, deviceType)
    (sim_dll::DigStateE, value)
)

BOOST_FUSION_ADAPT_STRUCT(
    sim_dll::Server::AnaPointInfo,
    (double, value)
    (double, rawValue)
    (double, defaultValue)
)

BOOST_FUSION_ADAPT_STRUCT(
    sim_dll::Server::CtrlPointInfo,
    (std::string, scanAlias)
    (sim_dll::Server::CtrlTypeE, type)
    (double, indSetPoint)
    (sim_dll::DigStateE, indStateTo)
    (double, indDelta)
    (uint8_t, protocolType)
    (uint32_t, protocolVal)
)

BOOST_FUSION_ADAPT_STRUCT(
    sim_dll::Server::Msg,
    (sim_dll::Server::MsgTypeE, msgType)
    (std::string, alias)
    (sim_dll::Server::PointInfoVariantT, pointInfo)
)

#endif /* _SIM_DLL_DATA_H_ */
