/*
 * SimDllProtocol.h
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#ifndef _SIM_DLL_DISPATCHER_H_
#define _SIM_DLL_DISPATCHER_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/strand.hpp>

#include <QtCore/qstring.h>

#include <simscada/DbSimscadaDeclaration.h>

#include "SimDllDeclaration.h"
#include "TcpClientFeedback.h"
#include "TcpClientDeclaration.h"
#include "Observable.h"
#include "Macros.h"

class Proactor;

namespace tcp_client
{
  class TcpClient;
}

namespace sim_dll
{

  class SimDllDispatcherObserver;

  /*! SimDllProtocol Brief Description
   * Long comment about the class
   *
   */
  class SimDllDispatcher : public boost::enable_shared_from_this<
      SimDllDispatcher >,
      public Observable< SimDllDispatcherObserver > ,
      public tcp_client::TcpClientFeedback
      DEBUG_CLASS_MIXIN(SimDllDispatcher)
  {

    public:
      SimDllDispatcher(boost::shared_ptr< Proactor > proactor,
          const std::string& host,
          const int port);

      virtual
      ~SimDllDispatcher();

    public:
      // ---------------
      // Interface used by MainSim
      // Functions to query simSCADA

      void
      requestPointInfo(db_simscada::sim::ObjectPtrT object);

      void
      registerForUpdates(db_simscada::sim::ObjectPtrT object);

      void
      deregisterForUpdates(const std::string alias);

    public:
      // ---------------
      // Interface used by MainSim
      // Functions to control simSCADA

      void
      setAnaRawValue(db_simscada::sim::ObjectPtrT object, double value);

      void
      setAnaEngValue(db_simscada::sim::ObjectPtrT object, double value);

      void
          setDigState(db_simscada::sim::ObjectPtrT object,
              sim_dll::DigStateE state);

    public:
      /// ---------------
      /// Public interface used by all

      void
      postedConnect();

      void
      postedDisconnect();

      bool
      isConnected();

    private:
      /// ---------------
      /// Observerable Stuff

      void
      notifyOfObjectUpdateReceived(Server::Msg msg);

      void
      notifyOfConnectionStateChanged(tcp_client::ConnectionStateE newState,
          std::string errorMessage);

      void
      notifyOfSimulatorDisconnected();


    private:
      // ---------------
      // AgentClientFeedback Interface impliementation

      void
      messageRecieved(std::string message);

      void
      connectionStateChanged(tcp_client::ConnectionStateE,
          std::string errorMessage);

    private:

      void
      strandedDisconnect();

      void
      sendMessage(sim_dll::Client::Msg& msg);

      void
      strandedSendMessage(sim_dll::Client::Msg& msg);

    private:

      boost::shared_ptr< tcp_client::TcpClient > mTcpClient;

      boost::shared_ptr< Proactor > mProactor;
      boost::asio::strand mStrand;
      const std::string mHost;
      const int mPort;

  };

} /* namespace sim_dll */

#endif /* _SIM_DLL_DISPATCHER_H_ */
