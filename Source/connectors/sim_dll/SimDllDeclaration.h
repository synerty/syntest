/*
 * ProtocolData.h
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#ifndef _SIM_DLL_DECLARATION_H_
#define _SIM_DLL_DECLARATION_H_

#include <string>
#include <vector>
#include <stdint.h>

#include <boost/variant/recursive_variant.hpp>
#include <boost/variant.hpp>

namespace sim_dll
{

  // ---------------
  // Dig info struct

  enum DigStateE
  {
    A,
    B,
    C,
    D,
    DigStateUnset
  };

  namespace Client
  {
    enum MsgTypeE
    {
      RegPoint = 'A',
      DeregPoint,
      QryState,
      SetAnaRawValue,
      SetAnaEngValue,
      SetDigState
    };

    typedef boost::variant< double, int, std::string > MsgDataVariantT;

    struct Msg
    {
        MsgTypeE msgType;
        std::string alias;
        MsgDataVariantT data;
    };
  }

  namespace Server
  {

    enum MsgTypeE
    {
      DigUpdate = 'a',
      AnaUpdate,
      AccUpdate,
      StrUpdate,
      CtrlUpdate,
      DigSetValAckMsg,
      AnaSetValAckMsg,
      AccSetValAckMsg,
      StrSetValAckMsg,
      RegPointFailed,
      RegPointSucceeded,
    };

    // ---------------
    // Dig info struct

    struct DigPointInfo
    {
        std::string textA;
        std::string textB;
        std::string textC;
        std::string textD;

        uint8_t rawA;
        uint8_t rawB;
        uint8_t rawC;
        uint8_t rawD;

        std::string deviceType;

        DigStateE value;
    };

    // ---------------
    // Ana info struct

    struct AnaPointInfo
    {
        double value;
        double rawValue;
        double defaultValue;
    };

    // ---------------
    // Acc info struct

    struct AccPointInfo
    {
        double value;
        double rawValue;
        double defaultValue;
    };

    // ---------------
    // Control info struct

    // Control notification support structures
    enum CtrlTypeE
    {
      CtrlState = 0,
      CtrlSetpoint = 1,
      CtrlDeltaup = 2,
      CtrlDeltadown = 3,
      CtrlUnknown,
      CtrlInvalid = 0xFF
    };

    struct CtrlPointInfo
    {
        std::string scanAlias; // The Alias of the  back indication.
        CtrlTypeE type; // The type of control

        // the following arethe values that the control wants to send the back
        // indication to, depending upon the type of control.
        double indSetPoint; // Used for TeControlType == CTRL_SETPOINT
        DigStateE indStateTo; // Used for TeControlType == CTRL_STATE
        double indDelta; // Used for TeControlType == CTRL_DELTAUP or CTRL_DELTADOWN

        // Used for protocol specific reasons. See scripting user manual details on
        // ss_RegisterOnControlExecute for use.
        uint8_t protocolType;
        uint32_t protocolVal;
    };

    // ---------------
    // Variant for the info sturcts

    typedef boost::variant< bool, DigPointInfo, AnaPointInfo, CtrlPointInfo > PointInfoVariantT;

    struct Msg
    {
        MsgTypeE msgType;
        std::string alias;
        PointInfoVariantT pointInfo;
    };
  }
}
#endif /* _SIM_DLL_DECLARATION_H_ */
