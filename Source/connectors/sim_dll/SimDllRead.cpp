/*
 * ProtocolRead.cpp
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

//#define BOOST_SPIRIT_DEBUG
#include "SimDllRead.h"

#include <boost/spirit/include/qi_char.hpp>

namespace sim_dll
{
  namespace ascii = boost::spirit::ascii;

  // ==========================================================================
  namespace Client
  {
    Read::Read() :
        Read::base_type(mRulStart)
    {

      mRulEnd = ascii::char_('\x02'); // Client record start and end
      mRulDelim = ascii::char_('\x03'); // Record separator

      mRulStrVal %= *(ascii::char_ - mRulDelim) >> mRulDelim;

      mRulDblVal %= qi::real_parser< double, qi::strict_real_policies< double > >()
          >> mRulDelim;

      mRulIntVal %= qi::int_ >> mRulDelim;

      mRulData %= mRulDblVal | mRulIntVal | mRulStrVal;

      mRulType %= mSymType >> mRulDelim;

      mRulStart %= mRulEnd // Start of text
      >> mRulType //
      >> mRulStrVal //
      >> mRulData //
      >> mRulEnd // End of text
          ;

    }
  // ------------------------------------------------------------------------
  }
  // ==========================================================================
  namespace Server
  {
    Read::Read() :
        Read::base_type(mRulStart)
    {
      // Common
      mRulEnd = ascii::char_('\x01'); // Server record start and end
      mRulDelim = ascii::char_('\x03'); // Record separator

      // Message type
      mRulType %= mSymType >> mRulDelim;

      // Generic string
      mRulStr %= *(ascii::char_ - mRulDelim) >> mRulDelim;

      // Generic bool
      mRulBool %= qi::bool_ >> mRulDelim;

      // DigInfo stuct
      mRulDigInfo %= mRulStr // text A
      >> mRulStr // text B
      >> mRulStr // text C
      >> mRulStr // text D
      >> qi::int_ >> mRulDelim // rawA
      >> qi::int_ >> mRulDelim // rawB
      >> qi::int_ >> mRulDelim // rawC
      >> qi::int_ >> mRulDelim // rawD
      >> mRulStr // deviceType
      >> mSymDigState >> mRulDelim // Value
          ;

      // AnaInfo stuct
      mRulAnaInfo %= //
      qi::double_ >> mRulDelim // value
      >> qi::double_ >> mRulDelim // rawValue
      >> qi::double_ >> mRulDelim // defaultValue
          ;

      // CtrlInfo stuct
      mRulCtrlInfo %= //
      mRulStr // scanAlias
      >> mSymCtrlType >> mRulDelim // type
      >> qi::double_ >> mRulDelim // indSetPoint
      >> mSymDigState >> mRulDelim // indStateTo
      >> qi::double_ >> mRulDelim // indDelta
      >> qi::ushort_ >> mRulDelim // protocolType
      >> qi::uint_ >> mRulDelim // protocolVal
          ;

      // Variant for the different info structs
      mRulVariantInfoStruct %= //
      mRulBool // bool
      | mRulDigInfo // DigPointInfo
      | mRulAnaInfo // AnaPointInfo
      | mRulCtrlInfo // mRulCtrlInfo
          ;

      // Start rule
      mRulStart %= mRulEnd // Start of text
      >> mRulType // Message Type
      >> mRulStr // Alias
      >> mRulVariantInfoStruct //(_a) // Point info structs
      >> mRulEnd // End of text
          ;

    }
  // ------------------------------------------------------------------------
  }
// ============================================================================
}

