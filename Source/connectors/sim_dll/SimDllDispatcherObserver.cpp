/*
 * SimDllDispatcherObserver.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "SimDllDispatcherObserver.h"

namespace sim_dll
{
  SimDllDispatcherObserver::~SimDllDispatcherObserver()
  {
  }
  // ------------------------------------------------------------------------

  void
  SimDllDispatcherObserver::postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
      std::string errorMessage)
  {
  }
  // ------------------------------------------------------------------------

  void
  SimDllDispatcherObserver::postedSimDllObjectUpdateRecieved(Server::Msg message)
  {
  }
// ------------------------------------------------------------------------

  void
  SimDllDispatcherObserver::postedSimulatorDisconnected()
  {
  }
// ------------------------------------------------------------------------

} /* namespace agent */
