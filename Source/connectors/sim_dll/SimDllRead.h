/*
 * ProtocolRead.h
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#ifndef _SIM_DLL_READ_H_
#define _SIM_DLL_READ_H_

#include <string>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
//#include <boost/spirit/include/support_multi_pass.hpp>

#include <string>

#include "SimDllData.h"

namespace sim_dll
{
  namespace qi = boost::spirit::qi;
  typedef std::string::const_iterator TdParserIterator;

//  typedef std::string::const_iterator TdParserBaseIterator;
//  typedef boost::spirit::multi_pass<TdParserBaseIterator> TdParserIterator;

// ==========================================================================
  namespace Client
  {

    struct Read : public qi::grammar< TdParserIterator, Msg
    () >
    {
        Read();

      private:
        qi::rule< TdParserIterator, Msg
        () > mRulStart;
        qi::rule< TdParserIterator > mRulEnd;
        qi::rule< TdParserIterator > mRulDelim;

        qi::rule< TdParserIterator, MsgTypeE
        () > mRulType;
        MsgTypeReadSym mSymType;

        qi::rule< TdParserIterator, std::string
        () > mRulStrVal;

        qi::rule< TdParserIterator, double
        () > mRulDblVal;

        qi::rule< TdParserIterator, int
        () > mRulIntVal;

        qi::rule< TdParserIterator, MsgDataVariantT
        () > mRulData;
    };
  }

  // ==========================================================================
  namespace Server
  {

    struct Read : public qi::grammar< TdParserIterator, Msg
    () >
    {
        Read();

      private:
        qi::rule< TdParserIterator, Msg
        () > mRulStart;
        qi::rule< TdParserIterator > mRulEnd;
        qi::rule< TdParserIterator > mRulDelim;

        qi::rule< TdParserIterator, MsgTypeE
        () > mRulType;
        MsgTypeReadSym mSymType;

        qi::rule< TdParserIterator, std::string
        () > mRulStr;

        qi::rule< TdParserIterator, bool
        () > mRulBool;

        // --- Point Info Struct stuff ---
        qi::rule< TdParserIterator, PointInfoVariantT
        () > mRulVariantInfoStruct;

        // --- Dig Info Struct stuff ---
        qi::rule< TdParserIterator, DigPointInfo
        () > mRulDigInfo;
        sim_dll::DigStateReadSym mSymDigState;

        // --- Ana Info Struct stuff ---
        qi::rule< TdParserIterator, AnaPointInfo
        () > mRulAnaInfo;

        // --- Ctrl Info Struct stuff ---
        qi::rule< TdParserIterator, CtrlPointInfo
        () > mRulCtrlInfo;
        sim_dll::Server::CtrlTypeReadSym mSymCtrlType;
    };

  }
// ==========================================================================
}

#endif /* _SIM_DLL_READ_H_ */
