#ifndef _SIM_DLL_WRITE_H_
#define _SIM_DLL_WRITE_H_

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/karma.hpp>

#include "SimDllData.h"

namespace sim_dll
{

  namespace karma = boost::spirit::karma;
  typedef std::back_insert_iterator< std::string > KarmaIteratorT;

  // ==========================================================================
  namespace Client
  {

    struct Write : karma::grammar< KarmaIteratorT, Msg
    () >
    {
        Write();

        karma::rule< KarmaIteratorT, Msg
        () > mRulStart;
        karma::rule< KarmaIteratorT > mRulEnd;
        karma::rule< KarmaIteratorT > mRulDelim;

        karma::rule< KarmaIteratorT, MsgTypeE
        () > mRulType;
        sim_dll::Client::MsgTypeWriteSym mSymType;

        karma::rule< KarmaIteratorT, std::string
        () > mRulStrVal;

        karma::rule< KarmaIteratorT, double
        () > mRulDblVal;

        karma::rule< KarmaIteratorT, int
        () > mRulIntVal;

        karma::rule< KarmaIteratorT, MsgDataVariantT
        () > mRulData;
    };

  }

  // ==========================================================================
  namespace Server
  {

    struct Write : karma::grammar< KarmaIteratorT/*, karma::locals<MsgTypeE>*/,
        Msg
        () >
    {
        Write();

        karma::rule< KarmaIteratorT/*, karma::locals<MsgTypeE>*/, Msg
        () > mRulStart;
        karma::rule< KarmaIteratorT > mRulEnd;
        karma::rule< KarmaIteratorT > mRulDelim;

        karma::rule< KarmaIteratorT, MsgTypeE
        () > mRulType;
        sim_dll::Server::MsgTypeWriteSym mSymType;

        karma::rule< KarmaIteratorT, std::string
        () > mRulStr;

        karma::rule< KarmaIteratorT, bool
        () > mRulBool;

        karma::rule< KarmaIteratorT,/*void(MsgTypeE),*/PointInfoVariantT
        () > mRulVariantInfoStruct;

        // --- Dig Info Struct stuff ---
        karma::rule< KarmaIteratorT, DigPointInfo
        () > mRulDigInfo;
        sim_dll::DigStateWriteSym mSymDigState;

        // --- Ana Info Struct stuff ---
        karma::rule< KarmaIteratorT, AnaPointInfo
        () > mRulAnaInfo;

        // --- Ctrl Info Struct stuff ---
        karma::rule< KarmaIteratorT, CtrlPointInfo
        () > mRulCtrlInfo;
        sim_dll::Server::CtrlTypeWriteSym mSymCtrlType;

    };

  }
// ==========================================================================
}
#endif /* _SIM_DLL_WRITE_H_ */
