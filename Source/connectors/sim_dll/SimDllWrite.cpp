/*
 * SimDllProtocolWrite.cpp
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#include "SimDllWrite.h"

namespace sim_dll
{
  namespace ascii = boost::spirit::ascii;
  //  using namespace karma::labels;

  // ==========================================================================
  namespace Client
  {
    Write::Write() :
        Write::base_type(mRulStart)
    {

      mRulEnd = ascii::char_('\x02'); // Client record start and end
      mRulDelim = ascii::char_('\x03'); // Record separator

      mRulType = mSymType << mRulDelim;

      mRulStrVal = *ascii::char_ << mRulDelim;

      mRulDblVal = karma::double_ << mRulDelim;

      mRulIntVal = karma::int_ << mRulDelim;

      mRulData = mRulDblVal | mRulIntVal | mRulStrVal;

      mRulStart = mRulEnd // Start of text
      << mRulType // msg type
          << mRulStrVal // alias
          << mRulData // data
          << mRulEnd // End of text
          ;
    }
  }
  // ==========================================================================
  namespace Server
  {
    Write::Write() :
        Write::base_type(mRulStart)
    {
      // Common
      mRulEnd = ascii::char_('\x01'); // Server record start and end
      mRulDelim = ascii::char_('\x03'); // Record separator

      // Message type
      mRulType = mSymType << mRulDelim;

      // Generic string
      mRulStr = *ascii::char_ << mRulDelim;

      // Generic bool
      mRulBool = karma::bool_ << mRulDelim;

      // DigInfo stuct
      mRulDigInfo = mRulStr // text A
      << mRulStr // text B
          << mRulStr // text C
          << mRulStr // text D
          << karma::int_ << mRulDelim // rawA
          << karma::int_ << mRulDelim // rawB
          << karma::int_ << mRulDelim // rawC
          << karma::int_ << mRulDelim // rawD
          << mRulStr // deviceType
          << mSymDigState << mRulDelim // Value
          ;

      // AnaInfo stuct
      mRulAnaInfo = //
      karma::double_ << mRulDelim // value
          << karma::double_ << mRulDelim // rawValue
          << karma::double_ << mRulDelim // defaultValue
          ;

      // CtrlInfo stuct
      mRulCtrlInfo = //
      mRulStr // scanAlias
      << mSymCtrlType << mRulDelim // type
          << karma::double_ << mRulDelim // indSetPoint
          << mSymDigState << mRulDelim // indStateTo
          << karma::double_ << mRulDelim // indDelta
          << karma::ushort_ << mRulDelim // protocolType
          << karma::uint_ << mRulDelim // protocolVal
          ;

      // Variant for the different info structs
      mRulVariantInfoStruct = //
      mRulBool // bool
      | mRulDigInfo // DigPointInfo
      | mRulAnaInfo // AnaPointInfo
      | mRulCtrlInfo // CtrlPointInfo
          ;

      // Start rule
      mRulStart = mRulEnd // Start of text
      << mRulType //[_a = _1] // Message Type
          << mRulStr // Alias
          << mRulVariantInfoStruct //(_a) // Point info structs
          << mRulEnd // End of text
          ;
    }
  }
// ==========================================================================
}
