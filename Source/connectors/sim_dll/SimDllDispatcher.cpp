/*
 * SimDllProtocol.cpp
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#include "SimDllDispatcher.h"

#include <boost/spirit/include/karma_generate.hpp>
#include <boost/spirit/include/qi_parse.hpp>

// For qWarning, etc
#include <QtCore/qglobal.h>
#include <QtGui/qmessagebox.h>

#include <simscada/sim/SimObject.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "Globals.h"
#include "TcpClient.h"
#include "SimDllWrite.h"
#include "SimDllRead.h"
#include "SimDllData.h"
#include "SimDllDispatcherObserver.h"
#include "UiMessage.h"

#include "Proactor.h"

// Excplitit instantiation
#include "Observable.ini"
template class Observable< sim_dll::SimDllDispatcherObserver > ;

using namespace db_simscada;
using namespace db_simscada::sim;

namespace sim_dll
{
  SimDllDispatcher::SimDllDispatcher(boost::shared_ptr< Proactor > proactor,
      const std::string& host,
      const int port) :
      mProactor(proactor), mStrand(proactor->ioService()), mHost(host), mPort(port)
  {
  }
  // ----------------------------------------------------------------------------

  SimDllDispatcher::~SimDllDispatcher()
  {
    if (mTcpClient.get())
      mTcpClient->disconnect();
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::requestPointInfo(db_simscada::sim::ObjectPtrT object)
  {
    if (object.get() == NULL)
      return;

    sim_dll::Client::Msg msg;
    msg.msgType = sim_dll::Client::QryState;
    msg.alias = object->alias();
    msg.data = object->objectType();
    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::registerForUpdates(db_simscada::sim::ObjectPtrT object)
  {
    if (object.get() == NULL)
      return;

    sim_dll::Client::Msg msg;
    msg.msgType = sim_dll::Client::RegPoint;
    msg.alias = object->alias();
    msg.data = object->objectType();
    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::deregisterForUpdates(const std::string alias)
  {
    sim_dll::Client::Msg msg;
    msg.msgType = sim_dll::Client::DeregPoint;
    msg.alias = alias;
    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::setAnaRawValue(db_simscada::sim::ObjectPtrT object,
      double value)
  {
    sim_dll::Client::Msg msg;
    msg.msgType = sim_dll::Client::SetAnaRawValue;
    msg.alias = object->alias();
    msg.data = value;
    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::setAnaEngValue(db_simscada::sim::ObjectPtrT object,
      double value)
  {
    sim_dll::Client::Msg msg;
    msg.msgType = sim_dll::Client::SetAnaEngValue;
    msg.alias = object->alias();
    msg.data = value;
    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::setDigState(db_simscada::sim::ObjectPtrT object,
      sim_dll::DigStateE state)
  {
    sim_dll::Client::Msg msg;
    msg.msgType = sim_dll::Client::SetDigState;
    msg.alias = object->alias();
    msg.data = state;
    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::postedConnect()
  {
    if (mTcpClient.get())
      mTcpClient->disconnect(true);

    tcp_client::TcpClientFeedbackPtrT dispatcher = shared_from_this();
    mTcpClient.reset(new tcp_client::TcpClient(dispatcher,
        mProactor,
        mHost,
        mPort));

    mTcpClient->connect();
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::postedDisconnect()
  {
    mStrand.post(bind(&SimDllDispatcher::strandedDisconnect,
        shared_from_this()));
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::strandedDisconnect()
  {
    if (!mTcpClient.get())
      return;

    assert(mTcpClient.get());
    mTcpClient->disconnect();
    mTcpClient.reset();
  }
  // ----------------------------------------------------------------------------

  bool
  SimDllDispatcher::isConnected()
  {
    return (mTcpClient.get() != NULL);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::messageRecieved(std::string message)
  {
    qDebug("Received message |%s|", message.c_str());

    // Its QI time :-)
    sim_dll::Server::Msg msg;
    sim_dll::Server::Read protocolParser;
    std::string::const_iterator itr = message.begin();
    std::string::const_iterator itrEnd = message.end();

    bool ok = boost::spirit::qi::phrase_parse(itr,
        itrEnd,
        protocolParser,
        boost::spirit::ascii::space,
        msg);

    if (!ok)
    {
      qCritical("Failed to parse message from simSCADA");
      QMessageBox::critical(MsgBoxParent(),
          "Comms Error",
          "An error occurred parsing"
              " a message from simSCADA");
      qDebug("Message failed, |%s|", message.c_str());
      return;
    }

    notifyOfObjectUpdateReceived(msg);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::connectionStateChanged(tcp_client::ConnectionStateE newState,
      std::string errorMessage)
  {
    notifyOfConnectionStateChanged(newState, errorMessage);
  }
  // ----------------------------------------------------------------------------

  void
  SimDllDispatcher::notifyOfObjectUpdateReceived(Server::Msg msg)
  {
    qDebug("Entering %s", __PRETTY_FUNCTION__);

    ObserverPtrsT observerPtrs;
    getObserverPtrs(observerPtrs);
    for (ObserverPtrsT::iterator itr = observerPtrs.begin();
        itr != observerPtrs.end(); ++itr)
    {
      mProactor->ioService().post(boost::bind( //
      &SimDllDispatcherObserver::postedSimDllObjectUpdateRecieved,
          *itr,
          msg));
    }

    qDebug("Exiting %s", __PRETTY_FUNCTION__);
  }
  // --------------------------------------------------------------------------

  void
  SimDllDispatcher::notifyOfConnectionStateChanged(tcp_client::ConnectionStateE newState,
      std::string errorMessage)
  {
    ObserverPtrsT observerPtrs;
    getObserverPtrs(observerPtrs);
    for (ObserverPtrsT::iterator itr = observerPtrs.begin();
        itr != observerPtrs.end(); ++itr)
    {
      mProactor->ioService().post(boost::bind( //
      &SimDllDispatcherObserver::postedSimDllConnectionStateChanged,
          *itr,
          newState,
          errorMessage));
    }
  }
  // --------------------------------------------------------------------------

  void
  SimDllDispatcher::notifyOfSimulatorDisconnected()
  {
    ObserverPtrsT observerPtrs;
    getObserverPtrs(observerPtrs);
    for (ObserverPtrsT::iterator itr = observerPtrs.begin();
        itr != observerPtrs.end(); ++itr)
    {
      mProactor->ioService().post(boost::bind( //
      &SimDllDispatcherObserver::postedSimulatorDisconnected,
          *itr));
    }
  }
  // --------------------------------------------------------------------------

  void
  SimDllDispatcher::sendMessage(sim_dll::Client::Msg& msg)
  {
    mStrand.post(bind(&SimDllDispatcher::strandedSendMessage,
        shared_from_this(),
        msg));
  }
  // --------------------------------------------------------------------------

  void
  SimDllDispatcher::strandedSendMessage(sim_dll::Client::Msg& msg)
  {
    if (!mTcpClient.get())
    {
      qDebug("SimDllDispatcher::sendMessage, Simulator is disconnected");
      notifyOfSimulatorDisconnected();
      return;
    }
    // throw std::runtime_error("SimDllDispatcher::sendMessage, mTcpClient not connected");

    std::string str;
    sim_dll::KarmaIteratorT sink(str);
    sim_dll::Client::Write gen;
    bool ok = boost::spirit::karma::generate(sink, // destination: output iterator
        gen, // the generator
        msg // the data to output
        );

    if (!ok)
    {
      qCritical("Failed to generate message to send to simSCADA");
      if (mTcpClient.get())
        mTcpClient->disconnect(true);
      notifyOfConnectionStateChanged(tcp_client::disconnectedState,
          "Comms Error\n"
              "An error occurred generating"
              " message to send to simSCADA");
      return;
    }

    mTcpClient->send(str);
  }
// ----------------------------------------------------------------------------

} /* namespace sim_dll */
