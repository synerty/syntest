/*
 * SimDllDispatcherObserver.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _SIM_DLL_DISPATCHER_OBSERVER_H_
#define _SIM_DLL_DISPATCHER_OBSERVER_H_

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "SimDllDeclaration.h"
#include "TcpClientDeclaration.h"
#include "Macros.h"

namespace sim_dll
{
  class SimDllDispatcherObserver;
  typedef boost::shared_ptr< SimDllDispatcherObserver > SimDllDispatcherObserverPtrT;
  typedef boost::weak_ptr< SimDllDispatcherObserver > SimDllDispatcherObserverWptrT;

  /*! SimDllDispatcher Observer
   * Classes implementing this will be informed of changes in test processing
   */
  class SimDllDispatcherObserver : DebugClassMixin< SimDllDispatcherObserver >
  {
    public:
      virtual
      ~SimDllDispatcherObserver();

    public:
      virtual void
      postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
          std::string errorMessage);

      virtual void
      postedSimDllObjectUpdateRecieved(Server::Msg message);

      virtual void
      postedSimulatorDisconnected();

  };

} /* namespace sim_dll */
#endif /* _SIM_DLL_DISPATCHER_OBSERVER_H_ */
