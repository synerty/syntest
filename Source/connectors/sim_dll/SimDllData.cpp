/*
 * ProtocolData.cpp
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#include "SimDllData.h"

sim_dll::DigStateReadSym::DigStateReadSym()
{
  add("DA", A) //
  ("DB", B) //
  ("DC", C) //
  ("DD", D) //
  ("DU", DigStateUnset) //
      ;
}

sim_dll::DigStateWriteSym::DigStateWriteSym()
{
  add //
  (A, "DA") //
  (B, "DB") //
  (C, "DC") //
  (D, "DD") //
  (DigStateUnset, "DU") //
      ;
}

sim_dll::Client::MsgTypeReadSym::MsgTypeReadSym()
{
  add("A", RegPoint) //
  ("B", DeregPoint) //
  ("C", QryState) //
  ("D", SetAnaRawValue) //
  ("E", SetAnaEngValue) //
  ("F", SetDigState) //
      ;
}

sim_dll::Client::MsgTypeWriteSym::MsgTypeWriteSym()
{
  add //
  (RegPoint, 'A') //
  (DeregPoint, 'B') //
  (QryState, 'C') //
  (SetAnaRawValue, 'D') //
  (SetAnaEngValue, 'E') //
  (SetDigState, 'F') //
      ;
}

sim_dll::Server::MsgTypeReadSym::MsgTypeReadSym()
{
  add("a", DigUpdate) //
  ("b", AnaUpdate) //
  ("c", AccUpdate) //
  ("d", StrUpdate) //
  ("e", CtrlUpdate) //
  ("f", DigSetValAckMsg) //
  ("g", AnaSetValAckMsg) //
  ("h", AccSetValAckMsg) //
  ("i", StrSetValAckMsg) //
  ("j", RegPointFailed) //
  ("k", RegPointSucceeded) //
      ;
}

sim_dll::Server::MsgTypeWriteSym::MsgTypeWriteSym()
{
  add //
  (DigUpdate, 'a') //
  (AnaUpdate, 'b') //
  (AccUpdate, 'c') //
  (StrUpdate, 'd') //
  (CtrlUpdate, 'e') //
  (DigSetValAckMsg, 'f') //
  (AnaSetValAckMsg, 'g') //
  (AccSetValAckMsg, 'h') //
  (StrSetValAckMsg, 'i') //
  (RegPointFailed, 'j') //
  (RegPointSucceeded, 'k') //
      ;
}

sim_dll::Server::CtrlTypeReadSym::CtrlTypeReadSym()
{
  add("DA", CtrlState) //
  ("DB", CtrlSetpoint) //
  ("DC", CtrlDeltaup) //
  ("DD", CtrlDeltadown) //
  ("DE", CtrlUnknown) //
  ("DF", CtrlInvalid) //
      ;
}

sim_dll::Server::CtrlTypeWriteSym::CtrlTypeWriteSym()
{
  add //
  (CtrlState, "DA") //
  (CtrlSetpoint, "DB") //
  (CtrlDeltaup, "DC") //
  (CtrlDeltadown, "DD") //
  (CtrlUnknown, "DE") //
  (CtrlInvalid, "DF") //
      ;
}
