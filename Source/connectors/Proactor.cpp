/*
 * Proactor.cpp
 *
 *  Created on: Dec 23, 2010
 *      Author: Administrator
 */

#include "Proactor.h"

#include <boost/asio.hpp>

using namespace boost::asio;

Proactor::Proactor() :
    mIsRunning(false)
{
}

Proactor::~Proactor()
{
}
// ----------------------------------------------------------------------------

void
Proactor::run()
{
  mWork.reset(new boost::asio::io_service::work(mIoService));
  mThreadPool.reset(new boost::thread_group());

  // Start the threads
  for (int i = 0; i < THREAD_COUNT; i++)
  {
    mThreadPool->create_thread(boost::bind(&boost::asio::io_service::run,
        &mIoService));
  }

  mIsRunning = true;

}
// ----------------------------------------------------------------------------

void
Proactor::stop()
{
  mIsRunning = false;
  mWork.reset();
  mIoService.stop();
  mThreadPool->join_all();
  mThreadPool.reset();
  mIoService.reset();
}
// ----------------------------------------------------------------------------

void
Proactor::terminate()
{
  mIsRunning = false;
  mWork.reset();
  mIoService.stop();
  mThreadPool->interrupt_all();
  mThreadPool.reset();
  mIoService.reset();
}
// ----------------------------------------------------------------------------

boost::asio::io_service&
Proactor::ioService()
{
  return mIoService;
}
// ----------------------------------------------------------------------------

bool
Proactor::isRunning()
{
  return mIsRunning;
}
// ----------------------------------------------------------------------------
