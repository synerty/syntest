/*
 * AgentRead.h
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#ifndef _AGENT_READ_H_
#define _AGENT_READ_H_

#include <string>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
//#include <boost/spirit/include/support_multi_pass.hpp>

#include <string>

#include "AgentData.h"

namespace agent
{
  namespace qi = boost::spirit::qi;
  typedef std::string::const_iterator TdParserIterator;

  //  typedef std::string::const_iterator TdParserBaseIterator;
  //  typedef boost::spirit::multi_pass<TdParserBaseIterator> TdParserIterator;

  // ==========================================================================
  namespace Client
  {

    struct Read : public qi::grammar< TdParserIterator, Msg
    () >
    {
        Read();

      private:
        qi::rule< TdParserIterator, Msg
        () > mRulStart;
        qi::rule< TdParserIterator > mRulEnd;
        qi::rule< TdParserIterator > mRulDelim;

        qi::rule< TdParserIterator, MsgTypeE
        () > mRulType;
        MsgTypeReadSym mSymType;

        qi::rule< TdParserIterator, std::string
        () > mRulStrVal;

        qi::rule< TdParserIterator, double
        () > mRulDblVal;

        qi::rule< TdParserIterator, int
        () > mRulIntVal;

        qi::rule< TdParserIterator, int
        () > mRulBoolVal;
        BoolReadSym mSymBool;

        qi::rule< TdParserIterator, MsgDataT
        () > mRulData;

        // Generic status message data
        qi::rule< TdParserIterator, GenericStatusMsgData
        () > mRulGenericStatusMsgData;

        // Monitoring Started message data
        qi::rule< TdParserIterator, MonitoringStartedMsgData
        () > mRulMonitoringStartedMsgData;

        // Digitial Indication Changed message data
        qi::rule< TdParserIterator, DiChangedMsgData
        () > mRulDiChangedMsgData;

        // Analog Indication Changed message data
        qi::rule< TdParserIterator, AiChangedMsgData
        () > mRulAiChangedMsgData;

        // Control has been executed message data
        qi::rule< TdParserIterator, ControlExecutedMsgData
        () > mRulControlExecutedMsgData;
    };
  }
// ==========================================================================
}

#endif /* _AGENT_READ_H_ */
