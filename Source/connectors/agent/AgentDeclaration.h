/*
 * AgentDeclaration.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AGENT_DECLARATIONS_H_
#define _AGENT_DECLARATIONS_H_

#include <string>
#include <vector>
#include <map>
#include <stdint.h>

#include <boost/variant/recursive_variant.hpp>
#include <boost/variant.hpp>

#include <syntest/DbSyntestDeclaration.h>

#include "TcpClientDeclaration.h"

namespace agent
{
  enum ConnectionStateE
  {
    //! Attempting to connect
    connectingAgentState = tcp_client::connectingState,

    //! Connected
    connectedAgentState = tcp_client::connectedState,

    //! Failed to connect
    connectionAttemptFailedAgentState = tcp_client::connectionAttemptFailedState,

    //! DisconnectedS
    disconnectedAgentState = tcp_client::disconnectedState,

    //! Disconnected because of an error
    connectionBrokenAgentState = tcp_client::connectionBrokenState,

    //! Downloading data to agent
    downloadAgentState,

  };

  // ---------------
  // Generic status message data

  struct GenericStatusMsgData
  {
      bool value;
  };

  namespace Client
  {

    enum MsgTypeE
    {
      downloadInProgressMsgType,
      downloadCompleteMsgType,
      monitoringStartedMsgType,
      diChangedMsgType,
      aiChangedMsgType,
      acChangedMsgType,
      controlExecutedMsgType
    };

    // ---------------
    // Download in progress message data
    // Indicates that the agent is still alive and downloading

    // Use GenericStatusMsgData

    // ---------------
    // Download in progress message data
    // Indicates that the agent is still alive and downloading

    // Use GenericStatusMsgData

    // ---------------
    // Monitor Change Registered message data
    // Indicates that the request to monitor the change was su

    struct MonitoringStartedMsgData
    {
        std::string id;
        bool success;
        int timeout;
    };

    // ---------------
    // Digitial Indication Changed message data

    struct DiChangedMsgData
    {
        std::string id;
        int value;
        std::string state;
    };

    // ---------------
    // Analog Indication Changed message data

    struct AiChangedMsgData
    {
        std::string id;
        double value;
    };

    // ---------------
    // Control has been executed message data

    struct ControlExecutedMsgData
    {
        std::string id;
        double value;
        bool success;
    };

    // ---------------
    // Variant for the info sturcts

    typedef boost::variant< GenericStatusMsgData, MonitoringStartedMsgData,
        DiChangedMsgData, AiChangedMsgData, ControlExecutedMsgData > MsgDataT;

    struct Msg
    {
        MsgTypeE msgType;
        MsgDataT msgData;
    };
  }

  namespace Server
  {

    enum MsgTypeE
    {
      downloadPointMsgType,
      downloadEndMsgType,
      monitorPointMsgType,
      cancelMonitoringMsgType,
      requestControlPointMsgType,
      executeControlPointMsgType
    };

    // ---------------
    // Monitor Point message data

    struct MonitorPointMsgData
    {
        std::string id;
        int timeout;
    };

    // ---------------
    // Cancel Monitoring message data

    struct CancelMonitoringMsgData
    {
        std::string id;
    };

    // ---------------
    // Request Control message data

    struct RequestControlMsgData
    {
        std::string id;
        double value;
    };

    // ---------------
    // Execute Control message data

    struct ExecuteControlMsgData
    {
        std::string id;
        double value;
    };

    // ---------------
    // Variant for the info sturcts

    typedef boost::variant< GenericStatusMsgData, MonitorPointMsgData,
        CancelMonitoringMsgData, RequestControlMsgData, ExecuteControlMsgData,
        db_syntest::st::SysPointPtrT > MsgDataT;

    struct Msg
    {
        MsgTypeE msgType;
        MsgDataT msgData;
    };
  }

} /* namespace agent */

#endif /* _AGENT_DECLARATIONS_H_ */
