/*
 * AgentRead.cpp
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

//#define BOOST_SPIRIT_DEBUG
#include "AgentRead.h"

#include <boost/spirit/include/qi_char.hpp>

namespace agent
{
  namespace ascii = boost::spirit::ascii;

  // ==========================================================================
  namespace Client
  {
    Read::Read() :
        Read::base_type(mRulStart)
    {

      mRulEnd = ascii::char_('\x02'); // Client record start and end
//      mRulEnd.name("mRulEnd");

      mRulDelim = ascii::char_('\x03'); // Record separator
//      mRulDelim.name("mRulDelim");

      mRulStrVal %= *(ascii::char_ - mRulDelim) >> mRulDelim;
//      mRulStrVal.name("mRulStrVal");

      mRulDblVal %= qi::real_parser< double, qi::strict_real_policies< double > >()
          >> mRulDelim;
//      mRulDblVal.name("mRulDblVal");

      mRulIntVal %= qi::int_ >> mRulDelim;
//      mRulIntVal.name("mRulIntVal");

      mRulBoolVal %= mSymBool >> mRulDelim;
//      mRulBoolVal.name("mRulBoolVal");

      // Generatic status message data
      mRulGenericStatusMsgData = //
      mRulBoolVal // success
      ;
//      mRulMonitoringStartedMsgData.name("mRulGenericStatusMsgData");

      // Monitoring Started message data
      mRulMonitoringStartedMsgData = //
      mRulStrVal // id
      >> mRulBoolVal // success
      >> mRulIntVal // timeout
          ;
//      mRulMonitoringStartedMsgData.name("mRulMonitoringStartedMsgData");

      // Digitial Indication Changed message data
      mRulDiChangedMsgData = //
      mRulStrVal // id
      >> mRulIntVal // value
      >> mRulStrVal // state
          ;
//      mRulDiChangedMsgData.name("mRulDiChangedMsgData");

      // Analog Indication Changed message data
      mRulAiChangedMsgData = //
      mRulStrVal // id
      >> mRulDblVal // value
          ;
//      mRulAiChangedMsgData.name("mRulAiChangedMsgData");

      // Control has been executed message data
      mRulControlExecutedMsgData = //
      mRulStrVal // id
      >> mRulDblVal // value
      >> mRulBoolVal // success
          ;
//      mRulControlExecutedMsgData.name("mRulControlExecutedMsgData");

      mRulData %= mRulGenericStatusMsgData //
      | mRulControlExecutedMsgData //
      | mRulDiChangedMsgData //
      | mRulAiChangedMsgData //
      | mRulMonitoringStartedMsgData;
//      mRulData.name("mRulData");

      mRulType %= mSymType >> mRulDelim;
//      mRulType.name("mRulType");

      mRulStart %= mRulEnd // Start of text
      >> mRulType //
      >> mRulData //
      >> mRulEnd // End of text
          ;
//      mRulStart.name("mRulStart");

//      debug(mRulStart);
//      debug(mRulData);
//      debug(mRulGenericStatusMsgData);
//      debug(mRulControlExecutedMsgData);
//      debug(mRulMonitoringStartedMsgData);
//      debug(mRulDiChangedMsgData);
//      debug(mRulAiChangedMsgData);
//      debug(mRulType);
//      debug(mRulBoolVal);
//      debug(mRulIntVal);
//      debug(mRulDblVal);
//      debug(mRulStrVal);
//      debug(mRulDelim);
//      debug(mRulEnd);

    }
  // ------------------------------------------------------------------------
  }
// ==========================================================================
}

