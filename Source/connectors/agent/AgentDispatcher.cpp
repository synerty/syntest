/*
 * AgentDispatcher.cpp
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#include "AgentDispatcher.h"

#include <boost/spirit/include/karma_generate.hpp>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/asio/placeholders.hpp>

// For qWarning, etc
#include <QtCore/qglobal.h>
#include <QtGui/qmessagebox.h>

#include <simscada/sim/SimObject.h>
#include <simscada_util/DbSimscadaUtil.h>

#include "Globals.h"
#include "TcpClient.h"
#include "AgentWrite.h"
#include "AgentRead.h"
#include "AgentData.h"
#include "AgentDispatcherObserver.h"
#include "UiMessage.h"

#include "Proactor.h"

// Disable debug code here
#undef FUNCTION_START
#define FUNCTION_START(DESC)
#undef FUNCTION_END
#define FUNCTION_END(DESC)

// Excplitit instantiation
#include "Observable.ini"
template class Observable< agent::AgentDispatcherObserver > ;

using namespace db_syntest::st;

namespace agent
{

  // ----------------------------------------------------------------------------
  // AgentDispatcher
  // ----------------------------------------------------------------------------

  AgentDispatcher::AgentDispatcher(db_syntest::st::SystemPtrT systemRecord,
      boost::shared_ptr< Proactor > proactor,
      const std::string& host,
      const int port) :

      mSystemRecord(systemRecord), //
      mProactor(proactor), //
      mHost(host), //
      mPort(port), //

      mStrand(proactor->ioService()), //
      mRespAckTimer(proactor->ioService()), //
      mConnectionState(disconnectedAgentState), //
      mMessagingState(idleMessagingState)
  {
  }
  // ----------------------------------------------------------------------------

  AgentDispatcher::~AgentDispatcher()
  {
    mStrand.dispatch(bind(&AgentDispatcher::strandedDisconnect, this));
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::postedConnect()
  {
    mStrand.post(bind(&AgentDispatcher::strandedConnect, shared_from_this()));
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::strandedConnect()
  {

    if (mTcpClient.get())
      mTcpClient->disconnect(true);

    tcp_client::TcpClientFeedbackPtrT dispatcher = shared_from_this();
    mTcpClient.reset(new tcp_client::TcpClient(dispatcher,
        mProactor,
        mHost,
        mPort));
    mTcpClient->connect();
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::postedDisconnect()
  {
    mStrand.post(bind(&AgentDispatcher::strandedDisconnect,
        shared_from_this()));
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::strandedDisconnect()
  {
    if (!mTcpClient.get())
      return;

    assert(mTcpClient.get());

    mTcpClient->disconnect(true);
    mTcpClient.reset();

    notifyOfConnectionStateChanged(agent::disconnectedAgentState, "");
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::messageRecieved(std::string message)
  {
    qDebug("Received message |%s|", message.c_str());

    // Its QI time :-)
    agent::Client::Msg msg;

    agent::Client::Read protocolParser;
    std::string::const_iterator itr = message.begin();
    std::string::const_iterator itrEnd = message.end();

    bool ok = boost::spirit::qi::phrase_parse(itr,
        itrEnd,
        protocolParser,
        boost::spirit::ascii::space,
        msg);

    if (!ok)
    {
      qCritical("Failed to parse message from the agent");
      QMessageBox::critical(MsgBoxParent(),
          "Comms Error",
          "An error occurred parsing"
              " a message from the agent");
      qDebug("Message failed, |%s|", message.c_str());
      return;
    }

    switch (msg.msgType)
    {
      case Client::downloadInProgressMsgType:
        downloadHeartBeat();
        return;

      case Client::downloadCompleteMsgType:
        downloadFinished();
        return;

      default:
        break;
    };

    notifyOfMessageRecieve(msg);
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::connectionStateChanged(tcp_client::ConnectionStateE newState,
      std::string errorMessage)
  {
    // If we're just connected, kick off the download
    if (newState == tcp_client::connectedState // new state
    && mConnectionState == connectingAgentState // last state
        )
    {
      downloadStart();
      return;
    }

    if (newState != tcp_client::connectedState
        && mConnectionState == downloadAgentState)
    {
      downloadFinished(false, errorMessage);
      return;
    }

    switch (newState)
    {
      case tcp_client::connectingState:
        mConnectionState = agent::connectingAgentState;
        break;

      case tcp_client::connectedState:
        throw std::runtime_error("Agent connected with out download");

      case tcp_client::connectionAttemptFailedState:
        mConnectionState = agent::connectionAttemptFailedAgentState;
        break;

      case tcp_client::disconnectedState:
        mConnectionState = agent::disconnectedAgentState;
        break;

      case tcp_client::connectionBrokenState:
        mConnectionState = agent::connectionBrokenAgentState;
        break;

      THROW_UNHANDLED_CASE_EXCEPTION
    }

    notifyOfConnectionStateChanged(mConnectionState, errorMessage);
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::monitorPoint(db_syntest::st::SysPointPtrT sysPoint)
  {
    using namespace agent::Server;

    assert(sysPoint.get());
    assert(sysPoint->simPoint().get());

    Msg msg;

    MonitorPointMsgData mon;
    mon.id = sysPoint->id();
    mon.timeout = sysPoint->simPoint()->feedbackTimeout();
    msg.msgData = mon;
    msg.msgType = monitorPointMsgType;

    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::cancelMontoring(db_syntest::st::SysPointPtrT sysPoint)
  {
    using namespace agent::Server;

    Msg msg;

    CancelMonitoringMsgData mon;
    mon.id = sysPoint->id();
    msg.msgData = mon;
    msg.msgType = cancelMonitoringMsgType;

    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::executeControl(db_syntest::st::SysPointPtrT sysPoint,
      double value)
  {
    using namespace agent::Server;

    Msg msg;

    RequestControlMsgData ctrlReq;
    ctrlReq.id = sysPoint->id();
    ctrlReq.value = value;
    msg.msgData = ctrlReq;
    msg.msgType = requestControlPointMsgType;

    sendMessage(msg);

    ExecuteControlMsgData ctrlExec;
    ctrlExec.id = sysPoint->id();
    ctrlExec.value = value;
    msg.msgData = ctrlExec;
    msg.msgType = executeControlPointMsgType;

    sendMessage(msg);
  }
  // ----------------------------------------------------------------------------

  void
  AgentDispatcher::downloadStart()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    if (!mTcpClient.get())
      throw std::runtime_error("AgentDispatcher::downloadToAgent, mTcpClient not connected");

    // Setup connection state
    mConnectionState = agent::downloadAgentState;
    mMessagingState = downloadMessagingState;
    notifyOfConnectionStateChanged(mConnectionState, "");

    SysPointListPtrT sysPoints(new db_syntest::st::SysPointListT());
    mSystemRecord->sysPointSystems(*sysPoints);

    mProactor->ioService().post(boost::bind(&AgentDispatcher::downloadPoint,
        shared_from_this(),
        sysPoints));

FUNCTION_END  ( __PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------

  void
  AgentDispatcher::downloadPoint(SysPointListPtrT sysPoints)
  {
    using namespace agent::Server;
    FUNCTION_START( __PRETTY_FUNCTION__)

    if (sysPoints->empty())
    {
      mProactor->ioService().post(boost::bind(&AgentDispatcher::downloadPointEnd,
          shared_from_this()));
      return;
    }

    AgentServerMsgPtrT msg(new agent::Server::Msg());
    msg->msgType = downloadPointMsgType;
    msg->msgData = sysPoints->back();

    mProactor->ioService().post(boost::bind(&AgentDispatcher::postedSendMessage,
        shared_from_this(),
        msg));

    sysPoints->pop_back();

    mProactor->ioService().post(boost::bind(&AgentDispatcher::downloadPoint,
        shared_from_this(),
        sysPoints));

FUNCTION_END  ( __PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------

  void
  AgentDispatcher::downloadPointEnd()
  {
    using namespace agent::Server;
    FUNCTION_START( __PRETTY_FUNCTION__)

    // Download points

    // Signals the end of the data download
    GenericStatusMsgData msgData;
    msgData.value = true;

    AgentServerMsgPtrT msg(new agent::Server::Msg());
    msg->msgType = downloadEndMsgType;
    msg->msgData = msgData;
    mProactor->ioService().post(boost::bind(&AgentDispatcher::postedSendMessage,
        shared_from_this(),
        msg));

    // Start the timer
    // Double the initial timeout because we are preparing messages
    mRespAckTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
    mRespAckTimer.async_wait(boost::bind(&AgentDispatcher::postedRespAckTimer,
        shared_from_this(),
        boost::asio::placeholders::error));

FUNCTION_END  ( __PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------
  void
  AgentDispatcher::downloadHeartBeat()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)

    // Cancel the timer
    mRespAckTimer.cancel();

    // Kick off another timeout
    mRespAckTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
    mRespAckTimer.async_wait(boost::bind(&AgentDispatcher::postedRespAckTimer,
        shared_from_this(),
        boost::asio::placeholders::error));

FUNCTION_END  ( __PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------

  void
  AgentDispatcher::downloadFinished(const bool success,
      const std::string errorMessage)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    mRespAckTimer.cancel();
    mMessagingState = idleMessagingState;

    if (!success)
    {
      if (mTcpClient.get())
        mTcpClient->disconnect(true);

      mConnectionState = connectionBrokenAgentState;
    }
    else
    {
      mConnectionState = connectedAgentState;
    }

    notifyOfConnectionStateChanged(mConnectionState, errorMessage);
FUNCTION_END  ( __PRETTY_FUNCTION__)
}
// ----------------------------------------------------------------------------

  void
  AgentDispatcher::notifyOfMessageRecieve(Client::Msg msg)
  {
    qDebug("Entering %s", __PRETTY_FUNCTION__);

    ObserverPtrsT observerPtrs;
    getObserverPtrs(observerPtrs);
    for (ObserverPtrsT::iterator itr = observerPtrs.begin();
        itr != observerPtrs.end(); ++itr)
    {
      mProactor->ioService().post(boost::bind( //
      &AgentDispatcherObserver::postedAgentRecievedMessage,
          *itr,
          msg));
    }

    qDebug("Exiting %s", __PRETTY_FUNCTION__);
  }
// --------------------------------------------------------------------------

  void
  AgentDispatcher::notifyOfConnectionStateChanged(agent::ConnectionStateE newState,
      std::string errorMessage)
  {
    ObserverPtrsT observerPtrs;
    getObserverPtrs(observerPtrs);
    for (ObserverPtrsT::iterator itr = observerPtrs.begin();
        itr != observerPtrs.end(); ++itr)
    {
      mProactor->ioService().post(boost::bind( //
      &AgentDispatcherObserver::postedAgentConnectionStateChanged,
          *itr,
          newState,
          errorMessage));
    }
  }
// --------------------------------------------------------------------------

  void
  AgentDispatcher::sendMessage(const agent::Server::Msg& msg)
  {
    AgentServerMsgPtrT msgPtr(new agent::Server::Msg(msg));
    mProactor->ioService().post(boost::bind(&AgentDispatcher::postedSendMessage,
        shared_from_this(),
        msgPtr));
  }
// --------------------------------------------------------------------------

  void
  AgentDispatcher::postedSendMessage(AgentServerMsgPtrT msg)
  {
    if (!mTcpClient.get())
      return;
    // throw std::runtime_error("AgentDispatcher::sendMessage, mTcpClient not connected");

    std::string str;
    agent::KarmaIteratorT sink(str);
    agent::Server::Write gen;
    bool ok = boost::spirit::karma::generate(sink, // destination: output iterator
        gen, // the generator
        *msg // the data to input
        );

    if (!ok)
    {
      qCritical("Failed to generate message to send to agent");
      if (mTcpClient.get())
        mTcpClient->disconnect(true);
      mMessagingState = idleMessagingState;
      mConnectionState = disconnectedAgentState;
      notifyOfConnectionStateChanged(mConnectionState, "Comms Error\n"
          "An error occurred generating"
          " message to send to agent");
      return;
    }

    mTcpClient->send(str);
  }
// ----------------------------------------------------------------------------

  void
  AgentDispatcher::postedRespAckTimer(const boost::system::error_code& error)
  {
    if (error == boost::asio::error::operation_aborted)
      return;

    downloadFinished(false, "Agent response timeout expired.");
  }
/// ---------------------------------------------------------------------------
}
