/*
 * AgentData.cpp
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#include "AgentData.h"

agent::BoolReadSym::BoolReadSym()
{
  add("True", true)//
  ("False", false)//
  ;
}

agent::BoolWriteSym::BoolWriteSym()
{
  add( true, std::string("True"))//
  ( false, std::string("False"))//
  ;
}

agent::Client::MsgTypeReadSym::MsgTypeReadSym()
{
  add//
  ("DLIP", downloadInProgressMsgType)//
  ("DOWNLOAD_COMPLETE", downloadCompleteMsgType)//
  ("MONITORING_STARTED", monitoringStartedMsgType)//
  ("DI_CHANGED", diChangedMsgType)//
  ("AI_CHANGED", aiChangedMsgType)//
  //("AC_CHANGED", acChangedMsgType)//
  ("CONTROL_EXECUTED", controlExecutedMsgType)//
  ;
}

agent::Server::MsgTypeWriteSym::MsgTypeWriteSym()
{
  add//
  (downloadPointMsgType, std::string("DLP"))// Download point
  (downloadEndMsgType, std::string("END_OF_DOWNLOAD"))// End of download
  (monitorPointMsgType, std::string("MONITOR_POINT"))//
  (cancelMonitoringMsgType, std::string("MONITOR_CANCEL"))//
  (requestControlPointMsgType, std::string("CONTROL_SELECT"))//
  (executeControlPointMsgType, std::string("CONTROL_EXECUTE"))//
  ;
}

