/*
 * AutoTestExecAgentDispatcherObserver.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AgentDispatcherObserver.h"

namespace agent
{
  AgentDispatcherObserver::~AgentDispatcherObserver()
  {
  }
  // ------------------------------------------------------------------------

  void
  AgentDispatcherObserver::postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
      std::string errorMessage)
  {
  }
  // ------------------------------------------------------------------------

  void
  AgentDispatcherObserver::postedAgentRecievedMessage(Client::Msg message)
  {
  }
// ------------------------------------------------------------------------

} /* namespace agent */
