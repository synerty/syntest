/*
 * AgentDispatcher.h
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#ifndef _AGENT_DISPATCHER_H_
#define _AGENT_DISPATCHER_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/asio/strand.hpp>
#include <boost/asio/deadline_timer.hpp>

#include <QtCore/qstring.h>

#include <syntest/DbSyntestDeclaration.h>

#include "AgentDeclaration.h"
#include "TcpClientFeedback.h"
#include "TcpClientDeclaration.h"
#include "Observable.h"
#include "Macros.h"

class Proactor;
namespace tcp_client
{
  class TcpClient;
}

namespace agent
{

  class AgentDispatcherObserver;

  /*! AgentDispatcher Brief Description
   * Long comment about the class
   *
   */
  class AgentDispatcher : public boost::enable_shared_from_this< AgentDispatcher >,
      public Observable< AgentDispatcherObserver >,
      public tcp_client::TcpClientFeedback

      DEBUG_CLASS_MIXIN(AgentDispatcher)

  {
    public:
      AgentDispatcher(db_syntest::st::SystemPtrT systemRecord,
          boost::shared_ptr< Proactor > proactor,
          const std::string& host,
          const int port);

      virtual
      ~AgentDispatcher();

    public:
      /// ---------------
      /// Public interface used by all

      void
      postedConnect();

      void
      strandedConnect();

      void
      postedDisconnect();

      void
      strandedDisconnect();

    public:
      /// ---------------
      /// Functions to control the agent

      void
      monitorPoint(db_syntest::st::SysPointPtrT sysPoint);

      void
      cancelMontoring(db_syntest::st::SysPointPtrT sysPoint);

      void
      executeControl(db_syntest::st::SysPointPtrT sysPoint, double value);

      void
      strandedDownloadToAgent(db_syntest::st::SysPointListT sysPoints);

    public:
      /// ---------------
      /// Private functions that need to be public so timers can call them

      void
      postedRespAckTimer(const boost::system::error_code& error);

    private:
      /// ---------------
      /// Observerable Stuff

      void
      notifyOfMessageRecieve(Client::Msg msg);

      void
      notifyOfConnectionStateChanged(agent::ConnectionStateE newState,
          std::string errorMessage);

    private:
      // ---------------
      // TcpClientFeedback Interface impliementation

      void
      messageRecieved(std::string message);

      void
      connectionStateChanged(tcp_client::ConnectionStateE,
          std::string errorMessage);

    private:
      enum MessagingStateE
      {
        idleMessagingState,
        downloadMessagingState
      };

      typedef boost::shared_ptr< agent::Server::Msg > AgentServerMsgPtrT;

      typedef boost::shared_ptr< db_syntest::st::SysPointListT > SysPointListPtrT;

      void
      downloadStart();

      void
      downloadPoint(SysPointListPtrT sysPoints);

      void
      downloadPointEnd();

      void
      downloadHeartBeat();

      void
      downloadFinished(const bool success = true,
          const std::string errorMessage = std::string());

      void
      sendMessage(const agent::Server::Msg& msg);

      void
      postedSendMessage(AgentServerMsgPtrT msg);

      db_syntest::st::SystemPtrT mSystemRecord;

      boost::shared_ptr< Proactor > mProactor;
      const std::string mHost;
      const int mPort;

      boost::asio::strand mStrand;
      boost::asio::deadline_timer mRespAckTimer;

      boost::shared_ptr< tcp_client::TcpClient > mTcpClient;
      agent::ConnectionStateE mConnectionState;
      MessagingStateE mMessagingState;
  };
}
#endif /* _AGENT_DISPATCHER_H_ */
