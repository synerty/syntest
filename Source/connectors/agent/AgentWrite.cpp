/*
 * AgentDispatcherWrite.cpp
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#include "AgentWrite.h"

namespace agent
{
  namespace ascii = boost::spirit::ascii;
  //  using namespace karma::labels;

  namespace Server
  {
    Write::Write() :
      Write::base_type(mRulStart)
    {
      // Common
      mRulEnd = ascii::char_('\x01'); // Server record start and end
      mRulDelim = ascii::char_('\x03'); // Record separator

      // Message type
      mRulType = mSymType << mRulDelim;

      // Generic string
      mRulStr = *ascii::char_ << mRulDelim;

      // Generic bool
      mRulBool = mSymBool << mRulDelim;

      // Generic command message data
      mRulGenericStatus = //
          mRulBool // id
      ;

      // Monitor Point message data
      mRulMonitorPoint = //
          mRulStr // id
          << karma::int_ << mRulDelim // timeout
      ;

      // Cancel Monitoring message data
      mRulCancelMonitoring = //
          mRulStr // id
      ;

      // Request Control message data
      mRulRequestControl = //
      mRulStr // id
          << karma::double_ << mRulDelim // value
      ;

      // Execute Control message data
      mRulExecuteControl = //
      mRulStr // id
          << karma::double_ << mRulDelim // value
      ;

      // Download Point message data
      mRulDownloadPoint = //
      mRulStr // type
          << mRulStr // id
          << mRulStr // pid
          << mRulStr // alias
          << mRulStr // name
          << mRulStr // function
          << mRulStr // sup1
          << mRulStr // sup2
      ;

      // Variant for the different info structs
      mRulVariantInfoStruct = //
      mRulGenericStatus // Generic status
          | mRulMonitorPoint // Monitor Point
          | mRulCancelMonitoring // Cancel Monitoring
          | mRulRequestControl // Select Control
          | mRulExecuteControl // Execute Control
          | mRulDownloadPoint // Download Point
      ;

      // Start rule
      mRulStart = mRulEnd // Start of text
          << mRulType //Message Type
          << mRulVariantInfoStruct // Point info structs
          << mRulEnd // End of text
      ;
    }
  }
// ==========================================================================
}
