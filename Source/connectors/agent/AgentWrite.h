#ifndef _AGENT_WRITE_H_
#define _AGENT_WRITE_H_

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/karma.hpp>

#include "AgentData.h"

namespace agent
{

  namespace karma = boost::spirit::karma;
  typedef std::back_insert_iterator< std::string > KarmaIteratorT;

  // ==========================================================================
  namespace Server
  {

    struct Write : karma::grammar< KarmaIteratorT/*, karma::locals<MsgTypeE>*/,
        Msg
        () >
    {
        Write();

        karma::rule< KarmaIteratorT/*, karma::locals<MsgTypeE>*/, Msg
        () > mRulStart;
        karma::rule< KarmaIteratorT > mRulEnd;
        karma::rule< KarmaIteratorT > mRulDelim;

        karma::rule< KarmaIteratorT, MsgTypeE
        () > mRulType;
        agent::Server::MsgTypeWriteSym mSymType;

        karma::rule< KarmaIteratorT, bool
        () > mRulBool;
        agent::BoolWriteSym mSymBool;

        karma::rule< KarmaIteratorT, std::string
        () > mRulStr;

        karma::rule< KarmaIteratorT,/*void(MsgTypeE),*/ MsgDataT
        () > mRulVariantInfoStruct;

        // GenericCommand message data
        karma::rule< KarmaIteratorT, GenericStatusMsgData
        () > mRulGenericStatus;

        // Download Point message data
        karma::rule< KarmaIteratorT, db_syntest::st::SysPointPtrT
        () > mRulDownloadPoint;

        // Monitor Point message data
        karma::rule< KarmaIteratorT, MonitorPointMsgData
        () > mRulMonitorPoint;

        // Cancel Monitoring message data
        karma::rule< KarmaIteratorT, CancelMonitoringMsgData
        () > mRulCancelMonitoring;

        // Request Control message data
        karma::rule< KarmaIteratorT, RequestControlMsgData
        () > mRulRequestControl;

        // Execute Control message data
        karma::rule< KarmaIteratorT, ExecuteControlMsgData
        () > mRulExecuteControl;

    };
  }
// ==========================================================================
}
#endif /* _AGENT_WRITE_H_ */
