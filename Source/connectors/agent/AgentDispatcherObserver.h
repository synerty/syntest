/*
 * AutoTestExecAgentDispatcherObserver.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AGENT_DISPATCHER_OBSERVER_H_
#define _AGENT_DISPATCHER_OBSERVER_H_

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "TcpClientDeclaration.h"
#include "AgentDeclaration.h"
#include "Macros.h"

namespace agent
{
  class AgentDispatcherObserver;
  typedef boost::shared_ptr< AgentDispatcherObserver > AgentDispatcherObserverPtrT;
  typedef boost::weak_ptr< AgentDispatcherObserver > AgentDispatcherObserverWptrT;

  /*! AgentDispatcher Observer
   * Classes implementing this will be informed of changes in test processing
   */
  class AgentDispatcherObserver : DebugClassMixin< AgentDispatcherObserver >
  {
    public:
      virtual
      ~AgentDispatcherObserver();

    public:
      virtual void
      postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
          std::string errorMessage);

      virtual void
      postedAgentRecievedMessage(Client::Msg message);
  };

} /* namespace agent */
#endif /* _AGENT_DISPATCHER_OBSERVER_H_ */
