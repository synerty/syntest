/*
 * AgentData.h
 *
 *  Created on: Dec 26, 2010
 *      Author: Administrator
 */

#ifndef _AGENT_DATA_H_
#define _AGENT_DATA_H_

#include <string>
#include <vector>
#include <map>
#include <stdint.h>

#include <boost/config/warning_disable.hpp>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/adapted/adt/adapt_adt.hpp>
#include <boost/fusion/include/adapt_adt.hpp>

#include <boost/variant/recursive_variant.hpp>
#include <boost/variant.hpp>

#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_object.hpp>

#include <boost/spirit/include/qi_symbols.hpp>
#include <boost/spirit/include/karma.hpp>

#include <syntest/DbSyntestDeclaration.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSystem.h>

#include "AgentDeclaration.h"

namespace agent
{

  struct BoolReadSym : public boost::spirit::qi::symbols< const char, bool >
  {
      BoolReadSym();
  };

  struct BoolWriteSym : public boost::spirit::karma::symbols< bool, const std::string >
  {
      BoolWriteSym();
  };
}


// Adapt the struts to become first class fusion tuples.
// This means spirit::qi can insert the data directly into theses structs
// more or less.
BOOST_FUSION_ADAPT_STRUCT(//
agent::GenericStatusMsgData,//
    (bool, value)//
    )

namespace agent
{
  namespace Client
  {

    struct MsgTypeReadSym : public boost::spirit::qi::symbols< char, MsgTypeE >
    {
        MsgTypeReadSym();
    };
  }
}

// Adapt the struts to become first class fusion tuples.
// This means spirit::qi can insert the data directly into theses structs
// more or less.

BOOST_FUSION_ADAPT_STRUCT( //
agent::Client::MonitoringStartedMsgData,//
    (std::string, id)//
    (bool, success)//
    (int, timeout)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Client::DiChangedMsgData,//
    (std::string, id)//
    (int, value)//
    (std::string, state)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Client::AiChangedMsgData,
    (std::string, id) //
    (double, value)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Client::ControlExecutedMsgData,
    (std::string, id) //
    (double, value)//
    (bool, success)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Client::Msg,
    (agent::Client::MsgTypeE, msgType) //
    (agent::Client::MsgDataT, msgData)//
    )

namespace agent
{
  namespace Server
  {

    //    struct MsgTypeReadSym : public boost::spirit::qi::symbols< const char,
    //        MsgTypeE >
    //    {
    //        MsgTypeReadSym();
    //    };

    struct MsgTypeWriteSym : public boost::spirit::karma::symbols< MsgTypeE,
        const std::string >
    {
        MsgTypeWriteSym();
    };

  // ---------------
  // Variant for the info sturcts

  }

}

// Adapt the struts to become first class fusion tuples.
// This means spirit::qi can insert the data directly into theses structs
// more or less.

BOOST_FUSION_ADAPT_STRUCT( //
agent::Server::MonitorPointMsgData,//
    (std::string, id)//
    (int, timeout)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Server::CancelMonitoringMsgData,//
    (std::string, id)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Server::RequestControlMsgData,
    (std::string, id) //
    (double, value)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Server::ExecuteControlMsgData,
    (std::string, id) //
    (double, value)//
    )

BOOST_FUSION_ADAPT_STRUCT( //
agent::Server::Msg,
    (agent::Server::MsgTypeE, msgType) //
    (agent::Server::MsgDataT, msgData)//
    )

/// ---------------
/// Adapt the DbSyntest orm objects

BOOST_FUSION_ADAPT_ADT(//
db_syntest::st::SysPointPtrT,
    (std::string, std::string, obj->simPoint()->type(),obj->simPoint()->setType(val)) //
    (std::string, std::string, obj->id(), obj->setId(val))//
    (std::string, std::string, obj->pid(), obj->setPid(val))//
    (std::string, std::string, obj->alias(), obj->setAlias(val))//
    (std::string, std::string, obj->name(), obj->setName(val))//
    (std::string, std::string, obj->function(), obj->setFunction(val))//
    (std::string, std::string, obj->sup1(), obj->setSup1(val))//
    (std::string, std::string, obj->sup2(), obj->setSup2(val))//
    )

#endif /* _AGENT_DATA_H_ */
