/*
 * Proactor.h
 *
 *  Created on: Dec 23, 2010
 *      Author: Administrator
 */

#ifndef _PROACTOR_H_
#define _PROACTOR_H_

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>

class Proactor;
typedef boost::shared_ptr< Proactor > ProactorPtrT;

/*! Proactor Brief Description
 * Long comment about the class
 *
 */
class Proactor : public boost::enable_shared_from_this< Proactor >
{

  public:
    Proactor();

    virtual
    ~Proactor();

    void
    run();

    void
    stop();

    void
    terminate();

    boost::asio::io_service&
    ioService();

    bool
    isRunning();

  private:

    boost::asio::io_service mIoService;

    // Holds the proactor running if we don't have anything on the queue
    // Which probably shouldn't happen
    boost::shared_ptr< boost::asio::io_service::work > mWork;

    boost::shared_ptr< boost::thread_group> mThreadPool;
    static const int THREAD_COUNT = 1;

    bool mIsRunning;
};

#endif /* _PROACTOR_H_ */
