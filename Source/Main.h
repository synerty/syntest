/*
 * Main.h
 *
 *  Created on: Dec 19, 2010
 *      Author: Administrator
 */
#ifndef MAIN_H_
#define MAIN_H_

#include <auto_ptr.h>
#include <set>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/deadline_timer.hpp>

#include <QtCore/qobject.h>

#include <simscada/DbSimscadaDeclaration.h>

#include "Settings.h"
#include "MainObserver.h"
#include "MainUiCenter.h"
#include "Observable.h"

class MainUi;

class Proactor;
typedef boost::shared_ptr< Proactor > ProactorPtrT;

class Main : public QObject,
    public Observable< MainObserver >,
    public boost::enable_shared_from_this< Main >,
    boost::noncopyable
{
  Q_OBJECT

  public:
    static Main&
    Inst();

    virtual
    ~Main();

  public:
    void
    Run();

  public slots:
    // ---------------
    // Interface used by Gui

    void
    openSettings();

    void
    exitApplication();

    void
    mainGuiClosing();

  public slots:
    // ---------------
    // Glaboal UI related interface

    void
    selectUiMode(main_ui::UiModeE uiMode,
        QWidget* ui,
        boost::shared_ptr< MainUiCenter > newDisplay);

    void
    defaultUiMode();

    void
    wakeQtEventLoopSlot();

  public:
    // ---------------
    // Glaboal interface

    QWidget*
    MsgBoxParent();

    ProactorPtrT
    proactor();

  public:
    // ---------------
    // Asyncronouse functions

    void
    wakeQtEventLoop();

  private:
    void
    notifyOfMainUiModeChange(main_ui::UiModeE oldUiMode);

    typedef std::set< MainObserver* > ObserversT;
    ObserversT mObservers;

  private:
    //    Settings mSettings;
    std::auto_ptr< MainUi > mGui;

    main_ui::UiModeE mUiMode;
    boost::shared_ptr<MainUiCenter> mCenterDisplay;

    ProactorPtrT mProactor;
    boost::asio::deadline_timer mWakeQtTimer;

  private:
    Main();
    static boost::shared_ptr< Main > sInst;

};
#endif /* MAIN_H_ */
