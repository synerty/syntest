/*
 * AutoTestGenBatchUiSystemModel.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "AutoTestGenBatchUiSystemModel.h"

#include <assert.h>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include <syntest/SynTestModel.h>
#include <syntest/st/StSystem.h>

#include "Globals.h"

using namespace db_syntest;
using namespace db_syntest::st;

namespace autotest
{
  namespace gen_batch
  { //---------------------------------------------------------------------------
    //                           SystemModel private
    //---------------------------------------------------------------------------

    namespace system_model_private
    {
      struct Node
      {
          bool includeSystem;
          db_syntest::st::SystemPtrT sys;

          Node(SystemPtrT sys_) :
              includeSystem(false), sys(sys_)
          {
          }
      };

      void
      testModel(QAbstractItemModel* model, QModelIndex parent = QModelIndex())
      {
        //    qDebug("Testing parent row=%d, col=%d, alias=%s",
        //        parent.row(),
        //        parent.column(),
        //        parent.isValid() ? model->data(parent, Qt::DisplayRole).toString().toStdString().c_str()
        //            : "");

        int rowCount = model->rowCount(parent);
        int colCount = model->columnCount(parent);

        for (int row = 0; row < rowCount; row++)
        {
          // Test the data
          for (int col = 0; col < colCount; col++)
          {

            QModelIndex index = model->index(row, col, parent);

            // Test the parent
            QModelIndex testParent = model->parent(index);
            if (testParent != parent)
              assert(testParent == parent);

            QVariant disp = model->data(index, Qt::DisplayRole);
            QVariant forCol = model->data(index, Qt::ForegroundRole);
            QVariant toolTip = model->data(index, Qt::ToolTipRole);
          }

          // Test the subtree
          QModelIndex index = model->index(row, 0, parent);
          testModel(model, index);

        }
      }
    }

    //---------------------------------------------------------------------------
    //                       SystemModel Implementation
    //---------------------------------------------------------------------------

    SystemModel::SystemModel(db_syntest::SynTestModelPtrT synModel) :
        mSynModel(synModel)
    {
      setupModel();

//#ifdef DEBUG
//      system_model_private::testModel(this);
//      qDebug("Model tested, no errors found");
//#endif

    } //---------------------------------------------------------------------------

    SystemModel::~SystemModel()
    {
    }
    //---------------------------------------------------------------------------

    db_syntest::st::SystemListT
    SystemModel::selectedSystems() const
    {
      db_syntest::st::SystemListT systems;
      for (NodesT::const_iterator itr = mNodes.begin(); itr != mNodes.end();
          ++itr)
      {
        if (itr->includeSystem)
          systems.push_back(itr->sys);
      }
      return systems;
    }
    //---------------------------------------------------------------------------

    // Abstract implementation
    QVariant
    SystemModel::headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole) const
    {
      if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
        return QVariant();

      switch (prmSection)
      {
        case ColInclude:
          return "Test ?";
        case ColName:
          return "Name";
        case ColAgentPort:
          return "Agent Port";
        case ColAgentIp:
          return "Agent Ip";
        case ColSimPort:
          return "Sim Port";
        case ColSimIp:
          return "Sim IP";
        case ColSimAliasPostfix:
          return "Sim Alias Postfix";
        default:
          assert(false);
          break;
      }

      return QVariant();
    }
    //---------------------------------------------------------------------------

    QModelIndex
    SystemModel::index(int row, int column, const QModelIndex& parent) const
    {
      assert(row >= 0 && row < mNodes.size());

      return createIndex(row, column);
    }
    //---------------------------------------------------------------------------

    QModelIndex
    SystemModel::parent(const QModelIndex& index) const
    {
      return QModelIndex();
    }
    //---------------------------------------------------------------------------

    int
    SystemModel::rowCount(const QModelIndex& parent) const
    {
      return mNodes.size();
    }
    //---------------------------------------------------------------------------

    int
    SystemModel::columnCount(const QModelIndex& parent) const
    {
      return ColCount;
    }
    //---------------------------------------------------------------------------

    bool
    SystemModel::hasChildren(const QModelIndex& parent) const
    {
      return false;
    }
    //---------------------------------------------------------------------------

    Qt::ItemFlags
    SystemModel::flags(const QModelIndex &index) const
    {
      Qt::ItemFlags f = QAbstractItemModel::flags(index);

      if (index.column() == ColInclude)
      {
        f |= Qt::ItemIsUserCheckable;
        f |= Qt::ItemIsEditable;
      }
      return f;
    }
    //---------------------------------------------------------------------------

    QVariant
    SystemModel::data(const QModelIndex& index, int role) const
    {
      // We only provide certain data.
      if (!index.isValid() //
          || (role != Qt::DisplayRole && role != Qt::ToolTipRole
              && role != Qt::CheckStateRole && role != Qt::BackgroundRole)
          || !(0 <= index.column() && index.column() < ColCount)
          || !(0 <= index.row() && index.row() < mNodes.size()))
      {
        return QVariant();
      }

      const system_model_private::Node& node = mNodes[index.row()];

      if (role == Qt::BackgroundRole)
      {
        if (node.includeSystem)
          return QBrush(QColor("LightGreen"));

        return QVariant();
      }

      switch (index.column())
      {
        case ColInclude:
          if (role == Qt::CheckStateRole)
            return (node.includeSystem ? Qt::Checked : Qt::Unchecked);
          return QVariant();

        case ColName:
          if (role == Qt::DisplayRole)
            return QString(node.sys->name().c_str());
          return QVariant();

        case ColAgentPort:
          if (role == Qt::DisplayRole)
          {
            const int i = node.sys->agentPort();
            return QString(boost::lexical_cast< std::string >(i).c_str());
          }
          return QVariant();

        case ColAgentIp:
          if (role == Qt::DisplayRole)
            return QString(node.sys->agentIp().c_str());
          return QVariant();

        case ColSimPort:
          if (role == Qt::DisplayRole)
          {
            const int i = node.sys->simulatorPort();
            return QString(boost::lexical_cast< std::string >(i).c_str());
          }
          return QVariant();

        case ColSimIp:
          if (role == Qt::DisplayRole)
            return QString(node.sys->simulatorIp().c_str());
          return QVariant();

        case ColSimAliasPostfix:
          if (role == Qt::DisplayRole)
            return QString(node.sys->simulatorAliasPostfix().c_str());
          return QVariant();

        case ColCount:
          assert(false);
          break;
      }

      assert(false);
      return QVariant();
    }
    //---------------------------------------------------------------------------

    bool
    SystemModel::setData(const QModelIndex &index,
        const QVariant &value,
        int role)
    { // We only provide certain data.
      assert(0 <= index.row() && index.row() < mNodes.size());

      system_model_private::Node& node = mNodes[index.row()];

      node.includeSystem = value.toUInt() == Qt::Checked;

      QModelIndex topLeft = createIndex(index.row(), 0);
      QModelIndex bottomRight = createIndex(index.row(), ColCount - 1);
      dataChanged(topLeft, bottomRight);
      return true;
    }
    //---------------------------------------------------------------------------

    void
    SystemModel::setupModel()
    {
      const SystemListT& systems = mSynModel->StSystems();
      mNodes.clear();

      for (SystemListT::size_type i = 0; i < systems.size(); ++i)
      {
        mNodes.push_back(system_model_private::Node(systems[i]));
      }
    }
  //---------------------------------------------------------------------------

  } /* namespace gen_batch */
} /* namespace autotest */
