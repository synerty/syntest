#ifndef AUTOTESTGENBATCHUI_H
#define AUTOTESTGENBATCHUI_H

#include <QtGui/QDialog>
#include "ui_AutoTestGenBatchUi.h"

#include <boost/shared_ptr.hpp>

namespace autotest
{
  namespace gen_batch
  {
    struct NewBatchConfig;
    class RtuModel;
    class SystemModel;
  }
}

class AutoTestGenBatchUi : public QDialog
{
  Q_OBJECT

  public:
    AutoTestGenBatchUi();
    ~AutoTestGenBatchUi();

  public:
    autotest::gen_batch::NewBatchConfig
    data();

    std::vector< std::string >
    selectedPointAliases();

  public slots:

    void
    generateClicked();

  private:
    Ui::AutoTestGenBatchUiClass ui;
    boost::shared_ptr< autotest::gen_batch::RtuModel > mQtRtuModel;
    boost::shared_ptr< autotest::gen_batch::SystemModel > mQtSystemModel;

    bool
    validateName();

    bool
    validateRtus();

    bool
    validateSystems();

    std::vector<std::string>
    pointAliases();
};

#endif // AUTOTESTGENBATCHUI_H
