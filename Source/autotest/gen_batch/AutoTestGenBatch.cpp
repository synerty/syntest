/*
 * AutoTestGenBatch.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "AutoTestGenBatch.h"

#include <stdexcept>
#include <math.h>
#include <vector>
#include <map>

#include <boost/format.hpp>

#include <QtGui/qmessagebox.h>

#include <orm/v1/Util.h>

#include <syntest/SynTestModel.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestBatch.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestSystem.h>
#include <syntest_util/DbSyntestUtil.h>

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimRtu.h>
#include <simscada/sim/SimTelePt.h>
#include <simscada/sim/SimAccPt.h>
#include <simscada/sim/SimDigPt.h>
#include <simscada/sim/SimDigDeviceType.h>
#include <simscada/sim/SimDigClass.h>
#include <simscada/sim/SimAnaPt.h>
#include <simscada/sim/SimCtrlPt.h>

#include "MainSimModel.h"
#include "MainSynModel.h"
#include "Macros.h"
#include "Globals.h"

#include "AutoTestGenBatchUi.h"

using namespace db_syntest;
using namespace db_syntest::st;
using namespace db_simscada;
using namespace db_simscada::sim;

AutoTestGenBatch::AutoTestGenBatch() :
  mSimModel(MainObjectModel::Inst().ormModel()) //
      , mSynModel(MainSynModel::Inst().ormModel())
{
  // Initialise a map
  assert(mSynModel.get());
  const SysPointListT& sysPoints = mSynModel->StSysPoints();

  for (SysPointListT::const_iterator itr = sysPoints.begin(); itr
      != sysPoints.end(); ++itr)
  {
    SysPointPtrT sysPoint = *itr;
    assert(sysPoint.get());
    mSysPtBtSimPtBySystem[sysPoint->system()][sysPoint->simPoint()] = sysPoint;
  }

}
/// ---------------------------------------------------------------------------

AutoTestGenBatch::~AutoTestGenBatch()
{
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::run()
{
  try
  {
    AutoTestGenBatchUi ui;
    int result = ui.exec();

    if (result == QDialog::Rejected)
      return;

    autotest::gen_batch::NewBatchConfig newBatchConfig = ui.data();
    generateBatch(newBatchConfig);
  }
  catch (std::exception& e)
  {
    boost::format
        info("An exception occurred during test batch generation\n%s");
    info % e.what();
    QMessageBox::critical(MsgBoxParent(),
        "Test Batch Generation Failed",
        info.str().c_str());

  }
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::generateBatch(autotest::gen_batch::NewBatchConfig config)
{
  mSynModel->beginTransaction();

  TestBatchPtrT batch = addBatch(config.name, config.comment);

  TestSystemListT systems = addSystems(batch, config.systems);

  addPoints(systems, batch, config.rtus, config.pointAliases);

  mSynModel->save();
  mSynModel->commitTransaction();
}
/// ---------------------------------------------------------------------------

TestBatchPtrT
AutoTestGenBatch::addBatch(const std::string& name, const std::string& comment)
{
  TestBatchPtrT batch(new TestBatch());

  batch->setId(nextId(mSynModel));
  batch->setName(name);
  if (!comment.empty())
    batch->setComment(comment);

  batch->addToModel(mSynModel);

  return batch;
}
/// ---------------------------------------------------------------------------

TestSystemListT
AutoTestGenBatch::addSystems(TestBatchPtrT& batch, SystemListT& systems)
{
  TestSystemListT testSystems;

  for (SystemListT::const_iterator itr = systems.begin(); itr != systems.end(); ++itr)
  {
    TestSystemPtrT testSystem(new TestSystem());
    testSystem->setId(nextId(mSynModel));
    testSystem->setBatch(batch);
    testSystem->setSystem(*itr);
    testSystem->addToModel(mSynModel);
    testSystems.push_back(testSystem);
  }

  return testSystems;
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addPoints(const TestSystemListT& systems,
    TestBatchPtrT& batch,
    RtuListT& rtus,
    autotest::gen_batch::PointAliasesT &pointAliases)
{
  typedef std::set< std::string > PntIndexT;
  PntIndexT indexedPoints = PntIndexT(pointAliases.begin(), pointAliases.end());

  for (RtuListT::iterator rtuItr = rtus.begin(); rtuItr != rtus.end(); ++rtuItr)
  {
    RtuPtrT rtu = *rtuItr;

    ObjectListT dataGroups;
    rtu->objectParents(dataGroups);

    for (ObjectListT::iterator dgItr = dataGroups.begin(); dgItr
        != dataGroups.end(); ++dgItr)
    {
      ObjectPtrT dataGroup = *dgItr;

      ObjectListT pointsBase;
      TelePtListT points;
      dataGroup->objectParents(pointsBase);
      orm::v1::util::downcast(pointsBase, points);

      for (TelePtListT::iterator ptItr = points.begin(); ptItr != points.end(); ++ptItr)
      {
        // If we have certain points entered, then only include those points
        // Otherwise, include all points.
        std::string pointAlias = (*ptItr)->alias();

        if (indexedPoints.empty() //
            || indexedPoints.find(pointAlias) != indexedPoints.end())
        {
          addPoint(systems, batch, *ptItr);
        }
      }

    }

  }
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addPoint(const TestSystemListT& systems,
    TestBatchPtrT& batch,
    TelePtPtrT& simTelePoint)
{
  SimPointPtrT synSimPoint = mSynModel->StSimPoint(simTelePoint->alias());

  if (synSimPoint.get() == NULL)
  {
    boost::format info("Unable to add point %s to batch\n"
      "The point has not been configured in the SynTEST databse");
    info % simTelePoint->alias();
    logMessage(mSynModel, info.str(), errorSeverity, batch);

    return;
  }

  TestPointPtrT synPoint(new TestPoint());
  synPoint->setId(nextId(mSynModel));
  synPoint->setBatch(batch);
  synPoint->setSimPoint(synSimPoint);
  synPoint->addToModel(mSynModel);

  switch (toSimPointType(synPoint->simPoint()))
  {
    case di1SimPointType:
    case di2SimPointType:
      addValuesDi(systems, synPoint, DigPt::downcast(simTelePoint));
      break;

    case aiSimPointType:
      addValuesAi(systems, synPoint, AnaPt::downcast(simTelePoint));
      break;

    case acSimPointType:
      addValuesAc(systems, synPoint, AccPt::downcast(simTelePoint));
      break;

    case aoSimPointType:
      addValuesAo(systems, synPoint, CtrlPt::downcast(simTelePoint));
      break;

    case doSimPointType:
      addValuesDo(systems, synPoint, CtrlPt::downcast(simTelePoint));
      break;

    THROW_UNHANDLED_CASE_EXCEPTION
  }
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addValuesDi(const TestSystemListT& systems,
    db_syntest::st::TestPointPtrT testPoint,
    db_simscada::sim::DigPtPtrT simPoint)
{
  SimPointTypeE simPointType = toSimPointType(testPoint->simPoint());
  assert(simPointType == di1SimPointType || simPointType == di2SimPointType);
  int valueCount = (simPointType == di1SimPointType ? 2 : 4);

  typedef std::map< int, std::string > IntStrMapT;
  IntStrMapT states;
  IntStrMapT statesValues;

  DigClassPtrT simCls = simPoint->deviceClass();
  DigDeviceTypePtrT simTyp = simPoint->deviceType();

  states[simTyp->stateA()] = simCls->stateAText();
  states[simTyp->stateB()] = simCls->stateBText();

  statesValues[simTyp->stateA()] = "A";
  statesValues[simTyp->stateB()] = "B";

  if (simPointType == di2SimPointType)
  {
    states[simTyp->stateC()] = simCls->stateCText();
    states[simTyp->stateD()] = simCls->stateDText();

    statesValues[simTyp->stateC()] = "C";
    statesValues[simTyp->stateD()] = "D";
  }

  for (int value = 0; value < valueCount; value++)
  {
    TestValuePtrT testValue(new TestValue());
    testValue->setId(nextId(mSynModel));
    testValue->setTestPoint(testPoint);
    testValue->setSimValue(value);
    testValue->setSimDigValue(statesValues[value]);
    testValue->setSimDigState(states[value]);
    testValue->addToModel(mSynModel);

    addSysValues(systems, testValue);
  }
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addValuesAi(const TestSystemListT& systems,
    db_syntest::st::TestPointPtrT testPoint,
    db_simscada::sim::AnaPtPtrT simPoint)
{
  typedef std::vector< double > VecIntT;

  // Define the test
  std::vector< double > tests;
  tests.push_back(0.0f);
  tests.push_back(0.35f);
  //  tests.push_back(0.50f);
  tests.push_back(0.65f);
  tests.push_back(1.0f);

  const double range = simPoint->engMax() - simPoint->engMin();

  for (VecIntT::iterator itr = tests.begin(); itr != tests.end(); ++itr)
  {
    const double value = simPoint->engMin() + (*itr * range);

    TestValuePtrT testValue(new TestValue());
    testValue->setId(nextId(mSynModel));
    testValue->setTestPoint(testPoint);
    testValue->setSimValue(value);
    testValue->addToModel(mSynModel);

    addSysValues(systems, testValue);
  }
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addValuesAc(const TestSystemListT& systems,
    db_syntest::st::TestPointPtrT testPoint,
    db_simscada::sim::AccPtPtrT simPoint)
{
  typedef std::vector< int > VecIntT;

  // Define the test
  VecIntT tests;
  tests.push_back(500);
  tests.push_back(1000);
  //  tests.push_back(0.50f);
  tests.push_back(2000);
  //tests.push_back(1.0f);

  for (VecIntT::iterator itr = tests.begin(); itr != tests.end(); ++itr)
  {
    const int value = simPoint->startValue() + (*itr);

    TestValuePtrT testValue(new TestValue());
    testValue->setId(nextId(mSynModel));
    testValue->setTestPoint(testPoint);
    testValue->setSimValue(value);
    testValue->addToModel(mSynModel);

    addSysValues(systems, testValue);
  }
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addValuesAo(const TestSystemListT& systems,
    db_syntest::st::TestPointPtrT testPoint,
    db_simscada::sim::CtrlPtPtrT simPoint)
{
  typedef std::vector< double > VecIntT;

  // Define the test
  std::vector< double > tests;
  tests.push_back(0.0f);
  tests.push_back(0.25f);
  tests.push_back(0.50f);
  tests.push_back(0.75f);
  tests.push_back(1.0f);

  SimPointPtrT synSimPoint = testPoint->simPoint();
  const double range = synSimPoint->aoEngMax() - synSimPoint->aoEngMin();

  for (VecIntT::iterator itr = tests.begin(); itr != tests.end(); ++itr)
  {
    const double value = synSimPoint->aoEngMin() + (*itr * range);

    TestValuePtrT testValue(new TestValue());
    testValue->setId(nextId(mSynModel));
    testValue->setTestPoint(testPoint);
    testValue->setSimValue(value);
    testValue->addToModel(mSynModel);

    addSysValues(systems, testValue);
  }

}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addValuesDo(const TestSystemListT& systems,
    db_syntest::st::TestPointPtrT testPoint,
    db_simscada::sim::CtrlPtPtrT simPoint)
{
  TestValuePtrT testValue(new TestValue());
  testValue->setId(nextId(mSynModel));
  testValue->setTestPoint(testPoint);
  testValue->addToModel(mSynModel);

  addSysValues(systems, testValue);
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatch::addSysValues(const TestSystemListT& testSystems,
    db_syntest::st::TestValuePtrT testValue)
{
  st::SimPointPtrT simPoint = testValue->testPoint()->simPoint();
  assert(simPoint.get());

  for (TestSystemListT::const_iterator itr = testSystems.begin(); itr
      != testSystems.end(); ++itr)
  {
    const TestSystemPtrT& testSystem = *itr;
    SystemPtrT system = (*itr)->system();

    // Find the submap
    SysPtBtSimPtBySystemMapT::iterator
        itrSimPnt = mSysPtBtSimPtBySystem.find(system);
    if (itrSimPnt == mSysPtBtSimPtBySystem.end())
    {
      boost::format info("Test system %s has no points defined");
      info % system->name();
      logMessage(mSynModel, info.str(), errorSeverity, testSystem->batch());
      continue;
    }

    // Find the sys point
    SysPtBySimPtMapT& sysPtBySimPtMap = itrSimPnt->second;
    SysPtBySimPtMapT::iterator itrSysPnt = sysPtBySimPtMap.find(simPoint);

    if (itrSysPnt == sysPtBySimPtMap.end())
    {
      boost::format info("Test system %s isn't configured for point %s");
      info % system->name();
      info % simPoint->simAlias();

      logMessage(mSynModel, info.str(), errorSeverity, testSystem->batch());
      continue;
    }

    SysPointPtrT sysPoint = itrSysPnt->second;
    assert(sysPoint.get());

    TestSysValuePtrT testSysValue(new TestSysValue());
    testSysValue->setId(nextId(mSynModel));
    testSysValue->setTestValue(testValue);
    testSysValue->setSysPoint(sysPoint);
    testSysValue->addToModel(mSynModel);
  }
}
/// ---------------------------------------------------------------------------
