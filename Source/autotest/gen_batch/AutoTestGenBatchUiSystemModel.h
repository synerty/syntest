/*
 * AutoTestGenBatchUiSystemModel.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef _AUTO_TEST_GEN_BATCH_UI_SYSTEM_MODEL_H_
#define _AUTO_TEST_GEN_BATCH_UI_SYSTEM_MODEL_H_

#include <vector>

#include <qabstractitemmodel.h>

#include <syntest/DbSyntestDeclaration.h>

namespace autotest
{
  namespace gen_batch
  {

    namespace system_model_private
    {
      class Node;
    }

    /*! autotest::gen_batch::SystemModel Brief Description
     * Long comment about the class
     *
     */
    class SystemModel : public QAbstractItemModel
    {
      public:
        enum TeCol
        {
          ColInclude,
          ColName,
          ColAgentPort,
          ColAgentIp,
          ColSimPort,
          ColSimIp,
          ColSimAliasPostfix,
          ColCount
        };

      public:
        SystemModel(db_syntest::SynTestModelPtrT synModel);
        virtual
        ~SystemModel();

      public:
        db_syntest::st::SystemListT
        selectedSystems() const;

      public:
        // Abstract implementation
        QVariant
        headerData(int prmSection,
            Qt::Orientation prmOrientation,
            int prmRole = Qt::DisplayRole) const;

        QModelIndex
        index(int row,
            int column,
            const QModelIndex& parent = QModelIndex()) const;
        QModelIndex
        parent(const QModelIndex& index) const;

        int
        rowCount(const QModelIndex& parent = QModelIndex()) const;

        int
        columnCount(const QModelIndex& parent = QModelIndex()) const;

        bool
        hasChildren(const QModelIndex &parent = QModelIndex()) const;

        Qt::ItemFlags
        flags(const QModelIndex &index) const;

        QVariant
        data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

        bool
        setData(const QModelIndex &index,
            const QVariant &value,
            int role = Qt::EditRole);

      private:
        // ---------------
        // ObjectModel methods

        void
        setupModel();

        db_syntest::SynTestModelPtrT mSynModel;

        typedef std::vector< system_model_private::Node > NodesT;
        NodesT mNodes;

    };
  // ----------------------------------------------------------------------------

  } /* namespace gen_batch */
} /* namespace autotest */
#endif /* _AUTO_TEST_GEN_BATCH_UI_SYSTEM_MODEL_H_ */
