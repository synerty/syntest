/*
 * AutoTestGenBatch.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef AUTOTESTGENBATCH_H_
#define AUTOTESTGENBATCH_H_

#include <string>
#include <map>
#include <vector>

#include <simscada/DbSimscadaDeclaration.h>
#include <syntest/DbSyntestDeclaration.h>

namespace autotest
{

  namespace gen_batch
  {
    typedef std::vector<std::string> PointAliasesT;

    struct NewBatchConfig
    {
        std::string name;
        std::string comment;
        db_simscada::sim::RtuListT rtus;
        PointAliasesT pointAliases;
        db_syntest::st::SystemListT systems;
    };
  }
}

/*! AutoTestGenBatch Brief Description
 * Long comment about the class
 *
 */
class AutoTestGenBatch
{
  public:

  public:
    AutoTestGenBatch();
    virtual
    ~AutoTestGenBatch();

  public:
    void
    run();

  private:

    db_simscada::SimScadaModelPtrT mSimModel;
    db_syntest::SynTestModelPtrT mSynModel;

    typedef std::map< db_syntest::st::SimPointPtrT, db_syntest::st::SysPointPtrT > SysPtBySimPtMapT;
    typedef std::map< db_syntest::st::SystemPtrT, SysPtBySimPtMapT > SysPtBtSimPtBySystemMapT;

    SysPtBtSimPtBySystemMapT mSysPtBtSimPtBySystem;

    void
    generateBatch(autotest::gen_batch::NewBatchConfig config);

    db_syntest::st::TestBatchPtrT
    addBatch(const std::string& name, const std::string& comment);

    db_syntest::st::TestSystemListT
    addSystems(db_syntest::st::TestBatchPtrT& batch,
        db_syntest::st::SystemListT& systems);

    void
    addPoints(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestBatchPtrT& batch,
        db_simscada::sim::RtuListT& rtus,
        autotest::gen_batch::PointAliasesT &pointAliases);

    void
    addPoint(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestBatchPtrT& batch,
        db_simscada::sim::TelePtPtrT& point);

    void
    addValuesDi(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestPointPtrT testPoint,
        db_simscada::sim::DigPtPtrT simPoint);

    void
    addValuesAi(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestPointPtrT testPoint,
        db_simscada::sim::AnaPtPtrT simPoint);

    void
    addValuesAc(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestPointPtrT testPoint,
        db_simscada::sim::AccPtPtrT simPoint);

    void
    addValuesAo(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestPointPtrT testPoint,
        db_simscada::sim::CtrlPtPtrT simPoint);

    void
    addValuesDo(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestPointPtrT testPoint,
        db_simscada::sim::CtrlPtPtrT simPoint);

    void
    addSysValues(const db_syntest::st::TestSystemListT& systems,
        db_syntest::st::TestValuePtrT testValue);

};

#endif /* AUTOTESTGENBATCH_H_ */
