/*
 * AutoTestGenBatchUiRtuModel.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "AutoTestGenBatchUiRtuModel.h"

#include <assert.h>

#include <boost/algorithm/string.hpp>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimProtocol.h>
#include <simscada/sim/SimRtu.h>

#include "Globals.h"

using namespace db_simscada;
using namespace db_simscada::sim;

namespace autotest
{
  namespace gen_batch
  { //---------------------------------------------------------------------------
    //                           RtuModel private
    //---------------------------------------------------------------------------

    namespace rtu_model_private
    {
      struct Node
      {
          bool includeRtu;
          db_simscada::sim::RtuPtrT rtu;

          Node(RtuPtrT rtu_) :
              includeRtu(false), rtu(rtu_)
          {
          }
      };

      void
      testModel(QAbstractItemModel* model, QModelIndex parent = QModelIndex())
      {
        //    qDebug("Testing parent row=%d, col=%d, alias=%s",
        //        parent.row(),
        //        parent.column(),
        //        parent.isValid() ? model->data(parent, Qt::DisplayRole).toString().toStdString().c_str()
        //            : "");

        int rowCount = model->rowCount(parent);
        int colCount = model->columnCount(parent);

        for (int row = 0; row < rowCount; row++)
        {
          // Test the data
          for (int col = 0; col < colCount; col++)
          {

            QModelIndex index = model->index(row, col, parent);

            // Test the parent
            QModelIndex testParent = model->parent(index);
            if (testParent != parent)
              assert(testParent == parent);

            QVariant disp = model->data(index, Qt::DisplayRole);
            QVariant forCol = model->data(index, Qt::ForegroundRole);
            QVariant toolTip = model->data(index, Qt::ToolTipRole);
          }

          // Test the subtree
          QModelIndex index = model->index(row, 0, parent);
          testModel(model, index);

        }
      }
    }

    //---------------------------------------------------------------------------
    //                       RtuModel Implementation
    //---------------------------------------------------------------------------

    RtuModel::RtuModel(db_simscada::SimScadaModelPtrT simModel) :
        mSimModel(simModel)
    {
      setupModel();

//#ifdef DEBUG
//      rtu_model_private::testModel(this);
//      qDebug("Model tested, no errors found");
//#endif

    } //---------------------------------------------------------------------------

    RtuModel::~RtuModel()
    {
    }
    //---------------------------------------------------------------------------

    db_simscada::sim::RtuListT
    RtuModel::selectedRtus() const
    {
      db_simscada::sim::RtuListT rtus;
      for (NodesT::const_iterator itr = mNodes.begin(); itr != mNodes.end();
          ++itr)
      {
        if (itr->includeRtu)
          rtus.push_back(itr->rtu);
      }
      return rtus;
    }
    //---------------------------------------------------------------------------

    // Abstract implementation
    QVariant
    RtuModel::headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole) const
    {
      if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
        return QVariant();

      switch (prmSection)
      {
        case ColInclude:
          return "Test ?";
        case ColProtocol:
          return "Protocol";
        case ColAlias:
          return "Alias";
        case ColName:
          return "Name";
        case ColChannel:
          return "Channel";
        default:
          assert(false);
          break;
      }

      return QVariant();
    }
    //---------------------------------------------------------------------------

    QModelIndex
    RtuModel::index(int row, int column, const QModelIndex& parent) const
    {
      assert(row >= 0 && row < mNodes.size());

      return createIndex(row, column);
    }
    //---------------------------------------------------------------------------

    QModelIndex
    RtuModel::parent(const QModelIndex& index) const
    {
      return QModelIndex();
    }
    //---------------------------------------------------------------------------

    int
    RtuModel::rowCount(const QModelIndex& parent) const
    {
      return mNodes.size();
    }
    //---------------------------------------------------------------------------

    int
    RtuModel::columnCount(const QModelIndex& parent) const
    {
      return ColCount;
    }
    //---------------------------------------------------------------------------

    bool
    RtuModel::hasChildren(const QModelIndex& parent) const
    {
      return false;
    }
    //---------------------------------------------------------------------------

    Qt::ItemFlags
    RtuModel::flags(const QModelIndex &index) const
    {
      Qt::ItemFlags f = QAbstractItemModel::flags(index);

      if (index.column() == ColInclude)
      {
        f |= Qt::ItemIsUserCheckable;
        f |= Qt::ItemIsEditable;
      }
      return f;
    }
    //---------------------------------------------------------------------------

    QVariant
    RtuModel::data(const QModelIndex& index, int role) const
    {
      // We only provide certain data.
      if (!index.isValid() //
          || (role != Qt::DisplayRole && role != Qt::ToolTipRole
              && role != Qt::CheckStateRole && role != Qt::BackgroundRole)
          || !(0 <= index.column() && index.column() < ColCount)
          || !(0 <= index.row() && index.row() < mNodes.size()))
      {
        return QVariant();
      }

      const rtu_model_private::Node& node = mNodes[index.row()];

      if (role == Qt::BackgroundRole)
      {
        if (node.includeRtu)
          return QBrush(QColor("LightGreen"));

        return QVariant();
      }

      switch (index.column())
      {
        case ColInclude:
          if (role == Qt::CheckStateRole)
            return (node.includeRtu ? Qt::Checked : Qt::Unchecked);
          return QVariant();

        case ColProtocol:
          if (role == Qt::DisplayRole)
            return QString(node.rtu->protocol()->name().c_str());
          return QVariant();

        case ColAlias:
          if (role == Qt::DisplayRole)
            return QString(node.rtu->alias().c_str());
          return QVariant();

        case ColName:
          if (role == Qt::DisplayRole)
            return QString(node.rtu->name().c_str());
          return QVariant();

        case ColChannel:
          if (role == Qt::DisplayRole)
          {
            assert(node.rtu.get());
            assert(node.rtu->parent().get());
            return QString(node.rtu->parent()->name().c_str());
          }
          return QVariant();

        case ColCount:
          assert(false);
          break;
      }

      assert(false);
      return QVariant();
    }
    //---------------------------------------------------------------------------

    bool
    RtuModel::setData(const QModelIndex &index, const QVariant &value, int role)
    { // We only provide certain data.
      assert(0 <= index.row() && index.row() < mNodes.size());

      rtu_model_private::Node& node = mNodes[index.row()];

      node.includeRtu = value.toUInt() == Qt::Checked;

      QModelIndex topLeft = createIndex(index.row(), 0);
      QModelIndex bottomRight = createIndex(index.row(), ColCount - 1);
      dataChanged(topLeft, bottomRight);
      return true;
    }
    //---------------------------------------------------------------------------

    void
    RtuModel::setupModel()
    {
      const RtuListT& rtus = mSimModel->SimRtus();
      mNodes.clear();

      for (RtuListT::size_type i = 0; i < rtus.size(); ++i)
      {
        if (boost::ends_with(rtus[i]->alias(), DUAL_ALIAS_TAG))
          continue;

        mNodes.push_back(rtu_model_private::Node(rtus[i]));
      }
    }
  //---------------------------------------------------------------------------

  } /* namespace gen_batch */
} /* namespace autotest */
