#include "AutoTestGenBatchUi.h"

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimRtu.h>

using namespace db_simscada;
using namespace db_simscada::sim;

#include <syntest/SynTestModel.h>
#include <syntest/st/StTestBatch.h>
#include <syntest/st/StSystem.h>
#include <syntest_util/DbSyntestUtil.h>

using namespace db_syntest;
using namespace db_syntest::st;

#include <QtCore/qabstractitemmodel.h>
#include <QtGui/qsortfilterproxymodel.h>
#include <QtGui/qmessagebox.h>

#include "MainSimModel.h"
#include "MainSynModel.h"
#include "AutoTestGenBatch.h"
#include "AutoTestGenBatchUiRtuModel.h"
#include "AutoTestGenBatchUiSystemModel.h"

#include "Globals.h"

using namespace autotest::gen_batch;

/// ---------------------------------------------------------------------------
/// AutoTestGenBatchUi implementation
/// ---------------------------------------------------------------------------

AutoTestGenBatchUi::AutoTestGenBatchUi() :
  QDialog(MsgBoxParent())
{
  ui.setupUi(this);
  connect(ui.btnGenerate, SIGNAL(clicked()), this, SLOT(generateClicked()));
  connect(ui.btnCancel, SIGNAL(clicked()), this, SLOT(reject()));

  mQtRtuModel.reset(new RtuModel(MainObjectModel::Inst().ormModel()));

  QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
  proxyModel->setSourceModel(mQtRtuModel.get());
  ui.tblRtus->setModel(proxyModel);

  mQtSystemModel.reset(new SystemModel(MainSynModel::Inst().ormModel()));

  QSortFilterProxyModel *sysProxyModel = new QSortFilterProxyModel(this);
  sysProxyModel->setSourceModel(mQtSystemModel.get());
  ui.tblSystems->setModel(sysProxyModel);
}
/// ---------------------------------------------------------------------------

AutoTestGenBatchUi::~AutoTestGenBatchUi()
{

}

autotest::gen_batch::NewBatchConfig
AutoTestGenBatchUi::data()
{
  autotest::gen_batch::NewBatchConfig cfg;
  cfg.name = ui.txtName->text().toStdString();
  cfg.comment = ui.txtComment->document()->toPlainText().toStdString();
  cfg.rtus = mQtRtuModel->selectedRtus();
  cfg.pointAliases = pointAliases();
  cfg.systems = mQtSystemModel->selectedSystems();
  return cfg;
}
/// ---------------------------------------------------------------------------

void
AutoTestGenBatchUi::generateClicked()
{
  bool valid = validateName() && validateRtus() && validateSystems();

  if (valid)
    accept();
}
/// ---------------------------------------------------------------------------

bool
AutoTestGenBatchUi::validateName()
{
  // Validate the name
  std::string name = ui.txtName->text().toStdString();
  if (name.empty())
  {
    QMessageBox::critical(MsgBoxParent(),
        "Invalid Name",
        "Please enter a name for the new test batch");
    return false;
  }

  const bool
      exists = isTestBatchNameUsed(MainSynModel::Inst().ormModel(), name);
  if (exists)
  {
    QMessageBox::critical(MsgBoxParent(),
        "Invalid Name",
        "Test batch name already exists");
    return false;
  }

  return true;
}
/// ---------------------------------------------------------------------------

bool
AutoTestGenBatchUi::validateRtus()
{
  if (mQtRtuModel->selectedRtus().empty())
  {
    QMessageBox::critical(MsgBoxParent(),
        "No RTUs Selected",
        "Please select one or more RTUs to generate a test batch for");
    return false;
  }

  return true;
}
/// ---------------------------------------------------------------------------

bool
AutoTestGenBatchUi::validateSystems()
{
  if (mQtSystemModel->selectedSystems().empty())
  {
    QMessageBox::critical(MsgBoxParent(),
        "No Systems Selected",
        "Please select one or more Systems to generate a test batch for");
    return false;
  }

  return true;
}
/// ---------------------------------------------------------------------------

PointAliasesT
AutoTestGenBatchUi::pointAliases()
{
  PointAliasesT result;
  QString qStr = ui.txtPointAliases->toPlainText();
  QStringList qList = qStr.split(QRegExp("\n|\r\n|\r"));

  for (int i = 0; i < qList.count(); i++)
  {
    qStr = qList[i].trimmed();
    std::string sStr = qStr.toStdString();

    if (qStr.toLower().contains(QString("http")))
      continue;

    if (sStr.empty())
      continue;

    result.push_back(sStr);
  }
  return result;
}

/// ---------------------------------------------------------------------------
