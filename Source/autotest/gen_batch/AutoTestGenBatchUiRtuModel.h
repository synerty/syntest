/*
 * AutoTestGenBatchUiRtuModel.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef AUTOTESTGENBATCHUIRTUMODEL_H_
#define AUTOTESTGENBATCHUIRTUMODEL_H_

#include <vector>

#include <qabstractitemmodel.h>

#include <simscada/DbSimscadaDeclaration.h>

namespace autotest
{
  namespace gen_batch
  {

    namespace rtu_model_private
    {
      class Node;
    }

    /*! autotest::gen_batch::RtuModel Brief Description
     * Long comment about the class
     *
     */
    class RtuModel : public QAbstractItemModel
    {
      public:
        enum TeCol
        {
          ColInclude,
          ColProtocol,
          ColAlias,
          ColName,
          ColChannel,
          ColCount
        };

      public:
        RtuModel(db_simscada::SimScadaModelPtrT simModel);
        virtual
        ~RtuModel();

      public:
        db_simscada::sim::RtuListT
        selectedRtus() const;

      public:
        // Abstract implementation
        QVariant
        headerData(int prmSection,
            Qt::Orientation prmOrientation,
            int prmRole = Qt::DisplayRole) const;

        QModelIndex
        index(int row,
            int column,
            const QModelIndex& parent = QModelIndex()) const;
        QModelIndex
        parent(const QModelIndex& index) const;

        int
        rowCount(const QModelIndex& parent = QModelIndex()) const;

        int
        columnCount(const QModelIndex& parent = QModelIndex()) const;

        bool
        hasChildren(const QModelIndex &parent = QModelIndex()) const;

        Qt::ItemFlags
        flags(const QModelIndex &index) const;

        QVariant
        data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

        bool
        setData(const QModelIndex &index,
            const QVariant &value,
            int role = Qt::EditRole);

      private:
        // ---------------
        // ObjectModel methods

        void
        setupModel();

        db_simscada::SimScadaModelPtrT mSimModel;

        typedef std::vector< rtu_model_private::Node > NodesT;
        NodesT mNodes;

    };
  // ----------------------------------------------------------------------------

  } /* namespace gen_batch */
} /* namespace autotest */
#endif /* AUTOTESTGENBATCHUIRTUMODEL_H_ */
