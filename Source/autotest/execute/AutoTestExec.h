/*
 * AutoTestExec.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef AUTOTESTEXEC_H_
#define AUTOTESTEXEC_H_

#include <string>
#include <map>
#include <stdint.h>

#include <QtCore/qobject.h>

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

#include <syntest/DbSyntestDeclaration.h>

#include "MainObserver.h"
#include "MainUiCenter.h"
#include "AutoTestExecProcessorObserver.h"
#include "AutoTestExecDeclarations.h"

namespace autotest
{
  namespace exec
  {
    class ExecUi;
    class TestQueue;
    class Summary;
    class Processor;
  }
}

/*! autotest::Exec Brief Description
 * Long comment about the class
 *
 */
class AutoTestExec : public QObject,
    public MainObserver,
    public MainUiCenter,
    public autotest::exec::ProcessorObserver,
    public boost::enable_shared_from_this< AutoTestExec >,
    DebugClassMixin< AutoTestExec >
{
  Q_OBJECT

  public:
    typedef boost::shared_ptr< AutoTestExec > PtrT;

  public:
    AutoTestExec();
    virtual
    ~AutoTestExec();

    static void
    launch();

  public:
    void
    run();

  public:

    void
    testBatchSelected(const std::string name);

    void
    testRunSelected(const int runNumber);

  public slots:
    // ---------------
    // Ui Interface

    void
    close();

    void
    start();

    void
    stop();

    void
    newTestRun();

  private:
    // ---------------
    // MainUiCenter implementation

    void
    mainUiCenterClose();

  private:
    // ---------------
    // MainObserver implementation

    void
    mainUiModeChanged(main_ui::UiModeE oldUiMode, main_ui::UiModeE newUiMode);

  private:
    /// ---------------
    /// autotest::exec::ProcessorObserver implementation

    void
    processorTestingStarted();

    void
    processorTestingStopped(const autotest::exec::TestStopReasonE reason,
        const std::string message);

  private:

    /*! Initialise UI
     * Initialises the UI and the controller classes that run it
     */
    void
    init();

    /*! Load Test Baches
     * Loads the test batches names into the ui to choose from
     */
    void
    loadTestBatches();

    /*! Load Test Runs
     * Loads the test runs for this batch into the UI
     * @param name Batch Name
     */
    void
    loadTestRuns(const std::string name);

    /*! Load Test Run
     * Finally loads the test run data into the tester
     * @param runNumber
     */
    void
    loadTestRun(const int runNumber);

    /*! Get Batch
     * Converts the batch name to the batch object
     * @param batchName the name of the batch
     * @return the orm object
     */
    db_syntest::st::TestBatchPtrT
    getBatch(const std::string batchName);

    /*! Get Run
     * Converts the run number test run object
     * @param runNumber the name of the batch
     * @return the orm object
     */
    db_syntest::st::TestRunPtrT
    getRun(const int runNumber);

  private:
    boost::shared_ptr< autotest::exec::ExecUi > mUi;
    boost::shared_ptr< autotest::exec::TestQueue > mTestQueue;
    boost::shared_ptr< autotest::exec::Summary > mSummary;
    boost::shared_ptr< autotest::exec::Processor > mProcessor;

    bool mInitialising;

    db_syntest::SynTestModelPtrT mSynModel;

    typedef std::map< std::string, uint32_t > BatchIdByNameT;
    BatchIdByNameT mBatchIdByName;

    typedef std::map< int, uint32_t > RunIdByNumberT;
    RunIdByNumberT mRunIdByNumber;
};

#endif /* AUTOTESTEXEC_H_ */
