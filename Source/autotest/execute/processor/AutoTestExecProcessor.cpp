/*
 * AutoTestExecProcessor.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#undef UNICODE

#include "AutoTestExecProcessor.h"

#include <vector>
#include <time.h>

#include <boost/bind.hpp>

#include <QtCore/qstring.h>
#include <QtGui/qmessagebox.h>
#include <QtGui/qclipboard.h>

#include <syntest/SynTestModel.h>
#include <syntest/st/StSystem.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestBatch.h>
#include <syntest/st/StTestSystem.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestRun.h>
#include <syntest/st/StTestValueRun.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Main.h"
#include "MainSynModel.h"
#include "Proactor.h"

#include "AutoTestExecProcessorObserver.h"
#include "SimDispatcherProxy.h"
#include "AgentDispatcher.h"
#include "AutoTestExecProcessorSystem.h"

// Excplitit instantiation
#include "Observable.ini"
template class Observable< autotest::exec::ProcessorObserver > ;

using namespace db_syntest;
using namespace autotest::exec::processor;

namespace autotest
{
  namespace exec
  {

    struct StSytemsSortBySimDetailsLt
    {
        bool
        operator()(const db_syntest::st::SystemPtrT o1,
            const db_syntest::st::SystemPtrT o2) const
        {
          assert(o1.get() && o2.get());
          if (o1->simulatorIp() != o2->simulatorIp())
            return o1->simulatorIp() < o2->simulatorIp();

          return o1->simulatorPort() < o2->simulatorPort();
        }
    };

    typedef std::map< db_syntest::st::SystemPtrT, SimDispatcherProxyPtrT,
        StSytemsSortBySimDetailsLt > SimProxiesByDetailsMapT;

    // ------------------------------------------------------------------------
    // Processor::SystemData Implementation
    // ------------------------------------------------------------------------

    Processor::SystemData::SystemData() :
      state(idleSystemWorkerState)
    {
    }

    // ------------------------------------------------------------------------
    // Processor Implementation
    // ------------------------------------------------------------------------

    Processor::Processor(QWidget* panel) :
      mTestingState(testingStopped), //
          mPanel(panel), //
          mSynOrmModel(MainSynModel::Inst().ormModel()), //
          mProactor(Main::Inst().proactor()),//
          mLastTestPointId("")
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      connect(this,
          SIGNAL(moveToNextTestValueSignal()),
          this,
          SLOT(moveToNextTestValueSlot()),
          Qt::QueuedConnection);
      connect(this,
          SIGNAL(interruptSignal()),
          this,
          SLOT(interruptSlot()),
          Qt::QueuedConnection);
FUNCTION_END    ( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  Processor::~Processor()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    FUNCTION_END( __PRETTY_FUNCTION__ )
  }
  // ------------------------------------------------------------------------

  /// ---------------
  /// Processor Stuff

  void
  Processor::run()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    assert(mTestQueueModel.get());
    assert(mProactor.get());

    startTest();
    notifyOfSomthing(&ProcessorObserver::processorTestingStarted);
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  void
  Processor::stop()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    assert(mProactor.get());
    mTestingState = testingStopping;
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  void
  Processor::close()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    assert(mProactor.get());
    reset();
    FUNCTION_END( __PRETTY_FUNCTION__)

  }
  // ------------------------------------------------------------------------

  void
  Processor::interrupt()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    emit interruptSignal();
    FUNCTION_END( __PRETTY_FUNCTION__)

  }
  // ------------------------------------------------------------------------

  /// ---------------
  /// TestQueueObserver observer implementation

  void
  Processor::testQueueModelChanged(test_queue::ModelPtrT model)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    if (mTestingState == testingRunning)
    {
      notifyOfTestingStopped(modelChangedTestStopReason, "");
      mTestingState = testingStopped;
    }

    reset();
    mTestQueueModel = model;
    initialise();
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  /// ---------------
  /// Processor Stuff (this class)

  void
  Processor::reset()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)

    if (mTestingState == testingRunning)
    {
      notifyOfTestingStopped(unknownTestStopReason,
          "Reset called while still testing");
      mTestingState = testingStopped;
    }

    mSimDispatchers.clear();

    // The software SIGSEGVs if this isn't done like this.
    while (!mSystems.empty())
    {
      mSystems.begin()->second.sysRecord.reset();
      mSystems.begin()->second.sysWorker.reset();
      mSystems.erase(mSystems.begin());
    }

    mStrand.reset();

    mTestingState = testingStopped;

    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ---------------------------------------------------------------------------


  bool sortTestSystemList (st::TestSystemPtrT a, st::TestSystemPtrT b)
  {
    assert(a.get() && !a->isSystemNull());
    assert(b.get() && !b->isSystemNull());
    return (a->system() < b->system());
  }

  void
  Processor::initialise()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    mStrand.reset(new boost::asio::strand(mProactor->ioService()));

    /* Map for combining sim proxies
     * The comparitor for this map ensures one SimDispatcherProxy will
     * be created and used for each simSCADA instance.
     *
     * Multiple systems simulated with the same simSCADA will be assigned the
     * same SimDispatcherProxy.
     */
    SimProxiesByDetailsMapT simProxiesMap;

    // Iterate through the systems in our batch and create System Workers for each
    st::TestSystemListT testSystems;
    mTestQueueModel->testRun()->batch()->testSystemBatchs(testSystems);

    std::sort(testSystems.begin(), testSystems.end(), sortTestSystemList);

    for (st::TestSystemListT::iterator itr = testSystems.begin();
        itr != testSystems.end(); ++itr)
    {
      // Get the system record
      st::SystemPtrT systemRecord = (*itr)->system();
      assert(systemRecord.get());

      // Create / reuse the SimDispatcher
      SimDispatcherProxyPtrT simDispatcher;
      SimProxiesByDetailsMapT::iterator itrSimFind = simProxiesMap.find(systemRecord);
      if (itrSimFind != simProxiesMap.end())
      {
        simDispatcher = itrSimFind->second;
      }
      else
      {
        simDispatcher.reset(new SimDispatcherProxy(mProactor,
                systemRecord->simulatorIp(),
                systemRecord->simulatorPort()));
        mSimDispatchers.push_back(simDispatcher);
        simProxiesMap.insert(std::make_pair(systemRecord, simDispatcher));

        // Add this processesor as an agent dispatcher observer, so we know when its closed its connection
        simDispatcher->addObserver(shared_from_this());
      }

      // Create the system worker data struct
      assert(mSystems.find(systemRecord->name()) == mSystems.end());
      SystemData& systemData = mSystems[systemRecord->name()];

      systemData.sysRecord = systemRecord;

      // Create the agent dispatcher
      boost::shared_ptr< agent::AgentDispatcher > agentDispatcher(new agent::AgentDispatcher(systemRecord,
              mProactor,
              systemRecord->agentIp(),
              systemRecord->agentPort()));

      // Create the System worker
      systemData.sysWorker.reset(new processor::System(shared_from_this(),
              mProactor,
              simDispatcher,
              agentDispatcher,
              systemRecord,
              mPanel));

      // Add the system worker as an observer to the agent dispatcher
      agentDispatcher->addObserver(systemData.sysWorker);

      // Add this processesor as an agent dispatcher observer,
      // So we can stop the test if the connection closes
      agentDispatcher->addObserver(shared_from_this());

      // Tell the simdispatcher about the new system worker using it
      simDispatcher->addSystem(systemData.sysWorker);
    }
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  /// ---------------
  /// Processor Stuff - System controll and state machine
  /// Interface used by the System objects

  void
  Processor::moveToNextTestValueSlot()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    // Save any test result changes
    mSynOrmModel->beginTransaction();
    mSynOrmModel->save();
    mSynOrmModel->commitTransaction();

    // Check if we need stop or keep going
    if (mTestingState == testingStopped)
    {
      return;
    }
    else if (mTestingState == testingStopping)
    {
      notifyOfTestingStopped(userTestStopReason);
      mTestingState = testingStopped;
      return;
    }

    assert(mTestingState == testingRunning);

    // Get the current test value
    test_queue::TqValuePtrT tqValue = mTestQueueModel->currentTestValue();

    // Notify objects that care
    if (tqValue->passed())
    mTestQueueModel->notifyOfCurrentTestComplete(test_queue::pointValuePassed);
    else
    mTestQueueModel->notifyOfCurrentTestComplete(test_queue::pointValueFailed);

    // Now move on and start testing the next point
    if (mTestQueueModel->moveToNextTestValue())
    {
      startTest();
    }
    else
    {
      stop();
      notifyOfTestingStopped(completedTestStopReason);
    }
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  void
  Processor::interruptSlot()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    stop();
    //        reset();
    //        initialise();
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ------------------------------------------------------------------------

  void
  Processor::postedSystemTestComplete(const std::string systemName)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    // Repost the call using our strand
    mStrand->post(bind(&Processor::strandedSystemTestComplete,
            shared_from_this(),
            systemName));
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  /// -----------------------------------------------------------------------

  /// ---------------
  /// SimDllDispatcherObserver interface implementation

  void
  Processor::postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
      std::string errorMessage)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    if (!(newState == tcp_client::connectionBrokenState
            || newState == tcp_client::disconnectedState))
    return;

    if (mTestingState != testingRunning)
    return;

    notifyOfTestingStopped(simulatorDisconnectTestStopReason, errorMessage);
    interrupt();

    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  /// -----------------------------------------------------------------------


  // Send keystrokes to Diagram
  int sendPointIdToPofDiagram(const char* text)
  {
    assert( text != NULL );

    //Get the handle of the Notepad window.
    HWND hvDiagram = FindWindow( "WindowsForms10.Window.8.app.0.2004eee", NULL);
    if( hvDiagram == NULL )
    return 0;

    //Get the handle of the Notepad window's searchText control.
    HWND searchText = FindWindowEx( hvDiagram, NULL,
        "WindowsForms10.EDIT.app.0.2004eee1" , NULL );
    if( searchText == NULL )
    return 0;

    SendMessage( searchText, EM_REPLACESEL, ( WPARAM )TRUE, ( LPARAM )text );
    return 1;
  }

  /// ---------------
  /// AgentDispatcherObserver interface implementation

  void
  Processor::postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
      std::string errorMessage)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    if (!(newState == agent::connectionBrokenAgentState
            || newState == agent::disconnectedAgentState
            || newState == agent::downloadAgentState))
    return;

    if (mTestingState != testingRunning)
    return;

    notifyOfTestingStopped(agentDisconnectTestStopReason, errorMessage);
    interrupt();

    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  /// -----------------------------------------------------------------------

  /// ---------------
  /// Observerable stuff

  void
  Processor::notifyOfTestingStopped(const TestStopReasonE reason,
      const std::string message)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    ObserverPtrsT observerPtrs;
    getObserverPtrs(observerPtrs);
    for (ObserverPtrsT::iterator itr = observerPtrs.begin();
        itr != observerPtrs.end(); ++itr)
    (*itr)->processorTestingStopped(reason, message);
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  /// -----------------------------------------------------------------------

  /// ---------------
  /// Processor Stuff - System controll and state machine

  void
  Processor::startTest()
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    mTestingState = testingRunning;

    test_queue::TqValuePtrT tqValue = mTestQueueModel->currentTestValue();
    if (!tqValue.get())
    {
      qDebug("Processor::startTest, Stopping test, tqValue is null");
      stop();
      notifyOfTestingStopped(noCurrentPointTestStopReason);
      return;
    }
    assert(tqValue.get());

    // If we have a test run for this test then use it, else create one
    if (tqValue->testValueRun.get())
    {
      tqValue->testValueRun->setDate(time(NULL));
    }
    else
    {
      st::TestValueRunPtrT valRun(new st::TestValueRun());
      valRun->setId(nextId(mSynOrmModel));
      valRun->setTestRun(mTestQueueModel->testRun());
      valRun->setTestValue(tqValue->testValue);
      valRun->setDate(time(NULL));
      valRun->addToModel(mSynOrmModel);
      tqValue->testValueRun = valRun;
    }

    bool systemInitialised = false;

    // Initialise SimDispatchers
    for (SimDispatcherListT::iterator itr = mSimDispatchers.begin();
        itr != mSimDispatchers.end(); ++itr)
    (*itr)->initialiseTest(tqValue);

    /* Initialise system workers
     * Iterate through all the TqSysValues, match with the system and initialise
     * the correct system worker with the correct value.
     * Loop in a loop? There should only be a couple in each list
     */
    for (SystemWorkerMapT::iterator sysItr = mSystems.begin();
        sysItr != mSystems.end(); ++sysItr)
    {
      SystemData& sysData = sysItr->second;

      // Reset the workers
      sysData.sysWorker->resetTest();

      // Find the test value for this system and initialise the system with it
      for (test_queue::TqSysValueListT::iterator valItr = tqValue->sysValues.begin();
          valItr != tqValue->sysValues.end(); ++valItr)
      {
        test_queue::TqSysValuePtrT tqSysVal = *valItr;
        assert(tqSysVal.get());
        assert(tqSysVal->testSysValue.get());

        st::SystemPtrT sysRecord = tqSysVal->testSysValue->sysPoint()->system();

        if (sysData.sysRecord == sysRecord)
        {
          // Matching TestSysValue found, Great. Now initilise the system worker with it
          sysData.state = workingSystemWorkerState;
          sysData.sysWorker->initialiseTest(tqSysVal);
          systemInitialised |= true;
          break;
        }
      }
    }

    // Log warnings when the there are no test values for this system.
    if (!systemInitialised)
    {
      boost::format info("None of the systems have point %s,"
          " value %f configured for testing");
      info % tqValue->testValue->testPoint()->simPoint()->simAlias();
      info % tqValue->testValue->simValue();

      logMessage(mSynOrmModel,
          info.str(),
          warningSeverity,
          mTestQueueModel->testRun()->batch(),
          mTestQueueModel->testRun());
      emit
      moveToNextTestValueSignal();
      return;
    }

    assert(tqValue->sysValues.size());
    assert(tqValue->sysValues.front().get());
    assert(tqValue->sysValues.front()->testSysValue.get());
    assert(tqValue->sysValues.front()->testSysValue->sysPoint().get());

    st::SimPointPtrT simPoint = tqValue->testValue->testPoint()->simPoint();
    st::SysPointPtrT sysPoint = tqValue->sysValues.front()->testSysValue->sysPoint();
    const bool hasManualResultor = sysPoint->requiresUser()
        && (toSimPointType(simPoint) == di1SimPointType
            || toSimPointType(simPoint) == di2SimPointType
            || toSimPointType(simPoint) == aiSimPointType
            || toSimPointType(simPoint) == acSimPointType);

    // All systems workers now initialised
    if (hasManualResultor)
    {
      st::SysPointPtrT sysPoint = tqValue->sysValues.front()->testSysValue->sysPoint();
      std::string pointId = sysPoint->pid();

      if (mLastTestPointId != pointId)
      {
        mLastTestPointId = pointId;

        // Clipboard copy
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(pointId.c_str());

        // Try to locate it on the HV Diagram
        // sendPointIdToPofDiagram(pointId.c_str());

        // Popup
        QString qstr = "The point name ";
        qstr.append(pointId.c_str());
        qstr.append(" has been copied to clipboard.\n");
        qstr.append("Please locate it before I change it.");

        QMessageBox msgBox;
        msgBox.setText(qstr);
        msgBox.exec();
      }
    }

    // start the tests
    qDebug("Processor - posting system start calls ");
    for (SystemWorkerMapT::iterator itr = mSystems.begin();
        itr != mSystems.end(); ++itr)
    {
      mProactor->ioService().post(bind(&System::postedStart,
              itr->second.sysWorker));
    }
    FUNCTION_END( __PRETTY_FUNCTION__)

  }
  // ----------------------------------------------------------------------------

  void
  Processor::strandedSystemTestComplete(const std::string systemName)
  {
    FUNCTION_START( __PRETTY_FUNCTION__)
    assert(mSystems.find(systemName) != mSystems.end());
    SystemData& systemData = mSystems[systemName];

    systemData.state = idleSystemWorkerState;

    bool allComplete = true;
    for (SystemWorkerMapT::iterator itr = mSystems.begin(); //
        itr != mSystems.end(); //
        ++itr)
    allComplete &= (itr->second.state == idleSystemWorkerState);

    if (!allComplete)
    return;

    qDebug("Processor::postedAllSystemsCompleteCheck"
        " - All system workers are idle now");

    emit
    moveToNextTestValueSignal();
    FUNCTION_END( __PRETTY_FUNCTION__)
  }
  // ----------------------------------------------------------------------------

} /* namespace exec */
} /* namespace autotest */
