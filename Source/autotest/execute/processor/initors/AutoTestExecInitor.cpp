/*
 * AutoTestInitor.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecInitor.h"

#include <QtGui/qlabel.h>
#include <QtGui/qlayout.h>
#include <QtGui/qapplication.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Macros.h"
#include "Proactor.h"
#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecInitorSim.h"
#include "AutoTestExecInitorManual.h"
#include "AutoTestExecInitorAgent.h"
#include "AutoTestExecResultor.h"
#include "AutoTestExecTestQueueModel.h"

#include "AgentDispatcher.h"

#include "Macros.h"

using namespace db_syntest;

namespace autotest
{
  namespace exec
  {

    namespace processor
    {
      Initor::Initor(InitorData& data) :
          mData(data) //
          , mTimer(data.proactor->ioService()) //
          , mUiUpdateTimer(data.proactor->ioService()) //
          , mStrand(data.proactor->ioService()) //
          , mState(idleInitorState)
      {
        mTimer.expires_from_now(boost::posix_time::seconds(0));
        moveToThread(QApplication::instance()->thread());
        connect(this,
            SIGNAL(tearDownUiSignal()),
            this,
            SLOT(tearDownUiSlot()),
            Qt::BlockingQueuedConnection);

        st::TestValuePtrT testValue = mData.tqSysValue->testSysValue->testValue();
        mTimeout = testValue->testPoint()->simPoint()->feedbackTimeout();
      }

      Initor::~Initor()
      {
      }

      void
      Initor::reset()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mStrand.post(boost::bind(&Initor::strandedReset, getThis()));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      Initor::setResultor(boost::shared_ptr< Resultor > resultor)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mResultor = resultor;
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      /// ---------------
      /// Resultor functions for binding

      void
      Initor::strandedReset()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mState = resetInitorState;
        mUiUpdateTimer.cancel();
        emit
        tearDownUiSignal();
        mData = ResultorData();
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      /// ---------------
      /// Initor interface

      void
      Initor::postedPrecheck()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mState = resettingTestInitorState;
        qDebug("Initor %s prechecking", mData.systemRecord->name().c_str());
        precheckComplete();
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      InitorPtrT
      Initor::create(InitorData data, QWidget* panel)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        assert(data.tqSysValue.get());
        assert(data.tqSysValue->testSysValue.get());

        st::TestValuePtrT testValue = data.tqSysValue->testSysValue->testValue();
        st::SimPointPtrT simPoint = testValue->testPoint()->simPoint();

        const bool agentEnabled = !(data.systemRecord->isAgentIpNull()
            || data.systemRecord->isAgentPortNull());

        assert(testValue.get());
        assert(simPoint.get());

        switch (toSimPointType(simPoint))
        {
          case di1SimPointType:
          case di2SimPointType:
          case acSimPointType:
          case aiSimPointType:
          {
            boost::shared_ptr< InitorSim > initor(new InitorSim(data, panel));
            initor->startUiUpdateTimer();
            return initor;
          }

          case doSimPointType:
          case aoSimPointType:
          {
            st::SysPointPtrT sysPoint = data.tqSysValue->testSysValue->sysPoint();
            if (sysPoint->requiresUser() || !agentEnabled)
            {
              boost::shared_ptr< InitorManual > initor(new InitorManual(data,
                  panel));
              initor->startUiUpdateTimer();
              return initor;
            }
            else
            {
              boost::shared_ptr< InitorAgent > initor(new InitorAgent(data,
                  panel));
              initor->startUiUpdateTimer();

              // InitorAgent is a observer of agentDispatcher
              data.agentDispatcher->addObserver(initor);

              return initor;
            }
          }

          THROW_UNHANDLED_CASE_EXCEPTION
        }

        assert(false);
        FUNCTION_END( __PRETTY_FUNCTION__ )
        return InitorPtrT();
      }

      void
      Initor::tearDownUiSlot()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        assert(mWidget);
        mWidget->setVisible(false);
        mWidget->deleteLater();
        mWidget = NULL;
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      Initor::precheckComplete(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        // Nothing to for Agent Controls
        mData.proactor->ioService().post(bind(&System::postedInitorPrecheckCompleted,
            mData.system,
            success));

        mState = idleInitorState;
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      Initor::executeComplete(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        boost::shared_ptr< Resultor > resultor = mResultor.lock();
        assert(resultor.get());

        // Tell the system we've completed the execute
        mData.proactor->ioService().post(bind(&Resultor::postedInitorExecuteCompleted,
            resultor,
            success));

        mState = idleInitorState;
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      std::string
      Initor::stateStr() const
      {
//        FUNCTION_START( __PRETTY_FUNCTION__ )
        std::string str;

        switch (mState)
        {
          case idleInitorState:
            str = "Initor at idle";
            break;

          case resettingTestInitorState:
            str = "Resetting test";
            break;

          case executingTestInitorState:
            str = "Executing change";
            break;

          THROW_UNHANDLED_CASE_EXCEPTION
        }

        double remainingTime = mTimer.expires_from_now().total_milliseconds()
            / 1000.0f;

        if (remainingTime <= 0.0f)
          return str;

        boost::format status("%s  (%.3fs Remaining)");
        status % str;
        status % remainingTime;

        return status.str();
//        FUNCTION_END( __PRETTY_FUNCTION__ )
//        return "";
      }

      void
      Initor::startUiUpdateTimer()
      {
//        FUNCTION_START( __PRETTY_FUNCTION__ )
        mUiUpdateTimer.expires_from_now(boost::posix_time::millisec(250));
        mUiUpdateTimer.async_wait(mStrand.wrap(bind(&Initor::strandedUiUpdateTimerExpired,
            getThis())));
//        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      Initor::strandedUiUpdateTimerExpired()
      {
//        FUNCTION_START( __PRETTY_FUNCTION__ )
        // Check if we've been reset
        if (!mData.system.get())
          return;

        emit
        uiRefreshTick();

        startUiUpdateTimer();
//        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
