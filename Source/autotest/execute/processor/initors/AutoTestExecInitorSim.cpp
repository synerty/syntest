#include "AutoTestExecInitorSim.h"

#include <QtGui/qwidget.h>

#include <syntest/st/StSystem.h>

#include "Proactor.h"
#include "AutoTestExecProcessorSystem.h"
#include "SimDispatcherProxy.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      InitorSim::InitorSim(InitorData& data, QWidget* panel) :
          Initor(data)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        connect(this,
            SIGNAL(setupUiSignal(QWidget*)),
            this,
            SLOT(setupUiSlot(QWidget*)),
            Qt::BlockingQueuedConnection);

        emit setupUiSignal(panel);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      InitorSim::~InitorSim()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::setupUiSlot(QWidget* panel)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mWidget = new QWidget(panel);
        ui.setupUi(mWidget);
        panel->layout()->addWidget(mWidget);

        connect(this,
            SIGNAL(uiRefreshTick()),
            this,
            SLOT(refreshUiSlot()),
            Qt::QueuedConnection);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::refreshUiSlot()
      {
        if (!mWidget)
          return;
        ui.txtStatus->setText(stateStr().c_str());
      }

      void
      InitorSim::postedPrecheck()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mState = resettingTestInitorState;
        qDebug("InitorSim %s prechecking", mData.systemRecord->name().c_str());

        mData.proactor->ioService().post(bind(&SimDispatcherProxy::requestReset,
            mData.simDispatcherProxy,
            mData.system,
            shared_from_this()));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::postedSimResetRequestDispatched(bool success,
          bool resetRequiredStateChange)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        qDebug("InitorSim %s - Simlualtor has dispatched pretest reset",
            mData.systemRecord->name().c_str());

        if (resetRequiredStateChange && success)
        {
          // Create a timer and make it wait
          mTimer.expires_from_now(boost::posix_time::seconds(mTimeout));
          mTimer.async_wait(boost::bind(&InitorSim::postedSimulatorReset,
              shared_from_this()));
        }
        else
        {
          precheckComplete(success);
        }
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::postedSimulatorReset()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        qDebug("InitorSim %s - Simulater reset wait period has finished",
            mData.systemRecord->name().c_str());

        precheckComplete(true);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::postedExecute()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mState = executingTestInitorState;

        qDebug("InitorSim %s starting", mData.systemRecord->name().c_str());

        mData.proactor->ioService().post(bind(&SimDispatcherProxy::requestExecute,
            mData.simDispatcherProxy,
            mData.system,
            shared_from_this()));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::postedSimExecuteRequestDispatched(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mStrand.post(bind(&InitorSim::strandedSimExecuteRequestDispatched,
            shared_from_this(),
            success));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorSim::strandedSimExecuteRequestDispatched(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        if (mState == resetInitorState)
        {
          qDebug("InitorSim shutdown before execute request confirmation,\n"
              "this should be because the agent has observed the change aleady");
          return;
        }

        qDebug("InitorSim %s Sim change dispatched",
            mData.systemRecord->name().c_str());

        executeComplete(success);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      boost::shared_ptr< Initor >
      InitorSim::getThis()
      {
        // basePtr seems to be conflicting with somewhere else
        boost::shared_ptr< Initor > basePtr_;
        basePtr_ = shared_from_this();
        return basePtr_;
      }

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
