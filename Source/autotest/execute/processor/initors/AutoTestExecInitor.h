/*
 * AutoTestInitor.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AUTO_TEST_EXEC_INITOR_H_
#define _AUTO_TEST_EXEC_INITOR_H_

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/strand.hpp>

#include <QtCore/qobject.h>

#include <syntest/DbSyntestDeclaration.h>

#include "AutoTestExecProcessorSystem.h"
#include "Macros.h"

class QWidget;

class Proactor;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class System;
      class Initor;
      typedef boost::shared_ptr< Initor > InitorPtrT;
      class Resultor;

      /*! autotest::Initor Brief Description
       * Long comment about the class
       *
       */
      class Initor : public QObject, DebugClassMixin< Initor >
      {
        Q_OBJECT

        public:
          Initor(InitorData& data);

          virtual
          ~Initor();

        public:
          /// ---------------
          /// Miniture Factory

          static InitorPtrT
          create(InitorData data, QWidget* panel);

          void
          reset();

          void
          postedShutdown();

          void
          setResultor(boost::shared_ptr< Resultor > resultor);

        private:
          /// ---------------
          /// Resultor functions for binding

          void
          strandedReset();

        public:
          /// ---------------
          /// Initor interface

          virtual void
          postedPrecheck();

          virtual void
          postedExecute() = 0;

        public slots:

          void
          tearDownUiSlot();

        signals:
          void
          tearDownUiSignal();

          void
          uiRefreshTick();

        public:
          /// ---------------
          /// Initor functions for binding

          void
          strandedUiUpdateTimerExpired();

        protected:
          enum InitorStateE
          {
            resetInitorState,
            idleInitorState,
            resettingTestInitorState,
            executingTestInitorState,
          };

          void
          precheckComplete(bool success = true);

          void
          executeComplete(bool success);

          std::string
          stateStr() const;

          void
          startUiUpdateTimer();

          virtual boost::shared_ptr< Initor >
          getThis() = 0;

          InitorData mData;
          boost::weak_ptr< Resultor > mResultor;

          boost::asio::deadline_timer mTimer;
          boost::asio::deadline_timer mUiUpdateTimer;
          boost::asio::strand mStrand;
          int mTimeout;

          QWidget* mWidget;

          InitorStateE mState;

      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* _AUTO_TEST_EXEC_INITOR_H_ */
