#include "AutoTestExecInitorManual.h"

#include <boost/format.hpp>

#include <QtGui/qwidget.h>
#include <QtGui/qclipboard.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest/st/StSysPoint.h>

#include "AutoTestExecTestQueueModel.h"

#include "Proactor.h"
#include "AutoTestExecProcessorSystem.h"
#include "SimDispatcherProxy.h"

using namespace db_syntest;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      InitorManual::InitorManual(InitorData& data, QWidget* panel) :
        Initor(data)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        connect(this,
            SIGNAL(setupUiSignal(QWidget*)),
            this,
            SLOT(setupUiSlot(QWidget*)),
            Qt::BlockingQueuedConnection);

        emit setupUiSignal(panel);
FUNCTION_END      ( __PRETTY_FUNCTION__ )
    }

    InitorManual::~InitorManual()
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    InitorManual::setupUiSlot(QWidget* panel)
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )
      mWidget = new QWidget(panel);
      ui.setupUi(mWidget);
      panel->layout()->addWidget(mWidget);

      assert(mData.tqSysValue.get());
      assert(mData.tqSysValue->testSysValue.get());
      assert(mData.tqSysValue->testSysValue->sysPoint().get());

      st::SysPointPtrT sysPoint = mData.tqSysValue->testSysValue->sysPoint();
      assert(sysPoint.get());

      st::TestSysValueRunPtrT sysVal = mData.tqSysValue->testSysValueRun;
      assert(sysVal.get());

      ui.txtPointId->setText(sysPoint->id().c_str());
      ui.txtPointPid->setText(sysPoint->pid().c_str());
      // ui.txtPointAlias->setText(sysPoint->alias().c_str());
      ui.txtPointName->setText(sysPoint->name().c_str());

      ui.txtComment->setText(sysVal->sysComment().c_str());

      connect(ui.btnControlIssued,
          SIGNAL(clicked()),
          this,
          SLOT(controlIssued()));

      connect(ui.txtComment,
          SIGNAL(textEdited()),
          this,
          SLOT(commentUpdated()));

      connect(ui.btnResetInd,
          SIGNAL(clicked()),
          this,
          SLOT(resetIndClicked()));

      connect(ui.btnCopyPid,
          SIGNAL(clicked()),
          this,
          SLOT(copyPointPidClicked()));

      connect(this,
          SIGNAL(uiRefreshTick()),
          this,
          SLOT(refreshUiSlot()),
          Qt::QueuedConnection);
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    InitorManual::refreshUiSlot()
    {
      if (!mWidget)
      return;

      mWidget->setEnabled(mState == executingTestInitorState);
      ui.txtStatus->setText(stateStr().c_str());
    }

    void
    InitorManual::postedExecute()
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )
      mState = executingTestInitorState;

      qDebug("InitorManual %s starting", mData.systemRecord->name().c_str());
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    InitorManual::commentUpdated()
    {
      std::string comment = ui.txtComment->text().toStdString();
      if (!comment.empty())
      {
        mData.tqSysValue->testSysValueRun->setSysComment(comment);
      }
    }

    void
    InitorManual::controlIssued()
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )
      qDebug("InitorManual %s - User has dispatched request",
          mData.systemRecord->name().c_str());

      assert(mWidget);
      mWidget->setEnabled(false);

      executeComplete(true);
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    InitorManual::resetIndClicked()
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )
      qDebug("InitorManual %s - User requested indication reset",
          mData.systemRecord->name().c_str());

      assert(mData.simDispatcherProxy);
      mData.simDispatcherProxy->indResetForControl();

      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    InitorManual::copyPointIdClicked()
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )

      st::SysPointPtrT sysPoint = mData.tqSysValue->testSysValue->sysPoint();
      QClipboard *clipboard = QApplication::clipboard();
      clipboard->setText(sysPoint->id().c_str());

      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    InitorManual::copyPointPidClicked()
    {
      FUNCTION_START( __PRETTY_FUNCTION__ )

      st::SysPointPtrT sysPoint = mData.tqSysValue->testSysValue->sysPoint();
      QClipboard *clipboard = QApplication::clipboard();
      clipboard->setText(sysPoint->pid().c_str());

      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    boost::shared_ptr< Initor >
    InitorManual::getThis()
    {
      // basePtr seems to be conflicting with somewhere else
      boost::shared_ptr< Initor > basePtr_;
      basePtr_ = shared_from_this();
      return basePtr_;
    }

  } /* namespace processor */
} /* namespace exec */
} /* namespace autotest */
