#ifndef _AUTO_TEST_EXEC_INITOR_AGENT_H
#define _AUTO_TEST_EXEC_INITOR_AGENT_H

#include <boost/enable_shared_from_this.hpp>

#include "ui_AutoTestExecInitorAgentUi.h"

#include "AutoTestExecInitor.h"
#include "AgentDispatcherObserver.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class InitorAgent : public Initor,
          public agent::AgentDispatcherObserver,
          public boost::enable_shared_from_this< InitorAgent >,
          DebugClassMixin< InitorAgent >
      {
        Q_OBJECT

        public:
          InitorAgent(InitorData& data, QWidget* panel);

          virtual
          ~InitorAgent();

        public:
          /// ---------------
          /// Initor interface implementation

          void
          postedExecute();

        public:
          /// ---------------
          /// agent::AgentDispatcherObserver interface implementation

          void
          postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
              std::string errorMessage);

          void
          postedAgentRecievedMessage(agent::Client::Msg message);

        public slots:
          /// ---------------
          /// Stuff to make the UI thread process stuff

          void
          setupUiSlot(QWidget* panel);

          void
          refreshUiSlot();

        signals:

          void
          setupUiSignal(QWidget* panel);

        public:
          /// ---------------
          /// InitorAgent stuff

        private:

          void
          strandedAgentRecievedMessage(agent::Client::Msg message);

          boost::shared_ptr< Initor >
          getThis();

          Ui::AutoTestExecInitorAgentUiClass ui;
      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif // _AUTO_TEST_EXEC_INITOR_AGENT_H
