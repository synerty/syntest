#ifndef _AUTO_TEST_EXEC_INITOR_MANUAL_H
#define _AUTO_TEST_EXEC_INITOR_MANUAL_H

#include <boost/enable_shared_from_this.hpp>

#include "ui_AutoTestExecInitorManualUi.h"

#include "AutoTestExecInitor.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class InitorManual : public Initor,
          DebugClassMixin< InitorManual >,
          public boost::enable_shared_from_this< InitorManual >
      {
        Q_OBJECT

        public:
          InitorManual(InitorData& data, QWidget* panel);

          virtual
          ~InitorManual();

        public:
          /// ---------------
          /// Initor interface

          void
          postedExecute();

        public slots:
          /// ---------------
          /// Ui Feedback

          void
          controlIssued();

          void
          commentUpdated();

          void
          resetIndClicked();

          void
          copyPointIdClicked();

          void
          copyPointPidClicked();

        public slots:
          /// ---------------
          /// Stuff to make the UI thread process stuff

          void
          setupUiSlot(QWidget* panel);

          void
          refreshUiSlot();

        signals:

          void
          setupUiSignal(QWidget* panel);

        public:
          /// ---------------
          /// InitorManual stuff

        private:

          boost::shared_ptr< Initor >
          getThis();

          Ui::AutoTestExecInitorManualUiClass ui;
      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif // _AUTO_TEST_EXEC_INITOR_MANUAL_H
