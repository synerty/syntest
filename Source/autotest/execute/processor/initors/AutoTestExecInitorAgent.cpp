#include "AutoTestExecInitorAgent.h"

#include <QtGui/qwidget.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>

#include "Proactor.h"
#include "AgentDispatcher.h"
#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecTestQueueModel.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      InitorAgent::InitorAgent(InitorData& data, QWidget* panel) :
        Initor(data)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        connect(this,
            SIGNAL(setupUiSignal(QWidget*)),
            this,
            SLOT(setupUiSlot(QWidget*)),
            Qt::BlockingQueuedConnection);
        emit
        setupUiSignal(panel);
        FUNCTION_END( __PRETTY_FUNCTION__ )

      }

      InitorAgent::~InitorAgent()
      {
      }

      void
      InitorAgent::setupUiSlot(QWidget* panel)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mWidget = new QWidget(panel);
        ui.setupUi(mWidget);
        panel->layout()->addWidget(mWidget);

        connect(this,
            SIGNAL(uiRefreshTick()),
            this,
            SLOT(refreshUiSlot()),
            Qt::QueuedConnection);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorAgent::refreshUiSlot()
      {
        if (!mWidget)
          return;
        ui.txtStatus->setText(stateStr().c_str());

      }

      /// ---------------
      /// Initor interface implementation

      void
      InitorAgent::postedExecute()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mState = executingTestInitorState;

        qDebug("InitorAgent %s starting", mData.systemRecord->name().c_str());

        assert(mData.agentDispatcher.get());
        assert(mData.tqSysValue.get());
        assert(mData.tqSysValue->testSysValue.get());
        assert(mData.tqSysValue->testSysValue->sysPoint().get());

        db_syntest::st::SysPointPtrT
            sysPoint = mData.tqSysValue->testSysValue->sysPoint();

        test_queue::TqValuePtrT tqValue = mData.tqSysValue->tqValue.lock();
        assert(tqValue.get());
        assert(tqValue->testValue.get());

        double val = tqValue->testValue->simValue();

        mData.agentDispatcher->executeControl(sysPoint, val);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      /// ---------------
      /// agent::AgentDispatcherObserver interface implementation

      void
      InitorAgent::postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
          std::string errorMessage)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        // Ignore for the moment
            FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorAgent::postedAgentRecievedMessage(agent::Client::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mStrand.post(bind(&InitorAgent::strandedAgentRecievedMessage,
            shared_from_this(),
            message));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      InitorAgent::strandedAgentRecievedMessage(agent::Client::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        if (mState == resetInitorState){
          qDebug("InitorAgent shutdown before execute request confirmation,\n"
              "this should be because the simulator has observed the change aleady");
          return;
        }

        using namespace agent::Client;
        if (message.msgType != controlExecutedMsgType)
          return;

        db_syntest::st::SysPointPtrT
            sysPoint = mData.tqSysValue->testSysValue->sysPoint();

        ControlExecutedMsgData
            data = boost::get< ControlExecutedMsgData >(message.msgData);
        if (data.id != sysPoint->id())
          return;

        executeComplete(data.success);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      boost::shared_ptr< Initor >
      InitorAgent::getThis()
      {
        // basePtr seems to be conflicting with somewhere else
        boost::shared_ptr< Initor > basePtr_;
        basePtr_ = shared_from_this();;
        return basePtr_;
      }

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
