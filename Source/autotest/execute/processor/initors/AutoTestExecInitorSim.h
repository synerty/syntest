#ifndef _AUTO_TEST_EXEC_INITOR_SIM_H
#define _AUTO_TEST_EXEC_INITOR_SIM_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/deadline_timer.hpp>

#include "ui_AutoTestExecInitorSimUi.h"

#include "AutoTestExecInitor.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class InitorSim : public Initor,
          DebugClassMixin< InitorSim >,
          public boost::enable_shared_from_this< InitorSim >
      {
        Q_OBJECT

        public:
          InitorSim(InitorData& data, QWidget* panel);

          virtual
          ~InitorSim();

        public:
          /// ---------------
          /// Initor interface

          void
          postedPrecheck();

          void
          postedExecute();

        public:
          /// ---------------
          /// InitorSim stuff
          /// Used be SimDispatcherProxy

          void
          postedSimResetRequestDispatched(bool success,
              bool resetRequiredStateChange);

          /*! Sim Request Dispatched
           * Posted by the SimDispatcher when out request has been dispatched
           */
          void
          postedSimExecuteRequestDispatched(bool success);

        public slots:
          /// ---------------
          /// Stuff to make the UI thread process stuff

          void
          setupUiSlot(QWidget* panel);

          void
          refreshUiSlot();

        signals:

          void
          setupUiSignal(QWidget* panel);

        private:
          /// ---------------
          /// InitorSim stuff

          void
          postedSimulatorReset();

        private:

          void
          strandedSimExecuteRequestDispatched(bool success);

          boost::shared_ptr< Initor >
          getThis();

          Ui::AutoTestExecInitorSimUiClass ui;

      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif // _AUTO_TEST_EXEC_INITOR_SIM_H
