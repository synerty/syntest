/*
 * AutoTestExecProcessorObserver.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecProcessorObserver.h"

namespace autotest
{
  namespace exec
  {
    ProcessorObserver::~ProcessorObserver()
    {
    }
    // ------------------------------------------------------------------------

    void
    ProcessorObserver::processorTestingStarted()
    {
    }
    // ------------------------------------------------------------------------

    void
    ProcessorObserver::processorTestingStopped(const TestStopReasonE reason,
        const std::string message)
    {
    }
  // ------------------------------------------------------------------------

  } /* namespace exec */
} /* namespace autotest */
