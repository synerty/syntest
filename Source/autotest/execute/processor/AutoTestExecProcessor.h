/*
 * AutoTestExecProcessor.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef AUTOTESTEXECPROCESSOR_H_
#define AUTOTESTEXECPROCESSOR_H_

#include <set>
#include <vector>
#include <map>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/strand.hpp>

#include <QtCore/qobject.h>
class QWidget;

#include <syntest/DbSyntestDeclaration.h>

#include "AutoTestExecDeclarations.h"
#include "AutoTestExecTestQueueObserver.h"
#include "AgentDispatcherObserver.h"
#include "SimDllDispatcherObserver.h"
#include "Observable.h"
#include "Macros.h"

class Proactor;
typedef boost::shared_ptr< Proactor > ProactorPtrT;

namespace agent
{
  class AgentDispatcher;
}

namespace autotest
{
  namespace exec
  {
    class Processor;
    typedef boost::shared_ptr< Processor > ProcessorPtrT;

    class ProcessorObserver;
    typedef boost::weak_ptr< ProcessorObserver > ProcessorObserverWptrT;
    typedef boost::shared_ptr< ProcessorObserver > ProcessorObserverPtrT;
    typedef void
    (ProcessorObserver::*ProcObserveFuncT)();

    namespace processor
    {
      class System;
      typedef boost::shared_ptr< System > SystemPtrT;

      class SimDispatcherProxy;
      typedef boost::shared_ptr< SimDispatcherProxy > SimDispatcherProxyPtrT;
    }

    /*! Test Processor
     * This class is responsible for processing the tests
     *
     */
    class Processor : public QObject,
        public Observable< ProcessorObserver >,
        public TestQueueObserver,
        public agent::AgentDispatcherObserver, // Private
        public sim_dll::SimDllDispatcherObserver, // Private
        public boost::enable_shared_from_this< Processor >,
        DebugClassMixin< Processor >

    {
      Q_OBJECT

      public:
        Processor(QWidget* panel);
        virtual
        ~Processor();

      public:
        /// ---------------
        /// Processor Stuff

        void
        run();

        void
        stop();

        void
        interrupt();

        void
        close();

      public:
        /// ---------------
        /// TestQueueObserver observer implementation

        void
        testQueueModelChanged(test_queue::ModelPtrT model);

      private:
        /// ---------------
        /// Processor Stuff

        void
        reset();

        void
        initialise();

      public slots:
        /// ---------------
        /// Processor Stuff - System controll and state machine

        // Slot for moving to next test
        // Easy way to ensure the move command and its events occur in the Qt main loop
        void
        moveToNextTestValueSlot();

        void
        interruptSlot();

      signals:
        /// ---------------
        /// Processor Stuff - System controll and state machine

        // Signal for moving to next test
        // Easy way to ensure the move command and its events occur in the Qt main loop
        void
        moveToNextTestValueSignal();

        void
        interruptSignal();

      public:
        /// ---------------
        /// Processor Stuff - System controll and state machine
        /// Interface used by the System objects

        void
        postedSystemTestComplete(const std::string systemName);

      private:
        /// ---------------
        /// SimDllDispatcherObserver interface implementation

        virtual void
        postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
            std::string errorMessage);

      private:
        /// ---------------
        /// AgentDispatcherObserver interface implementation

        virtual void
        postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
            std::string errorMessage);

      private:
        /// ---------------
        /// Observerable stuff

        void
        notifyOfTestingStopped(const TestStopReasonE reason,
            const std::string message = std::string());

      private:
        /// ---------------
        /// Processor Stuff - System controll and state machine

        void
        startTest();

        void
        strandedSystemTestComplete(const std::string systemName);

        enum SystemWorkerStateE
        {
          idleSystemWorkerState,
          workingSystemWorkerState
        };

        struct SystemData
        {
            SystemData();

            db_syntest::st::SystemPtrT sysRecord;
            processor::SystemPtrT sysWorker;
            SystemWorkerStateE state;
        };

        typedef std::map< std::string, SystemData > SystemWorkerMapT;
        SystemWorkerMapT mSystems;

        typedef std::vector< processor::SimDispatcherProxyPtrT > SimDispatcherListT;
        SimDispatcherListT mSimDispatchers;

      private:
        /// ---------------
        /// Processor Stuff (this class)

        enum TestingStateE
        {
          testingRunning,
          testingStopping,
          testingStopped
        };

        TestingStateE mTestingState;

        QWidget* mPanel;
        db_syntest::SynTestModelPtrT mSynOrmModel;

        ProactorPtrT mProactor;
        boost::shared_ptr< boost::asio::strand > mStrand;

        test_queue::ModelPtrT mTestQueueModel;

        std::string mLastTestPointId;

    };

  } /* namespace exec */
} /* namespace autotest */
#endif /* AUTOTESTEXECPROCESSOR_H_ */
