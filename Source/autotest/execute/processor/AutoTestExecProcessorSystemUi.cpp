#include "AutoTestExecProcessorSystemUi.h"

#include <boost/format.hpp>

#include <QtGui/qapplication.h>

#include <syntest/st/StSystem.h>

#include "AutoTestExecProcessorSystem.h"

using namespace db_syntest;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      SystemUi::SystemUi(System& uc,
          st::SystemPtrT systemRecord,
          QWidget *parent) :
          QWidget(parent), //
          mUc(uc)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          moveToThread(QApplication::instance()->thread());
          ui.setupUi(this);
          parent->layout()->addWidget(this);

          ui.grpSystem->setTitle(systemRecord->name().c_str());

          const bool agentEnabled = !(systemRecord->isAgentIpNull()
              || systemRecord->isAgentPortNull());

          if (!agentEnabled)
          {
//          ui.wgtAgentManual->setVisible(true);
            ui.wgtAgentAuto->setVisible(false);
          }
          else
          {
            ui.wgtAgentManual->setVisible(false);
//          ui.wgtAgentAuto->setVisible(true);
            boost::format sysLabel("%s:%d");
            sysLabel % systemRecord->agentIp();
            sysLabel % systemRecord->agentPort();
            mAgentStr = sysLabel.str();

            ui.txtAgentAddress->setText(mAgentStr.c_str());

            connect(ui.btnSysConnToggle,
                SIGNAL(clicked(bool)),
                this,
                SLOT(agentConnectionToggleClicked(bool)),
                Qt::QueuedConnection);

            /// Allows calls to the GUI thread
            connect(this,
                SIGNAL(agentConnectionStateChangedSignal(int, QString)),
                this,
                SLOT(agentConnectionStateChangedSlot(int, QString)),
                Qt::QueuedConnection);
          }

          boost::format simLabel("%s:%d");
          simLabel % systemRecord->simulatorIp();
          simLabel % systemRecord->simulatorPort();
          mSimulatorStr = simLabel.str();

          if (!systemRecord->isSimulatorAliasPostfixNull())
            mSimulatorStr += " (" + systemRecord->simulatorAliasPostfix() + ")";

          ui.txtSimAddress->setText(mSimulatorStr.c_str());

          connect(ui.btnSimConnToggle,
              SIGNAL(clicked(bool)),
              this,
              SLOT(simulatorConnectionToggleClicked(bool)));

          /// Allows calls to the GUI thread
          connect(this,
              SIGNAL(simulatorConnectionStateChangedSignal(int, QString)),
              this,
              SLOT(simulatorConnectionStateChangedSlot(int, QString)));

          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------

      SystemUi::~SystemUi()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }
      /// ---------------------------------------------------------------------

      QWidget*
      SystemUi::getWorkerPanel()
      {
        return ui.wgtWorker;
      }
      /// ---------------------------------------------------------------------

      void
      SystemUi::agentConnectionStateChanged(agent::ConnectionStateE state,
          std::string errorMessage)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          int stateInt = state;
          emit
          agentConnectionStateChangedSignal(stateInt,
              QString(errorMessage.c_str()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------

      void
      SystemUi::simulatorConnectionStateChanged(tcp_client::ConnectionStateE state,
          std::string errorMessage)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          int stateInt = state;
          emit
          simulatorConnectionStateChangedSignal(stateInt,
              QString(errorMessage.c_str()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------

      void
      SystemUi::agentConnectionStateChangedSlot(int stateInt, QString reason)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          using namespace tcp_client;
          agent::ConnectionStateE state = static_cast< agent::ConnectionStateE >(stateInt);

          if (state == agent::connectedAgentState)
          {
            ui.btnSysConnToggle->setText("Disconnect");
            ui.btnSysConnToggle->setChecked(true);
            ui.btnSysConnToggle->setEnabled(true);
          }
          else if (state == agent::connectingAgentState)
          {
            reason = "Connecting ...";
            ui.btnSysConnToggle->setText(reason);
            ui.btnSysConnToggle->setChecked(false);
            ui.btnSysConnToggle->setEnabled(false);
          }
          else if (state == agent::downloadAgentState)
          {
            reason = "Downloading ...";
            ui.btnSysConnToggle->setText(reason);
            ui.btnSysConnToggle->setChecked(false);
            ui.btnSysConnToggle->setEnabled(false);
          }
          else
          {
            ui.btnSysConnToggle->setText("Connect");
            ui.btnSysConnToggle->setChecked(false);
            ui.btnSysConnToggle->setEnabled(true);
          }

          if (reason.isEmpty())
            ui.txtAgentAddress->setText(QString(mAgentStr.c_str()));
          else
            ui.txtAgentAddress->setText(QString(mAgentStr.c_str()) + " - "
                + reason);

          ui.txtAgentAddress->setToolTip(reason);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------

      void
      SystemUi::simulatorConnectionStateChangedSlot(int stateInt,
          QString reason)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          using namespace tcp_client;
          ConnectionStateE state = static_cast< ConnectionStateE >(stateInt);

          if (state == connectedState)
          {
            ui.btnSimConnToggle->setText("Disconnect");
            ui.btnSimConnToggle->setChecked(true);
            ui.btnSimConnToggle->setEnabled(true);
          }
          else if (state == connectingState)
          {
            ui.btnSimConnToggle->setText("Connecting ...");
            ui.btnSimConnToggle->setChecked(false);
            ui.btnSimConnToggle->setEnabled(false);
          }
          else
          {
            ui.btnSimConnToggle->setText("Connect");
            ui.btnSimConnToggle->setChecked(false);
            ui.btnSimConnToggle->setEnabled(true);
          }

          if (reason.isEmpty())
            ui.txtSimAddress->setText(QString(mSimulatorStr.c_str()));
          else
            ui.txtSimAddress->setText(QString(mSimulatorStr.c_str()) + " - "
                + reason);

          ui.txtSimAddress->setToolTip(reason);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------

      void
      SystemUi::agentConnectionToggleClicked(bool checked)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (checked)
          {
            mUc.connectAgent();
          }
          else
          {
            mUc.disconnectAgent();
          }

          // Reject the checked change
          ui.btnSysConnToggle->setChecked(!checked);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------

      void
      SystemUi::simulatorConnectionToggleClicked(bool checked)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (checked)
          {
            mUc.connectSimulator();
          }
          else
          {
            mUc.disconnectSimulator();
          }

          // Reject the checked change
          ui.btnSimConnToggle->setChecked(!checked);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
    /// ---------------------------------------------------------------------
    }
  }
}
