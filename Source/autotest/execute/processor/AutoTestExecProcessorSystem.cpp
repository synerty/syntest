/*
 * AutoTestExecProcessorSystem.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecProcessorSystem.h"

#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestValueRun.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest_util/DbSyntestUtil.h>

#include "MainSynModel.h"

#include "AutoTestExecTestQueueModel.h"

#include "AutoTestExecProcessor.h"
#include "Proactor.h"
#include "SimDispatcherProxy.h"
#include "AutoTestExecInitor.h"
#include "AutoTestExecResultor.h"
#include "AgentDispatcher.h"

using namespace db_syntest;
using namespace autotest::exec::test_queue;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {

      System::System(boost::shared_ptr< Processor > proc,
          boost::shared_ptr< Proactor > proactor,
          boost::shared_ptr< SimDispatcherProxy > simDispatcher,
          boost::shared_ptr< agent::AgentDispatcher > agentDispatcher,
          st::SystemPtrT systemRecord,
          QWidget* panel) :

          mSystemUi(*this, systemRecord, panel), //
          mSynOrmModel(MainSynModel::Inst().ormModel()), //
          mProcessor(proc), //
          mProactor(proactor), //
          mSimDispatcher(simDispatcher), //
          mAgentDispatcher(agentDispatcher), //
          mSystemRecord(systemRecord)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }
      /// ---------------------------------------------------------------------------

      System::~System()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (mResultor.get())
          {
            mResultor->reset();
            mResultor.reset();
          }

          if (mInitor.get())
          {
            mInitor->reset();
            mInitor.reset();
          }

          // Scope the variables
          {
            boost::shared_ptr< SimDispatcherProxy > simPtr = mSimDispatcher.lock();
            if (simPtr.get())
              simPtr->postedDisconnect();
            mSimDispatcher.reset();
          }

          mAgentDispatcher->removeObserver(this);
          mAgentDispatcher->postedDisconnect();
          mAgentDispatcher.reset();
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::resetTest()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mTqSysValue.reset();
          qDebug("System %s reset", mSystemRecord->name().c_str());
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::initialiseTest(boost::shared_ptr< test_queue::TqSysValue > tqSysValue)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          assert(tqSysValue.get());
          mTqSysValue = tqSysValue;

          if (mTqSysValue->testSysValueRun.get())
          {
            st::TestSysValueRunPtrT valRun = mTqSysValue->testSysValueRun;
            valRun->setFeedbackRecieved(0);
            valRun->setFeedbackTimeNull();
            valRun->setSysPointRecievedNull();

            valRun->setSysRawValueNull();
            valRun->setSysRawDigStateNull();

            valRun->setSysValueNull();
            valRun->setSysDigStateNull();
          }
          else
          {
            TqValuePtrT tqValue = tqSysValue->tqValue.lock();
            assert(tqValue.get());
            assert(tqValue->testValueRun.get());

            st::TestSysValueRunPtrT valRun(new st::TestSysValueRun());
            valRun->setId(nextId(mSynOrmModel));
            valRun->setTestSysValue(mTqSysValue->testSysValue);
            valRun->setTestValueRun(tqValue->testValueRun);
            valRun->setFeedbackRecieved(0);
            valRun->addToModel(mSynOrmModel);
            mTqSysValue->testSysValueRun = valRun;
          }

          qDebug("System %s initialised", mSystemRecord->name().c_str());
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::connectAgent()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mProactor->ioService().post(bind(&agent::AgentDispatcher::postedConnect,
              mAgentDispatcher));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::disconnectAgent()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mProactor->ioService().post(bind(&agent::AgentDispatcher::postedDisconnect,
              mAgentDispatcher));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::connectSimulator()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mProactor->ioService().post(bind(&SimDispatcherProxy::postedConnect,
              simDispatcherProxy()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::disconnectSimulator()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)

          mProactor->ioService().post(bind(&SimDispatcherProxy::postedDisconnect,
              simDispatcherProxy()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      /// ---------------
      /// AgentDispatcherObserver interface implementation

      void
      System::postedAgentConnectionStateChanged(agent::ConnectionStateE state,
          std::string errorMessage)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          assert(mAgentDispatcher.get());
          mSystemUi.agentConnectionStateChanged(state, errorMessage);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      /// ---------------
      /// SimDispatcherProxy functions

      void
      System::postedSimulatorConnectionStateChanged(tcp_client::ConnectionStateE state,
          std::string errorMessage)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mSystemUi.simulatorConnectionStateChanged(state, errorMessage);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::postedSimulatorObjectUpdateRecieved(sim_dll::Server::Msg message)
      {
      }
      /// ---------------------------------------------------------------------------

      /// ---------------
      /// State machine stuff

      void
      System::postedStart()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (!mTqSysValue.get())
          {
            qDebug("System %s skipping test, system is reset",
                mSystemRecord->name().c_str());
            mProactor->ioService().post(bind(&Processor::postedSystemTestComplete,
                mProcessor,
                mSystemRecord->name()));
            return;
          }

          qDebug("System %s testing started", mSystemRecord->name().c_str());

          // Create the initor
          mInitor = Initor::create(getInitorData(), mSystemUi.getWorkerPanel());

          // Create the resultor, it will know about the initor
          mResultor = Resultor::create(getResultorData(),
              mSystemUi.getWorkerPanel());

          // Tell the initor about the resultor (for callbacks)
          mInitor->setResultor(mResultor);

          // Call the initor to prechek
          mProactor->ioService().post(bind(&Initor::postedPrecheck, mInitor));

          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::postedInitorPrecheckCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        // FIXME do something with success
          qDebug("System %s back from initor precheck",
              mSystemRecord->name().c_str());

          // Now kick off the resultor
          mProactor->ioService().post(bind(&Resultor::postedRun, mResultor));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::postedResultorCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          qDebug("System %s back from resultor", mSystemRecord->name().c_str());
          mResultor->reset();
          mInitor->reset();

          mResultor.reset();
          mInitor.reset();

          mProactor->ioService().post(bind(&System::postedComplete,
              shared_from_this()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      void
      System::postedComplete()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          qDebug("System %s testing completed", mSystemRecord->name().c_str());

          mProactor->ioService().post(bind(&Processor::postedSystemTestComplete,
              mProcessor,
              mSystemRecord->name()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      InitorData
      System::getInitorData()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          InitorData data;
          data.proactor = mProactor;
          data.system = shared_from_this();
          data.systemRecord = mSystemRecord;
          data.agentDispatcher = mAgentDispatcher;

          data.simDispatcherProxy = mSimDispatcher.lock();
          assert(data.simDispatcherProxy.get());

          data.tqSysValue = mTqSysValue;

          return data;

          FUNCTION_END( __PRETTY_FUNCTION__)
        return InitorData();
      }
      /// ---------------------------------------------------------------------------

      ResultorData
      System::getResultorData()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          ResultorData data;
          data.proactor = mProactor;
          data.system = shared_from_this();
          data.systemRecord = mSystemRecord;
          data.agentDispatcher = mAgentDispatcher;

          data.simDispatcherProxy = mSimDispatcher.lock();
          assert(data.simDispatcherProxy.get());

          data.tqSysValue = mTqSysValue;

          data.initor = mInitor;

          return data;

          FUNCTION_END( __PRETTY_FUNCTION__)
        return ResultorData();
      }
      /// ---------------------------------------------------------------------------

      boost::shared_ptr< SimDispatcherProxy >
      System::simDispatcherProxy()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          boost::shared_ptr< SimDispatcherProxy > ptr = mSimDispatcher.lock();
          assert(ptr.get());
          return ptr;

          FUNCTION_END( __PRETTY_FUNCTION__)
        return boost::shared_ptr< SimDispatcherProxy >();
      }
    /// ---------------------------------------------------------------------------

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
