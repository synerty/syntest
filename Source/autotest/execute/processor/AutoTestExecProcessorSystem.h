/*
 * AutoTestExecProcessorSystem.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef AUTOTESTEXECPROCESSORSYSTEM_H_
#define AUTOTESTEXECPROCESSORSYSTEM_H_

#include <auto_ptr.h>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <QtCore/qobject.h>
class QWidget;
class QString;

#include <syntest/DbSyntestDeclaration.h>

#include "AutoTestExecProcessorSystemUi.h"
#include "AgentDispatcherObserver.h"
#include "SimDllDeclaration.h"
#include "Macros.h"

class Proactor;

namespace agent
{
  class AgentDispatcher;
}

namespace autotest
{
  namespace exec
  {
    namespace test_queue
    {
      class TqSysValue;
    }

    class Processor;

    namespace processor
    {
      class SimDispatcherProxy;
      class SystemUi;
      class Initor;
      class Resultor;

      struct InitorData
      {
          boost::shared_ptr< Proactor > proactor;
          boost::shared_ptr< System > system;
          db_syntest::st::SystemPtrT systemRecord;
          boost::shared_ptr< SimDispatcherProxy > simDispatcherProxy;
          boost::shared_ptr< agent::AgentDispatcher > agentDispatcher;
          boost::shared_ptr< test_queue::TqSysValue > tqSysValue;
      };

      struct ResultorData : InitorData
      {
          boost::shared_ptr< Initor > initor;
      };

      /*! System
       * Thes class represents the testing to/from a system.
       * An instance of this class is required for each system being tested
       * to.
       *
       */
      class System : public QObject, //
          public boost::enable_shared_from_this< System >,
          public agent::AgentDispatcherObserver, //
          DebugClassMixin< System >
      {
        Q_OBJECT

        public:
          System(boost::shared_ptr< Processor > proc,
              boost::shared_ptr< Proactor > proactor,
              boost::shared_ptr< SimDispatcherProxy > simDispatcher,
              boost::shared_ptr< agent::AgentDispatcher > agentDispatcher,
              db_syntest::st::SystemPtrT systemRecord,
              QWidget* panel);

          virtual
          ~System();

        public:

          void
          resetTest();

          void
          initialiseTest(boost::shared_ptr< test_queue::TqSysValue > tqSysValue);

        public slots:
          /// ---------------
          /// Interface used by UI

          void
          connectAgent();

          void
          disconnectAgent();

          void
          connectSimulator();

          void
          disconnectSimulator();

        public:
          /// ---------------
          /// AgentDispatcherObserver interface implementation

          void
          postedAgentConnectionStateChanged(agent::ConnectionStateE state,
              std::string errorMessage);

        public:
          /// ---------------
          /// SimDispatcherProxy functions

          void
          postedSimulatorConnectionStateChanged(tcp_client::ConnectionStateE state,
              std::string errorMessage);

          void
          postedSimulatorObjectUpdateRecieved(sim_dll::Server::Msg message);

        public:
          /// ---------------
          /// State machine stuff

          /*! Start
           * Instructs the test to begin testing the next point
           * The start function the proactor calls in a thread.
           */
          void
          postedStart();

          /*! Initor Completed
           * Posted by the Initor when its completed the change
           */
          void
          postedInitorPrecheckCompleted(bool success);

          /*! Resultor Completed
           * Posted by the Resultor when its recieved the indication or timed out
           */
          void
          postedResultorCompleted(bool success);

        signals:

          void
          testComplete(const QString& systemRecordId);

        private:
          /// ---------------
          /// State machine stuff

          /*! Posted Complete
           * The complete function the proactor calls in a thread.
           * Indicates that this class has completed the testing for this value
           */
          void
          postedComplete();

          InitorData
          getInitorData();

          ResultorData
          getResultorData();

          boost::shared_ptr< test_queue::TqSysValue > mTqSysValue;

        private:
          /// ---------------
          /// System Stuff

          boost::shared_ptr< SimDispatcherProxy >
          simDispatcherProxy();

          SystemUi mSystemUi;
          db_syntest::SynTestModelPtrT mSynOrmModel;

          boost::shared_ptr< Processor > mProcessor;
          boost::shared_ptr< Proactor > mProactor;

          boost::weak_ptr< SimDispatcherProxy > mSimDispatcher;
          boost::shared_ptr< agent::AgentDispatcher > mAgentDispatcher;

          db_syntest::st::SystemPtrT mSystemRecord;

          boost::shared_ptr< Initor > mInitor;
          boost::shared_ptr< Resultor > mResultor;

      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* AUTOTESTEXECPROCESSORSYSTEM_H_ */
