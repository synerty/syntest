/*
 * SimDispatcherProxyMonitorDelegate.cpp
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "SimDispatcherProxyMonitorDelegate.h"

#include <boost/asio/placeholders.hpp>

#include <simscada/SimscadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimAnaPt.h>
#include <simscada_util/DbSimscadaUtil.h>

#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Proactor.h"
#include "SimDllDeclaration.h"
#include "Globals.h"

#include "MainSimModel.h"
#include "AutoTestExecResultorSim.h"

using namespace db_simscada;
using namespace db_syntest;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {
        using namespace sim_dll::Server;

        /// -------------------------------------------------------------------
        /// MonitorDelegateTimer
        /// -------------------------------------------------------------------

        MonitorDelegateTimer::MonitorDelegateTimer(MonitorDelegatePtrT parent,
            DelegateDataPtrT data,
            const std::string alias) :
            mParent(parent), //
            mData(data), //
            mAlias(alias), //
            mTimer(data->proactor->ioService()), //
            mActive(false)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegateTimer::start()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
            mTimer.async_wait(bind(&MonitorDelegateTimer::expired,
                shared_from_this(),
                boost::asio::placeholders::error));
            mActive = true;
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegateTimer::stop()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mTimer.cancel();
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegateTimer::expired(const boost::system::error_code& error)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mActive = false;
            mData->strand.wrap(bind(&MonitorDelegate::strandedRegRespTimer,
                mParent,
                error,
                mAlias));
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        bool
        MonitorDelegateTimer::isActive() const
        {
          return mActive;
        }
        /// ---------------------------------------------------------------------------

        /// -------------------------------------------------------------------
        /// MonitorDelegate
        /// -------------------------------------------------------------------
        MonitorDelegate::MonitorDelegate(DelegateDataPtrT data) :
            Delegate(data)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        MonitorDelegate::~MonitorDelegate()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::run()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mData->strand.post(boost::bind(&MonitorDelegate::strandedRun,
                shared_from_this()));
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedRun()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (!mData->dispatcher->isConnected())
            {
              strandedMonitorFailed();
              return;
            }

            qDebug("SimDispatcherProxyMonitorDelegate performing simulator change");

            for (StringResultorsMapT::iterator itr = mData->resultors.begin(); //
            itr != mData->resultors.end(); ++itr)
            {
              std::string simAlias = itr->first;

              // Create, store and Kickoff a mTimer
              MonitorDelegateTimerPtrT timer(new MonitorDelegateTimer(shared_from_this(),
                  mData,
                  simAlias));
              mTimerMap.insert(std::make_pair(simAlias, timer));
              timer->start();

              mData->dispatcher->registerForUpdates(getSimObject(simAlias));
            }

            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
            std::string errorMessage)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mData->strand.post(bind(&MonitorDelegate::strandedMonitorFailed,
                shared_from_this()));
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)

            if (message.msgType == RegPointFailed
                || message.msgType == RegPointSucceeded)
              pointRegistrationMessage(message);

            else if (message.msgType == CtrlUpdate)
              controlUpdateMessage(message);
            FUNCTION_END( __PRETTY_FUNCTION__)

        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedRequestMonitorEnd(boost::shared_ptr< System > system,
            boost::shared_ptr< ResultorSim > resultorSim,
            std::string alias)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          // Get our stuff
            StringResultorsMapT::iterator itr = mData->resultors.find(alias);
            assert(itr != mData->resultors.end());

            ResultorSimListT& lst = itr->second;
            assert(lst.size() && lst.front() == resultorSim);

            // Pop the resultor
            lst.pop_front();

            // Remove the resultor map entry and exit if its the last one
            if (lst.empty())
            {
              mData->dispatcher->deregisterForUpdates(alias);

              mData->resultors.erase(itr);

              if (mData->resultors.empty())
                allDone();

              return;
            }

            // Else, tell the next resultor that its good to go :-)
            ResultorSimPtrT resultor = lst.front();
            mData->proactor->ioService().post(bind(&ResultorSim::postedSimulatorMonitoringStarted,
                resultor,
                true));
            FUNCTION_END( __PRETTY_FUNCTION__)

        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedRegRespTimer(const boost::system::error_code& error,
            const std::string alias)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (error == boost::asio::error::operation_aborted)
              return;

            mTimerMap.erase(alias);
            strandedRegistrationFailed(alias);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::pointRegistrationMessage(sim_dll::Server::Msg message)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (mData->resultors.find(message.alias) == mData->resultors.end())
              return;

            if (message.msgType == RegPointSucceeded)
              mData->strand.post(bind(&MonitorDelegate::strandedRegistrationSucceeded,
                  shared_from_this(),
                  message.alias));

            else if (message.msgType == RegPointFailed)
              mData->strand.post(bind(&MonitorDelegate::strandedRegistrationFailed,
                  shared_from_this(),
                  message.alias));

            else
              THROW_UNHANDLED_IF_EXCEPTION
            FUNCTION_END( __PRETTY_FUNCTION__)

        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::controlUpdateMessage(sim_dll::Server::Msg message)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            assert(message.msgType == CtrlUpdate);

            // Tell the System workers that the request has been processed
            qDebug("SimDispatcherProxy - Telling resultor that"
                " a contorl has been recieved");

            // Get our stuff
            StringResultorsMapT::iterator itr = mData->resultors.find(message.alias);
            assert(itr != mData->resultors.end());

            ResultorSimListT& lst = itr->second;
            assert(!lst.empty());

            ResultorSimPtrT resultor = lst.front();

            mData->proactor->ioService().post(bind(&ResultorSim::postedSimulatorObjectUpdateRecieved,
                resultor,
                message));
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedMonitorFailed()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            for (StringResultorsMapT::iterator itr = mData->resultors.begin();
                itr != mData->resultors.end(); ++itr)
              strandedRegistrationFailed(itr->first);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        db_simscada::sim::ObjectPtrT
        MonitorDelegate::getSimObject(const std::string& simAlias)
        {
          SimScadaModelPtrT simModel = MainObjectModel::Inst().ormModel();
          sim::ObjectPtrT simObject = simModel->SimObject(simAlias);

          if (!simObject.get())
          {
            boost::format info("SimPt %s does not exist in simSCADA database");
            info % simAlias;
            throw std::runtime_error(info.str());
          }

          return simObject;
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedRegistrationSucceeded(const std::string alias)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          // Get our stuff
            StringResultorsMapT::iterator itr = mData->resultors.find(alias);
            assert(itr != mData->resultors.end());

            ResultorSimListT& lst = itr->second;
            assert(lst.size());

            // Find the response timeout mTimer
            TimerMapT::iterator timerItr = mTimerMap.find(alias);
            if (timerItr == mTimerMap.end())
            {
              qDebug("MonitorDelegate::registrationSucceeded, Can't find mTimer it must have exipred");
              strandedRegistrationFailed(alias);
              return;
            }

            // Stop the mTimer
            timerItr->second->stop();
            mTimerMap.erase(timerItr);

            // Tell the first resultor that its ready to go
            ResultorSimPtrT resultor = lst.front();
            mData->proactor->ioService().post(bind(&ResultorSim::postedSimulatorMonitoringStarted,
                resultor,
                true));
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::strandedRegistrationFailed(const std::string alias)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mData->dispatcher->deregisterForUpdates(alias);

            // Get our stuff
            StringResultorsMapT::iterator itr = mData->resultors.find(alias);
            assert(itr != mData->resultors.end());

            ResultorSimListT& lst = itr->second;
            assert(!lst.empty());

            for (ResultorSimListT::iterator itr = lst.begin(); itr != lst.end();
                ++itr)
            {
              mData->proactor->ioService().post(bind(&ResultorSim::postedSimulatorMonitoringStarted,
                  *itr,
                  false));
            }

            mData->resultors.erase(itr);

            allDone();
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        MonitorDelegate::allDone()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (!mData->resultors.empty())
              return;

            for (SystemMapT::iterator itr = mData->systems.begin();
                itr != mData->systems.end(); ++itr)
              itr->second = idleMode;
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
      /// ---------------------------------------------------------------------------

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
