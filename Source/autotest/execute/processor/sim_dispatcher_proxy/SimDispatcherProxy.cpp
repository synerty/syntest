/*
 * AutoTestExecProcessorSimDispatcher.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "SimDispatcherProxy.h"

#include <cmath>

#include <boost/variant/get.hpp>

#include <simscada/SimscadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimAnaPt.h>
#include <simscada/sim/SimCtrlLink.h>
#include <simscada_util/DbSimscadaUtil.h>

#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest_util/DbSyntestUtil.h>

#include "MainSimModel.h"

#include "Proactor.h"
#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecResultorSim.h"
#include "AutoTestExecInitorSim.h"
#include "AutoTestExecTestQueueModel.h"

#include "SimDispatcherProxyExecuteDelegate.h"
#include "SimDispatcherProxyResetDelegate.h"
#include "SimDispatcherProxyMonitorDelegate.h"
#include "SimDispatcherProxyDelegate.h"

#include "SimDllDispatcher.h"
#include "SimDllDeclaration.h"

using namespace db_simscada;
using namespace db_syntest;
using namespace autotest::exec::processor::sim_dispatcher_proxy;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {

      SimDispatcherProxy::SimDispatcherProxy(ProactorPtrT proactor,
          const std::string& host,
          const int port) :
        mData(new DelegateData(proactor, host, port))
      {
FUNCTION_START      ( __PRETTY_FUNCTION__)
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }
    /// ---------------------------------------------------------------------------

    SimDispatcherProxy::~SimDispatcherProxy()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mData->systems.clear();
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::addObserver(sim_dll::SimDllDispatcherObserverPtrT observer)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mData->dispatcher->addObserver(observer);
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------

    void
    SimDispatcherProxy::removeObserver(sim_dll::SimDllDispatcherObserverPtrT observer)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mData->dispatcher->removeObserver(observer);
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------

    void
    SimDispatcherProxy::removeObserver(const sim_dll::SimDllDispatcherObserver* observer)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mData->dispatcher->removeObserver(observer);
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------

    void
    SimDispatcherProxy::postedConnect()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      // Re-adding our selves causes no problems, it uses a set.
      // We can't add our selves in the consturctor
      // because shared_from_this won't work there.
      mData->dispatcher->addObserver(shared_from_this());

      mData->proactor->ioService().post( //
          boost::bind(&sim_dll::SimDllDispatcher::postedConnect,
              mData->dispatcher));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::postedDisconnect()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mData->proactor->ioService().post( //
          boost::bind(&sim_dll::SimDllDispatcher::postedDisconnect,
              mData->dispatcher));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::addSystem(boost::shared_ptr< System > system)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      assert(mData->systems.find(system) == mData->systems.end());
      mData->systems.insert(std::make_pair(system, idleMode));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::initialiseTest(boost::shared_ptr< test_queue::TqValue > test)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mData->tqValue = test;
      mData->currentSimPt.reset();

      std::string simAlias = mData->tqValue->testValue->testPoint()->simPoint()->simAlias();

      SimScadaModelPtrT simModel = MainObjectModel::Inst().ormModel();
      sim::ObjectPtrT simObject = simModel->SimObject(simAlias);

      if (simObject.get())
      {
        mData->currentSimPt = simObject;
      }
      else
      {
        qCritical("SimPt %s does not exist in simSCADA database",
            simAlias.c_str());
      }

      mDelegate.reset();
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------


    /** Indication Reset For Control
     * This is a helper function for the user, if the need the indication point in
     * the opposite state to perform the control.
     * It's not apart of the testing state machine.
     */
    void
    SimDispatcherProxy::indResetForControl()
    {
      assert(mData.get());
      assert(mData->dispatcher.get());
      assert(mData->currentSimPt.get());
      assert(mDelegate.get());

      SimScadaModelPtrT simModel = MainObjectModel::Inst().ormModel();
      assert(simModel.get());

      std::string simAlias = mData->currentSimPt->alias();

      db_syntest::st::SimPointPtrT stSimCtrlPoint
              = mData->tqValue->testValue->testPoint()->simPoint();

      sim::CtrlLinkPtrT ctrlLink = simModel->SimCtrlLink(simAlias);
      if (!ctrlLink.get())
      {
        qDebug("There is no back indication link for control %s", simAlias.c_str());
        return;
      }

      sim::ObjectPtrT simIndPoint = sim::Object::downcast(ctrlLink->scanPoint());
      assert(simIndPoint.get());

      // If this is a tap raise/lower
      if (db_syntest::toSimPointType(stSimCtrlPoint) == doSimPointType
          && db_simscada::toObjectType(simIndPoint) == AnalogueObject)
      {
        mData->dispatcher->setAnaEngValue(simIndPoint, 10);
      }
      else if (ctrlLink->stateTo().empty())
      {
        const sim_dll::DigStateE simState = mDelegate->getSimDllDigState(ctrlLink->stateTo()[0]);

        // Change the state of the point
        if (simState == sim_dll::A)
        {
          mData->dispatcher->setDigState(simIndPoint, sim_dll::B);
        }
        else
        {
          mData->dispatcher->setDigState(simIndPoint, sim_dll::A);
        }
      }
    }

    void
    SimDispatcherProxy::requestReset(boost::shared_ptr< System > system,
        boost::shared_ptr< InitorSim > initorSim)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      // Repost and use the strand to prevent concurrent access
      mData->strand.post(bind(&SimDispatcherProxy::strandedRequestMade,
              shared_from_this(),
              system,
              initorSim,
              boost::shared_ptr< ResultorSim >(),
              "",
              resetMode));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::requestExecute(boost::shared_ptr< System > system,
        boost::shared_ptr< InitorSim > initorSim)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      // Repost and use the strand to prevent concurrent access
      mData->strand.post(bind(&SimDispatcherProxy::strandedRequestMade,
              shared_from_this(),
              system,
              initorSim,
              boost::shared_ptr< ResultorSim >(),
              "",
              executeMode));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::requestMonitor(boost::shared_ptr< System > system,
        boost::shared_ptr< ResultorSim > resultorSim,
        std::string alias)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      // Repost and use the strand to prevent concurrent access
      mData->strand.post(bind(&SimDispatcherProxy::strandedRequestMade,
              shared_from_this(),
              system,
              boost::shared_ptr< InitorSim >(),
              resultorSim,
              alias,
              monitorMode));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::requestMonitorEnd(boost::shared_ptr< System > system,
        boost::shared_ptr< ResultorSim > resultorSim,
        std::string alias)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      // Repost and use the strand to prevent concurrent access
      // Though if the code is working properly we shouldn't have concurrent access
      mData->strand.post(bind(&SimDispatcherProxy::strandedRequestMonitorEnd,
              shared_from_this(),
              system,
              resultorSim,
              alias));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    /// ---------------
    /// SimDllDispatcherObserver interface implementation

    void
    SimDispatcherProxy::postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
        std::string errorMessage)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      SystemsT systems = mData->getSystems();

      for (SystemsT::iterator itr = systems.begin(); itr != systems.end();
          ++itr)
      {
        SystemPtrT& systemPtr = *itr;

        // Post the update to the system
        mData->strand.post(bind(&System::postedSimulatorConnectionStateChanged,
                systemPtr,
                newState,
                errorMessage));
      }

      if (mDelegate.get())
      {
        // Post the update to the system
        mData->strand.post(bind(&Delegate::strandedSimDllConnectionStateChanged,
                mDelegate,
                newState,
                errorMessage));
      }
      FUNCTION_END( __PRETTY_FUNCTION__)

    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::postedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      if (!mDelegate.get())
      return;

      // Post the update to the system
      mData->strand.post(bind(&Delegate::strandedSimDllObjectUpdateRecieved,
              mDelegate,
              message));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::strandedRequestMade(boost::shared_ptr< System > system,
        boost::shared_ptr< InitorSim > initorSim,
        boost::shared_ptr< ResultorSim > resultorSim,
        std::string alias,
        SimChangeTypeE simChangeType)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      qDebug("SimDispatcher - Recieved change request");
      assert(mData->systems.find(system) != mData->systems.end());

      assert(mData->systems[system] == idleMode);
      mData->systems[system] = simChangeType;

      if (initorSim.get())
      mData->initors.push_back(initorSim);
      else if (resultorSim.get())
      mData->addResultor(alias, resultorSim);
      else
      THROW_UNHANDLED_IF_EXCEPTION

      bool allSystemsInRequestPendingState = true;
      bool allSystemsAreOutOfIdleMode = false;
      for (SystemMapT::iterator itr = mData->systems.begin();
          itr != mData->systems.end(); ++itr)
      {
        allSystemsInRequestPendingState &= (itr->second == simChangeType);
        allSystemsAreOutOfIdleMode &= (itr->second != idleMode);
      }

      if (!allSystemsInRequestPendingState)
      {
        assert(!allSystemsAreOutOfIdleMode);
        return;
      }

      // We need all systems to request the change before we can execute it.
      // Perform the change to the simulator
      switch (simChangeType)
      {
        case resetMode:
        qDebug("SimDispatcher recieved reset request for all systems");
        mDelegate.reset(new sim_dispatcher_proxy::ResetDelegate(mData));
        break;

        case executeMode:
        qDebug("SimDispatcher recieved execute request for all systems");
        mDelegate.reset(new sim_dispatcher_proxy::ExecuteDelegate(mData));
        break;

        case monitorMode:
        qDebug("SimDispatcher recieved monitor request for all systems");
        mDelegate.reset(new sim_dispatcher_proxy::MonitorDelegate(mData));
        break;

        THROW_UNHANDLED_CASE_EXCEPTION
      }

      mDelegate->run();
      FUNCTION_END( __PRETTY_FUNCTION__)

    }
    /// ---------------------------------------------------------------------------

    void
    SimDispatcherProxy::strandedRequestMonitorEnd(boost::shared_ptr< System > system,
        boost::shared_ptr< ResultorSim > resultorSim,
        std::string alias)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      assert(mDelegate.get());
      boost::shared_ptr< MonitorDelegate > monitorDelegate(mDelegate,
          boost::detail::polymorphic_cast_tag());
      monitorDelegate->strandedRequestMonitorEnd(system,
          resultorSim,
          alias);
      FUNCTION_END( __PRETTY_FUNCTION__)
    }
    /// ---------------------------------------------------------------------------

  } /* namespace processor */
} /* namespace exec */
} /* namespace autotest */
