/*
 * SimDispatcherProxyMonitorDelegate.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _SIM_DISPATCHER_PROXY_MONITOR_DELEGATE_H_
#define _SIM_DISPATCHER_PROXY_MONITOR_DELEGATE_H_

#include <boost/asio/error.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "SimDispatcherProxyDelegate.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {

        class MonitorDelegate;
        typedef boost::shared_ptr< MonitorDelegate > MonitorDelegatePtrT;

        class MonitorDelegateTimer;
        typedef boost::shared_ptr< MonitorDelegateTimer > MonitorDelegateTimerPtrT;

        class MonitorDelegateTimer : public boost::enable_shared_from_this<
            MonitorDelegateTimer >, DebugClassMixin< MonitorDelegateTimer >
        {
          public:
            MonitorDelegateTimer(MonitorDelegatePtrT parent,
                DelegateDataPtrT data,
                const std::string alias);

            void
            start();

            void
            stop();

            void
            expired(const boost::system::error_code& error);

            bool
            isActive() const;

          private:
            MonitorDelegatePtrT mParent;
            DelegateDataPtrT mData;
            std::string mAlias;
            boost::asio::deadline_timer mTimer;
            bool mActive;
        };

        /*! Monitor Delegate
         * This delegate is used when Resultors want to monitor for controls.
         * If more than one resultor wants to monitor for the same control then
         * this delegate will let one resultor execute at a time.
         */
        class MonitorDelegate : public Delegate,
            public boost::enable_shared_from_this< MonitorDelegate >,
            DebugClassMixin< MonitorDelegate >
        {
          public:
            MonitorDelegate(DelegateDataPtrT data);

            virtual
            ~MonitorDelegate();

          public:
            // ---------------
            // Delegate interface
            virtual void
            run();

            virtual void
            strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
                std::string errorMessage);

            virtual void
            strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message);

          public:
            // ---------------
            // MonitorDelegate methods

            void
            strandedRequestMonitorEnd(boost::shared_ptr< System > system,
                boost::shared_ptr< ResultorSim > resultorSim,
                std::string alias);

            void
            strandedRegRespTimer(const boost::system::error_code& error,
                const std::string alias);

          private:
            // ---------------
            // Stranded methods

            void
            strandedRun();

          private:

            /*! Point Registration Message
             * handles point event registration messages from the simulator.
             * @param message
             */
            void
            pointRegistrationMessage(sim_dll::Server::Msg message);

            /*! Control Update Message
             * handles control update messages from the simulator.
             * @param message
             */
            void
            controlUpdateMessage(sim_dll::Server::Msg message);

            void
            strandedMonitorFailed();

            db_simscada::sim::ObjectPtrT
            getSimObject(const std::string& simAlias);

            void
            strandedRegistrationSucceeded(const std::string alias);

            void
            strandedRegistrationFailed(const std::string alias);

            void
            allDone();

            typedef std::map< std::string, MonitorDelegateTimerPtrT > TimerMapT;
            TimerMapT mTimerMap;

        };

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* _SIM_DISPATCHER_PROXY_MONITOR_DELEGATE_H_ */
