/*
 * SimDispatcherProxyExecuteDelegate.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef SIMDISPATCHERPROXYEXECUTEDELEGATE_H_
#define SIMDISPATCHERPROXYEXECUTEDELEGATE_H_

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/error.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "SimDispatcherProxyDelegate.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {

        /*! Set Value Delegate
         * Long comment about the class
         *
         */
        class ExecuteDelegate : public Delegate,
            public boost::enable_shared_from_this< ExecuteDelegate >,
            DebugClassMixin< ExecuteDelegate >
        {
          public:
            ExecuteDelegate(DelegateDataPtrT data);

            virtual
            ~ExecuteDelegate();

          public:
            // ---------------
            // Delegate interface
            virtual void
            run();

            virtual void
            strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
                std::string errorMessage);

            virtual void
            strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message);

          public:
            // ---------------
            // ResetDelegate methods

          private:

            enum StateE
            {
              //! Not in any state
              idleDelegateState,

              //! Waiting for set value command acknoledgment state
              waitForAckDelegateState,

            };

            void
            completeChangeRequest(bool success);

            void
            strandedRespAckTimer(const boost::system::error_code& error);

            StateE mState;

            boost::asio::deadline_timer mRespAckTimer;
            bool mRespAckTimerActive;

        };

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* SIMDISPATCHERPROXYEXECUTEDELEGATE_H_ */
