/*
 * SimDispatcherProxyResetDelegate.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef SIMDISPATCHERPROXYRESETDELEGATE_H_
#define SIMDISPATCHERPROXYRESETDELEGATE_H_

#include <boost/asio/error.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "SimDispatcherProxyDelegate.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {

        /*! Set Value Delegate
         * Long comment about the class
         *
         */
        class ResetDelegate : public Delegate,
            public boost::enable_shared_from_this< ResetDelegate >,
            DebugClassMixin< ResetDelegate >
        {
          public:
            ResetDelegate(DelegateDataPtrT data);

            virtual
            ~ResetDelegate();

          public:
            // ---------------
            // Delegate interface
            virtual void
            run();

            virtual void
            strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
                std::string errorMessage);

            virtual void
            strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message);

          public:
            // ---------------
            // ResetDelegate methods

            void
            strandedRespQryTimer(const boost::system::error_code& error);

            void
            strandedRespAckTimer(const boost::system::error_code& error);

          private:

            enum StateE
            {
              //! Not in any state
              idleDelegateState,

              //! Waiting for query info return state
              waitForQueryInfoDelegateState,

              //! Waiting for set value command acknoledgment state
              waitForSetValAckDelegateState,

            };

            void
            performReset(sim_dll::Server::Msg msg);

            void
            completeChangeRequest(bool success,
                bool resetRequiredStateChange = false);

            StateE mState;

            boost::asio::deadline_timer mRespQryTimer;
            bool mRespQryTimerActive;

            boost::asio::deadline_timer mRespAckTimer;
            bool mRespAckTimerActive;

        };

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* SIMDISPATCHERPROXYRESETDELEGATE_H_ */
