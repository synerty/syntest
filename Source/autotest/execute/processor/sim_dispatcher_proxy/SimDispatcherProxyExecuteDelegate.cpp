/*
 * SimDispatcherProxyExecuteDelegate.cpp
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "SimDispatcherProxyExecuteDelegate.h"

#include <boost/asio/placeholders.hpp>

#include <simscada/SimscadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimAnaPt.h>
#include <simscada_util/DbSimscadaUtil.h>

#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Globals.h"
#include "Proactor.h"

#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecInitorSim.h"
#include "AutoTestExecTestQueueModel.h"

#include "SimDllDeclaration.h"

using namespace db_simscada;
using namespace db_syntest;
using namespace boost;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {

        ExecuteDelegate::ExecuteDelegate(DelegateDataPtrT data) :
            Delegate(data), //
            mState(idleDelegateState), //
            mRespAckTimer(data->proactor->ioService()), //
            mRespAckTimerActive(false)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }

        ExecuteDelegate::~ExecuteDelegate()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }

        void
        ExecuteDelegate::run()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mState = idleDelegateState;

            if (!mData->dispatcher->isConnected() || !mData->currentSimPt.get())
            {
              qDebug("ExecuteDelegate performing simulator change - FAILED");
              completeChangeRequest(false);
              return;
            }

            qDebug("ExecuteDelegate performing simulator change");

            assert(mData->dispatcher.get());
            assert(mData->tqValue.get());
            assert(mData->tqValue->testValue.get());

            db_simscada::ObjectTypeE simPtType = toObjectType(mData->currentSimPt);

            switch (simPtType)
            {
              case DigitalObject:
              {
                const std::string digStateStr = mData->tqValue->testValue->simDigValue();
                char state = digStateStr[0];
                mData->dispatcher->setDigState(mData->currentSimPt,
                    getSimDllDigState(state));

                break;
              }

              case AnalogueObject:
              {
                double value = mData->tqValue->testValue->simValue();
                mData->dispatcher->setAnaEngValue(mData->currentSimPt, value);

                break;
              }

              THROW_UNHANDLED_CASE_EXCEPTION
            }

            mState = waitForAckDelegateState;

            mRespAckTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
            mRespAckTimer.async_wait(mData->strand.wrap(bind(&ExecuteDelegate::strandedRespAckTimer,
                shared_from_this(),
                boost::asio::placeholders::error)));
            mRespAckTimerActive = true;
            FUNCTION_END( __PRETTY_FUNCTION__)

        }
        /// ---------------------------------------------------------------------------

        void
        ExecuteDelegate::strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
            std::string errorMessage)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            qDebug("ExecuteDelegate"
                " performing simulator change - FAILED, connection state changed");

            mRespAckTimer.cancel();
            mRespAckTimerActive = false;

            completeChangeRequest(false);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        ExecuteDelegate::strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            using namespace sim_dll::Server;

            bool isAck = message.msgType == DigSetValAckMsg //
            || message.msgType == AnaSetValAckMsg //
            || message.msgType == AccSetValAckMsg //
            || message.msgType == StrSetValAckMsg;

            assert(message.alias == mData->currentSimPt->alias());

            switch (mState)
            {
              case idleDelegateState:
              {
                throw std::runtime_error("ExecuteDelegate, Recieved message while at idle");
                break;
              }

              case waitForAckDelegateState:
              {
                if (!isAck)
                {
                  throw std::runtime_error("ExecuteDelegate, Waiting for ACK message"
                      ", Recieved something else instead");
                }

                mState = idleDelegateState;

                if (!mRespAckTimerActive)
                {
                  qDebug("ExecuteDelegate, Received SetValAck after timer expired");
                  return;
                }

                mRespAckTimer.cancel();
                mRespAckTimerActive = false;

                completeChangeRequest(true);
                break;
              }

              THROW_UNHANDLED_CASE_EXCEPTION
            }
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        ExecuteDelegate::completeChangeRequest(bool success)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          // Tell the System workers that the request has been processed
            qDebug("ExecuteDelegate - Telling Initor workers that"
                " the request has been processed");

            for (SystemMapT::iterator itr = mData->systems.begin();
                itr != mData->systems.end(); ++itr)
              itr->second = idleMode;

            for (InitorSimListT::iterator itr = mData->initors.begin();
                itr != mData->initors.end(); ++itr)
            {
              mData->proactor->ioService().post(bind(&InitorSim::postedSimExecuteRequestDispatched,
                  (*itr),
                  success));
            }

            mData->initors.clear();
            FUNCTION_END( __PRETTY_FUNCTION__)

        }
        /// ---------------------------------------------------------------------------

        void
        ExecuteDelegate::strandedRespAckTimer(const boost::system::error_code& error)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (error == boost::asio::error::operation_aborted)
              return;
            mRespAckTimerActive = false;

            completeChangeRequest(false);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
      /// ---------------------------------------------------------------------------

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
