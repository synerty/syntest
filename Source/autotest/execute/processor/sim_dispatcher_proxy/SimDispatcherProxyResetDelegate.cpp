/*
 * SimDispatcherProxyResetDelegate.cpp
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "SimDispatcherProxyResetDelegate.h"

#include <boost/asio/placeholders.hpp>

#include <simscada/SimscadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimAnaPt.h>
#include <simscada_util/DbSimscadaUtil.h>

#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Globals.h"
#include "Proactor.h"

#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecInitorSim.h"
#include "AutoTestExecTestQueueModel.h"

#include "SimDllDeclaration.h"

using namespace boost;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {

        ResetDelegate::ResetDelegate(DelegateDataPtrT data) :
            Delegate(data), //
            mState(idleDelegateState), //
            mRespQryTimer(data->proactor->ioService()), //
            mRespQryTimerActive(false), //
            mRespAckTimer(data->proactor->ioService()), //
            mRespAckTimerActive(false)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        ResetDelegate::~ResetDelegate()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::run()
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            mState = idleDelegateState;

            if (!mData->dispatcher->isConnected())
            {
              completeChangeRequest(false);
              return;
            }

            // Start the timeout timer
            mRespQryTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
            mRespQryTimer.async_wait(mData->strand.wrap(bind(&ResetDelegate::strandedRespQryTimer,
                shared_from_this(),
                boost::asio::placeholders::error)));
            mRespQryTimerActive = true;
            mState = waitForQueryInfoDelegateState;

            // Request point info
            mData->dispatcher->requestPointInfo(mData->currentSimPt);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::performReset(sim_dll::Server::Msg msg)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            using namespace sim_dll::Server;
            using namespace db_simscada;

            bool resetChangeRequired = false;

            switch (msg.msgType)
            {
              case DigUpdate:
              {
                // Get both states
                const sim_dll::DigStateE simState = boost::get< DigPointInfo >(msg.pointInfo).value;

                const std::string digStateStr = mData->tqValue->testValue->simDigValue();
                const sim_dll::DigStateE state = getSimDllDigState(digStateStr[0]);

                resetChangeRequired = simState == state;

                // If the states are different then the test is reset, return
                if (!resetChangeRequired)
                  break;

                // Change the state of the point
                if (simState == sim_dll::A)
                  mData->dispatcher->setDigState(mData->currentSimPt, sim_dll::B);
                else
                  mData->dispatcher->setDigState(mData->currentSimPt, sim_dll::A);

                break;
              }

              case AnaUpdate:
              {
                double simValue = boost::get< AnaPointInfo >(msg.pointInfo).value;
                double testValue = mData->tqValue->testValue->simValue();

                sim::AnaPtPtrT anaPt = sim::AnaPt::downcast(mData->currentSimPt);

                const double range = fabs(anaPt->engMax() - anaPt->engMin());

                // The differece needs to be more than 20%
                const double requiredDifference = range * 0.20;

                const double difference = fabs(simValue - testValue);

                resetChangeRequired = difference < requiredDifference;
                if (!resetChangeRequired)
                  break;

                // Else, find if its closer to the min or the max and change to the other

                const double distToMin = fabs(simValue - anaPt->engMin());
                const double distToMax = fabs(anaPt->engMax() - simValue);

                if (distToMax < distToMin)
                  mData->dispatcher->setAnaEngValue(mData->currentSimPt,
                      distToMin);
                else
                  mData->dispatcher->setAnaEngValue(mData->currentSimPt,
                      distToMax);

                break;
              }

              THROW_UNHANDLED_CASE_EXCEPTION
            }

            if (!resetChangeRequired)
            {
              mState = idleDelegateState;
              completeChangeRequest(true, false);
              return;
            }

            mState = waitForSetValAckDelegateState;

            mRespAckTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
            mRespAckTimer.async_wait(mData->strand.wrap(bind(&ResetDelegate::strandedRespAckTimer,
                shared_from_this(),
                boost::asio::placeholders::error)));
            mRespAckTimerActive = true;
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
            std::string errorMessage)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            using namespace sim_dll::Server;

            bool isAck = message.msgType == DigSetValAckMsg //
            || message.msgType == AnaSetValAckMsg //
            || message.msgType == AccSetValAckMsg //
            || message.msgType == StrSetValAckMsg;

            bool isUpdate = message.msgType == DigUpdate //
            || message.msgType == AnaUpdate //
            || message.msgType == AccUpdate //
            || message.msgType == StrUpdate;

            assert(message.alias == mData->currentSimPt->alias());

            switch (mState)
            {
              case idleDelegateState:
              {
                throw std::runtime_error("ExecuteDelegate, Recieved message while at idle");
                break;
              }

              case waitForQueryInfoDelegateState:
              {
                if (!isUpdate)
                {
                  throw std::runtime_error("ExecuteDelegate, Waiting for query response message"
                      ", Recieved something else instead");
                }

                mState = idleDelegateState;

                if (mRespQryTimerActive)
                {
                  mRespQryTimer.cancel();
                  mRespQryTimerActive = false;
                  performReset(message);
                }
                else
                {
                  qDebug("ExecuteDelegate, Received query info after timer expired");
                }

                break;
              }

              case waitForSetValAckDelegateState:
              {
                if (!isAck)
                {
                  throw std::runtime_error("ExecuteDelegate, Waiting for ACK message"
                      ", Recieved something else instead");
                }

                mState = idleDelegateState;

                if (mRespAckTimerActive)
                {
                  mRespAckTimer.cancel();
                  mRespAckTimerActive = false;

                  completeChangeRequest(true, true);
                }
                else
                {
                  qDebug("ExecuteDelegate, Received SetValAck after timer expired");
                }

                break;
              }

              THROW_UNHANDLED_CASE_EXCEPTION
            }
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::completeChangeRequest(bool success,
            bool resetRequiredStateChange)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
          // Tell the System workers that the request has been processed
            qDebug("ResetDelegate - Telling Initor workers that"
                " the request has been processed");

            for (SystemMapT::iterator itr = mData->systems.begin();
                itr != mData->systems.end(); ++itr)
              itr->second = idleMode;

            for (InitorSimListT::iterator itr = mData->initors.begin();
                itr != mData->initors.end(); ++itr)
            {
              mData->proactor->ioService().post(bind(&InitorSim::postedSimResetRequestDispatched,
                  (*itr),
                  success,
                  resetRequiredStateChange));
            }

            mData->initors.clear();
            FUNCTION_END( __PRETTY_FUNCTION__)

        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::strandedRespQryTimer(const boost::system::error_code& error)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (!mRespQryTimerActive
                || error == boost::asio::error::operation_aborted)
              return;
            mRespQryTimerActive = false;

            completeChangeRequest(false);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
        /// ---------------------------------------------------------------------------

        void
        ResetDelegate::strandedRespAckTimer(const boost::system::error_code& error)
        {
          FUNCTION_START( __PRETTY_FUNCTION__)
            if (!mRespAckTimerActive
                || error == boost::asio::error::operation_aborted)
              return;
            mRespAckTimerActive = false;

            completeChangeRequest(false);
            FUNCTION_END( __PRETTY_FUNCTION__)
        }
      /// ---------------------------------------------------------------------------

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
