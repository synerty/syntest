/*
 * SimDispatcherProxySetValueDelegate.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef SIMDISPATCHERPROXYDELEGATE_H_
#define SIMDISPATCHERPROXYDELEGATE_H_

#include <map>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/strand.hpp>

#include "SimDllDispatcher.h"
#include "AutoTestExecTestQueueModel.h"
#include "AutoTestExecDeclarations.h"

class Proactor;

namespace sim_dll
{
  class SimDllDispatcher;
}

namespace autotest
{
  namespace exec
  {
    namespace test_queue
    {
      class TqValue;
    }

    namespace processor
    {
      class System;
      class InitorSim;

      namespace sim_dispatcher_proxy
      {
        /*! System Change Type Enum
         * Each system must go through the same states together, this ensures
         * that each system is ready for the state change when it occures
         */
        enum SimChangeTypeE
        {
          idleMode,
          resetMode,
          executeMode,
          monitorMode
        };

        typedef std::map< boost::weak_ptr< System >, SimChangeTypeE >
            SystemMapT;
        typedef std::vector< boost::shared_ptr< InitorSim > > InitorSimListT;
        typedef std::list< ResultorSimPtrT > ResultorSimListT;
        typedef std::map< std::string, ResultorSimListT > StringResultorsMapT;

        class DelegateData;
        typedef boost::shared_ptr< DelegateData > DelegateDataPtrT;

        struct DelegateData : private DebugClassMixin< DelegateData >
        {
            DelegateData(boost::shared_ptr< Proactor > proactor,
                const std::string& host,
                const int port);

            SystemsT
            getSystems();

            void
            addResultor(std::string alias, ResultorSimPtrT resultor);

            boost::shared_ptr< Proactor > proactor;
            boost::asio::strand strand;
            boost::shared_ptr< sim_dll::SimDllDispatcher > dispatcher;

            SystemMapT systems;

            // List of initors which will be called when the change is performed.
            InitorSimListT initors;

            // Data structure of resultors which
            // want to monitor simSCADA for contorls.
            StringResultorsMapT resultors;

            boost::shared_ptr< test_queue::TqValue > tqValue;
            db_simscada::sim::ObjectPtrT currentSimPt;
        };

        /*! Sim Dispatcher Proxy Delegate
         * Delegate resonsible for interacting with the sim dispatcher for a
         * specific action, IE :
         * * Set value
         * * ?
         */
        class Delegate : DebugClassMixin< Delegate >
        {

          public:
            Delegate(DelegateDataPtrT data);

            virtual
            ~Delegate();

          public:
            virtual void
            run() = 0;

            virtual void
                strandedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
                    std::string errorMessage) = 0;

            virtual void
            strandedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message) = 0;

            sim_dll::DigStateE
            getSimDllDigState(char state);

          protected:
            DelegateDataPtrT mData;
        };

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* SIMDISPATCHERPROXYDELEGATE_H_ */
