/*
 * SimDispatcherProxy.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _SIM_DISPATCHER_PROXY_H_
#define _SIM_DISPATCHER_PROXY_H_

#include <map>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/strand.hpp>

#include <syntest/DbSyntestDeclaration.h>
#include <simscada/DbSimscadaDeclaration.h>

#include "SimDllDispatcherObserver.h"
#include "SimDispatcherProxyDelegate.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      /*! autotest::exec::processor::SimDispatcher Brief Description
       * Long comment about the class
       *
       */
      class SimDispatcherProxy : //
      public boost::enable_shared_from_this< SimDispatcherProxy >,
          public sim_dll::SimDllDispatcherObserver,
          DebugClassMixin< SimDispatcherProxy >
      {
        public:
          SimDispatcherProxy(boost::shared_ptr< Proactor > proactor,
              const std::string& host,
              const int port);

          virtual
          ~SimDispatcherProxy();

        public:
          /// ---------------
          /// SimDllDispatcher observable adaptor function

          virtual
          void
          addObserver(sim_dll::SimDllDispatcherObserverPtrT observer);

          virtual
          void
          removeObserver(sim_dll::SimDllDispatcherObserverPtrT observer);

          virtual
          void
          removeObserver(const sim_dll::SimDllDispatcherObserver* observer);

        public:
          void
          postedConnect();

          void
          postedDisconnect();

        public:
          void
          addSystem(boost::shared_ptr< System > system);

          void
          initialiseTest(boost::shared_ptr< test_queue::TqValue > test);

          void
          indResetForControl();

          void
          requestReset(boost::shared_ptr< System > system,
              boost::shared_ptr< InitorSim > initorSim);

          void
          requestExecute(boost::shared_ptr< System > system,
              boost::shared_ptr< InitorSim > initorSim);

          void
          requestMonitor(boost::shared_ptr< System > system,
              boost::shared_ptr< ResultorSim > resultorSim,
              std::string alias);

          void
          requestMonitorEnd(boost::shared_ptr< System > system,
              boost::shared_ptr< ResultorSim > resultorSim,
              std::string alias);

        private:
          /// ---------------
          /// SimDllDispatcherObserver interface implementation

          void
          postedSimDllConnectionStateChanged(tcp_client::ConnectionStateE newState,
              std::string errorMessage);

          void
          postedSimDllObjectUpdateRecieved(sim_dll::Server::Msg message);

        private:

          void
          strandedRequestMade(boost::shared_ptr< System > system,
              boost::shared_ptr< InitorSim > initorSim,
              boost::shared_ptr< ResultorSim > resultorSim,
              std::string alias,
              sim_dispatcher_proxy::SimChangeTypeE simChangeType);

          void
          strandedRequestMonitorEnd(boost::shared_ptr< System > system,
              boost::shared_ptr< ResultorSim > resultorSim,
              std::string alias);

          sim_dispatcher_proxy::DelegateDataPtrT mData;

          typedef boost::shared_ptr< sim_dispatcher_proxy::Delegate > DelegatePtrT;
          DelegatePtrT mDelegate;

      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* _SIM_DISPATCHER_PROXY_H_ */
