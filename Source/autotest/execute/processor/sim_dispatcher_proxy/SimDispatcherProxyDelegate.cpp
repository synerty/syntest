/*
 * SimDispatcherProxyResetDelegate.cpp
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "SimDispatcherProxyResetDelegate.h"

#include "Proactor.h"

#include <simscada/sim/SimObject.h>

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      namespace sim_dispatcher_proxy
      {
        /// -------------------------------------------------------------------
        /// DelegateData
        /// -------------------------------------------------------------------

        DelegateData::DelegateData(boost::shared_ptr< Proactor > proactor_,
            const std::string& host,
            const int port) :
            proactor(proactor_), //
            strand(proactor->ioService()), //
            dispatcher(new sim_dll::SimDllDispatcher(proactor, host, port))
        {
          FUNCTION_START( __PRETTY_FUNCTION__ )
            FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        SystemsT
        DelegateData::getSystems()
        {
          FUNCTION_START( __PRETTY_FUNCTION__ )
          SystemsT retSystems;
          for (SystemMapT::iterator itr = systems.begin();
              itr != systems.end();)
          {
            boost::shared_ptr< System > systemPtr = itr->first.lock();

            if (!systemPtr.get())
            {
              systems.erase(itr++);
              continue;
            }
            retSystems.push_back(systemPtr);
            ++itr;
          }
          return retSystems;
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        void
        DelegateData::addResultor(std::string alias, ResultorSimPtrT resultor)
        {
          FUNCTION_START( __PRETTY_FUNCTION__ )
          StringResultorsMapT::iterator itr = resultors.find(alias);
          if (itr == resultors.end())
            itr = resultors.insert(std::make_pair(alias, ResultorSimListT())).first;

          itr->second.push_back(resultor);
          FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        /// -------------------------------------------------------------------
        /// Delegate
        /// -------------------------------------------------------------------

        Delegate::Delegate(DelegateDataPtrT data) :
            mData(data)
        {
          FUNCTION_START( __PRETTY_FUNCTION__ )
            FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        Delegate::~Delegate()
        {
          FUNCTION_START( __PRETTY_FUNCTION__ )
            FUNCTION_END( __PRETTY_FUNCTION__ )
        }
        /// ---------------------------------------------------------------------------

        sim_dll::DigStateE
        Delegate::getSimDllDigState(char state)
        {
          FUNCTION_START( __PRETTY_FUNCTION__ )
          sim_dll::DigStateE digStateEnum = sim_dll::DigStateUnset;

          if (state == 'A')
            digStateEnum = sim_dll::A;

          else if (state == 'B')
            digStateEnum = sim_dll::B;

          else if (state == 'C')
            digStateEnum = sim_dll::C;

          else if (state == 'D')
            digStateEnum = sim_dll::D;

          else
          {
            std::string stateStr;
            stateStr.push_back(state);
            qCritical("Can not convert digital state \"%s\" of point %s to enum",
                stateStr.c_str(),
                mData->currentSimPt->alias().c_str());
          }

          return digStateEnum;

          FUNCTION_END( __PRETTY_FUNCTION__ )
          assert(false);
          return sim_dll::A;
        }
/// ---------------------------------------------------------------------------

      } /* namespace sim_dispatcher_proxy */
    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
