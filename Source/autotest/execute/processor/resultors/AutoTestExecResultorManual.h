#ifndef _AUTO_TEST_EXEC_RESULTOR_MANUAL_H
#define _AUTO_TEST_EXEC_RESULTOR_MANUAL_H

#include <boost/enable_shared_from_this.hpp>

#include "ui_AutoTestExecResultorManualUi.h"

#include "AutoTestExecResultor.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class ResultorManual : public Resultor,
          public boost::enable_shared_from_this< ResultorManual >,
          DebugClassMixin< ResultorManual >
      {
        Q_OBJECT

        public:
          ResultorManual(ResultorData& data, QWidget* panel);

          virtual
          ~ResultorManual();

        public:
          /// ---------------
          /// Resultor interface

          void
          postedRun();

          void
          postedInitorExecuteCompleted(bool success);

        public slots:
          /// ---------------
          /// Ui Feedback

          void
          resultRecieved();

        public slots:
          /// ---------------
          /// Stuff to make the UI thread process stuff

          void
          setupUiSlot(QWidget* panel);

          void
          okClicked();

          void
          allGoodClicked();

          void
          copyPointIdClicked();

          void
          copyPointPidClicked();

        signals:

          void
          setupUiSignal(QWidget* panel);

        public:
          /// ---------------
          /// ResultorManual stuff

        private:

          enum CheckStateE
          {
            ChkNull = 0,
            ChkGood = 1,
            ChkBad = 2
          };

          void
          refreshUiSlot();

          boost::shared_ptr< Resultor >
          getThis();

          CheckStateE
          getCheckState(QRadioButton &good, QRadioButton &bad);

          Ui::AutoTestExecResultorManualUiClass ui;

          bool mInitorExecuting;
      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif // _AUTO_TEST_EXEC_RESULTOR_MANUAL_H
