#include "AutoTestExecResultorAgent.h"

#include <boost/asio/placeholders.hpp>

#include <QtGui/qwidget.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Globals.h"
#include "Proactor.h"
#include "AgentDispatcher.h"
#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecTestQueueModel.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      ResultorAgent::ResultorAgent(ResultorData& data, QWidget* panel) :
          Resultor(data), //
          mAgentRespAckTimer(data.proactor->ioService())
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          assert(mData.agentDispatcher.get());

          connect(this,
              SIGNAL(setupUiSignal(QWidget*)),
              this,
              SLOT(setupUiSlot(QWidget*)),
              Qt::BlockingQueuedConnection);

          emit
          setupUiSignal(panel);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      ResultorAgent::~ResultorAgent()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorAgent::setupUiSlot(QWidget* panel)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mWidget = new QWidget(panel);
          ui.setupUi(mWidget);
          panel->layout()->addWidget(mWidget);

          connect(this,
              SIGNAL(uiRefreshTick()),
              this,
              SLOT(refreshUiSlot()),
              Qt::QueuedConnection);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorAgent::refreshUiSlot()
      {
//        FUNCTION_START( __PRETTY_FUNCTION__ )
        if (!mWidget)
          return;
        ui.txtStatus->setText(stateStr().c_str());
//        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorAgent::postedRun()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          qDebug("ResultorAgent %s starting",
              mData.systemRecord->name().c_str());
          mState = monitoringSetupResultorState;

          test_queue::TqValuePtrT tqValue = mData.tqSysValue->tqValue.lock();
          assert(tqValue.get());
          assert(tqValue->testValue.get());

          mData.agentDispatcher->monitorPoint(sysPoint());

          mAgentRespAckTimer.expires_from_now(boost::posix_time::millisec(AGENT_TCP_TIMEOUT));
          mAgentRespAckTimer.async_wait(boost::bind(&ResultorAgent::agentRespAckTimer,
              shared_from_this(),
              boost::asio::placeholders::error));

          FUNCTION_END( __PRETTY_FUNCTION__)

      }

      void
      ResultorAgent::agentPointMonitoringStarted(agent::Client::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          using namespace agent::Client;

          // If the agent timed out then the timer will have reset the state
          if (mState != monitoringSetupResultorState)
            return;

          MonitoringStartedMsgData & data = boost::get< MonitoringStartedMsgData >(message.msgData);

          if (data.id != sysPoint()->id())
            return;

          // Cancel the timer
          mAgentRespAckTimer.cancel();

          mState = monitoringResultorState;

          qDebug("ResultorAgent %s recieved agent monitoring acknoledgement",
              mData.systemRecord->name().c_str());

          // Tell the initor to execute the change
          mData.proactor->ioService().post(bind(&Initor::postedExecute,
              mData.initor));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorAgent::postedInitorExecuteCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mStrand.post(bind(&ResultorAgent::strandedInitorExecuteCompleted,
              shared_from_this(),
              success));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorAgent::strandedInitorExecuteCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        // Use a strand so we know nothing else is happening when we start the timer
          qDebug("ResultorAgent %s initor execute completed",
              mData.systemRecord->name().c_str());

          // Do something with success

          // Create a timer and make it wait
          mTimer.expires_from_now(boost::posix_time::seconds(mTimeout));
          mTimer.async_wait(mStrand.wrap(bind(&ResultorAgent::strandedResult,
              shared_from_this(),
              true)));
          FUNCTION_END( __PRETTY_FUNCTION__)

      }

      /// ---------------
      /// agent::AgentDispatcherObserver interface implementation

      void
      ResultorAgent::postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
          std::string errorMessage)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        // Ignore for the moment
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorAgent::postedAgentRecievedMessage(agent::Client::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (mState == resetResultorState)
          {
            qDebug("ResultorAgent shutdown before indication received message,\n"
                "this should be because the timeout has expired");
            return;
          }

          using namespace agent::Client;
          qDebug("ResultorAgent %s recieved agent message",
              mData.systemRecord->name().c_str());
          // Use a strand so we know nothing else is happening when we start the timer

          // We'll get a message when monitoring has started
          if (message.msgType == monitoringStartedMsgType)
          {
            agentPointMonitoringStarted(message);
            return;
          }

          // We'll get a message when monitoring has started
          if (message.msgType == diChangedMsgType
              || message.msgType == aiChangedMsgType
              || message.msgType == acChangedMsgType)
          {
            agentObservedStateChange(message);
            return;
          }

          qDebug("ResultorAgent %s agent message not for us",
              mData.systemRecord->name().c_str());
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorAgent::agentObservedStateChange(agent::Client::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          using namespace agent::Client;

          qDebug("ResultorAgent %s recieved agent result observed",
              mData.systemRecord->name().c_str());

          // There is nothing worse than that accessing null shared pointer bug
          db_syntest::SimPointTypeE simPointType = db_syntest::toSimPointType(simPoint());

          bool isDi = (simPointType == db_syntest::di1SimPointType);
          isDi |= (simPointType == db_syntest::di2SimPointType);
          isDi &= (message.msgType == diChangedMsgType);

          bool isAi = (simPointType == db_syntest::aiSimPointType);
          isAi &= (message.msgType == aiChangedMsgType);

          bool isAc = (simPointType == db_syntest::acSimPointType);
          isAc &= (message.msgType == acChangedMsgType);

          if (!(isAi || isDi || isAc))
            return;

          double remainingTime = mTimer.expires_from_now().total_milliseconds()
              / 1000.0f;
          mTimer.cancel();
          mData.tqSysValue->testSysValueRun->setFeedbackRecieved(true);
          mData.tqSysValue->testSysValueRun->setFeedbackTime(remainingTime);

          if (isAi)
          {
            AiChangedMsgData & data = boost::get< AiChangedMsgData >(message.msgData);
            if (data.id != sysPoint()->id())
              return;

            mData.tqSysValue->testSysValueRun->setSysRawValue(data.value);

            // FIXME Translation tables
            mData.tqSysValue->testSysValueRun->setSysValue(data.value);
          }

          if (isDi)
          {
            DiChangedMsgData & data = boost::get< DiChangedMsgData >(message.msgData);
            if (data.id != sysPoint()->id())
              return;

            mData.tqSysValue->testSysValueRun->setSysRawValue(data.value);
            mData.tqSysValue->testSysValueRun->setSysRawDigState(data.state);

            // FIXME Translation tables
            mData.tqSysValue->testSysValueRun->setSysValue(data.value);
            mData.tqSysValue->testSysValueRun->setSysDigState(data.state);
          }

          mStrand.post(bind(&ResultorAgent::strandedResult,
              shared_from_this(),
              false));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorAgent::strandedResult(bool timerExpired)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (mState != monitoringResultorState)
            return;

          // Use a strand so strandedResult doesn't get run by the timer and feedback
          // concurrently
          qDebug("ResultorAgent %s - Result obtained =  %s",
              mData.systemRecord->name().c_str(),
              (timerExpired ? "Timeout Expired" : "Agent Observed Change"));

          // Cancel the agentDispacher and cancel the timer
          mData.agentDispatcher->cancelMontoring(sysPoint());
          mTimer.cancel();

          const bool success = true;
          // Test out the SimDispatcher
          mData.proactor->ioService().post(bind(&System::postedResultorCompleted,
              mData.system,
              success));

          mState = idleResultorState;
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorAgent::agentRespAckTimer(const boost::system::error_code& error)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          if (error == boost::asio::error::operation_aborted)
            return;

          mState == idleResultorState;
          mData.proactor->ioService().post(bind(&System::postedResultorCompleted,
              mData.system,
              false));

          FUNCTION_END( __PRETTY_FUNCTION__)
      }
      /// ---------------------------------------------------------------------------

      boost::shared_ptr< Resultor >
      ResultorAgent::getThis()
      {
        // basePtr seems to be conflicting with somewhere else
        boost::shared_ptr< Resultor > basePtr_;
        basePtr_ = shared_from_this();
        return basePtr_;
      }

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
