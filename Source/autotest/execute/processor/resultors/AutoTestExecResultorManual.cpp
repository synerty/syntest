#include "AutoTestExecResultorManual.h"

#include <boost/format.hpp>

#include <QtGui/qwidget.h>
#include <QtGui/qclipboard.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest/st/StSysPoint.h>

#include "AutoTestExecTestQueueModel.h"

#include "Proactor.h"
#include "AutoTestExecProcessorSystem.h"

using namespace db_syntest;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      ResultorManual::ResultorManual(ResultorData& data, QWidget* panel) :
        Resultor(data), mInitorExecuting(false)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          connect(this,
              SIGNAL(setupUiSignal(QWidget*)),
              this,
              SLOT(setupUiSlot(QWidget*)),
              Qt::BlockingQueuedConnection);

          emit
          setupUiSignal(panel);
          FUNCTION_END ( __PRETTY_FUNCTION__)
      }

      ResultorManual::~ResultorManual()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorManual::setupUiSlot(QWidget* panel)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          mWidget = new QWidget(panel);
          ui.setupUi(mWidget);
          panel->layout()->addWidget(mWidget);

          assert(mData.tqSysValue.get());
          assert(mData.tqSysValue->testSysValue.get());
          assert(mData.tqSysValue->testSysValue->sysPoint().get());

          st::SysPointPtrT
              sysPoint = mData.tqSysValue->testSysValue->sysPoint();
          assert(sysPoint.get());

          ui.txtPointId->setText(sysPoint->id().c_str());
          ui.txtPointPid->setText(sysPoint->pid().c_str());
          // ui.txtPointAlias->setText(sysPoint->alias().c_str());
          ui.txtPointName->setText(sysPoint->name().c_str());

          // Only show for dig points
          ui.grpStateText->setVisible(isDigPoint());

          // Set the values from our last test
          db_syntest::st::TestSysValueRunPtrT
              sysVal = mData.tqSysValue->testSysValueRun;
          ui.txtComment->setText(QString(sysVal->sysComment().c_str()));

          if (!sysVal->isSysDisplayOkNull())
          {
            if (sysVal->sysDisplayOk())
              ui.optDisplayGood->setChecked(true);
            else
              ui.optDisplayBad->setChecked(true);
          }

          if (!sysVal->isSysEventOkNull())
          {
            if (sysVal->sysEventOk())
              ui.optEventGood->setChecked(true);
            else
              ui.optEventBad->setChecked(true);
          }

          if (!sysVal->isSysValueOkNull())
          {
            if (sysVal->sysValueOk())
              ui.optValueGood->setChecked(true);
            else
              ui.optValueBad->setChecked(true);
          }

          if (!sysVal->isSysStateTextOkNull())
          {
            if (sysVal->sysStateTextOk())
              ui.optStateTextGood->setChecked(true);
            else
              ui.optStateTextBad->setChecked(true);
          }

          std::string toState = //
          boost::lexical_cast< std::string >(testValue()->simValue());

          if (isDigPoint() && !testValue()->simDigState().empty())
          {
            toState = "(" + toState + ") " + testValue()->simDigState();
          }

          ui.txtToState->setText(toState.c_str());

          connect(ui.btnOk, SIGNAL(clicked()), this, SLOT(okClicked()));

          connect(ui.btnAllGood,
              SIGNAL(clicked()),
              this,
              SLOT(allGoodClicked()));

          connect(ui.btnCopyPid,
              SIGNAL(clicked()),
              this,
              SLOT(copyPointPidClicked()));

          connect(this,
              SIGNAL(uiRefreshTick()),
              this,
              SLOT(refreshUiSlot()),
              Qt::QueuedConnection);
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorManual::refreshUiSlot()
      {
        if (!mWidget)
          return;

        mWidget->setEnabled(!mInitorExecuting && mState
            == monitoringResultorState);
        ui.txtStatus->setText(stateStr().c_str());
      }

      void
      ResultorManual::postedRun()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          qDebug("ResultorManual %s starting",
              mData.systemRecord->name().c_str());
          mState = monitoringResultorState;

          // Tell the initor to execute the change
          mData.proactor->ioService().post(bind(&Initor::postedExecute,
              mData.initor));

          //        // Create a timer and make it wait
          //        mTimer.expires_from_now(boost::posix_time::seconds(mTimeout));
          //        mTimer.async_wait(bind(&ResultorAgent::changed, shared_from_this()));
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorManual::postedInitorExecuteCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorManual::okClicked()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          assert(mWidget);
          mWidget->setEnabled(false);

          resultRecieved();
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorManual::allGoodClicked()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
          assert(mWidget);
          mWidget->setEnabled(false);
          ui.optDisplayGood->setChecked(true);
          ui.optEventGood->setChecked(true);
          ui.optValueGood->setChecked(true);

          if (isDigPoint())
          {
            ui.optStateTextGood->setChecked(true);
          }

          resultRecieved();
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      void
      ResultorManual::copyPointIdClicked()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )

        st::SysPointPtrT sysPoint = mData.tqSysValue->testSysValue->sysPoint();
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(sysPoint->id().c_str());

        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorManual::copyPointPidClicked()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )

        st::SysPointPtrT sysPoint = mData.tqSysValue->testSysValue->sysPoint();
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(sysPoint->pid().c_str());

        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorManual::resultRecieved()
      {
        FUNCTION_START( __PRETTY_FUNCTION__)

        // If the users clicks the button before the resultor is ready we get a
        // SIGSERV - UI has also been diabled by default to fix this
          if (mState != monitoringResultorState)
          {
            return;
          }

          CheckStateE displayResult = getCheckState(*ui.optDisplayGood,
              *ui.optDisplayBad);

          CheckStateE eventResult = getCheckState(*ui.optEventGood,
              *ui.optEventBad);

          CheckStateE valueResult = getCheckState(*ui.optValueGood,
              *ui.optValueBad);

          CheckStateE stateTextResult = getCheckState(*ui.optStateTextGood,
              *ui.optStateTextBad);

          std::string comment = ui.txtComment->text().toStdString();

          qDebug("ResultorManual %s - Indication Recieved",
              mData.systemRecord->name().c_str());

          if (displayResult || eventResult || valueResult || stateTextResult
              || !comment.empty())
          {
            mData.tqSysValue->testSysValueRun->setFeedbackRecieved(true);
          }

          if (displayResult != ChkNull)
          {
            mData.tqSysValue->testSysValueRun->setSysDisplayOk(displayResult
                == ChkGood);
          }

          if (eventResult != ChkNull)
          {
            mData.tqSysValue->testSysValueRun->setSysEventOk(eventResult
                == ChkGood);
          }

          if (valueResult != ChkNull)
          {
            mData.tqSysValue->testSysValueRun->setSysValueOk(valueResult
                == ChkGood);
          }

          if (stateTextResult != ChkNull)
          {
            mData.tqSysValue->testSysValueRun->setSysStateTextOk(
                stateTextResult == ChkGood);
          }

          mData.tqSysValue->testSysValueRun->setSysComment(comment);

          const bool success = true;
          // Tell the System that we've received the result
          mData.proactor->ioService().post(bind(&System::postedResultorCompleted,
              mData.system,
              success));

          mState = idleResultorState;
          FUNCTION_END( __PRETTY_FUNCTION__)
      }

      boost::shared_ptr< Resultor >
      ResultorManual::getThis()
      {
        // basePtr seems to be conflicting with somewhere else
        boost::shared_ptr< Resultor > basePtr_;
        basePtr_ = shared_from_this();
        return basePtr_;
      }

      ResultorManual::CheckStateE
      ResultorManual::getCheckState(QRadioButton &good, QRadioButton &bad)
      {
        if (good.isChecked())
          return ChkGood;
        if (bad.isChecked())
          return ChkBad;
        return ChkNull;
      }

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
