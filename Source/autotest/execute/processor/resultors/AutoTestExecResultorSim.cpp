#include "AutoTestExecResultorSim.h"

#include <QtGui/qwidget.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Proactor.h"
#include "AutoTestExecProcessorSystem.h"
#include "AutoTestExecTestQueueModel.h"
#include "SimDispatcherProxy.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      ResultorSim::ResultorSim(ResultorData& data, QWidget* panel) :
          Resultor(data)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mSimPointAlias = simPoint()->simAlias()
            + mData.systemRecord->simulatorAliasPostfix();

        connect(this,
            SIGNAL(setupUiSignal(QWidget*)),
            this,
            SLOT(setupUiSlot(QWidget*)),
            Qt::BlockingQueuedConnection);

        emit setupUiSignal(panel);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      ResultorSim::~ResultorSim()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::setupUiSlot(QWidget* panel)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mWidget = new QWidget(panel);
        ui.setupUi(mWidget);
        panel->layout()->addWidget(mWidget);

        connect(this,
            SIGNAL(uiRefreshTick()),
            this,
            SLOT(refreshUiSlot()),
            Qt::QueuedConnection);
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::refreshUiSlot()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        if (!mWidget)
          return;
        ui.txtStatus->setText(stateStr().c_str());
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::postedRun()
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        qDebug("ResultorSim %s starting", mData.systemRecord->name().c_str());
        mState = monitoringSetupResultorState;

        // Register for controls
        mData.simDispatcherProxy->requestMonitor(mData.system,
            shared_from_this(),
            mSimPointAlias);
        //
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::postedInitorExecuteCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        mStrand.post(bind(&ResultorSim::strandedInitorExecuteCompleted,
            shared_from_this(),
            success));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::strandedInitorExecuteCompleted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        // Use a strand so we know nothing else is happening when we start the timer
        qDebug("ResultorSim %s initor execute completed",
            mData.systemRecord->name().c_str());

        // Do something with success

        // Create a timer and make it wait
        mTimer.expires_from_now(boost::posix_time::seconds(mTimeout));
        mTimer.async_wait(mStrand.wrap(bind(&ResultorSim::strandedResult,
            shared_from_this(),
            true)));
        FUNCTION_END( __PRETTY_FUNCTION__ )

      }

      void
      ResultorSim::postedSimulatorMonitoringStarted(bool success)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        if (!success)
        {
          mData.proactor->ioService().post(bind(&System::postedResultorCompleted,
              mData.system,
              success));
          return;
        }
        mState = monitoringResultorState;

        // Tell the initor to execute the change
        mData.proactor->ioService().post(bind(&Initor::postedExecute,
            mData.initor));
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::postedSimulatorObjectUpdateRecieved(sim_dll::Server::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        if (mState == resetResultorState)
        {
          qDebug("ResultorSim shutdown before control received message,\n"
              "this should be because the timeout has expired");
          return;
        }

        using namespace sim_dll::Server;
        qDebug("ResultorSim %s recieved object update",
            mData.systemRecord->name().c_str());

        if (message.msgType == CtrlUpdate)
        {
          simulatorObservedControl(message);
          return;
        }
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::simulatorObservedControl(sim_dll::Server::Msg message)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        using namespace sim_dll::Server;

        if (message.alias != mSimPointAlias)
          return;

        qDebug("ResultorSim %s simulator recieved control",
            mData.systemRecord->name().c_str());

        CtrlPointInfo& msgData = boost::get< CtrlPointInfo >(message.pointInfo);

        // There is nothing worse than that accessing null shared pointer bug
        db_syntest::SimPointTypeE simPointType = db_syntest::toSimPointType(simPoint());

        bool isDo = (simPointType == db_syntest::doSimPointType);
        isDo &= (msgData.type == CtrlState //
            || msgData.type == CtrlSetpoint //
            || msgData.type == CtrlDeltadown //
        || msgData.type == CtrlDeltaup);

        bool isAo = (simPointType == db_syntest::aoSimPointType);
        isAo &= (msgData.type == CtrlSetpoint);

        if (!(isAo || isDo))
          return;

        double remainingTime = mTimer.expires_from_now().total_milliseconds()
            / 1000.0f;
        mTimer.cancel();
        mData.tqSysValue->testSysValueRun->setFeedbackRecieved(true);
        mData.tqSysValue->testSysValueRun->setFeedbackTime(remainingTime);

        if (isAo)
        {
          mData.tqSysValue->testSysValueRun->setSysRawValue(msgData.indSetPoint);

          // FIXME Translation tables
          mData.tqSysValue->testSysValueRun->setSysValue(msgData.indSetPoint);
        }

        if (msgData.type == CtrlSetpoint)
        {
           mData.tqSysValue->testSysValueRun->setSysRawValue(msgData.indSetPoint);
        }

        // Use a strand so strandedResult doesn't get run by the timer and feedback
        // concurrently
        mStrand.post(bind(&ResultorSim::strandedResult,
            shared_from_this(),
            false));

        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      void
      ResultorSim::strandedResult(bool timerExpired)
      {
        FUNCTION_START( __PRETTY_FUNCTION__ )
        // Use a strand so strandedResult doesn't get run by the timer and feedback
        // concurrently
        if (mState != monitoringResultorState)
          return;

        qDebug("ResultorSim %s - Result obtained =  %s",
            mData.systemRecord->name().c_str(),
            (timerExpired ? "Timeout Expired" : "Simulator Observed Control"));

        // Cancel the agentDispacher and cancel the timer
        // SimProxy already registers and deregisters points for updates
        mData.simDispatcherProxy->requestMonitorEnd(mData.system,
            shared_from_this(),
            mSimPointAlias);
        mTimer.cancel();

        const bool success = true;
        // Test out the SimDispatcher
        mData.proactor->ioService().post(bind(&System::postedResultorCompleted,
            mData.system,
            success));

        mState = idleResultorState;
        FUNCTION_END( __PRETTY_FUNCTION__ )
      }

      boost::shared_ptr< Resultor >
      ResultorSim::getThis()
      {
        // basePtr seems to be conflicting with somewhere else
        boost::shared_ptr< Resultor > basePtr_;
        basePtr_ = shared_from_this();
        return basePtr_;
      }

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
