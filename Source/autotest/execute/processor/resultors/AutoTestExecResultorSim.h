#ifndef _AUTO_TEST_EXEC_RESULTOR_SIM_H
#define _AUTO_TEST_EXEC_RESULTOR_SIM_H

#include <boost/enable_shared_from_this.hpp>

#include "ui_AutoTestExecResultorSimUi.h"

#include "AutoTestExecResultor.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class ResultorSim : public Resultor,
          DebugClassMixin< ResultorSim >,
          public boost::enable_shared_from_this< ResultorSim >
      {
        Q_OBJECT

        public:
          ResultorSim(ResultorData& data, QWidget* panel);

          virtual
          ~ResultorSim();

        public:
          /// ---------------
          /// Resultor interface

          void
          postedRun();

          void
          postedInitorExecuteCompleted(bool success);

        public:
          /// ---------------
          /// ResultorSim stuff

          void
          strandedInitorExecuteCompleted(bool success);

          void
          strandedResult(bool timerExpired);

        public:
          /// ---------------
          /// Calls from SimDispatcherProxy

          void
          postedSimulatorMonitoringStarted(bool success);

          void
          postedSimulatorObjectUpdateRecieved(sim_dll::Server::Msg message);

        public slots:
          /// ---------------
          /// Stuff to make the UI thread process stuff

          void
          setupUiSlot(QWidget* panel);

          void
          refreshUiSlot();

        signals:

          void
          setupUiSignal(QWidget* panel);

        private:

          void
          simulatorObservedControl(sim_dll::Server::Msg message);

          boost::shared_ptr< Resultor >
          getThis();

          std::string mSimPointAlias;

          Ui::AutoTestExecResultorSimUiClass ui;
      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif // _AUTO_TEST_EXEC_RESULTOR_SIM_H
