/*
 * AutoTestResultor.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecResultor.h"

#include <QtGui/qlabel.h>
#include <QtGui/qlayout.h>
#include <QtGui/qapplication.h>

#include <simscada_util/DbSimscadaUtil.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest_util/DbSyntestUtil.h>

#include "AutoTestExecResultorSim.h"
#include "AutoTestExecResultorManual.h"
#include "AutoTestExecResultorAgent.h"

#include "Proactor.h"
#include "AgentDispatcher.h"
#include "AutoTestExecTestQueueModel.h"

#include "Macros.h"

using namespace db_syntest;

namespace autotest
{
  namespace exec
  {

    namespace processor
    {
      Resultor::Resultor(ResultorData& data) :
        mData(data) //
            , mTimer(data.proactor->ioService()) //
            , mUiUpdateTimer(data.proactor->ioService()) //
            , mStrand(data.proactor->ioService()) //
            , mTimeout(0) //
            , mWidget(NULL) //
            , mState(idleResultorState)
      {
        FUNCTION_START( __PRETTY_FUNCTION__)
        mTimer.expires_from_now(boost::posix_time::seconds(0));
        // There is nothing worse than that accessing null shared pointer bug
        assert(mData.tqSysValue.get());
        assert(mData.tqSysValue->testSysValue.get());
        assert(mData.tqSysValue->testSysValue->sysPoint().get());
        assert(mData.tqSysValue->testSysValue->sysPoint()->simPoint().get());
        assert(mData.tqSysValue->testSysValueRun.get());

        moveToThread(QApplication::instance()->thread());
        connect(this,
            SIGNAL(tearDownUiSignal()),
            this,
            SLOT(tearDownUiSlot()),
            Qt::BlockingQueuedConnection);

        st::TestValuePtrT
            testValue = mData.tqSysValue->testSysValue->testValue();
        mTimeout = testValue->testPoint()->simPoint()->feedbackTimeout();
FUNCTION_END      ( __PRETTY_FUNCTION__)
    }

    Resultor::~Resultor()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    void
    Resultor::reset()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mStrand.post(boost::bind(&Resultor::strandedReset, getThis()));
      FUNCTION_END( __PRETTY_FUNCTION__)
    }

    /// ---------------
    /// Resultor functions for binding

    void
    Resultor::strandedReset()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      mState = resetResultorState;
      mUiUpdateTimer.cancel();
      emit
      tearDownUiSignal();
      mData = ResultorData();
      FUNCTION_END( __PRETTY_FUNCTION__)
    }

    /// ---------------
    /// Resultor interface

    void
    Resultor::postedInitorExecuteCompleted(bool success)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      FUNCTION_END( __PRETTY_FUNCTION__ )
    }

    ResultorPtrT
    Resultor::create(ResultorData data, QWidget* panel)
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      assert(data.tqSysValue.get());
      assert(data.tqSysValue->testSysValue.get());

      st::TestValuePtrT testValue = data.tqSysValue->testSysValue->testValue();
      st::SimPointPtrT simPoint = testValue->testPoint()->simPoint();

      const bool agentEnabled = !(data.systemRecord->isAgentIpNull()
          || data.systemRecord->isAgentPortNull());

      assert(testValue.get());
      assert(simPoint.get());

      switch (toSimPointType(simPoint))
      {
        case di1SimPointType:
        case di2SimPointType:
        case acSimPointType:
        case aiSimPointType:
        {
          st::SysPointPtrT sysPoint = data.tqSysValue->testSysValue->sysPoint();
          if (sysPoint->requiresUser() || !agentEnabled)
          {
            boost::shared_ptr< ResultorManual > resultor(new ResultorManual(data,
                    panel));
            resultor->startUiUpdateTimer();
            return resultor;
          }
          else
          {
            boost::shared_ptr< ResultorAgent > resultor(new ResultorAgent(data,
                    panel));
            resultor->startUiUpdateTimer();
            data.agentDispatcher->addObserver(resultor);
            return resultor;
          }
        }

        case doSimPointType:
        case aoSimPointType:
        {
          boost::shared_ptr< ResultorSim > resultor(new ResultorSim(data,
                  panel));
          resultor->startUiUpdateTimer();
          return resultor;
        }

        THROW_UNHANDLED_CASE_EXCEPTION
      }

      assert(false);
      FUNCTION_END( __PRETTY_FUNCTION__)
      return ResultorPtrT();
    }

    void
    Resultor::tearDownUiSlot()
    {
      FUNCTION_START( __PRETTY_FUNCTION__)
      assert(mWidget);
      mWidget->setVisible(false);
      mWidget->deleteLater();
      mWidget = NULL;

      FUNCTION_END( __PRETTY_FUNCTION__)

    }

    db_syntest::st::SysPointPtrT
    Resultor::sysPoint()
    {
      return mData.tqSysValue->testSysValue->sysPoint();
    }

    db_syntest::st::SimPointPtrT
    Resultor::simPoint()
    {
      return mData.tqSysValue->testSysValue->sysPoint()->simPoint();
    }

    db_syntest::st::TestValuePtrT
    Resultor::testValue()
    {
      test_queue::TqValuePtrT tqValue = mData.tqSysValue->tqValue.lock();
      assert(tqValue.get());

      return tqValue->testValue;
    }

    bool
    Resultor::isDigPoint()
    {
      SimPointTypeE simPointType = db_syntest::toSimPointType( simPoint());
      return simPointType == di1SimPointType || simPointType == di2SimPointType;
    }

    std::string
    Resultor::stateStr() const
    {
      std::string str;
      switch (mState)
      {
        case resetResultorState:
        str = "Resultor at resetting";
        break;

        case idleResultorState:
        str = "Resultor at idle";
        break;

        case monitoringSetupResultorState:
        str = "Setting up monitoring";
        break;

        case monitoringResultorState:
        str = "Monitoring for result";
        break;

        THROW_UNHANDLED_CASE_EXCEPTION
      }

      double remainingTime = mTimer.expires_from_now().total_milliseconds()
      / 1000.0f;

      if (remainingTime <= 0.0f)
      return str;

      boost::format status("%s  (%.3fs Remaining)");
      status % str;
      status % remainingTime;

      return status.str();
    }

    void
    Resultor::startUiUpdateTimer()
    {
      mUiUpdateTimer.expires_from_now(boost::posix_time::millisec(250));
      mUiUpdateTimer.async_wait(mStrand.wrap(bind(&Resultor::strandedUiUpdateTimerExpired,
                  getThis())));
    }

    void
    Resultor::strandedUiUpdateTimerExpired()
    {
      // Check if we've been reset
      if (!mData.system.get())
      return;

      emit
      uiRefreshTick();

      startUiUpdateTimer();
    }

  } /* namespace processor */
} /* namespace exec */
} /* namespace autotest */
