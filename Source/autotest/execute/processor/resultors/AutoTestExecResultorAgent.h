#ifndef _AUTO_TEST_EXEC_RESULTOR_AGENT_H
#define _AUTO_TEST_EXEC_RESULTOR_AGENT_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/error.hpp>

#include "ui_AutoTestExecResultorAgentUi.h"

#include "AutoTestExecResultor.h"
#include "AgentDispatcherObserver.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class ResultorAgent : public Resultor,
          public boost::enable_shared_from_this< ResultorAgent >,
          public agent::AgentDispatcherObserver,
          DebugClassMixin< ResultorAgent >
      {
        Q_OBJECT

        public:
          ResultorAgent(ResultorData& data, QWidget* panel);

          virtual
          ~ResultorAgent();

        public:
          /// ---------------
          /// Resultor interface

          void
          postedRun();

          void
          postedInitorExecuteCompleted(bool success);

        public:
          /// ---------------
          /// agent::AgentDispatcherObserver interface implementation

          void
          postedAgentConnectionStateChanged(agent::ConnectionStateE newState,
              std::string errorMessage);

          void
          postedAgentRecievedMessage(agent::Client::Msg message);

        public slots:
          /// ---------------
          /// Stuff to make the UI thread process stuff

          void
          setupUiSlot(QWidget* panel);

          void
          refreshUiSlot();

        signals:

          void
          setupUiSignal(QWidget* panel);

        public:
          /// ---------------
          /// ResultorAgent stuff

          void
          strandedInitorExecuteCompleted(bool success);

          void
          strandedResult(bool timerExpired);

          void
          agentRespAckTimer(const boost::system::error_code& error);

        private:

          void
          agentPointMonitoringStarted(agent::Client::Msg message);

          void
          agentObservedStateChange(agent::Client::Msg message);

          boost::shared_ptr< Resultor >
          getThis();

          Ui::AutoTestExecResultorAgentUiClass ui;
          boost::asio::deadline_timer mAgentRespAckTimer;
      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif // _AUTO_TEST_EXEC_RESULTOR_AGENT_H
