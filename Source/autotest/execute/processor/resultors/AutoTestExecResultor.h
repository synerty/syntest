/*
 * AutoTestResultor.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AUTO_TEST_EXEC_RESULTOR_H_
#define _AUTO_TEST_EXEC_RESULTOR_H_

#include <boost/shared_ptr.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/strand.hpp>

#include <QtCore/qobject.h>

#include <syntest/DbSyntestDeclaration.h>

#include "AutoTestExecProcessorSystem.h"

#include "AutoTestExecInitor.h"
#include "Macros.h"

class QWidget;

class Proactor;

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class System;
      class Resultor;
      typedef boost::shared_ptr< Resultor > ResultorPtrT;

      /*! autotest::Resultor Brief Description
       * Long comment about the class
       *
       */
      class Resultor : public QObject, DebugClassMixin< Resultor >
      {
        Q_OBJECT

        public:
          Resultor(ResultorData& data);

          virtual
          ~Resultor();

        public:
          /// ---------------
          /// Miniture Factory

          static ResultorPtrT
          create(ResultorData data, QWidget* panel);

          void
          reset();

        private:
          /// ---------------
          /// Resultor functions for binding

          void
          strandedReset();

        public:
          /// ---------------
          /// Resultor interface

          virtual void
          postedRun() = 0;

          virtual void
          postedInitorExecuteCompleted(bool success);

        public slots:

          void
          tearDownUiSlot();

        signals:
          void
          tearDownUiSignal();

          void
          uiRefreshTick();

        public:
          /// ---------------
          /// Resultor functions for binding

          void
          strandedUiUpdateTimerExpired();

        protected:
          enum ResultorStateE
          {
            resetResultorState,
            idleResultorState,
            monitoringSetupResultorState,
            monitoringResultorState,
          };

          db_syntest::st::SysPointPtrT
          sysPoint();

          db_syntest::st::SimPointPtrT
          simPoint();

          db_syntest::st::TestValuePtrT
          testValue();

          bool
          isDigPoint();

          std::string
          stateStr() const;

          void
          startUiUpdateTimer();

          virtual boost::shared_ptr< Resultor >
          getThis() = 0;

          ResultorData mData;

          boost::asio::deadline_timer mTimer;
          boost::asio::deadline_timer mUiUpdateTimer;
          boost::asio::strand mStrand;

          int mTimeout;

          QWidget* mWidget;

          ResultorStateE mState;
      };

    } /* namespace processor */
  } /* namespace exec */
} /* namespace autotest */
#endif /* _AUTO_TEST_EXEC_RESULTOR_H_ */
