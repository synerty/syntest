#ifndef AUTOTESTEXECPROCESSORSYSTEMUI_H
#define AUTOTESTEXECPROCESSORSYSTEMUI_H

#include <string>

#include <boost/shared_ptr.hpp>

#include <QtGui/QWidget>

#include <syntest/DbSyntestDeclaration.h>

#include "ui_AutoTestExecProcessorSystemUi.h"

#include "TcpClientDeclaration.h"
#include "AgentDeclaration.h"
#include "Macros.h"

namespace autotest
{
  namespace exec
  {
    namespace processor
    {
      class System;

      class SystemUi : public QWidget, //
          DebugClassMixin< SystemUi >
      {
        Q_OBJECT

        public:
          SystemUi(System& uc,
              db_syntest::st::SystemPtrT systemRecord,
              QWidget *parent);

          virtual
          ~SystemUi();

        public:
          QWidget*
          getWorkerPanel();

          void
          agentConnectionStateChanged(agent::ConnectionStateE state,
              std::string errorMessage);

          void
          simulatorConnectionStateChanged(tcp_client::ConnectionStateE state,
              std::string errorMessage);

        public slots:

          void
          agentConnectionStateChangedSlot(int stateInt, QString reason);

          void
          simulatorConnectionStateChangedSlot(int stateInt, QString reason);

          void
          agentConnectionToggleClicked(bool checked);

          void
          simulatorConnectionToggleClicked(bool checked);

        signals:

          void
          agentConnectionStateChangedSignal(int state, const QString reason);

          void
          simulatorConnectionStateChangedSignal(int state,
              const QString reason);

        private:
          Ui::AutoTestExecProcessorSystemUiClass ui;

          System& mUc;

          std::string mAgentStr;
          std::string mSimulatorStr;
      };
    }
  }
}

#endif // AUTOTESTEXECPROCESSORSYSTEMUI_H
