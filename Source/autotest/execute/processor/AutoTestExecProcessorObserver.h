/*
 * AutoTestExecProcessorObserver.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef AUTOTESTEXECPROCESSOROBSERVER_H_
#define AUTOTESTEXECPROCESSOROBSERVER_H_

#include <syntest/DbSyntestDeclaration.h>

#include <boost/weak_ptr.hpp>

#include "AutoTestExecDeclarations.h"
#include "AutoTestExecTestQueueModel.h"
#include "Macros.h"

namespace autotest
{
  namespace exec
  {
    class Processor;
    typedef boost::weak_ptr< Processor > ProcessorWptrT;

    /*! Processor Observer
     * Classes implementing this will be informed of changes in test processing
     */
    class ProcessorObserver //
DEBUG_CLASS_BASE    (ProcessorObserver)
    {
      public:
      virtual
      ~ProcessorObserver();

      public:
      virtual void
      processorTestingStarted();

      virtual void
      processorTestingStopped(const TestStopReasonE reason, const std::string message);
    };

  } /* namespace exec */
} /* namespace autotest */
#endif /* AUTOTESTEXECPROCESSOROBSERVER_H_ */
