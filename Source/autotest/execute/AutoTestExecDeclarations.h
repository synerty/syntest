/*
 * AutoTestExecDeclarations.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef AUTOTESTEXECDECLARATIONS_H_
#define AUTOTESTEXECDECLARATIONS_H_

#include <vector>
#include <boost/shared_ptr.hpp>

namespace autotest
{
  namespace exec
  {
    enum ExecUiStateE
    {
      choosingBatchUiState,
      choosingRunUiState,
      testingStoppedUiState,
      testingRunningUiState
    };

    enum TestStopReasonE {
      //! Finished testing all the points
      completedTestStopReason,

      //! User has stoped the testing
      userTestStopReason,

      //! There is no currently selected test value in the test queue
      noCurrentPointTestStopReason,

      //! Agent connection interupt stopped the testing
      agentDisconnectTestStopReason,

      //! Simulator connection interupt stopped the testing
      simulatorDisconnectTestStopReason,

      //! The model changed
      modelChangedTestStopReason,

      //! Unsure as to why the testing was stopped
      unknownTestStopReason
    };

    namespace test_queue
    {
      class TqValue;
    }

    namespace processor
    {
      // ---------------
      // System
      class System;
      typedef boost::shared_ptr< System > SystemPtrT;
      typedef std::vector< SystemPtrT > SystemsT;

      // ---------------
      // Initor
      class Initor;
      typedef boost::shared_ptr< Initor > InitorPtrT;
      typedef std::vector< InitorPtrT > InitorsT;

      // ---------------
      // InitorSim
      class InitorSim;
      typedef boost::shared_ptr< InitorSim > InitorSimPtrT;
      typedef std::vector< InitorSimPtrT > InitorSimsT;

      // ---------------
      // Resultor
      class Resultor;
      typedef boost::shared_ptr< Resultor > ResultorPtrT;
      typedef std::vector< ResultorPtrT > ResultorsT;

      // ---------------
      // ResultorSim
      class ResultorSim;
      typedef boost::shared_ptr< ResultorSim > ResultorSimPtrT;
      typedef std::vector< ResultorSimPtrT > ResultorSimsT;
    }
  }
}

#endif /* AUTOTESTEXECDECLARATIONS_H_ */
