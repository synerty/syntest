/*
 * AutoTestSummary.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecSummary.h"

#include <assert.h>

#include <QtGui/qlabel.h>
#include <QtGui/qlayout.h>

#include "Macros.h"

using namespace autotest::exec::test_queue;

namespace autotest
{
  namespace exec
  {

    Summary::Summary(QWidget* panel) :
      mPointCount(0), //
          mValueCount(0), //
          mValueCompletedCount(0), //
          mValuePassedCount(0)

    {
      QWidget* wgt = new QWidget(panel); // Owned and cleaned up by Qt
      ui.setupUi(wgt);
      panel->layout()->addWidget(wgt);
    }
    // --------------------------------------------------------------------------

    Summary::~Summary()
    {
    }
    // --------------------------------------------------------------------------

    void
    Summary::testQueueModelChanged(test_queue::ModelPtrT model)
    {
      mModel = model;
      recalculate();
    }
    // --------------------------------------------------------------------------

    void
    Summary::testQueueCurrentTestCompleted(test_queue::TestCompleteResultE testCompleteResult)
    {
      recalculate();
    }
    // --------------------------------------------------------------------------


    void
    Summary::recalculate()
    {
      assert(mModel.get());

      const TqPointListT& points = mModel->model();

      mPointCount = points.size();
      mValueCount = 0;
      mValueCompletedCount = 0;
      mValuePassedCount = 0;

      for (TqPointListT::const_iterator itrPoints = points.begin(); itrPoints
          != points.end(); ++itrPoints)
      {
        const TqValueListT& values = (*itrPoints)->values;

        mValueCount += values.size();

        for (TqValueListT::const_iterator itrValues = values.begin(); itrValues
            != values.end(); ++itrValues)
        {
          const TqValuePtrT value = *itrValues;

          if (!value->completed())
            continue;

          mValueCompletedCount++;

          if (value->passed())
            mValuePassedCount++;
        }
      }

      boost::format lbl("%d values to test for %d points");
      lbl % mValueCount;
      lbl % mPointCount;

      ui.lblTotals->setText(lbl.str().c_str());

      ui.pgrTestsPassed->setMaximum(mValueCount);
      ui.pgrTestsFailed->setMaximum(mValueCount);
      ui.pgrTestsPerformed->setMaximum(mValueCount);

      update();
    }
    // --------------------------------------------------------------------------

    void
    Summary::update()
    {
      ui.pgrTestsPassed->setValue(mValuePassedCount);
      ui.pgrTestsFailed->setValue(mValueCompletedCount - mValuePassedCount);
      ui.pgrTestsPerformed->setValue(mValueCompletedCount);
    }
  // --------------------------------------------------------------------------

  /// ---------------
  /// TestQueue implementation

  } /* namespace exec */
} /* namespace autotest */
