/*
 * AutoTestSummary.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AUTO_TEST_EXEC_SUMMARY_H_
#define _AUTO_TEST_EXEC_SUMMARY_H_

#include <boost/shared_ptr.hpp>
#include <syntest/DbSyntestDeclaration.h>

#include "AutoTestExecTestQueueObserver.h"
#include "ui_AutoTestExecSummaryUi.h"

class QWidget;
class QLabel;

namespace autotest
{
  namespace exec
  {

    /*! autotest::Summary Brief Description
     * Long comment about the class
     *
     */
    class Summary : public TestQueueObserver, DebugClassMixin< Summary >
    {
      public:
        Summary(QWidget* panel);
        virtual
        ~Summary();

      public:
        /// ---------------
        /// TestQueue implementation

        void
        testQueueModelChanged(test_queue::ModelPtrT model);

        void
        testQueueCurrentTestCompleted(test_queue::TestCompleteResultE testCompleteResult);

      private:
        Ui::AutoTestExecSummaryUiClass ui;

        void
        recalculate();

        void
        update();

        test_queue::ModelPtrT mModel;

        int mPointCount;
        int mValueCount;
        int mValueCompletedCount;
        int mValuePassedCount;
    };

  } /* namespace exec */
} /* namespace autotest */
#endif /* _AUTO_TEST_EXEC_SUMMARY_H_ */
