/*
 * AutoTestExecTestQueueObserver.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecTestQueueObserver.h"

namespace autotest
{
  namespace exec
  {
    TestQueueObserver::~TestQueueObserver()
    {
    }
    // ------------------------------------------------------------------------

    void
    TestQueueObserver::testQueueModelChanged(test_queue::ModelPtrT model)
    {
    }
    // --------------------------------------------------------------------------

    void
    TestQueueObserver::testQueueCurrentTestChanged(test_queue::TqValuePtrT tqValue)
    {
    }
    // --------------------------------------------------------------------------

    void
    TestQueueObserver::testQueueCurrentTestCompleted(test_queue::TestCompleteResultE testCompleteResult)
    {
    }
  // --------------------------------------------------------------------------

  } /* namespace exec */
} /* namespace autotest */
