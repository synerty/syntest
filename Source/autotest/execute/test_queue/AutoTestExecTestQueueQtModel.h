/*
 * AutoTestExecTestQueueQtModel.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AUTO_TEST_EXEC_TEST_QUEUE_QT_MODEL_H_
#define _AUTO_TEST_EXEC_TEST_QUEUE_QT_MODEL_H_

#include <map>

#include <boost/shared_ptr.hpp>

#include <qabstractitemmodel.h>

#include "AutoTestExecTestQueueModel.h"

namespace autotest
{
  namespace exec
  {
    class TestQueue;

    namespace test_queue
    {
      class QtModel;
      typedef boost::shared_ptr< QtModel > QtModelPtrT;

      namespace _private
      {
        struct Node;
      }

      class TqValue;

      /*! autotest::exec::test_queue::QtModel Brief Description
       * Long comment about the class
       *
       */
      class QtModel : public QAbstractItemModel,
      DebugClassMixin< QtModel >
      {
        public:
          QtModel(ModelPtrT model);

          virtual
          ~QtModel();

          QModelIndex
          getIndex(boost::shared_ptr< TqValue > tqValue) const;

          void
          refreshResult(boost::shared_ptr< TqValue > tqValue);

        public:
          TqPointPtrT
          tqPoint(const QModelIndex& index);

          TqValuePtrT
          tqValue(const QModelIndex& index);

          TqSysValuePtrT
          tqSysValue(const QModelIndex& index);

        public:
          /// ---------------
          // QAbstractItemModel interface

          QModelIndex
          index(int row,
              int column,
              const QModelIndex& parent = QModelIndex()) const;

          QModelIndex
          parent(const QModelIndex& child) const;

          int
          rowCount(const QModelIndex& parent = QModelIndex()) const;

          int
          columnCount(const QModelIndex& parent = QModelIndex()) const;

          QVariant
          headerData(int section,
              Qt::Orientation orientation,
              int role = Qt::DisplayRole) const;

          QVariant
          data(const QModelIndex& index, int role = Qt::DisplayRole) const;

        private:
          enum ColsE
          {
            colFirst,
            colName = colFirst,
            colProgress,
            colType,
            colLast = colType,
            colCount
          };

          QVariant
          data(const TqPointPtrT point, int column, int role) const;

          QVariant
          data(const TqValuePtrT value, int column, int role) const;

          QVariant
          data(const TqSysValuePtrT sysValue, int column, int role) const;

          void
          setupModel();

          void
          setupModelPoint(_private::Node& parentNode, TqPointPtrT tqPoint);

          void
          setupModelValue(_private::Node& parentNode, TqValuePtrT tqValue);

          void
          setupModelSysValue(_private::Node& parentNode,
              TqSysValuePtrT tqSysValue);

          _private::Node* mRootNode; // data owned by mNodes map
          ModelPtrT mModel;

          const _private::Node&
          getNode(int id) const;

          typedef std::map< int, _private::Node > NodesT;
          NodesT mNodes;
      };

    } /* namespace test_queue */
  } /* namespace exec */
} /* namespace autotest */
#endif /* _AUTO_TEST_EXEC_TEST_QUEUE_QT_MODEL_H_ */
