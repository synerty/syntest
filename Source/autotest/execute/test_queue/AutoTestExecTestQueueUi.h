#ifndef AUTOTESTEXECTESTQUEUEUI_H
#define AUTOTESTEXECTESTQUEUEUI_H

#include <boost/shared_ptr.hpp>

#include <QtGui/QWidget>
class QObject;
class QEvent;

#include "Macros.h"
#include "ui_AutoTestExecTestQueueUi.h"

namespace autotest
{
  namespace exec
  {
    class TestQueue;

    namespace test_queue
    {
      class Model;
      typedef boost::shared_ptr< Model > ModelPtrT;

      class QtModel;
      typedef boost::shared_ptr< QtModel > QtModelPtrT;

      class TqValue;

      class TestQueueUi : public QWidget,
      DebugClassMixin< TestQueueUi >
      {
        Q_OBJECT

        public:
          TestQueueUi(TestQueue& uc, QWidget *parent);

          virtual
          ~TestQueueUi();

        public:
          void
          setModel(ModelPtrT model);

          void
          select(boost::shared_ptr< TqValue > tqValue);

          void
          refreshResult(boost::shared_ptr< TqValue > tqValue);

          void
          runningStateChanged(bool isRunning);

        public slots:

          void
          itemSelected(const QModelIndex& current, const QModelIndex& previous);

        private:

          TestQueue& mUc;
          bool mIsRunning;
          bool mUserSelectionInProgress;
          Ui::AutoTestExecTestQueueUiClass ui;

          ModelPtrT mModel;
          QtModelPtrT mQtModel;

      };
    }
  }
}

#endif // AUTOTESTEXECTESTQUEUEUI_H
