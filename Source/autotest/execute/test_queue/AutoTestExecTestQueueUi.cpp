#include "AutoTestExecTestQueueUi.h"

#include <assert.h>

#include "AutoTestExecTestQueue.h"
#include "AutoTestExecTestQueueQtModel.h"
#include "QtModelTest.h"

namespace autotest
{
  namespace exec
  {
    namespace test_queue
    {

      TestQueueUi::TestQueueUi(TestQueue& uc, QWidget *parent) :
        QWidget(parent), mUc(uc), mIsRunning(false),
            mUserSelectionInProgress(false)
      {
        parent->layout()->addWidget(this);
        ui.setupUi(this);
      }
      /// ---------------------------------------------------------------------------

      TestQueueUi::~TestQueueUi()
      {

      }
      /// ---------------------------------------------------------------------------

      void
      TestQueueUi::setModel(ModelPtrT model)
      {
        mModel = model;

        QtModelPtrT oldModel = mQtModel;
        mQtModel.reset(new QtModel(model));
        ui.treeView->setModel(mQtModel.get());
        ui.treeView->resizeColumnToContents(0);

        connect(ui.treeView->selectionModel(),
            SIGNAL(currentRowChanged(const QModelIndex&,const QModelIndex&)),
            this,
            SLOT(itemSelected(const QModelIndex&, const QModelIndex&)));

#ifdef DEBUG
        QtModelTest(mQtModel.get()).run();
#endif
      }
      /// ---------------------------------------------------------------------------

      void
      TestQueueUi::select(boost::shared_ptr< TqValue > tqValue)
      {
        assert(mQtModel.get());
        if (!mIsRunning)
          return;

        QModelIndex index = mQtModel->getIndex(tqValue);

        ui.treeView->scrollTo(index, QAbstractItemView::PositionAtCenter);

        ui.treeView->selectionModel()->select(index,
            QItemSelectionModel::ClearAndSelect);
        ui.treeView->expand(index);
      }
      /// ---------------------------------------------------------------------------

      void
      TestQueueUi::refreshResult(boost::shared_ptr< TqValue > tqValue)
      {
        assert(mQtModel.get());
        mQtModel->refreshResult(tqValue);
      }
      /// ---------------------------------------------------------------------------

      void
      TestQueueUi::runningStateChanged(bool isRunning)
      {
        mIsRunning = isRunning;
        if (isRunning)
          ui.treeView->setSelectionMode(QAbstractItemView::NoSelection);
        else
          ui.treeView->setSelectionMode(QAbstractItemView::SingleSelection);
      }
      /// ---------------------------------------------------------------------------

      void
      TestQueueUi::itemSelected(const QModelIndex& current,
          const QModelIndex& previous)
      {
        if (!current.isValid() || mUserSelectionInProgress)
          return;

        mUserSelectionInProgress = true;

        assert(mModel.get());
        assert(mQtModel.get());

        // Point
        TqPointPtrT tqPoint = mQtModel->tqPoint(current);
        if (tqPoint.get())
        {
          assert(!tqPoint->values.empty());
          mModel->moveToTestValue(tqPoint->values.front());

          mUserSelectionInProgress = false;
          return;
        }

        // Value
        TqValuePtrT tqValue = mQtModel->tqValue(current);
        if (tqValue.get())
        {
          mModel->moveToTestValue(tqValue);

          mUserSelectionInProgress = false;
          return;
        }

        // Sys Value
        TqSysValuePtrT tqSysValue = mQtModel->tqSysValue(current);
        if (tqSysValue.get())
        {
          TqValuePtrT tqValue = tqSysValue->tqValue.lock();
          assert(tqValue.get());
          mModel->moveToTestValue(tqValue);

          mUserSelectionInProgress = false;
          return;
        }

      }
    /// ---------------------------------------------------------------------------

    }
  }
}
