/*
 * AutoTestTestQueue.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecTestQueue.h"

#include "AutoTestExecTestQueueObserver.h"

#include "AutoTestExecTestQueueModel.h"

using namespace db_syntest;
using namespace db_syntest::st;

namespace autotest
{
  namespace exec
  {

    TestQueue::TestQueue(QWidget* panel) :
        mUi(*this, panel)
    {

    }

    TestQueue::~TestQueue()
    {
    }

    /// ---------------
    /// Test Queue stuff

    void
    TestQueue::setTestRun(db_syntest::st::TestRunPtrT testValueRun)
    {
      mModel.reset(new test_queue::Model(*this, testValueRun));
      mModel->load();
      mUi.setModel(mModel);
      notifyOfModelChanged();
      notifyOfCurrentTestChanged(mModel->currentTestValue());
    }

    /// ---------------
    /// Interface used by test_queue::Model

    void
    TestQueue::notifyOfCurrentTestChanged(test_queue::TqValuePtrT tqValue)
    {
      ObserverPtrsT observerPtrs;
      getObserverPtrs(observerPtrs);
      for (ObserverPtrsT::iterator itr = observerPtrs.begin();
          itr != observerPtrs.end(); ++itr)
        (*itr)->testQueueCurrentTestChanged(tqValue);

      mUi.select(tqValue);
    }
    // ------------------------------------------------------------------------

    void
    TestQueue::notifyOfCurrentTestComplete(test_queue::TestCompleteResultE testCompleteResult)
    {
      ObserverPtrsT observerPtrs;
      getObserverPtrs(observerPtrs);
      for (ObserverPtrsT::iterator itr = observerPtrs.begin();
          itr != observerPtrs.end(); ++itr)
        (*itr)->testQueueCurrentTestCompleted(testCompleteResult);

      mUi.refreshResult(mModel->currentTestValue());
    }
    // ------------------------------------------------------------------------

    /// ---------------
    /// autotest::exec::ProcessorObserver implementation

    void
    TestQueue::processorTestingStarted()
    {
      mUi.runningStateChanged(true);
      mUi.select(mModel->currentTestValue());
    }
    // ----------------------------------------------------------------------------

    void
    TestQueue::processorTestingStopped(const TestStopReasonE reason,
        const std::string message)
    {
      mUi.runningStateChanged(false);
    }
    // ----------------------------------------------------------------------------

    /// ---------------
    /// Observerable Stuff

    void
    TestQueue::addObserver(TestQueueObserverPtrT observer)
    {
      TestQueueObserverWptrT wptr(observer);
      mObservers.insert(wptr);
    }
    // ------------------------------------------------------------------------

    void
    TestQueue::removeObserver(TestQueueObserverPtrT observer)
    {
      TestQueueObserverWptrT wptr(observer);
      mObservers.erase(wptr);
    }
    // ------------------------------------------------------------------------

    void
    TestQueue::notifyOfSomthing(TestQueueObserveFuncT func)
    {
      ObserverPtrsT observerPtrs;
      getObserverPtrs(observerPtrs);
      for (ObserverPtrsT::iterator itr = observerPtrs.begin();
          itr != observerPtrs.end(); ++itr)
        ((*itr).get()->*func)();
    }
    // ------------------------------------------------------------------------

    void
    TestQueue::notifyOfModelChanged()
    {
      ObserverPtrsT observerPtrs;
      getObserverPtrs(observerPtrs);
      for (ObserverPtrsT::iterator itr = observerPtrs.begin();
          itr != observerPtrs.end(); ++itr)
        (*itr)->testQueueModelChanged(mModel);
    }
    // ------------------------------------------------------------------------

    void
    TestQueue::getObserverPtrs(ObserverPtrsT& observerPtrs)
    {
      for (ObserversT::iterator itr = mObservers.begin();
          itr != mObservers.end();)
      {
        TestQueueObserverPtrT oPtr = itr->lock();

        if (!oPtr.get())
        {
          mObservers.erase(itr++);
          continue;
        }

        observerPtrs.push_back(oPtr);
        ++itr;
      }
    }
  // ---------------------------------------------------------------------------

  /// ---------------
  /// Processor Stuff (this class)

  } /* namespace exec */
} /* namespace autotest */
