/*
 * AutoTestExecTestQueueQtModel.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecTestQueueQtModel.h"

#include <boost/variant.hpp>
#include <boost/format.hpp>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>
#include <QtGui/qicon.h>

#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestRun.h>
#include <syntest/st/StTestSystem.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestValueRun.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Macros.h"

#define TICK_ICON ":/synerty/tick_cross/tick_32x32.png"
#define CROSS_ICON ":/synerty/tick_cross/cross_32x32.png"
#define NOT_TESTED_ICON ":/synerty/tick_cross/Null_32_v2.png"

namespace autotest
{
  namespace exec
  {
    namespace test_queue
    {
      namespace _private
      {
        enum NodeTypeE
        {
          rootNodeType,
          pointNodeType,
          valueNodeType,
          sysValueNodeType
        };

        typedef boost::variant< bool, TqPointPtrT, TqValuePtrT, TqSysValuePtrT >
            NodeDataT;
        typedef std::vector< Node* > NodesT;

        struct Node
        {
            int id;
            NodeTypeE type;
            NodeDataT data;
            NodesT children;
            Node* parent;
            int row;
        };
      }

      using namespace _private;

      QtModel::QtModel(ModelPtrT model) :
        mRootNode(NULL), mModel(model)
      {
        setupModel();
      }
      /// ---------------------------------------------------------------------

      QtModel::~QtModel()
      {
      }

      /// ---------------------------------------------------------------------

      QModelIndex
      QtModel::getIndex(boost::shared_ptr< TqValue > tqValue) const
      {
        if (!tqValue.get())
          return QModelIndex();

        NodeDataT tqValueVariant(tqValue);

        for (NodesT::const_iterator itr = mNodes.begin(); itr != mNodes.end(); ++itr)
        {
          const Node& node = itr->second;
          if (node.type != valueNodeType)
            continue;

          if (node.data == tqValueVariant)
            return createIndex(node.row, 0, node.id);

        }

        assert(false);
        return QModelIndex();

      }

      /// ---------------------------------------------------------------------

      void
      QtModel::refreshResult(boost::shared_ptr< TqValue > tqValue)
      {
        QModelIndex row = getIndex(tqValue);

        if (!row.isValid())
          return;

        const int nodeId = row.internalId();

        QModelIndex topLeft = createIndex(row.row(), colFirst, nodeId);
        QModelIndex bottomRight = createIndex(row.row(), colLast, nodeId);

        dataChanged(topLeft, bottomRight);
      }

      /// ---------------------------------------------------------------------

      TqPointPtrT
      QtModel::tqPoint(const QModelIndex& index)
      {
        assert(index.isValid());
        const int id = index.internalId();
        const Node& node = getNode(id);

        if (node.type == pointNodeType)
          return boost::get< TqPointPtrT >(node.data);

        return TqPointPtrT();
      }
      /// ---------------------------------------------------------------------

      TqValuePtrT
      QtModel::tqValue(const QModelIndex& index)
      {
        assert(index.isValid());
        const int id = index.internalId();
        const Node& node = getNode(id);

        if (node.type == valueNodeType)
          return boost::get< TqValuePtrT >(node.data);

        return TqValuePtrT();
      }
      /// ---------------------------------------------------------------------

      TqSysValuePtrT
      QtModel::tqSysValue(const QModelIndex& index)
      {
        assert(index.isValid());
        const int id = index.internalId();
        const Node& node = getNode(id);

        if (node.type == sysValueNodeType)
          return boost::get< TqSysValuePtrT >(node.data);

        return TqSysValuePtrT();
      }
      /// ---------------------------------------------------------------------

      /// ---------------
      // QAbstractItemModel interface

      QModelIndex
      QtModel::index(int row, int column, const QModelIndex& parent) const
      {
        if (parent == QModelIndex())
          return createIndex(row, column, mRootNode->children[row]->id);

        const int parentId = parent.internalId();
        const Node& parentNode = getNode(parentId);
        const Node* node = parentNode.children[row];
        assert(node->row == row);
        return createIndex(row, column, node->id);
      }
      /// ---------------------------------------------------------------------

      QModelIndex
      QtModel::parent(const QModelIndex& child) const
      {
        assert(child.isValid());

        const int id = child.internalId();
        const Node* parentNode = getNode(id).parent;

        if (parentNode == mRootNode)
          return QModelIndex();

        return createIndex(parentNode->row, 0, parentNode->id);
      }
      /// ---------------------------------------------------------------------

      int
      QtModel::rowCount(const QModelIndex& parent) const
      {
        if (parent == QModelIndex())
          return mRootNode->children.size();

        const int parentId = parent.internalId();
        return getNode(parentId).children.size();
      }
      /// ---------------------------------------------------------------------

      int
      QtModel::columnCount(const QModelIndex& parent) const
      {
        return colCount;
      }
      /// ---------------------------------------------------------------------

      QVariant
      QtModel::headerData(int section, Qt::Orientation orientation, int role) const
      {
        if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
          return QAbstractItemModel::headerData(section, orientation, role);

        switch (section)
        {
          case colName:
            return QString("Name");

          case colType:
            return QString("Type");

          case colProgress:
            return QString("Progress");

          case colCount:
            assert(false);
            break;
        }

        return QVariant();
      }
      /// ---------------------------------------------------------------------

      QVariant
      QtModel::data(const QModelIndex& index, int role) const
      {
        assert(index.isValid());

        const int id = index.internalId();
        const Node& node = getNode(id);

        switch (node.type)
        {
          case rootNodeType:
            assert(false);
            break;

          case pointNodeType:
            return data(boost::get< TqPointPtrT >(node.data),
                index.column(),
                role);

          case valueNodeType:
            return data(boost::get< TqValuePtrT >(node.data),
                index.column(),
                role);

          case sysValueNodeType:
            return data(boost::get< TqSysValuePtrT >(node.data),
                index.column(),
                role);
        }

        return QVariant();
      }
      /// ---------------------------------------------------------------------

      QVariant
      QtModel::data(const TqPointPtrT point, int column, int role) const
      {
        const bool isPassed = point->passed();
        const bool isCompleted = point->completed();

        /*        if (role == Qt::BackgroundRole)
         {
         if (!isCompleted)
         return QVariant();

         if (isPassed)
         return QBrush(QColor("LightGreen"));

         return QBrush(QColor("Red"));
         }*/

        switch (column)
        {
          case colName:
          {
            if (role == Qt::DisplayRole)
              return QString(point->testPoint->simPoint()->simAlias().c_str());

            if (role == Qt::DecorationRole)
            {
              if (!isCompleted)
                return QIcon(NOT_TESTED_ICON);

              if (isPassed)
                return QIcon(TICK_ICON);

              return QIcon(CROSS_ICON);
            }

            break;
          }

          case colType:
          {
            if (role == Qt::DisplayRole)
              return QString(point->testPoint->simPoint()->type().c_str());
            break;
          }

          case colProgress:
          {
            if (role != Qt::DisplayRole)
              break;

            if (!isCompleted)
              return QString("");

            if (isPassed)
              return QString("Passed");

            return QString("Failed");
            break;
          }
        }

        return QVariant();
      }
      /// ---------------------------------------------------------------------

      QVariant
      QtModel::data(const TqValuePtrT value, int column, int role) const
      {
        assert(value.get());
        assert(value->testValue.get());
        assert(value->testValue->testPoint().get());
        assert(value->testValue->testPoint()->simPoint().get());

        db_syntest::SimPointTypeE simPointType = db_syntest::toSimPointType( //
        value->testValue->testPoint()->simPoint());

        const bool isPassed = value->passed();
        const bool isCompleted = value->completed();

        /*
         if (role == Qt::BackgroundRole)
         {
         if (!isCompleted)
         return QVariant();

         if (isPassed)
         return QBrush(QColor("LightGreen"));

         return QBrush(QColor("Red"));
         }
         */

        switch (column)
        {
          case colName:
          {
            // Show the icon
            if (role == Qt::DecorationRole)
            {
              if (!isCompleted)
                return QIcon(NOT_TESTED_ICON);

              if (isPassed)
                return QIcon(TICK_ICON);

              return QIcon(CROSS_ICON);
            }

            if (role != Qt::DisplayRole)
              break;

            // Show the test
            switch (simPointType)
            {
              case db_syntest::di1SimPointType:
              case db_syntest::di2SimPointType:
              {
                boost::format name("%d/%s (%s)");
                name % value->testValue->simValue();
                name % value->testValue->simDigValue();
                name % value->testValue->simDigState();
                return QString(name.str().c_str());
              }

              case db_syntest::aoSimPointType:
              case db_syntest::acSimPointType:
              case db_syntest::aiSimPointType:
              {
                boost::format name("%f");
                name % value->testValue->simValue();
                return QString(name.str().c_str());
              }

              case db_syntest::doSimPointType:
              {
                return QVariant("Control"); // Control
              }

              THROW_UNHANDLED_CASE_EXCEPTION
            }

            break;
          }

          case colType:
          {
            if (role == Qt::DisplayRole)
              return QString("");
            break;
          }

          case colProgress:
          {
            if (role != Qt::DisplayRole)
              break;

            if (!isCompleted)
              return QString("");

            if (isPassed)
              return QString("Passed");

            return QString("Failed");
            break;
          }
        }

        return QVariant();
      }
      /// ---------------------------------------------------------------------

      QVariant
      QtModel::data(const TqSysValuePtrT sysValue, int column, int role) const
      {
        const ResultE result = sysValue->result();
        assert(sysValue.get());
        assert(sysValue->testSysValue.get());
        assert(sysValue->testSysValue->testValue().get());
        assert(sysValue->testSysValue->testValue()->testPoint().get());
        assert(sysValue->testSysValue->testValue()->testPoint()->simPoint().get());

        db_syntest::SimPointTypeE simPointType = db_syntest::toSimPointType( //
        sysValue->testSysValue->testValue()->testPoint()->simPoint());

        /*        if (role == Qt::BackgroundRole)
         {
         if (!sysValue->completed())
         return QVariant();

         if (result == passedResult)
         return QBrush(QColor("LightGreen"));

         return QBrush(QColor("Red"));
         }*/

        switch (column)
        {
          case colName:
          {
            // Show the icon
            if (role == Qt::DecorationRole)
            {
              if (!sysValue->completed())
                return QIcon(NOT_TESTED_ICON);

              if (result == passedResult)
                return QIcon(TICK_ICON);

              return QIcon(CROSS_ICON);
            }

            if (role == Qt::DisplayRole)
            {
              db_syntest::st::TestSysValueRunPtrT
                  sysValRun = sysValue->testSysValueRun;

              if (!sysValRun.get())
                return QString();

              if (!sysValRun->feedbackRecieved())
                return QString();

              switch (simPointType)
              {
                /*
                 case db_syntest::di1SimPointType:
                 case db_syntest::di2SimPointType:
                 {
                 boost::format s("%s (%d) <- %s (%d)");
                 s % sysValue->testSysValueRun->sysDigState();
                 s % ((int) sysValue->testSysValueRun->sysValue());

                 s % sysValue->testSysValueRun->sysRawDigState();
                 s % ((int) sysValue->testSysValueRun->sysRawValue());
                 return QString(s.str().c_str());
                 }

                 case db_syntest::aoSimPointType:
                 case db_syntest::aiSimPointType:
                 {
                 boost::format s("%f <- %f");
                 s % sysValue->testSysValueRun->sysValue();
                 s % sysValue->testSysValueRun->sysRawValue();
                 return QString(s.str().c_str());
                 }
                 */

                case db_syntest::aoSimPointType:
                {
                  boost::format s("%f <- %f");
                  s % sysValue->testSysValueRun->sysValue();
                  s % sysValue->testSysValueRun->sysRawValue();
                  return QString(s.str().c_str());
                }

                case db_syntest::di1SimPointType:
                case db_syntest::di2SimPointType:
                case db_syntest::acSimPointType:
                case db_syntest::aiSimPointType:
                {
                  QString val = QString();

                  if (!sysValRun->isSysDisplayOkNull())
                    val += (sysValRun->sysDisplayOk() ? "D " : "!d ");

                  if (!sysValRun->isSysEventOkNull())
                    val += (sysValRun->sysEventOk() ? "E " : "!e ");

                  val += (sysValRun->sysValueOk() ? "V " : "!v ");

                  if (!sysValRun->isSysStateTextOkNull())
                    val += (sysValRun->sysStateTextOk() ? "ST " : "!st ");

                  if (!sysValRun->isSysCommentNull()
                      && !sysValRun->sysComment().empty())
                  {
                    val += '"';
                    val += sysValRun->sysComment().c_str();
                    val += '"';
                  }

                  return val;
                }

                case db_syntest::doSimPointType:
                {
                  // DO Controls do not need states, because simSCADA is very particular
                  // as to what it will accept.
                  QString val = QString("Received");

                  if (!sysValRun->sysComment().empty())
                  {
                    val += '"';
                    val += sysValRun->sysComment().c_str();
                    val += '"';
                  }

                  return val;
                }

                THROW_UNHANDLED_CASE_EXCEPTION
              }

            }
            break;
          }

          case colType:
          {
            if (role == Qt::DisplayRole)
              return QString(sysValue->testSysValue->sysPoint()->system()->name().c_str());
            break;
          }

          case colProgress:
          {
            if (role != Qt::DisplayRole)
              break;

            if (!sysValue->completed())
              return QString("");

            return QString(toString(result).c_str());

            break;
          }
        }

        return QVariant();
      }
      /// ---------------------------------------------------------------------

      /// ---------------
      // QtModel interface

      void
      QtModel::setupModel()
      {
        assert(mNodes.empty());

        const int id = mNodes.size();
        mRootNode = &mNodes[id];
        mRootNode->id = id;
        mRootNode->type = rootNodeType;
        mRootNode->parent = NULL;
        mRootNode->row = 0;

        const TqPointListT& tqPoints = mModel->model();

        for (TqPointListT::const_iterator itr = tqPoints.begin(); itr
            != tqPoints.end(); ++itr)
          setupModelPoint(*mRootNode, *itr);

      }
      /// ---------------------------------------------------------------------

      void
      QtModel::setupModelPoint(Node& parentNode, TqPointPtrT tqPoint)
      {
        const int id = mNodes.size();
        Node& node = mNodes[id];
        node.id = id;
        node.type = pointNodeType;
        node.data = tqPoint;
        node.parent = &parentNode;
        node.row = parentNode.children.size();
        parentNode.children.push_back(&node);

        for (TqValueListT::iterator itr = tqPoint->values.begin(); itr
            != tqPoint->values.end(); ++itr)
          setupModelValue(node, *itr);
      }
      /// ---------------------------------------------------------------------

      void
      QtModel::setupModelValue(Node& parentNode, TqValuePtrT tqValue)
      {
        const int id = mNodes.size();
        Node& node = mNodes[id];
        node.id = id;
        node.type = valueNodeType;
        node.data = tqValue;
        node.parent = &parentNode;
        node.row = parentNode.children.size();
        parentNode.children.push_back(&node);

        for (TqSysValueListT::iterator itr = tqValue->sysValues.begin(); itr
            != tqValue->sysValues.end(); ++itr)
          setupModelSysValue(node, *itr);
      }
      /// ---------------------------------------------------------------------

      void
      QtModel::setupModelSysValue(Node& parentNode, TqSysValuePtrT tqSysValue)
      {
        const int id = mNodes.size();
        Node& node = mNodes[id];
        node.id = id;
        node.type = sysValueNodeType;
        node.data = tqSysValue;
        node.parent = &parentNode;
        node.row = parentNode.children.size();
        parentNode.children.push_back(&node);
      }
      /// ---------------------------------------------------------------------

      const Node&
      QtModel::getNode(int id) const
      {
        assert(id >= 0);

        NodesT::const_iterator itrFind = mNodes.find(id);
        assert(itrFind != mNodes.end());

        return itrFind->second;
      }
    /// ---------------------------------------------------------------------

    } /* namespace test_queue */
  } /* namespace exec */
} /* namespace autotest */
