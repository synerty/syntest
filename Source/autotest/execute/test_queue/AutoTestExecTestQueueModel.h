/*
 * AutoTestExecTestQueueModel.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef AUTOTESTEXECTESTQUEUEMODEL_H_
#define AUTOTESTEXECTESTQUEUEMODEL_H_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <QtCore/qobject.h>

#include <syntest/DbSyntestDeclaration.h>

#include "Macros.h"

namespace autotest
{
  namespace exec
  {
    class TestQueue;

    namespace test_queue
    {
      class Model;
      typedef boost::shared_ptr< Model > ModelPtrT;
      typedef boost::weak_ptr< Model > ModelWptrT;

      struct TqValue;
      typedef boost::weak_ptr< TqValue > TqValueWptrT;
      typedef boost::shared_ptr< TqValue > TqValuePtrT;
      typedef std::vector< TqValuePtrT > TqValueListT;

      struct TqSysValue;
//      typedef boost::weak_ptr< TqSysValue > TqSysValueWptrT;
      typedef boost::shared_ptr< TqSysValue > TqSysValuePtrT;
      typedef std::vector< TqSysValuePtrT > TqSysValueListT;

      struct TqPoint;
      typedef boost::weak_ptr< TqPoint > TqPointWptrT;
      typedef boost::shared_ptr< TqPoint > TqPointPtrT;
      typedef std::vector< TqPointPtrT > TqPointListT;

      enum ResultE
      {
        testNotExectutedResult,
        feedbackNotReceivedResult,
        pointMismatchResult,
        valueMismatchResult,
        digStateMismatchResult,
        eventWrongResult,
        displayWrongResult,
        passedResult
      };

      std::string
      toString(ResultE result);

      struct TqPoint : boost::enable_shared_from_this< TqPoint >
      {
          TqPoint(db_syntest::st::TestRunPtrT testRun,
              db_syntest::st::TestPointPtrT testPoint);

          db_syntest::st::TestRunPtrT testRun;
          db_syntest::st::TestPointPtrT testPoint;

          bool
          completed();

          bool
          passed();

          //! Children
          TqValueListT values;
      };

      struct TqValue : boost::enable_shared_from_this< TqValue >
      {
          TqValue(TqPointWptrT tqPoint,
              db_syntest::st::TestValuePtrT testValue);

          bool
          completed();

          bool
          passed();

          db_syntest::st::TestValuePtrT testValue;
          db_syntest::st::TestValueRunPtrT testValueRun;

          //! Children
          TqSysValueListT sysValues;

          //! Parent
          TqPointWptrT tqPoint;
      };

      struct TqSysValue : boost::enable_shared_from_this< TqSysValue >
      {
          TqSysValue(TqValueWptrT tqValue,
              db_syntest::st::TestSysValuePtrT testSysValue);

          bool
          completed() const;

          ResultE
          result() const;

          db_syntest::st::TestSysValuePtrT testSysValue;
          db_syntest::st::TestSysValueRunPtrT testSysValueRun;

          //! Parent
          TqValueWptrT tqValue;
      };

      enum TestCompleteResultE
      {
        pointValuePassed,
        pointValueFailed
      };

      /*! Test Queue Model
       * This model stores the test queue data in the order that it will be
       * processed.
       *
       */
      class Model : public QObject,
          public boost::enable_shared_from_this< Model >, //
          DebugClassMixin< Model >
      {
        Q_OBJECT

        public:
          Model(TestQueue& uc, db_syntest::st::TestRunPtrT testRun);

          virtual
          ~Model();

        public:

          void
          load();

          const test_queue::TqPointListT&
          model() const;

          db_syntest::st::TestRunPtrT
          testRun();

        public:

          TqValuePtrT
          currentTestValue();

          bool
          moveToFirstTestValue(bool notify = true);

          bool
          moveToTestValue(const TqValuePtrT tqValue);

          bool
          moveToNextTestValue(bool notify = true);

          void
          notifyOfCurrentTestComplete(TestCompleteResultE testCompleteResult);

        private:

          TqPointPtrT
          createTqPoint(db_syntest::st::TestRunPtrT testRun,
              db_syntest::st::TestPointPtrT testPoint);

          TqValuePtrT
          createTqValue(db_syntest::st::TestRunPtrT testRun,
              TqPointPtrT tqPoint,
              db_syntest::st::TestValuePtrT testValue);

          TqSysValuePtrT
          createTqSysValue(TqValuePtrT tqValue,
              db_syntest::st::TestSysValuePtrT testSysValue);

          TestQueue& mUc;

          test_queue::TqPointListT mTqPoints;

          db_syntest::st::TestRunPtrT mTestRun;

          TqPointListT::iterator mCurrentTqPointItr;
          TqValueListT::iterator mCurrentTqValueItr;

      };

    }/* namespace test_queue */
  } /* namespace exec */
} /* namespace autotest */
#endif /* AUTOTESTEXECTESTQUEUEMODEL_H_ */
