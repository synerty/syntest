/*
 * AutoTestExecTestQueueObserver.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AUTO_TEST_EXEC_TEST_QUEUE_OBSERVER_H_
#define _AUTO_TEST_EXEC_TEST_QUEUE_OBSERVER_H_

#include <syntest/DbSyntestDeclaration.h>

#include <boost/weak_ptr.hpp>

#include "AutoTestExecTestQueueModel.h"

namespace autotest
{
  namespace exec
  {
    class TestQueue;
    typedef boost::weak_ptr< TestQueue > TestQueueWptrT;

    /*! TestQueue Observer
     * Classes implementing this will be informed of changes in test processing
     */
    class TestQueueObserver : //
    DebugClassMixin< TestQueueObserver >
    {
      public:
        virtual
        ~TestQueueObserver();

      public:

        virtual void
        testQueueModelChanged(test_queue::ModelPtrT model);

        virtual void
        testQueueCurrentTestChanged(test_queue::TqValuePtrT tqValue);

        virtual void
        testQueueCurrentTestCompleted(test_queue::TestCompleteResultE testCompleteResult);
    };

  } /* namespace exec */
} /* namespace autotest */
#endif /* _AUTO_TEST_EXEC_TEST_QUEUE_OBSERVER_H_ */
