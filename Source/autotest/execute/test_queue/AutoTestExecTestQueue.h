/*
 * AutoTestTestQueue.h
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef _AUTO_TEST_EXEC_TESTQUEUE_H_
#define _AUTO_TEST_EXEC_TESTQUEUE_H_

#include <set>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include <syntest/DbSyntestDeclaration.h>

#include "AutoTestExecTestQueueModel.h"
#include "AutoTestExecTestQueueQtModel.h"
#include "AutoTestExecTestQueueUi.h"

#include "AutoTestExecProcessorObserver.h"

class QWidget;

namespace autotest
{
  namespace exec
  {
    class TestQueueObserver;
    typedef boost::weak_ptr< TestQueueObserver > TestQueueObserverWptrT;
    typedef boost::shared_ptr< TestQueueObserver > TestQueueObserverPtrT;
    typedef void
    (TestQueueObserver::*TestQueueObserveFuncT)();

    /*! autotest::TestQueue Brief Description
     * Long comment about the class
     *
     */
    class TestQueue: public autotest::exec::ProcessorObserver, //
    DebugClassMixin< TestQueue >
    {
      public:
        TestQueue(QWidget* panel);

        virtual
        ~TestQueue();

      public:
        /// ---------------
        /// Test Queue stuff

        void
        setTestRun(db_syntest::st::TestRunPtrT testValueRun);

      public:
        /// ---------------
        /// Interface used by test_queue::Model

        void
        notifyOfCurrentTestChanged(test_queue::TqValuePtrT tqValue);

        void
        notifyOfCurrentTestComplete(test_queue::TestCompleteResultE testCompleteResult);

      private:
        /// ---------------
        /// autotest::exec::ProcessorObserver implementation

        void
        processorTestingStarted();

        void
        processorTestingStopped(const TestStopReasonE reason, const std::string message);

      public:
        /// ---------------
        /// Observerable Stuff

        void
        addObserver(TestQueueObserverPtrT observer);

        void
        removeObserver(TestQueueObserverPtrT observer);

      private:
        /// ---------------
        /// Observerable Stuff

        typedef std::set< TestQueueObserverWptrT > ObserversT;
        typedef std::vector< TestQueueObserverPtrT > ObserverPtrsT;

        void
        notifyOfSomthing(TestQueueObserveFuncT func);

        void
        notifyOfModelChanged();

        void
        notifyOfModelEvent(test_queue::TestCompleteResultE event);

        void
        getObserverPtrs(ObserverPtrsT& observerPtrs);

        ObserversT mObservers;

      private:
        /// ---------------
        /// Processor Stuff (this class)

      private:

//      test_queue::QtModel mQtModel;
        test_queue::TestQueueUi mUi;

        // Probably should use an interface
        friend class test_queue::Model;
        test_queue::ModelPtrT mModel;

    };
  } /* namespace exec */
} /* namespace autotest */
#endif /* _AUTO_TEST_EXEC_TESTQUEUE_H_ */
