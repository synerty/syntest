/*
 * AutoTestExecTestQueueModel.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#include "AutoTestExecTestQueueModel.h"

#include <simscada_util/DbSimscadaUtil.h>

#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StTestBatch.h>
#include <syntest/st/StTestSystem.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestRun.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSysValue.h>
#include <syntest/st/StTestValueRun.h>
#include <syntest/st/StTestSysValueRun.h>
#include <syntest_util/DbSyntestUtil.h>

#include "Macros.h"

#include "AutoTestExecTestQueue.h"

using namespace db_syntest;
using namespace db_syntest::st;
using namespace autotest::exec::test_queue;

namespace autotest
{
  namespace exec
  {

    namespace test_queue
    {
      std::string
      toString(ResultE result)
      {
        switch (result)
        {
          case testNotExectutedResult:
            return "Test Not Executed";

          case feedbackNotReceivedResult:
            return "Feedback Not Received";

          case pointMismatchResult:
            return "Point Missmatch";

          case valueMismatchResult:
            return "Value Missmatch";

          case digStateMismatchResult:
            return "Digital State Missmatch";

          case eventWrongResult:
            return "Event Wrong";

          case displayWrongResult:
            return "Display Wrong";

          case passedResult:
            return "Passed";

          THROW_UNHANDLED_CASE_EXCEPTION
        }
        return "";
      }
      /// ---------------------------------------------------------------------
      /// TqPoint implementation
      /// ---------------------------------------------------------------------

      TqPoint::TqPoint(TestRunPtrT _testRun, TestPointPtrT _testPoint) :
        testRun(_testRun), testPoint(_testPoint)
      {
      }
      /// ---------------------------------------------------------------------

      bool
      TqPoint::completed()
      {
        for (TqValueListT::iterator itr = values.begin(); itr != values.end(); ++itr)
        {
          if (!(*itr)->completed())
            return false;
        }
        return true;
      }
      /// ---------------------------------------------------------------------

      bool
      TqPoint::passed()
      {
        for (TqValueListT::iterator itr = values.begin(); itr != values.end(); ++itr)
        {
          if (!(*itr)->passed())
            return false;
        }
        return true;
      }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// TqValue implementation
      /// ---------------------------------------------------------------------

      TqValue::TqValue(TqPointWptrT _tqPoint, TestValuePtrT _testValue) :
        testValue(_testValue), tqPoint(_tqPoint)
      {

      }
      /// ---------------------------------------------------------------------

      bool
      TqValue::completed()
      {
        return testValueRun.get() != NULL;
      }
      /// ---------------------------------------------------------------------

      bool
      TqValue::passed()
      {
        if (!completed())
          return false;
        assert(testValueRun.get());

        // Iterate though the sys points
        for (TqSysValueListT::const_iterator itr = sysValues.begin(); itr
            != sysValues.end(); ++itr)
        {
          const TqSysValuePtrT& sysValue = *itr;

          if (sysValue->result() != passedResult)
            return false;
        }

        return true;
      }
      /// ---------------------------------------------------------------------

      /// ---------------------------------------------------------------------
      /// TqSysValue implementation
      /// ---------------------------------------------------------------------

      TqSysValue::TqSysValue(TqValueWptrT _tqValue,
          TestSysValuePtrT _testSysValue) :
        testSysValue(_testSysValue), tqValue(_tqValue)
      {
      }
      /// ---------------------------------------------------------------------

      bool
      TqSysValue::completed() const
      {
        return testSysValueRun.get() != NULL;
      }
      /// ---------------------------------------------------------------------

      ResultE
      TqSysValue::result() const
      {
        assert(testSysValue.get());

        TestValuePtrT testValue = testSysValue->testValue();

        db_syntest::SimPointTypeE simPointType = db_syntest::toSimPointType( //
        testSysValue->testValue()->testPoint()->simPoint());

        // We havn't received the results from all the configured sys values
        if (testSysValueRun.get() == NULL)
          return testNotExectutedResult;

        if (!testSysValueRun->feedbackRecieved())
          return feedbackNotReceivedResult;

        switch (simPointType)
        {
          case db_syntest::aoSimPointType:
          {
            if (testSysValueRun->sysValue() != testValue->simValue())
              return valueMismatchResult;
          }

          case db_syntest::di1SimPointType:
          case db_syntest::di2SimPointType:
          case db_syntest::acSimPointType:
          case db_syntest::aiSimPointType:
          {
            if (!testSysValueRun->isSysDisplayOkNull()
                && !testSysValueRun->sysDisplayOk())
              return displayWrongResult;

            if (!testSysValueRun->isSysEventOkNull()
                && !testSysValueRun->sysEventOk())
              return eventWrongResult;

            if (!testSysValueRun->sysValueOk())
              return valueMismatchResult;

            if (!testSysValueRun->isSysStateTextOkNull()
                && !testSysValueRun->sysStateTextOk())
              return digStateMismatchResult;

            break;
          }

          case db_syntest::doSimPointType:
          {
            break;
          }

          default:
          {
            return testNotExectutedResult;
          }
        }

        //        // FIXME For the moment, assume that if there is feedback registered that its the correct point
        //        //        if (testSysValueRun->sysPoint() != testSysValue->sysPoint()->simPoint())
        //        //          return pointMismatchResult;
        //
        //
        //
        //        // We prefer state text matches for digitals.
        //        // This may be wrong in some instances
        //        if (!testValue->isSimDigStateNull()
        //            && testSysValueRun->sysDigState() == testValue->simDigState())
        //          return passedResult;
        //
        //        if (testSysValueRun->sysValue() != testValue->simValue())
        //          return valueMismatchResult;
        //
        //        if (!testValue->isSimDigStateNull()
        //            && testSysValueRun->sysDigState() != testValue->simDigState())
        //          return digStateMismatchResult;

        return passedResult;
      }
    /// ---------------------------------------------------------------------

    }

    namespace test_queue
    {

      /// -----------------------------------------------------------------------
      /// TestQueueModel implermentation
      /// -----------------------------------------------------------------------

      Model::Model(TestQueue& uc, TestRunPtrT testRun) :
        mUc(uc), mTestRun(testRun)
      {
      }

      Model::~Model()
      {
      }

      const TqPointListT&
      Model::model() const
      {
        return mTqPoints;
      }

      db_syntest::st::TestRunPtrT
      Model::testRun()
      {
        return mTestRun;
      }

      bool
      testPointSort(TqPointPtrT i, TqPointPtrT j)
      {
        if (!(i.get() && j.get()))
          return false;

        if (i->completed() && !j->completed())
          return true;

        if (i->testPoint->simPoint()->type()
            != j->testPoint->simPoint()->type())
        {
          return i->testPoint->simPoint()->type()
              < j->testPoint->simPoint()->type();
        }

        return i->testPoint->simPoint()->simAlias()
            < j->testPoint->simPoint()->simAlias();
      }

      void
      Model::load()
      {
        assert(mTestRun.get());
        assert(mTqPoints.empty());

        TestBatchPtrT batch = mTestRun->batch();
        assert(batch.get());

        TestPointListT testPoints;
        batch->testPointBatchs(testPoints);

        for (TestPointListT::iterator itr = testPoints.begin(); itr
            != testPoints.end(); ++itr)
        {
          TestPointPtrT testPoint = *itr;
          TqPointPtrT tqPoint = createTqPoint(mTestRun, testPoint);
          mTqPoints.push_back(tqPoint);
        }

        std::sort(mTqPoints.begin(), mTqPoints.end(), testPointSort);

        moveToFirstTestValue(false);
      }
      /// ---------------------------------------------------------------------

      TqValuePtrT
      Model::currentTestValue()
      {
        if (mCurrentTqPointItr == mTqPoints.end())
          return TqValuePtrT();

        TqValueListT& tqValues = (*mCurrentTqPointItr)->values;

        // If we're not at the end of the points then we should be on a valid value
        // There is an exption for the during the load
        assert(mCurrentTqValueItr != tqValues.end());

        return *mCurrentTqValueItr;
      }
      /// ---------------------------------------------------------------------

      bool
      Model::moveToFirstTestValue(bool notify)
      {
        mCurrentTqPointItr = mTqPoints.begin();

        if (mCurrentTqPointItr == mTqPoints.end())
          mCurrentTqValueItr = TqValueListT::iterator();
        else
          mCurrentTqValueItr = (*mCurrentTqPointItr)->values.begin();

        TqValuePtrT tqValue = currentTestValue();

        if (notify)
          mUc.notifyOfCurrentTestChanged(tqValue);

        return (tqValue != TqValuePtrT());
      }
      /// ---------------------------------------------------------------------

      bool
      Model::moveToNextTestValue(bool notify)
      {
        assert(mCurrentTqPointItr != mTqPoints.end());
        assert(mCurrentTqValueItr != (*mCurrentTqPointItr)->values.end());

        ++mCurrentTqValueItr;
        if (mCurrentTqValueItr == (*mCurrentTqPointItr)->values.end())
        {
          ++mCurrentTqPointItr;

          if (mCurrentTqPointItr == mTqPoints.end())
          {
            mCurrentTqValueItr = TqValueListT::iterator();
          }
          else
          {
            assert(!(*mCurrentTqPointItr)->values.empty());
            mCurrentTqValueItr = (*mCurrentTqPointItr)->values.begin();
          }
        }

        TqValuePtrT tqValue = currentTestValue();

        if (notify)
          mUc.notifyOfCurrentTestChanged(tqValue);

        return (tqValue != TqValuePtrT());
      }
      /// ---------------------------------------------------------------------

      bool
      Model::moveToTestValue(const TqValuePtrT tqValue)
      {
        if (currentTestValue() == tqValue)
          return true;

        bool success = moveToFirstTestValue(false);

        while (success && currentTestValue() != tqValue)
          success = moveToNextTestValue(false);

        TqValuePtrT curentTqValue = currentTestValue();
        mUc.notifyOfCurrentTestChanged(curentTqValue);
        return (curentTqValue == tqValue);
      }
      /// ---------------------------------------------------------------------

      void
      Model::notifyOfCurrentTestComplete(TestCompleteResultE testCompleteResult)
      {
        mUc.notifyOfCurrentTestComplete(testCompleteResult);
      }
      /// ---------------------------------------------------------------------

      bool
      testValueSort(TqValuePtrT i, TqValuePtrT j)
      {
        if (!(i.get() && j.get()))
          return false;

        if (i->completed() && !j->completed())
          return true;

        TestValuePtrT a = i->testValue;
        TestValuePtrT b = j->testValue;

        if (a->simValue() != b->simValue())
          return a->simValue() < b->simValue();

        return a->simDigValue() < b->simDigState();
      }

      TqPointPtrT
      Model::createTqPoint(db_syntest::st::TestRunPtrT testRun,
          db_syntest::st::TestPointPtrT testPoint)
      {
        assert(testRun.get());
        assert(testPoint.get());

        TqPointPtrT tqPoint(new TqPoint(testRun, testPoint));

        TestValueListT testValues;
        testPoint->testValueTestPoints(testValues);

        // All test points should have values
        if (testValues.empty())
          throw std::runtime_error("An error has occurred"
            ", test point has no test values");

        for (TestValueListT::iterator itr = testValues.begin(); itr
            != testValues.end(); ++itr)
        {
          TestValuePtrT testValue = *itr;
          TqValuePtrT tqValue = createTqValue(testRun, tqPoint, testValue);
          tqPoint->values.push_back(tqValue);
        }

        // Sort the point values
        std::sort(tqPoint->values.begin(), tqPoint->values.end(), testValueSort);

        return tqPoint;
      }
      /// ---------------------------------------------------------------------

      bool
      sysValueSort(TqSysValuePtrT i, TqSysValuePtrT j)
      {
        if (!(i.get() && j.get()))
          return false;

        return i->testSysValue->sysPoint()->system()->name()
            < j->testSysValue->sysPoint()->system()->name();
      }

      TqValuePtrT
      Model::createTqValue(db_syntest::st::TestRunPtrT testRun,
          TqPointPtrT tqPoint,
          db_syntest::st::TestValuePtrT testValue)
      {
        assert(testValue.get());

        TqValuePtrT tqValue(new TqValue(tqPoint, testValue));

        // Find the TestValueRun if it exists
        TestValueRunListT testValueRuns;
        testValue->testValueRunTestValues(testValueRuns);

        for (TestValueRunListT::iterator itr = testValueRuns.begin(); itr
            != testValueRuns.end(); ++itr)
        {
          if ((*itr)->testRun() == testRun)
          {
            tqValue->testValueRun = *itr;
            break;
          }
        }

        // Add the TestSysValues
        TestSysValueListT testSysValues;
        testValue->testSysValueTestValues(testSysValues);

        for (TestSysValueListT::iterator itr = testSysValues.begin(); itr
            != testSysValues.end(); ++itr)
        {
          TestSysValuePtrT testSysValue = *itr;
          TqSysValuePtrT tqSysValue = createTqSysValue(tqValue, testSysValue);
          tqValue->sysValues.push_back(tqSysValue);
        }

        std::sort(tqValue->sysValues.begin(),
            tqValue->sysValues.end(),
            sysValueSort);

        return tqValue;
      }
      /// ---------------------------------------------------------------------

      TqSysValuePtrT
      Model::createTqSysValue(TqValuePtrT tqValue,
          db_syntest::st::TestSysValuePtrT testSysValue)
      {

        TqSysValuePtrT tqSysValue(new TqSysValue(tqValue, testSysValue));

        // Find the TestValueRun if it exists
        if (tqValue->testValueRun.get() == NULL)
          return tqSysValue;

        TestValueRunPtrT& testValueRun = tqValue->testValueRun;

        TestSysValueRunListT testSysValueRuns;
        testValueRun->testSysValueRunTestValueRuns(testSysValueRuns);

        for (TestSysValueRunListT::iterator itr = testSysValueRuns.begin(); itr
            != testSysValueRuns.end(); ++itr)
        {
          TestSysValueRunPtrT testSysValueRun = *itr;
          if (testSysValueRun->testValueRun() == testValueRun //
              && testSysValueRun->testSysValue() == testSysValue)
          {
            tqSysValue->testSysValueRun = *itr;
            break;
          }
        }
        return tqSysValue;
      }
    /// ---------------------------------------------------------------------

    }/* namespace test_queue */
  } /* namespace exec */
} /* namespace autotest */
