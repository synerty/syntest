/*
 * AutoTestExec.cpp
 *
 *  Copyright Synerty Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */
#include "AutoTestExec.h"

#include <time.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/local_time_adjustor.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
//#include <boost/date_time/gregorian/gregorian.hpp>

#include <QtGui/qmessagebox.h>
#include <QtGui/qinputdialog.h>

#include <syntest_util/DbSyntestUtil.h>
#include <syntest/DbSyntestDeclaration.h>
#include <syntest/SynTestModel.h>
#include <syntest/st/StTestBatch.h>
#include <syntest/st/StTestRun.h>

#include "MainAutoTest.h"
#include "MainSynModel.h"
#include "Main.h"

#include "AutoTestExecUi.h"
#include "AutoTestExecTestQueue.h"
#include "AutoTestExecSummary.h"
#include "AutoTestExecProcessor.h"

#include "Globals.h"

using namespace autotest;
using namespace autotest::exec;

using namespace db_syntest;
using namespace db_syntest::st;

AutoTestExec::AutoTestExec() :
    mSynModel(MainSynModel::Inst().ormModel())
{
  assert(mSynModel.get());
}
/// -------------------------------------------------------------------------

AutoTestExec::~AutoTestExec()
{
}
/// -------------------------------------------------------------------------

void
AutoTestExec::launch()
{
  PtrT ptr(new AutoTestExec);
  boost::shared_ptr< MainObserver > basePtr(ptr);
  Main::Inst().addObserver(basePtr);
  ptr->run();
}
/// -------------------------------------------------------------------------

void
AutoTestExec::run()
{
  if (mSynModel->StTestBatchs().empty())
  {
    QMessageBox::information(MsgBoxParent(),
        "No Test Batches",
        "Can not execute tests.\n"
            "No test batches are defined, Please create at least one test batch.");
    Main::Inst().defaultUiMode();
    return;
  }

  mInitialising = true;

  init();

  Main::Inst().selectUiMode(main_ui::autoTestUiMode,
      mUi.get(),
      shared_from_this());

  mInitialising = false;
}
// ----------------------------------------------------------------------------

// ---------------
// MainUiCenter implementation

void
AutoTestExec::mainUiCenterClose()
{
  stop();

  mTestQueue->removeObserver(mProcessor);
  mTestQueue->removeObserver(mSummary);
  mProcessor->removeObserver(mTestQueue);
  mProcessor->removeObserver(this);

  mProcessor->close();

  mProcessor.reset();
  mSummary.reset();
  mTestQueue.reset();
}
// ----------------------------------------------------------------------------

void
AutoTestExec::close()
{
  Main::Inst().defaultUiMode();
}
// ----------------------------------------------------------------------------

void
AutoTestExec::start()
{
  mProcessor->run();
}
// ----------------------------------------------------------------------------

void
AutoTestExec::stop()
{
  mProcessor->stop();
}
// ----------------------------------------------------------------------------

void
AutoTestExec::newTestRun()
{
  using namespace boost::posix_time;

  TestBatchPtrT batch = getBatch(mUi->selectedBatch());

//  QString desc = QInputDialog::getTest(MsgBoxParent(),
//      "New Run Description",
//      "Please enter a description for the new test run");

  TestRunListT runs;
  batch->testRunBatchs(runs);

  TestRunPtrT newTestRun(new TestRun());
  newTestRun->setId(nextId(mSynModel));
  newTestRun->setBatch(batch);
  newTestRun->setRunNumber(runs.size() + 1);
  newTestRun->setDateStarted(time(NULL));
  newTestRun->addToModel(mSynModel);

  mSynModel->save();

  loadTestRuns(batch->name());
}
// ----------------------------------------------------------------------------

void
AutoTestExec::init()
{
  mUi.reset(new autotest::exec::ExecUi(*this));
  assert(mUi.get());

  mSummary.reset(new exec::Summary(mUi->summaryPanel()));
  mTestQueue.reset(new exec::TestQueue(mUi->testQueuePanel()));
  mProcessor.reset(new exec::Processor(mUi->systemsPanel()));

  mTestQueue->addObserver(mProcessor);
  mTestQueue->addObserver(mSummary);
  mProcessor->addObserver(mTestQueue);
  mProcessor->addObserver(shared_from_this());

  mUi->setUiState(autotest::exec::choosingBatchUiState);

  loadTestBatches();
}
// ----------------------------------------------------------------------------

// ---------------
// MainObserver implementation

void
AutoTestExec::mainUiModeChanged(main_ui::UiModeE oldUiMode,
    main_ui::UiModeE newUiMode)
{
}
// ----------------------------------------------------------------------------

/// ---------------
/// autotest::exec::ProcessorObserver implementation

void
AutoTestExec::processorTestingStarted()
{
  mUi->setUiState(autotest::exec::testingRunningUiState);
}
// ----------------------------------------------------------------------------

void
AutoTestExec::processorTestingStopped(const TestStopReasonE reason,
    const std::string message)
{
  mUi->setUiState(autotest::exec::testingStoppedUiState);
  mUi->alertOfTestStopReason(reason, message);
}
// ----------------------------------------------------------------------------

void
AutoTestExec::testBatchSelected(const std::string name)
{
  loadTestRuns(name);
}
// ----------------------------------------------------------------------------

void
AutoTestExec::testRunSelected(const int runNumber)
{
  loadTestRun(runNumber);
}
// ----------------------------------------------------------------------------

void
AutoTestExec::loadTestBatches()
{
  mBatchIdByName.clear();

  const TestBatchListT& batches = mSynModel->StTestBatchs();
  TestBatchPtrT newestBatch;

  exec::ExecUi::CboBatchDataT batchData;

  for (TestBatchListT::const_iterator itr = batches.begin();
      itr != batches.end(); ++itr)
  {
    TestBatchPtrT batch = *itr;

    if (!newestBatch.get() || newestBatch->id() < batch->id())
      newestBatch = batch;

    batchData.push_back(batch->name());
    mBatchIdByName.insert(std::make_pair(batch->name(), batch->id()));
  }

  mUi->setTestBatches(batchData);
  mUi->setUiState(exec::choosingBatchUiState);
  mUi->selectBatch(newestBatch->name());
}
// ----------------------------------------------------------------------------

void
AutoTestExec::loadTestRuns(const std::string batchName)
{
  using namespace boost::posix_time;
  using namespace boost::date_time;
  typedef boost::date_time::c_local_adjustor< ptime > local_adj;

  TestBatchPtrT batch = getBatch(batchName);

  TestRunListT runs;
  batch->testRunBatchs(runs);

  exec::ExecUi::CboRunDataT runData;
  mRunIdByNumber.clear();

  int newsetRunNumber = -1;

  for (TestRunListT::iterator itr = runs.begin(); itr != runs.end(); ++itr)
  {
    TestRunPtrT run = *itr;
    if (newsetRunNumber < run->runNumber())
      newsetRunNumber = run->runNumber();

    boost::format label("%d - %s");
    label % run->runNumber();
    ptime utc = from_time_t(run->dateStarted());
    label % to_simple_string(local_adj::utc_to_local(utc));

    runData.insert(std::make_pair(run->runNumber(), label.str()));
    mRunIdByNumber.insert(std::make_pair(run->runNumber(), run->id()));
  }

  mUi->setTestRuns(runData);
  mUi->setUiState(exec::choosingRunUiState);

  if (newsetRunNumber != -1)
    mUi->selectRun(newsetRunNumber);
}
// ----------------------------------------------------------------------------

TestBatchPtrT
AutoTestExec::getBatch(const std::string batchName)
{
  BatchIdByNameT::iterator batchFindItr = mBatchIdByName.find(batchName);
  assert(batchFindItr != mBatchIdByName.end());

  TestBatchPtrT batch = mSynModel->StTestBatch(batchFindItr->second);
  assert(batch.get());

  return batch;
}
// ----------------------------------------------------------------------------

TestRunPtrT
AutoTestExec::getRun(const int runNumber)
{
  RunIdByNumberT::iterator runFindItr = mRunIdByNumber.find(runNumber);
  assert(runFindItr != mRunIdByNumber.end());

  TestRunPtrT testRun = mSynModel->StTestRun(runFindItr->second);
  assert(testRun.get());

  return testRun;
}
// ----------------------------------------------------------------------------

void
AutoTestExec::loadTestRun(const int runNumber)
{
  mUi->setUiState(exec::testingStoppedUiState);
  TestRunPtrT testRun = getRun(runNumber);

  assert(mProcessor.get());

  mTestQueue->setTestRun(testRun);
}
// ----------------------------------------------------------------------------

