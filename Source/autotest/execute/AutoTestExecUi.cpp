#include "AutoTestExecUi.h"

// Std C++
#include <cassert>

// Boost
#include <boost/format.hpp>

// Qt
#include <QtCore/qstring.h>

#include "Globals.h"
#include "AutoTestExec.h"
#include "UiMessage.h"

namespace autotest
{
  namespace exec
  {
    ExecUi::ExecUi(AutoTestExec& uc) :
        mUc(uc)
    {
      ui.setupUi(this);

      setUiState(choosingBatchUiState);

      connectSlots();
    }
    /// -----------------------------------------------------------------------

    ExecUi::~ExecUi()
    {

    }
    /// -----------------------------------------------------------------------

    QWidget*
    ExecUi::testQueuePanel()
    {
      return ui.wgtTestQueue;
    }
    /// -----------------------------------------------------------------------

    QWidget*
    ExecUi::systemsPanel()
    {
      return ui.wgtSystems;
    }
    /// -----------------------------------------------------------------------

    QWidget*
    ExecUi::summaryPanel()
    {
      return ui.wgtSummary;
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::setUiState(const ExecUiStateE state)
    {
      const bool running = (state == testingRunningUiState);
      const bool stopped = (state == testingStoppedUiState);
      const bool chBatch = (state == choosingBatchUiState);
//      const bool chRun = (state == choosingRunUiState);

      assert(ui.cboTestRun);
      const bool runsExist = (ui.cboTestRun->count() != 0);

      ui.cboTestBatch->setEnabled(!running);
      ui.cboTestRun->setEnabled(!chBatch && runsExist && !running);

      ui.wgtControlRunning->setVisible(running);

      ui.wgtControlStopped->setVisible(!running);
      ui.wgtControlStopped->setEnabled(stopped);

      ui.wgtSystems->setEnabled(running || stopped);
//      ui.wgtSummary->setEnabled(running || stopped);
      ui.wgtTestQueue->setEnabled(running || stopped);

    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::setTestBatches(const CboBatchDataT& batches)
    {
      QStringList qStrs;
      for (CboBatchDataT::const_iterator itr = batches.begin();
          itr != batches.end(); ++itr)
        qStrs.append(QString(itr->c_str()));

      qStrs.sort();

      QComboBox* cbo = ui.cboTestBatch;
      assert(cbo);
      cbo->blockSignals(true);
      cbo->clear();
      cbo->addItems(qStrs);

      // Reset the test runs
      setTestRuns(CboRunDataT());
      cbo->blockSignals(false);
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::selectBatch(const std::string& batch)
    {
      QComboBox* cbo = ui.cboTestBatch;
      assert(cbo);

      int index = cbo->findText(QString(batch.c_str()));
      assert(index >= 0);

      // The UI events trigger the UC to do its work.
      // These events won't happen if the first item is already selected from
      // when the combo box was populated so call this event manually
      if (cbo->currentIndex() == index)
        mUc.testBatchSelected(batch);
      else
        cbo->setCurrentIndex(index);
    }
    /// -----------------------------------------------------------------------

    std::string
    ExecUi::selectedBatch()
    {
      QComboBox* cbo = ui.cboTestBatch;
      assert(cbo);

      std::string batchName = cbo->currentText().toStdString();
      assert(batchName.size());

      return batchName;
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::setTestRuns(const CboRunDataT& runs)
    {
      QComboBox* cbo = ui.cboTestRun;
      assert(cbo);
      cbo->blockSignals(true);
      cbo->clear();

      for (CboRunDataT::const_iterator itr = runs.begin(); itr != runs.end();
          ++itr)
        cbo->addItem(QString(itr->second.c_str()), QVariant(itr->first));
      cbo->blockSignals(false);
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::selectRun(const int runNumber)
    {
      QComboBox* cbo = ui.cboTestRun;
      assert(cbo);

      int index = cbo->findData(QVariant(runNumber));
      assert(index >= 0);

      // The UI events trigger the UC to do its work.
      // These events won't happen if the first item is already selected from
      // when the combo box was populated so call this event manually
      if (cbo->currentIndex() == index)
        mUc.testRunSelected(runNumber);
      else
        cbo->setCurrentIndex(index);
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::alertOfTestStopReason(const TestStopReasonE reason,
        const std::string message)
    {
      boost::format info("%s%s%s");

      UiMessage::MessageTypeE type = UiMessage::criticalMessage;

      switch (reason)
      {
        case completedTestStopReason:
          info % "Test run complete";
          type = UiMessage::infomationMessage;
          break;

        case userTestStopReason:
          // The user doesn't need to be told of this
          return;

        case noCurrentPointTestStopReason:
          info % "Test not started, select a test value to start testing from";
          break;

        case agentDisconnectTestStopReason:
          info % "Testing stopped, An agent connection has been lost";
          break;

        case simulatorDisconnectTestStopReason:
          info % "Testing stopped, A simulator connection has been lost";
          break;

        case modelChangedTestStopReason:
          info
              % "Abnormal condition, Model was changed when testing wasn't stopped";
          break;

        case unknownTestStopReason:
          info % "Testing has stopped, Consult technical support";
          break;

        THROW_UNHANDLED_CASE_EXCEPTION

      }

      if (!message.empty())
        info % '\n' % message;
      else
        info % "" % "";

      const std::string title = "Testing Stopped";
      UiMessage().showMessage(type, title, info.str());

    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::testBatchChanged(const QString& newBatch)
    {
      assert(!newBatch.isEmpty());
      mUc.testBatchSelected(newBatch.toStdString());
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::testRunChanged(int runIndex)
    {
      QComboBox* cbo = ui.cboTestRun;
      assert(cbo);
      QVariant runNumberVariant = cbo->itemData(runIndex);

      bool ok = false;
      int runNumber = runNumberVariant.toInt(&ok);
      assert(ok);

      mUc.testRunSelected(runNumber);
    }
    /// -----------------------------------------------------------------------

    void
    ExecUi::connectSlots()
    {

      connect(ui.btnClose, SIGNAL(clicked()), &mUc, SLOT(close()));
      connect(ui.btnTestRunNew, SIGNAL(clicked()), &mUc, SLOT(newTestRun()));
      connect(ui.btnStart, SIGNAL(clicked()), &mUc, SLOT(start()));
      connect(ui.btnStop, SIGNAL(clicked()), &mUc, SLOT(stop()));

      connect(ui.cboTestBatch,
          SIGNAL(currentIndexChanged(const QString&)),
          this,
          SLOT(testBatchChanged(const QString&)));

      connect(ui.cboTestRun,
          SIGNAL(currentIndexChanged(int)),
          this,
          SLOT(testRunChanged(int)));
    }
  /// -----------------------------------------------------------------------

  } /* namespace exec */
} /* namespace autotest */
