#ifndef AUTOTESTEXECUI_H
#define AUTOTESTEXECUI_H

#include <vector>
#include <map>

#include <QtGui/QWidget>

#include "ui_AutoTestExecUi.h"
#include "AutoTestExecDeclarations.h"
#include "Macros.h"

class AutoTestExec;

namespace autotest
{

  namespace exec
  {

    class ExecUi : public QWidget, DebugClassMixin< ExecUi >
    {
      Q_OBJECT

      public:
        ExecUi(AutoTestExec& uc);
        ~ExecUi();

      public:

        QWidget*
        testQueuePanel();

        QWidget*
        systemsPanel();

        QWidget*
        summaryPanel();

      public:
        typedef std::vector< std::string > CboBatchDataT;
        typedef std::map< int, std::string > CboRunDataT;

        void
        setUiState(const ExecUiStateE state);

        void
        setTestBatches(const CboBatchDataT& batches);

        void
        selectBatch(const std::string& batch);

        std::string
        selectedBatch();

        void
        setTestRuns(const CboRunDataT& runs);

        void
        selectRun(const int runNumber);

        void
        alertOfTestStopReason(const TestStopReasonE reason,
            const std::string message);

      public slots:

        void
        testBatchChanged(const QString& newBatch);

        void
        testRunChanged(int runIndex);

      private:

        void
        connectSlots();

        Ui::AutoTestExecUiClass ui;
        AutoTestExec& mUc;

    };
  } /* namespace exec */
} /* namespace autotest */
#endif // AUTOTESTEXECUI_H
