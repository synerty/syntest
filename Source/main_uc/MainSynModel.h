/*
 * MainSynModel.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_SYN_MODEL_H_
#define _MAIN_SYN_MODEL_H_

#include <set>
#include <string>

#include <boost/noncopyable.hpp>

#include <QtCore/qobject.h>

#include <syntest/DbSyntestDeclaration.h>

class MainSynModelObserver;

/*! Main SynModel
 * Core application controller for filter related activity
 *
 */
class MainSynModel : public QObject, boost::noncopyable
{
  Q_OBJECT

  public:
    static MainSynModel&
    Inst();

    virtual
    ~MainSynModel();

  public:
    db_syntest::SynTestModelPtrT
    ormModel();

    bool
    isOrmLoaded();

    void
    loadOrmModel(std::string fileName);

    void
    newDatabaseWithFile(std::string fileName);

  public slots:

    void
    newDatabase();

    void
    initialise();

  public:
    void
    addObserver(MainSynModelObserver* observer);

    void
    removeObserver(MainSynModelObserver* observer);

  private:
    void
    notifyOfSynTestModelChange();

    typedef std::set< MainSynModelObserver* > ObserversT;
    ObserversT mObservers;

  private:
    db_syntest::SynTestModelPtrT mSynTestModel;

  private:
    // ---------------
    // Singleton stuff
    MainSynModel();

    static MainSynModel sInst;
};

#endif /* _MAIN_SYN_MODEL_H_ */
