/*
 * MainObjectModel.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_OBJECT_MODEL_H_
#define _MAIN_OBJECT_MODEL_H_

#include <set>
#include <vector>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include <QtCore/qobject.h>

class MainObjectModelObserver;

#include <simscada/DbSimscadaDeclaration.h>

/*! Main ObjectModel
 * Core application controller for filter related activity
 *
 */
class MainObjectModel : public QObject, boost::noncopyable
{
  Q_OBJECT

  public:
    static MainObjectModel&
    Inst();

    virtual
    ~MainObjectModel();

  public:

    bool
    loadOrmModel(std::string fileName);

  public:
    // ---------------
    // Methods for getting the models

    db_simscada::SimScadaModelPtrT
    ormModel();

    bool
    isOrmLoaded();

  public:
    void
    addObserver(MainObjectModelObserver* observer);

    void
    removeObserver(MainObjectModelObserver* observer);

  private:
    void
    notifyOfOrmModelChange();

    void
    saveSimScadaFileName(const std::string& filename);

    typedef std::set<MainObjectModelObserver*> ObserversT;
    ObserversT mObservers;

    db_simscada::SimScadaModelPtrT mSimScadaModel;

  private:
    // ---------------
    // Singleton stuff
    MainObjectModel();

    static MainObjectModel sInst;
};

#endif /* _MAIN_OBJECT_MODEL_H_ */
