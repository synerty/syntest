/*
 * MainAutoTestObserver.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_AUTO_TEST_OBSERVER_H_
#define _MAIN_AUTO_TEST_OBSERVER_H_

#include <syntest/DbSyntestDeclaration.h>

/*! Main AutoTest Observer
 *
 */
class MainAutoTestObserver
{
  public:
    MainAutoTestObserver();

    virtual
    ~MainAutoTestObserver();

  public:
//    virtual void
//    autoTestModelChanged(db_syntest::SynTestModelPtrT synTestModelr);
};

#endif /* _MAIN_AUTO_TEST_OBSERVER_H_ */
