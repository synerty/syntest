/*
 * MainAutoTestObserver.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainAutoTestObserver.h"

#include "MainAutoTest.h"

MainAutoTestObserver::MainAutoTestObserver()
{
  MainAutoTest::Inst().addObserver(this);
}
// ----------------------------------------------------------------------------

MainAutoTestObserver::~MainAutoTestObserver()
{
  MainAutoTest::Inst().removeObserver(this);
}
// ----------------------------------------------------------------------------

//void
//MainAutoTestObserver::autoTestModelChanged(db_syntest::SynTestModelPtrT synTestModelr)
//{
//}
// ----------------------------------------------------------------------------

