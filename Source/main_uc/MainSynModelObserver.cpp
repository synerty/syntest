/*
 * MainSynModelObserver.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainSynModelObserver.h"

#include "MainSynModel.h"

MainSynModelObserver::MainSynModelObserver()
{
  MainSynModel::Inst().addObserver(this);
}
// ----------------------------------------------------------------------------

MainSynModelObserver::~MainSynModelObserver()
{
  MainSynModel::Inst().removeObserver(this);
}
// ----------------------------------------------------------------------------

void
MainSynModelObserver::autoTestModelChanged(db_syntest::SynTestModelPtrT synTestModelr)
{
}
// ----------------------------------------------------------------------------

