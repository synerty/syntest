/*
 * MainSynModel.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainSynModel.h"

#include <stdexcept>

#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include <orm/v1/Conn.h>

#include <syntest/SynTestModel.h>

#include "MainSynModelObserver.h"
#include "Settings.h"
#include "OrmModelThread.h"

#include "SynModelCreate.h"
#include "SynModelInit.h"

// ----------------------------------------------------------------------------
//                          MainSynModel implementation
// ----------------------------------------------------------------------------

// Initialise static variables
// Initialise before MainObjectModel
MainSynModel MainSynModel::sInst __attribute__((init_priority(64000)));

MainSynModel::MainSynModel()
{
}
// ----------------------------------------------------------------------------

MainSynModel&
MainSynModel::Inst()
{
  return sInst;
}
// ----------------------------------------------------------------------------

MainSynModel::~MainSynModel()
{
}
// ----------------------------------------------------------------------------

db_syntest::SynTestModelPtrT
MainSynModel::ormModel()
{
  assert(mSynTestModel.get());
  return mSynTestModel;
}
// ----------------------------------------------------------------------------

bool
MainSynModel::isOrmLoaded()
{
  return !!mSynTestModel.get();
}
// ----------------------------------------------------------------------------

void
MainSynModel::loadOrmModel(std::string fileName)
{
  namespace bf = boost::filesystem2;

  if (fileName.empty())
    fileName = Settings().synTestModelFileName();

  // Store filename for later
  Settings().setSynTestModelFileName(fileName);

  try
  {
    bf::path dbPath(fileName);

    if (!bf::exists(dbPath) || !bf::is_regular_file(dbPath))
      throw std::runtime_error("File does not exists or is not a file");

    orm::v1::Conn conn;
    conn.Database = fileName;
    conn.setTypeFromDatabaseName();

    mSynTestModel.reset(new db_syntest::SynTestModel(conn));

    loadModel(mSynTestModel, "SynTEST");

    notifyOfSynTestModelChange();
  }
  catch (std::exception& e)
  {
    // That didn't work, make sure the filename in the settings is not stored.
    Settings().setSynTestModelFileName(std::string());

    mSynTestModel.reset();
    notifyOfSynTestModelChange();

    boost::format info("Can not load SynTEST database %s\n%s");
    info % fileName;
    info % e.what();
    throw std::runtime_error(info.str());
  }
}
// ----------------------------------------------------------------------------

void
MainSynModel::newDatabaseWithFile(std::string fileName)
{
  SynModelCreate o;
  o.run(fileName);
}
// ----------------------------------------------------------------------------

void
MainSynModel::newDatabase()
{
  SynModelCreate o;
  o.run();
}
// ----------------------------------------------------------------------------

void
MainSynModel::initialise()
{
  SynModelInit o;
  o.run();
}
// ----------------------------------------------------------------------------

void
MainSynModel::addObserver(MainSynModelObserver* observer)
{
  mObservers.insert(observer);
}
// ----------------------------------------------------------------------------

void
MainSynModel::removeObserver(MainSynModelObserver* observer)
{
  mObservers.erase(observer);
}
// ----------------------------------------------------------------------------

void
MainSynModel::notifyOfSynTestModelChange()
{
  for (ObserversT::iterator itr = mObservers.begin(); //
  itr != mObservers.end(); //
      ++itr)
  {
    (*itr)->autoTestModelChanged(mSynTestModel);
  }
}
// ----------------------------------------------------------------------------
