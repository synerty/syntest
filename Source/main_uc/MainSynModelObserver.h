/*
 * MainSynModelObserver.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_SYN_MODEL_OBSERVER_H_
#define _MAIN_SYN_MODEL_OBSERVER_H_

#include <syntest/DbSyntestDeclaration.h>

/*! Main SynModel Observer
 *
 */
class MainSynModelObserver
{
  public:
    MainSynModelObserver();

    virtual
    ~MainSynModelObserver();

  public:
    virtual void
    autoTestModelChanged(db_syntest::SynTestModelPtrT synTestModelr);
};

#endif /* _MAIN_SYN_MODEL_OBSERVER_H_ */
