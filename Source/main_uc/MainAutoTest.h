/*
 * MainAutoTest.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_AUTO_TEST_H_
#define _MAIN_AUTO_TEST_H_

#include <set>

#include <boost/noncopyable.hpp>

#include <QtCore/qobject.h>

#include <syntest/DbSyntestDeclaration.h>

class MainAutoTestObserver;

/*! Main AutoTest
 * Core application controller for filter related activity
 *
 */
class MainAutoTest : public QObject, boost::noncopyable
{
  Q_OBJECT

  public:
    static MainAutoTest&
    Inst();

    virtual
    ~MainAutoTest();

  public:

  public slots:

    void
    newTest();

    void
    executeTest();

    void
    displayTestResults();

    void
    exportTestResults();

  public:
    void
    addObserver(MainAutoTestObserver* observer);

    void
    removeObserver(MainAutoTestObserver* observer);

  private:

  private:
//    void
//    notifyOfSynTestModelChange();

    typedef std::set< MainAutoTestObserver* > ObserversT;
    ObserversT mObservers;

  private:
    // ---------------
    // Singleton stuff
    MainAutoTest();

    static MainAutoTest sInst;
};

#endif /* _MAIN_AUTO_TEST_H_ */
