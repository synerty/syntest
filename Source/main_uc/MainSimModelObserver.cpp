/*
 * MainObjectModelObserver.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainSimModelObserver.h"

#include "MainSimModel.h"

MainObjectModelObserver::MainObjectModelObserver()
{
  MainObjectModel::Inst().addObserver(this);
}
// ----------------------------------------------------------------------------

MainObjectModelObserver::~MainObjectModelObserver()
{
  MainObjectModel::Inst().removeObserver(this);
}
// ----------------------------------------------------------------------------

void
MainObjectModelObserver::simOrmModelChanged(boost::weak_ptr<db_simscada::SimScadaModel> ormModel)
{
}
// ----------------------------------------------------------------------------

