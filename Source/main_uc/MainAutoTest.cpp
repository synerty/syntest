/*
 * MainAutoTest.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainAutoTest.h"

#include <stdexcept>

#include "MainAutoTestObserver.h"

#include "AutoTestGenBatch.h"
#include "AutoTestExec.h"

// ----------------------------------------------------------------------------
//                          MainAutoTest implementation
// ----------------------------------------------------------------------------

// Initialise static variables
// Initialise before MainObjectModel
MainAutoTest MainAutoTest::sInst __attribute__((init_priority(64000)));

MainAutoTest::MainAutoTest()
{
}
// ----------------------------------------------------------------------------

MainAutoTest&
MainAutoTest::Inst()
{
  return sInst;
}
// ----------------------------------------------------------------------------

MainAutoTest::~MainAutoTest()
{
}
// ----------------------------------------------------------------------------

void
MainAutoTest::newTest()
{
  AutoTestGenBatch o;
  o.run();
}
// ----------------------------------------------------------------------------

void
MainAutoTest::executeTest()
{
  AutoTestExec::launch();
}
// ----------------------------------------------------------------------------

void
MainAutoTest::displayTestResults()
{
}
// ----------------------------------------------------------------------------

void
MainAutoTest::exportTestResults()
{
}
// ----------------------------------------------------------------------------

void
MainAutoTest::addObserver(MainAutoTestObserver* observer)
{
  mObservers.insert(observer);
}
// ----------------------------------------------------------------------------

void
MainAutoTest::removeObserver(MainAutoTestObserver* observer)
{
  mObservers.erase(observer);
}
// ----------------------------------------------------------------------------

//void
//MainAutoTest::notifyOfSynTestModelChange()
//{
//  for (ObserversT::iterator itr = mObservers.begin(); //
//  itr != mObservers.end(); //
//      ++itr)
//  {
//    (*itr)->autoTestModelChanged(mSynTestModel);
//  }
//}
//// ----------------------------------------------------------------------------
