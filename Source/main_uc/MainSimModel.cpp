/*
 * MainObjectModel.cpp
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#include "MainSimModel.h"

#include <QtGui/qmessagebox.h>

#include <boost/format.hpp>
#include <boost/filesystem.hpp>

#include <simscada/SimScadaModel.h>
#include <simscada_util/DbSimScadaUtil.h>
#include <orm/v1/Conn.h>

#include "MainSimModelObserver.h"
#include "MainSynModel.h"
#include "Settings.h"
#include "SimScadaSettings.h"
#include "Globals.h"
#include "OrmModelThread.h"
#include "SynTestDbValidator.h"

// ----------------------------------------------------------------------------
//                          MainObjectModel implementation
// ----------------------------------------------------------------------------

// Initialise static variables
MainObjectModel MainObjectModel::sInst __attribute__((init_priority(65000)));

MainObjectModel::MainObjectModel()
{
}
// ----------------------------------------------------------------------------

MainObjectModel&
MainObjectModel::Inst()
{
  return sInst;
}
// ----------------------------------------------------------------------------

MainObjectModel::~MainObjectModel()
{
}
// ----------------------------------------------------------------------------

db_simscada::SimScadaModelPtrT
MainObjectModel::ormModel()
{
  return mSimScadaModel;
}
// ----------------------------------------------------------------------------

bool
MainObjectModel::isOrmLoaded()
{
  return !!mSimScadaModel.get();
}
// ----------------------------------------------------------------------------

bool
MainObjectModel::loadOrmModel(std::string fileName)
{
  orm::v1::Conn ormConn;
  ormConn.Database = fileName;

  // Validate database path
  if (!ormConn.Database.empty())
  {
    namespace bf = boost::filesystem2;
    bf::path dbPath(ormConn.Database);

    if (!bf::exists(dbPath) || !bf::is_regular_file(dbPath))
    {
      boost::format msg("Can not load simSCADA database %s\n"
          "file not found");
      msg % ormConn.Database;
      QMessageBox::critical(MsgBoxParent(),
          "Database Path Error",
          msg.str().c_str());

      ormConn.Database = "";
    }
    else
    {

      try
      {
        ormConn.setTypeFromDatabaseName();
      }
      catch (std::exception& e)
      {
        QMessageBox::critical(MsgBoxParent(), "Database Path Error", e.what());
        ormConn.Database = "";
      }
    }
  }

  if (ormConn.Database.empty())
  {
    mSimScadaModel.reset(new db_simscada::SimScadaModel());
  }
  else
  {
    mSimScadaModel.reset(new db_simscada::SimScadaModel(ormConn));
    db_simscada::setupModel(mSimScadaModel);
    loadModel(mSimScadaModel, "simSCADA");
  }

  notifyOfOrmModelChange();

  saveSimScadaFileName(mSimScadaModel.get() ? fileName : std::string());

  SynTestDbValidator validator(MainSynModel::Inst().ormModel(),
      MainObjectModel::Inst().ormModel());
  validator.run();

  return true;
}
// ----------------------------------------------------------------------------

// ---------------
// MainFilterObserver implentation

void
MainObjectModel::addObserver(MainObjectModelObserver* observer)
{
  mObservers.insert(observer);
}
// ----------------------------------------------------------------------------

void
MainObjectModel::removeObserver(MainObjectModelObserver* observer)
{
  mObservers.erase(observer);
}
// ----------------------------------------------------------------------------

// ---------------
// Private functions

void
MainObjectModel::notifyOfOrmModelChange()
{
  for (ObserversT::iterator itr = mObservers.begin(); //
  itr != mObservers.end(); //
      ++itr)
  {
    (*itr)->simOrmModelChanged(mSimScadaModel);
  }
}
// ----------------------------------------------------------------------------

void
MainObjectModel::saveSimScadaFileName(const std::string& filename)
{
  // Store it
  SimScadaSettings settings = Settings().simScada();
  settings.DbFilename = filename;
  Settings().setSimScada(settings);
}
// ----------------------------------------------------------------------------

