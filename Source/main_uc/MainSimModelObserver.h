/*
 * MainObjectModelObserver.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_OBJECT_MODEL_OBSERVER_H_
#define _MAIN_OBJECT_MODEL_OBSERVER_H_

#include <vector>

#include <boost/weak_ptr.hpp>

#include <simscada/DbSimscadaDeclaration.h>

class SsQtModel;

/*! Main ObjectModel Observer
 *
 */
class MainObjectModelObserver
{
  public:
    typedef std::vector<boost::weak_ptr<db_simscada::sim::Object> > SimObjectsSelectedT;

  public:
    MainObjectModelObserver();

    virtual
    ~MainObjectModelObserver();

  public:
    virtual void
    simOrmModelChanged(boost::weak_ptr<db_simscada::SimScadaModel> ormModel);

};

#endif /* _MAIN_OBJECT_MODEL_OBSERVER_H_ */
