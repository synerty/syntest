#ifndef SETTINGSUI_H
#define SETTINGSUI_H

#include <QtGui/QDialog>
#include "ui_SettingsUi.h"


class SettingsUi : public QDialog
{
  Q_OBJECT

  public:
    SettingsUi();
    virtual
    ~SettingsUi();

  public:

    void
    run();

  private:
    void
    loadSimScada();

    void
    storeSimScada();

  private:
    Ui::SettingsUiClass ui;
};

#endif // SETTINGSUI_H
