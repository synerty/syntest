/*
 * SimScadaSettings.h
 *
 *  Created on: Dec 24, 2010
 *      Author: Administrator
 */

#ifndef SIMSCADASETTINGS_H_
#define SIMSCADASETTINGS_H_

#include <string>

/*! simScadaSettings
 * Stores the settings related to simSCADA
 *
 */
struct SimScadaSettings
{
    std::string DbFilename;

    std::string Host;

    SimScadaSettings();
};

#endif /* SIMSCADASETTINGS_H_ */
