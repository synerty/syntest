/*
 * Settings.h
 *
 *  Created on: Dec 19, 2010
 *      Author: Administrator
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <string>

#include <QtCore/qsettings.h>

#include "ObjectTreeFilterSettings.h"
#include "SimScadaSettings.h"

/*! Settings Brief Description
 * Long comment about the class
 *
 */
class Settings
{
  public:
    Settings();
    virtual
    ~Settings();

  public:
    // ---------------
    // SynTEST database filename

    std::string
    synTestModelFileName() const;

    void
    setSynTestModelFileName(const std::string fileName);

  public:
    // ---------------
    // simSCADA database filename

    SimScadaSettings
    simScada();

    void
    setSimScada(const SimScadaSettings& settings);

  public:
    // ---------------
    // Object Tree ObjectModel settings
    ObjectTreeFilterSettings
    objectTreeFilter();

    void
    setObjectTreeFilter(const ObjectTreeFilterSettings& filter);

  private:
    QSettings mSettings;
};

#endif /* SETTINGS_H_ */
