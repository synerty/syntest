#include "SettingsUi.h"

#include "Globals.h"
#include "Settings.h"
#include "SimScadaSettings.h"
#include "ObjectTreeFilterSettings.h"

SettingsUi::SettingsUi() :
  QDialog(MsgBoxParent())
{
  ui.setupUi(this);

  connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}
// ----------------------------------------------------------------------------

SettingsUi::~SettingsUi()
{
}
// ----------------------------------------------------------------------------

void
SettingsUi::run()
{
  loadSimScada();

  if (exec() != Accepted)
    return;

  storeSimScada();
}
// ----------------------------------------------------------------------------

void
SettingsUi::loadSimScada()
{
  SimScadaSettings data = Settings().simScada();

  ui.txtSimScadaHost->setText(data.Host.c_str());
  ui.txtSimScadaDb->setText(data.DbFilename.c_str());

}
// ----------------------------------------------------------------------------

void
SettingsUi::storeSimScada()
{
  SimScadaSettings data;

  data.Host = ui.txtSimScadaHost->text().toStdString();
  data.DbFilename = ui.txtSimScadaDb->text().toStdString();

  Settings().setSimScada(data);
}
// ----------------------------------------------------------------------------
