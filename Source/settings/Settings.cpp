/*
 * Settings.cpp
 *
 *  Created on: Dec 19, 2010
 *      Author: Administrator
 */

#include "Settings.h"

#include <QtGui/qapplication.h>

#define SS_MODEL_DATABASE_KEY "db_simscada::SimScadaModelFileName"

#define SIM_SCADA_GROUP "SimScada"

#define OBJECT_TREE_FILTER_GROUP "ObjectTreeFilter"

Settings::Settings() :
    mSettings(QApplication::applicationFilePath().replace(".exe", ".ini"),
        QSettings::IniFormat)
{
}

Settings::~Settings()
{
}

// ---------------
// SynTEST database filename

std::string
Settings::synTestModelFileName() const
{
  return mSettings.value("SynTestModel").toString().toStdString();
}

void
Settings::setSynTestModelFileName(const std::string fileName)
{
  mSettings.setValue("SynTestModel", QString(fileName.c_str()));
}

// ---------------
// simSCADA database filename

SimScadaSettings
Settings::simScada()
{
  SimScadaSettings data;
  mSettings.beginGroup(SIM_SCADA_GROUP);

  data.DbFilename = mSettings.value("DbFilename", data.DbFilename.c_str()).toString().toStdString();

  data.Host = mSettings.value("Host", data.Host.c_str()).toString().toStdString();

  mSettings.endGroup();
  return data;
}

void
Settings::setSimScada(const SimScadaSettings& settings)
{
  mSettings.beginGroup(SIM_SCADA_GROUP);

  mSettings.setValue("DbFilename", QString(settings.DbFilename.c_str()));

  mSettings.setValue("Host", QString(settings.Host.c_str()));

  mSettings.endGroup();
}

ObjectTreeFilterSettings
Settings::objectTreeFilter()
{
  ObjectTreeFilterSettings filter;
  mSettings.beginGroup(OBJECT_TREE_FILTER_GROUP);

  filter.Filter = static_cast< ObjectTreeFilterSettings::FilterE >(mSettings.value("Filter",
      filter.Filter).toUInt());

  filter.Sort = static_cast< ObjectTreeFilterSettings::SortE >(mSettings.value("Sort",
      filter.Sort).toUInt());

  mSettings.endGroup();
  return filter;
}

void
Settings::setObjectTreeFilter(const ObjectTreeFilterSettings& filter)
{
  mSettings.beginGroup(OBJECT_TREE_FILTER_GROUP);

  mSettings.setValue("Filter", filter.Filter);

  mSettings.setValue("Sort", filter.Sort);

  mSettings.endGroup();
}
