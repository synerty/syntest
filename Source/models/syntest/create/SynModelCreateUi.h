#ifndef SYN_MODEL_CREATE_UI_H
#define SYN_MODEL_CREATE_UI_H

#include <QtGui/QDialog>
#include "ui_SynModelCreateUi.h"

#include <boost/shared_ptr.hpp>

namespace autotest
{
  namespace gen_batch
  {
    struct NewBatchConfig;
    class RtuModel;
  }
}

class SynModelCreateUi : public QDialog
{
  Q_OBJECT

  public:
    SynModelCreateUi();
    ~SynModelCreateUi();

  public:

  public slots:

    void
    initialiseClicked();

  private:
    Ui::SynModelCreateUiClass ui;
};

#endif // SYN_MODEL_CREATE_UI_H
