#include "SynModelCreateUi.h"

#include <QtGui/qmessagebox.h>

#include "SynModelCreate.h"

#include "Globals.h"

using namespace autotest::gen_batch;

/// ---------------------------------------------------------------------------
/// SynModelInitUi implementation
/// ---------------------------------------------------------------------------

SynModelCreateUi::SynModelCreateUi() :
    QDialog(MsgBoxParent())
{
  ui.setupUi(this);
  connect(ui.btnInitialise, SIGNAL(clicked()), this, SLOT(initialiseClicked()));
  connect(ui.btnCancel, SIGNAL(clicked()), this, SLOT(reject()));
}
/// ---------------------------------------------------------------------------

SynModelCreateUi::~SynModelCreateUi()
{

}

void
SynModelCreateUi::initialiseClicked()
{
    accept();
}
/// ---------------------------------------------------------------------------

