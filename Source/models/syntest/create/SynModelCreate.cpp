/*
 * SynModelInit.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "SynModelCreate.h"

#include <fstream>
#include <stdexcept>

#include <boost/format.hpp>

#include <QtGui/qmessagebox.h>
#include <QtGui/qfiledialog.h>
#include <QtGui/qapplication.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlquery.h>
#include <QtSql/qsqlerror.h>

#include "Globals.h"
#include "MainSynModel.h"

#define SYNTEST_DB_SCHEMA_FILE "SynTestDbSchema.sql"

SynModelCreate::SynModelCreate()
{
}
/// ---------------------------------------------------------------------------

SynModelCreate::~SynModelCreate()
{
}
/// ---------------------------------------------------------------------------

void
SynModelCreate::run(std::string fileName)
{
  try
  {
//    SynModelInitUi ui;
//    int result = ui.exec();
//
//    if (result == QDialog::Rejected)
//      return;

    std::string syntestDbSchemaFile = (QApplication::applicationDirPath()
        + QDir::separator() + SYNTEST_DB_SCHEMA_FILE).toStdString();

    std::string schemaSql;
    try
    {

      std::ifstream ifs(syntestDbSchemaFile.c_str());
      schemaSql = std::string((std::istreambuf_iterator< char >(ifs)),
          std::istreambuf_iterator< char >());
    }
    catch (std::exception& e)
    {
      boost::format info("Unable to create new SynTEST database\n"
          "Template file location is '%s'\n"
          "%s");
      info % syntestDbSchemaFile;
      info % e.what();
      QMessageBox::critical(MsgBoxParent(),
          "Failed To Load Schema Template File",
          info.str().c_str());

      return;
    }

    QString newFileName;

    if (!fileName.empty())
    {
      newFileName = QString(fileName.c_str());
    }
    else
    {
      newFileName = QFileDialog::getSaveFileName(MsgBoxParent(),
          "New Database",
          QApplication::applicationDirPath(),
          "Sqlite Database (*.sqlite);;");
    }

    if (newFileName.isEmpty())
      return;

    // Delete the file if it exists
    if (QFile::exists(newFileName))
      QFile::remove(newFileName);

    const QString dbName("DbForCreate");
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", dbName);
    db.setDatabaseName(newFileName);

    if (!db.open())
    {
      boost::format info("Can not create database %s");
      info % newFileName.toStdString();
      throw std::runtime_error(info.str());
    }

    QStringList statements = QString(schemaSql.c_str()).split(';');
    for (QStringList::iterator itr = statements.begin();
        itr != statements.end(); ++itr)
    {
      QString sql = itr->trimmed();
      if (sql.isEmpty())
        continue;

      QSqlQuery qry(db);
      if (!qry.exec(sql) || qry.lastError().isValid())
      {
        boost::format info("Failed to execute schema creation SQL\n%s\n%s");
        info % qry.lastError().text().toStdString();
        info % sql.toStdString();
        throw std::runtime_error(info.str());
      }
      qry.finish();
    }

    MainSynModel::Inst().loadOrmModel(newFileName.toStdString());

    QMessageBox::information(MsgBoxParent(),
        "Creation Successful",
        "SynTEST database has been successfully created.");
  }
  catch (std::exception& e)
  {
    boost::format info("An exception occurred during database creation\n%s");
    info % e.what();
    QMessageBox::critical(MsgBoxParent(),
        "SynTEST Database Create Failed",
        info.str().c_str());

  }
}
/// ---------------------------------------------------------------------------
