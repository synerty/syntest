/*
 * SynModelInit.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef SYN_MODEL_CREATE_H_
#define SYN_MODEL_CREATE_H_

#include <string>
#include <map>

#include <simscada/DbSimscadaDeclaration.h>
#include <syntest/DbSyntestDeclaration.h>

#include <enmac/DbEnmacDeclaration.h>

//namespace autotest
//{
//
//  namespace gen_batch
//  {
//    struct NewBatchConfig
//    {
//        std::string name;
//        std::string comment;
//        db_simscada::sim::RtuListT rtus;
//    };
//  }
//}

/*! SynModelInit Brief Description
 * Long comment about the class
 *
 */
class SynModelCreate
{
  public:

  public:
    SynModelCreate();
    virtual
    ~SynModelCreate();

  public:
    void
    run(std::string fileName = std::string());

  private:


};

#endif /* SYN_MODEL_CREATE_H_ */
