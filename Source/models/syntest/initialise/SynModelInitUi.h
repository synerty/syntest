#ifndef SYN_MODEL_INITUI_H
#define SYN_MODEL_INITUI_H

#include <QtGui/QDialog>
#include "ui_SynModelInitUi.h"

#include <boost/shared_ptr.hpp>

namespace autotest
{
  namespace gen_batch
  {
    struct NewBatchConfig;
    class RtuModel;
  }
}

class SynModelInitUi : public QDialog
{
  Q_OBJECT

  public:
    SynModelInitUi();
    ~SynModelInitUi();

  public:

  public slots:

    void
    initialiseClicked();

  private:
    Ui::SynModelInitUiClass ui;
};

#endif // SYN_MODEL_INITUI_H
