/*
 * SynModelInit.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "SynModelInit.h"

#include <stdexcept>
#include <math.h>
#include <vector>
#include <map>

#include <boost/format.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <QtGui/qmessagebox.h>
#include <QtCore/qstring.h>

#include <orm/v1/Util.h>

#include <syntest/SynTestModel.h>
#include <syntest/st/StSimPoint.h>
#include <syntest/st/StSystem.h>
#include <syntest/st/StSysPoint.h>
#include <syntest/st/StTestBatch.h>
#include <syntest/st/StTestPoint.h>
#include <syntest/st/StTestValue.h>
#include <syntest/st/StTestSystem.h>
#include <syntest_util/DbSyntestUtil.h>

#include <simscada/SimScadaModel.h>
#include <simscada/sim/SimObject.h>
#include <simscada/sim/SimRtu.h>
#include <simscada/sim/SimTelePt.h>
#include <simscada/sim/SimDigPt.h>
#include <simscada/sim/SimDigDeviceType.h>
#include <simscada/sim/SimDigClass.h>
#include <simscada/sim/SimAnaPt.h>
#include <simscada/sim/SimCtrlPt.h>
#include <simscada/sim/SimCtrlLink.h>
#include <simscada_util/DbSimscadaUtil.h>

#include <enmac/rt/RtControlPointComponent.h>
#include <enmac/EnmacModel.h>

#include <orm/v1/Conn.h>

#include "MainSimModel.h"
#include "MainSynModel.h"
#include "Macros.h"
#include "Globals.h"

#include "SynModelInitUi.h"
#include "OrmModelThread.h"

// To get the DUAL_ALIAS constant
#include "PlantModel.h"

using namespace db_syntest;
using namespace db_syntest::st;
using namespace db_simscada;
using namespace db_simscada::sim;

SynModelInit::SynModelInit() :
  mSimModel(MainObjectModel::Inst().ormModel()) //
      , mSynModel(MainSynModel::Inst().ormModel())
{
}
/// ---------------------------------------------------------------------------

SynModelInit::~SynModelInit()
{
}
/// ---------------------------------------------------------------------------

void
SynModelInit::run()
{
  try
  {
    SynModelInitUi ui;
    int result = ui.exec();

    if (result == QDialog::Rejected)
      return;

    orm::v1::Conn conn;
    conn.Type = orm::v1::Conn::Oracle;
    conn.Host = "192.168.10.220";
    conn.Port = 1521;
    conn.Username = "enmac";
    conn.Password = "Grim5Payday";
    conn.Database = "NMS";

    mEnmacModel.reset(new db_enmac::EnmacModel(conn));

    // DISABLED - We don't want to connect to PoF
    // loadModel(mEnmacModel, "PowerOn Fusion");
    // createEnmacCtrlPntMap();

    initialiseDatabase();
  }
  catch (std::exception& e)
  {
    boost::format
        info("An exception occurred during database initialisation\n%s");
    info % e.what();
    QMessageBox::critical(MsgBoxParent(),
        "SynTEST Database Initialisation Failed",
        info.str().c_str());

  }
}
/// ---------------------------------------------------------------------------

void
SynModelInit::initialiseDatabase()
{
  mSynModel->beginTransaction();

  addSimPoints();
  addSystems();

  mSynModel->save();
  mSynModel->commitTransaction();
}
/// ---------------------------------------------------------------------------

void
SynModelInit::addSimPoints()
{
  const TelePtListT& telePts = mSimModel->SimTelePts();
  for (TelePtListT::const_iterator itr = telePts.begin(); itr != telePts.end(); ++itr)
  {
    TelePtPtrT telePt = *itr;

    SimPointTypeE type;

    switch (toObjectType(telePt))
    {
      case DigitalObject:
      {
        const std::string
            str2Bit = DigPt::downcast(telePt)->deviceType()->doubleBit();
        if (str2Bit.empty())
          throw std::runtime_error("DigDeviceType.DoubleBit is empty or null");

        char chr2Bit = str2Bit[0];
        chr2Bit = std::tolower(chr2Bit);

        bool is2Bit = !(chr2Bit == '0' || chr2Bit == 'f' || chr2Bit == 'n');

        if (is2Bit)
          type = di2SimPointType;
        else
          type = di1SimPointType;

        break;
      }

      case AnalogueObject:
      {
        type = aiSimPointType;
        break;
      }

      case AccumulatorObject:
      {
        type = acSimPointType;
        break;
      }

      case ControlObject:
      {
        switch (toCtrlType(CtrlPt::downcast(telePt)))
        {
          case digitalCtrlType:
            type = doSimPointType;
            break;

          case setpointCtrlType:
            type = aoSimPointType;
            break;

          case raiseLowerCtrlType:
            type = doSimPointType;
            break;

          THROW_UNHANDLED_CASE_EXCEPTION
        }
        break;

        THROW_UNHANDLED_CASE_EXCEPTION
      }
    }

    SimPointPtrT simPoint(new SimPoint());
    simPoint->setSimAlias(telePt->alias());
    simPoint->setType(toSimPointType(type));
    simPoint->setFeedbackTimeout(5);
    simPoint->addToModel(mSynModel);

  }
}
/// ---------------------------------------------------------------------------

void
SynModelInit::addSystems()
{
  //  typedef std::vector< std::string > VecStrT;
  //  VecStrT names;
  //  names.push_back("PowerOn Fusion");
  //  names.push_back("Legacy");
  //
  //  for (VecStrT::const_iterator itr = names.begin(); itr != names.end(); ++itr)
  //  {
  //    SystemPtrT sys(new System());
  //    sys->setName(*itr);
  //    sys->setAgentIp("127.0.0.1");
  //    sys->setAgentPort(25600);
  //    sys->setSimulatorIp("127.0.0.1");
  //    sys->setAgentPort(25600);
  //    sys->addToModel(mSynModel);
  //
  //    addSystemPoints(sys);
  //  }

  //  {
  //    SystemPtrT sys(new System());
  //    sys->setName("prodapp3");
  //    sys->setAgentIp("192.168.10.50");
  //    sys->setAgentPort(25600);
  //    sys->setSimulatorIp("192.168.10.34");
  //    sys->setSimulatorPort(25610);
  //    sys->addToModel(mSynModel);
  //    addSystemPoints(sys, false);
  //  }
  //
  //  {
  //    SystemPtrT sys(new System());
  //    sys->setName("PowerOn Fusion Dev");
  //    sys->setAgentIp("10.211.55.39");
  //    sys->setAgentPort(25600);
  //    sys->setSimulatorIp("127.0.0.1");
  //    sys->setSimulatorPort(25610);
  //    sys->addToModel(mSynModel);
  //    addSystemPoints(sys, false);
  //  }

  {
    SystemPtrT sys(new System());
    sys->setName("PowerOn Fusion");
    // sys->setAgentIp("10.211.55.39");
    // sys->setAgentPort(25600);
    sys->setSimulatorIp("127.0.0.1");
    sys->setSimulatorPort(25610);
    sys->addToModel(mSynModel);
    addSystemPoints(sys, true);
  }

  {
    SystemPtrT sys(new System());
    sys->setName("Legacy SCADA System");
    //    sys->setAgentIp("10.211.55.3");
    //    sys->setAgentPort(25600);
    sys->setSimulatorIp("127.0.0.1");
    sys->setSimulatorPort(25610);
    sys->setSimulatorAliasPostfix(DUAL_ALIAS_TAG);
    sys->addToModel(mSynModel);
    addSystemPoints(sys, true);
  }
}
/// ---------------------------------------------------------------------------

void
SynModelInit::addSystemPoints(SystemPtrT sys, bool requiresUser)
{

  const SimPointListT& simPoints = mSynModel->StSimPoints();
  for (SimPointListT::const_iterator itr = simPoints.begin(); itr
      != simPoints.end(); ++itr)
  {
    SimPointPtrT simPoint = *itr;

    sim::ObjectPtrT simObject = mSimModel->SimObject(simPoint->simAlias());
    assert(simObject.get());

    std::string compAlias = simObject->externalId();
    if (compAlias.empty())
      compAlias = simObject->alias();

    // Just do the normal points.
    if (boost::algorithm::icontains(compAlias, DUAL_ALIAS_TAG))
      continue;

    // Ignore dummy block length points
    if (boost::algorithm::icontains(simObject->name(), "BLOCK LENGTH"))
      continue;

    // Create the syspoint
    SysPointPtrT sysPoint(new SysPoint());
    sysPoint->setId(sys->name() + "-" + simPoint->simAlias());
    sysPoint->setPid(compAlias);
    sysPoint->setName(simObject->name());
    sysPoint->setSystem(sys);
    sysPoint->setSimPoint(simPoint);
    sysPoint->setRequiresUser(requiresUser);

    if (requiresUser)
    {
      sysPoint->addToModel(mSynModel);
      continue;
    }

    // Fill in the alias
    if (toObjectType(simObject) == db_simscada::AnalogueObject)
    {
      // End of scan points stuff
      sysPoint->setSup1("Eng Value");
      sysPoint->addToModel(mSynModel);
      continue;
    }

    // Fill in the alias
    if (toObjectType(simObject) == db_simscada::AccumulatorObject)
    {
      // TODO Accumulators
      //      // End of scan points stuff
      //      sysPoint->setSup1("Eng Value");
      //      sysPoint->addToModel(mSynModel);
      continue;
    }

    if (toObjectType(simObject) == db_simscada::DigitalObject)
    {
      // End of scan points stuff
      sysPoint->setSup1("Tele Scan Value");
      sysPoint->setSup2("State Text");
      sysPoint->addToModel(mSynModel);
      continue;
    }

    // Default
    sysPoint->setSup1("ERROR");

    CtrlPtPtrT ctrlPt = CtrlPt::downcast(simObject);
    CtrlLinkPtrT ctrlLink = mSimModel->SimCtrlLink(ctrlPt->alias());

    std::string toStateStr;

    if (ctrlLink.get() != NULL)
    {
      // Assign the scan point alias
      compAlias = ctrlLink->scanPoint()->externalId();

      // For digital points, get the attribute name
      // No case for AOs at the moment
      if (toObjectType(ctrlLink->scanPoint()) == DigitalObject)
      {

        DigPtPtrT digPt = DigPt::downcast(ctrlLink->scanPoint());

        if (ctrlLink->stateTo() == "A")
        {
          int toState = digPt->deviceType()->stateA();
          toStateStr = boost::lexical_cast< std::string >(toState);
        }
        else if (ctrlLink->stateTo() == "B")
        {
          int toState = digPt->deviceType()->stateB();
          toStateStr = boost::lexical_cast< std::string >(toState);
        }
        else
          assert(false); // Unhandled state
      }
    }

    CtrlPntCompByValueByAliasMapT::iterator byCompByValItr;
    byCompByValItr = mCtrlPntCompByValueByAlias.find(compAlias);
    if (byCompByValItr != mCtrlPntCompByValueByAlias.end())
    {
      // Make this work for controls in simSCADA that don't have control links
      CtrlPntCompByValueMapT::iterator byValItr = byCompByValItr->second.end();
      if (toStateStr.empty())
        byValItr = byCompByValItr->second.begin();
      else
        byValItr = byCompByValItr->second.find(toStateStr);

      if (byValItr != byCompByValItr->second.end())
      {
        sysPoint->setPid(byValItr->second->componentAlias());
        sysPoint->setSup1(byValItr->second->controlAttributeName());
        sysPoint->setSup2(byValItr->second->controlValue());
      }
    }

    sysPoint->addToModel(mSynModel);

  }
}
/// ---------------------------------------------------------------------------

void
SynModelInit::createEnmacCtrlPntMap()
{

  // Create a map of the back indications
  typedef db_enmac::rt::ControlPointComponent CpcT;
  CpcT::ListT list = mEnmacModel->RtControlPointComponents();
  for (CpcT::ListT::iterator itr = list.begin(); itr != list.end(); ++itr)
  {
    CpcT::PtrT cpc = *itr;
    mCtrlPntCompByValueByAlias[cpc->componentAlias()][cpc->controlValue()] = cpc;
  }
}
/// ---------------------------------------------------------------------------
