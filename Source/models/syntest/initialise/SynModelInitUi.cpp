#include "SynModelInitUi.h"

#include <QtGui/qmessagebox.h>

#include "SynModelInit.h"

#include "Globals.h"

using namespace autotest::gen_batch;

/// ---------------------------------------------------------------------------
/// SynModelInitUi implementation
/// ---------------------------------------------------------------------------

SynModelInitUi::SynModelInitUi() :
    QDialog(MsgBoxParent())
{
  ui.setupUi(this);
  connect(ui.btnInitialise, SIGNAL(clicked()), this, SLOT(initialiseClicked()));
  connect(ui.btnCancel, SIGNAL(clicked()), this, SLOT(reject()));
}
/// ---------------------------------------------------------------------------

SynModelInitUi::~SynModelInitUi()
{

}

void
SynModelInitUi::initialiseClicked()
{
    accept();
}
/// ---------------------------------------------------------------------------

