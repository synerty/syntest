/*
 * SynModelInit.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef SYN_MODEL_INIT_H_
#define SYN_MODEL_INIT_H_

#include <string>
#include <map>

#include <simscada/DbSimscadaDeclaration.h>
#include <syntest/DbSyntestDeclaration.h>

#include <enmac/DbEnmacDeclaration.h>

//namespace autotest
//{
//
//  namespace gen_batch
//  {
//    struct NewBatchConfig
//    {
//        std::string name;
//        std::string comment;
//        db_simscada::sim::RtuListT rtus;
//    };
//  }
//}

/*! SynModelInit Brief Description
 * Long comment about the class
 *
 */
class SynModelInit
{
  public:

  public:
    SynModelInit();
    virtual
    ~SynModelInit();

  public:
    void
    run();

  private:

    void
    initialiseDatabase();

    void
    addSimPoints();

    void
    addSystems();

    void
    addSystemPoints(db_syntest::st::SystemPtrT sys, bool requiresUser);

    void
    createEnmacCtrlPntMap();

    db_simscada::SimScadaModelPtrT mSimModel;
    db_syntest::SynTestModelPtrT mSynModel;
    db_enmac::EnmacModelPtrT mEnmacModel;

    typedef std::map< std::string, db_enmac::rt::ControlPointComponentPtrT > CtrlPntCompByValueMapT;
    typedef std::map< std::string, CtrlPntCompByValueMapT > CtrlPntCompByValueByAliasMapT;

    CtrlPntCompByValueByAliasMapT  mCtrlPntCompByValueByAlias;

};

#endif /* SYN_MODEL_INIT_H_ */
