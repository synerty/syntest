/*
 * MainAutoTestObserver.h
 *
 *  Created on: Dec 21, 2010
 *      Author: Administrator
 */

#ifndef _MAIN_OBSERVER_H_
#define _MAIN_OBSERVER_H_

#include "MainDeclarations.h"

#include <boost/shared_ptr.hpp>

/*! Main Observer
 *
 */
class MainObserver
{
  public:
    MainObserver();

    virtual
    ~MainObserver();

  public:
    virtual void
    mainUiModeChanged(main_ui::UiModeE oldUiMode, main_ui::UiModeE newUiMode);
};

#endif /* _MAIN_OBSERVER_H_ */
