/*
 * MainUiCenter.h
 *
 *  Copyright Synerty Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  Synerty Pty Ltd
 */

#ifndef MAINUICENTER_H_
#define MAINUICENTER_H_

/*! MainUiCenter Brief Description
 * Long comment about the class
 *
 */
class MainUiCenter
{
  public:
    MainUiCenter();
    virtual
    ~MainUiCenter();

  public:
    virtual
    void
    mainUiCenterClose() = 0;
};

#endif /* MAINUICENTER_H_ */
