#
# =============================================================================
# Version History 
#
# <version>	<yymmdd_hhmm>	<Name>
#	<comment ....>
# =============================================================================
#
# 0017		110704_2052		Jarrod Chesney
#	Added -p to copy commands
# 0016		110201_1551		Jarrod Chesney
#	* Fixed bug in project modify checking code
# 0015		110129_1410		Jarrod Chesney
#	* Improved code for copying over gdb.exe and dlls required by gdb.exe
# 0014		110118_1131		Jarrod Chesney
#	* Added file modify time comparison to skip building 
#		if there are no changes.
# 0013		110114_2147		Jarrod Chesney
#	* Minor tweeks to allow building for 64bit linux. 
# 0012		110106_1053		Jarrod Chesney
#	* Added sed command to add prefix to windres command in 
#		function qmake_makefile
# 0011		101226_1756		Jarrod Chesney
#	* Added filter to remove excess boost template instantiated from 
#		information.
# 0010		100609_1208		Jarrod Chesney
#	* Fixed some drive letter issues that happen when the project is on a
#		different drive to the devenv root.
# 0009		100531_1342		Jarrod Chesney
#	* Fixed make command so time is displayed at the end of the process
# 0008		100530_1237		Jarrod Chesney
#	* Forced build type for Qt to be release.
#		This is due to an issue in Qt, The debug build always links to the 
#		release versions of hte DLLs.
# 0007		100530_1140		Jarrod Chesney
#	* Removed requirement for :
#		DEVENV_BOOST_VERSION
#		DEVENV_WX_VERSION
# 0006		100527_2241		Jarrod Chesney
#	* Fixed return code from make.
# 0005		100505_1344		Jarrod Chesney
#	* Removed gdb.bat code, replaced with a copy of gdb.exe to output
#		directory.
# 0004		100503_1110		Jarrod Chesney
#	* Changed path of libs to be build_type/link_type
#		instead of just link_type
#	* Added code to copy the DLLs to the output dir if required
# 0003		100330_2304		Jarrod Chesney
#	* Added option to always rebuild the makefile
# 0002		100318_2010		Jarrod Chesney
#	* Extracted tasks into functions
#	* Made qmake not get called unless "make.sh qmake" was called or the build
#		or the build directory doesn't exist. This will speed things up a bit
#		but will most likely cause problems when adding files and forgetting to
#		run qmake - still unsure about this decision.
# 0001		100226_2010		Jarrod Chesney
#	Initial Writing
#
# =============================================================================

# This script will execute qmake in project mode
# then execute qmake in makefile mode
# and then run make.

# =============================================================================
# Check Environment
# Check that the environment variables are set
function check_environment {
	# Make sure the environment is set
	echo "Using environment"
	echo "DEVENV_BUILD_DIR         =${DEVENV_BUILD_DIR:?'[${ConfigName}]	The name of the directory under Output where this build configuration will place files'}"
	echo "DEVENV_BUILD_TARGET      =${DEVENV_BUILD_TARGET:?'[i686-w64-mingw32]	The target platform of the binary you want to generate.'}"
	echo "DEVENV_BUILD_TYPE        =${DEVENV_BUILD_TYPE:?'[debug]	debug or release?'}"
	echo "DEVENV_LINK_TYPE         =${DEVENV_LINK_TYPE:?'[shared]	shared or static? (where possible)'}"
	echo "DEVENV_MAKE_JOBS         =${DEVENV_MAKE_JOBS:?'[3]	Number of jobs to execute concurrently when compiling.'}"
	echo "DEVENV_PROJECT           =${DEVENV_PROJECT:?'[${ProjName}]	The name of the project, used for the output binary filename.'}"
	echo "DEVENV_ROOT              =${DEVENV_ROOT:?'[c:\devenv]	The root directory where all of the libs, compilers, and build tools live'}"
	echo "DEVENV_USE_DISTCC        =${DEVENV_USE_DISTCC:?'[0]	Enable distcc 0/1'}"
	echo "DEVENV_ALWAYS_REBUILD_MAKEFILE=${DEVENV_ALWAYS_REBUILD_MAKEFILE:?'[0]	Enable always rebuild the makefile 0/1'}"
	echo "DISTCC_HOSTS             =${DISTCC_HOSTS:?'[]	space separated list of host names participating in the distcc cluster'}"
	echo "DEVENV_GCC_VERSION       =${DEVENV_GCC_VERSION:?'[4.4.4]	The version of GCC to build with'}"
	echo "DEVENV_QT_VERSION        =${DEVENV_QT_VERSION:?'[4.6.2.0]	The version of the QT lib being used'}"
}

# =============================================================================
# Create qmake project file
# Uses qmake to create a project file containing all the files in the project
# This is later included by build.pro to make a makefile
function qmake_project {
	echo ' * Generating the list of files to include in the compile'
	cd ${MAKE_SH_PROJECT_PATH}/Output/$DEVENV_BUILD_DIR

	qmake -project -recursive -o qm.pri ../../Source
}

# Create makefile with qmake
# Creates a makefile for this build with qmake using the project files
function qmake_makefile {
	echo ' * Generating the makefiles from the pro file'
	cd ${MAKE_SH_PROJECT_PATH}/Output/$DEVENV_BUILD_DIR
	
	if [[ ${DEVENV_BUILD_TARGET:?} == "*-pc-cygwin" ]]; then
		export QMAKESPEC="win32-g++"
	fi
	
	qmake -unix -recursive ../../build/qm.pro
	
	if [ ${MAKE_SH_BUILD_HOST_TYPE:?} == "i686-pc-cygwin" ]; then
		# Fix the remainder of the issues in the makefile
		# Fix the drive letter for the project path
		DRIVE_LETTER_L=`echo $MAKE_SH_PROJECT_PATH | sed 's/\/cygdrive\///g' | cut -c1 | tr [:upper:] [:lower:]`
		DRIVE_LETTER_U=`echo $DRIVE_LETTER_L | tr [:lower:] [:upper:]`
		sed -i "s/[${DRIVE_LETTER_L}${DRIVE_LETTER_U}]\:\//\/cygdrive\/${DRIVE_LETTER_L}\//g" Makefile*
		
		# Fix the drive letter for the drive the devenv root is on
		DRIVE_LETTER_L=`echo $DEVENV_ROOT | sed 's/\/cygdrive\///g' | cut -c1 | tr [:upper:] [:lower:]`
		DRIVE_LETTER_U=`echo $DRIVE_LETTER_L | tr [:lower:] [:upper:]`
		sed -i "s/[${DRIVE_LETTER_L}${DRIVE_LETTER_U}]\:\//\/cygdrive\/${DRIVE_LETTER_L}\//g" Makefile*

		sed -i "s/xcopy \/s \/q \/y \/i/cp -r/g" Makefile*
		sed -i "s/rmdir/rm -r/g" Makefile*
		
		# Add the prefix to the windres command
		sed -i "s/\twindres /\t${DEVENV_BUILD_TARGET}-windres /g" Makefile*
	fi
	
}

# =============================================================================
# Copy toolchain files to output directory
# This function will copy the reqiuired binaries from the toolchains into the
# output directory.

function copy_toolchain_binaries {

	if [ ${MAKE_SH_BUILD_HOST_TYPE} == "i686-pc-cygwin" ]; then
		echo ' * Copying over toolchain DLLs'
		cp -puv ${MAKE_SH_TOOLCHAIN}/bin/libgcc_s_sjlj-1.dll ${MAKE_SH_OUTPUT_DIR}
		
		if [ ${DEVENV_BUILD_TYPE:?} == "debug" ]; then
			echo ' * Copying over debugger and debugger DLLs'
			for file in gdb.exe libcharset-1.dll libexpat-1.dll libiconv-2.dll
			do
				file_path=${MAKE_SH_TOOLCHAIN}/bin/${file}
				if [ -f ${file_path} ]; then
					cp -puv ${file_path} ${MAKE_SH_OUTPUT_DIR}
				fi
			done
		fi
		
	fi
	
}

# =============================================================================
# Make clean
# Clean the build directory for this build profile
function make_clean {
	echo ' * Cleaning Build'
	cd ${MAKE_SH_PROJECT_PATH}
	if [ -d "Output/$DEVENV_BUILD_DIR" ]; then	
		rm -rf Output/$DEVENV_BUILD_DIR
	fi

}

# =============================================================================
# File Change Check
# Check if files in the project have changed
function file_change_check {

	echo ' * Checking for changes'
	
	# Make shore the old file list exists so we can compare to it
	if [ ! -f "${OLD_FILE_LIST}" ]; then
		touch ${OLD_FILE_LIST}
	fi
	
	# Create a new file list
	find ${MAKE_SH_PROJECT_PATH} -ls \
		| grep -v "${MAKE_SH_PROJECT_PATH}/Output/" \
		> ${NEW_FILE_LIST}
	
	# Perform the check
	if [ -z "`diff -q ${NEW_FILE_LIST} ${OLD_FILE_LIST}`" ]; then
		echo ' * No changes found, not rebuilding'
		exit 0
	else
		echo ' * Changes found, rebuild required'
	fi

}

# =============================================================================
# File Change Check Complete
# This function is called when the build is complete
function file_change_check_complete {
	if [ "${MAKE_SH_MAKE_TARGET}" = "all" ]; then
		mv ${NEW_FILE_LIST} ${OLD_FILE_LIST}
	fi
}

# =============================================================================
# Setup environment

# Call check environment function
check_environment

export PATH=/usr/local/bin:/usr/bin:/bin

# Setup make target variable
MAKE_SH_MAKE_TARGET="${1}"

if [ `uname -o` == "Cygwin" ]; then
	MAKE_SH_BUILD_HOST_TYPE="i686-pc-cygwin"
elif [ `uname -o` == "GNU/Linux" ]; then
	MAKE_SH_BUILD_HOST_TYPE=${DEVENV_BUILD_TARGET}
else
	echo "ERROR unknown operating system"
fi

# qmake won't care that this is in cygwin format
export MAKE_SH_PROJECT_PATH=`pwd`

if [ ${MAKE_SH_BUILD_HOST_TYPE:?} == "i686-pc-cygwin" ]; then
	export MAKE_SH_PROJECT_PATH=`cygpath ${MAKE_SH_PROJECT_PATH:?}`
fi

# If this is cygwin we will have been passed windows paths.
# Convert the windows paths to unix paths
if [ ${MAKE_SH_BUILD_HOST_TYPE} = "i686-pc-cygwin" ]; then
	export DEVENV_ROOT_WIN="${DEVENV_ROOT:?}"
	export DEVENV_BUILD_DIR_WIN="${DEVENV_BUILD_DIR:?}"

	export DEVENV_ROOT=`cygpath ${DEVENV_ROOT_WIN:?}`
	export DEVENV_BUILD_DIR=`cygpath ${DEVENV_BUILD_DIR_WIN:?}`
fi

export MAKE_SH_OUTPUT_DIR=${MAKE_SH_PROJECT_PATH}/Output/${DEVENV_BUILD_DIR}

# Get our build tools and libs directory for this target and version
export MAKE_SH_DEVENV_DIR="${DEVENV_ROOT}/${DEVENV_BUILD_TARGET:?}/${DEVENV_GCC_VERSION:?}"
echo " * Using build tools directory ${MAKE_SH_DEVENV_DIR}"

# Get our toolchain path
export MAKE_SH_TOOLCHAIN="${MAKE_SH_DEVENV_DIR:?}/toolchain/${MAKE_SH_BUILD_HOST_TYPE}"
echo " * Using toolchain ${MAKE_SH_TOOLCHAIN}"

# Get our qt path
export MAKE_SH_QTDIR="${MAKE_SH_DEVENV_DIR:?}/qt/${DEVENV_QT_VERSION:?}/release/${DEVENV_LINK_TYPE:?}"
echo " * Using QTDIR ${MAKE_SH_QTDIR}"
export QTDIR=${MAKE_SH_QTDIR}

# Setup the file change filenames
NEW_FILE_LIST="${MAKE_SH_OUTPUT_DIR}/file_list.txt"
OLD_FILE_LIST="${NEW_FILE_LIST}.old"

echo ' * Set path to include the toolchain and qt'
export PATH=$MAKE_SH_TOOLCHAIN/bin:$MAKE_SH_QTDIR/bin:$MAKE_SH_QTDIR/lib:${PATH}

# =============================================================================
# Begin procedure code
export PATH=/cygdrive/c/devenv/i686-w64-mingw32/pre-compiled/postgresql_libpg/8.4.2/shared/lib:$PATH
mkdir -p ${MAKE_SH_OUTPUT_DIR}
	
if [ "${MAKE_SH_MAKE_TARGET}" == "clean" ]; then
	# Call make clean function
	make_clean
	echo " * CLEAN COMPLETED SUCCESSFULLY"
	exit 0
fi

# Call file change check function
file_change_check

if [ "${MAKE_SH_MAKE_TARGET}" == "qmake" -o \
		! -f ${MAKE_SH_PROJECT_PATH}/Output/${DEVENV_BUILD_DIR}/Makefile -o \
		${DEVENV_ALWAYS_REBUILD_MAKEFILE} -eq 1 ]; then
	
	# Call functions
	qmake_project
	qmake_makefile
	
	# Copy the gcc dll into the output directory
	copy_toolchain_binaries

	echo " * QMAKE GENERATE COMPLETED SUCCESSFULLY"
	if [ "${MAKE_SH_MAKE_TARGET}" == "qmake" -a ${DEVENV_ALWAYS_REBUILD_MAKEFILE} -eq 0 ]; then
		exit 0
	fi
fi

echo ' * Finally building the project'
cd ${MAKE_SH_PROJECT_PATH}/Output/$DEVENV_BUILD_DIR
set | sort > make.log

# Some trickery to get times results separate from makes stdout/stderr
# Setup redirections
exec 3>&1

# Execute make
# Times results will goto a file called TIME_RESULTS
# Makes results will goto stdout
# Command status will be stored in EXIT_STATUS
EXIT_STATUS=$( { \
    time make -j${DEVENV_MAKE_JOBS:?} ${1} 2>&1 \
      | sed 's/’//g;s/‘//g' \
      | tee -a make.log \
      | sed -e '/\/include\/boost\/.*instantiated from/d' \
      | sed -e '/\/include\/boost\/.* warning/d' \
      | sed -e 's/std::basic_string<char, std::char_traits<char>, std::allocator<char> >/std::string/g' \
      1>&3; \
      echo $PIPESTATUS 1>&4; \
  } 2> TIME_RESULTS 4>&1 )

# Reset redirections
exec 3>&-

echo
if [ ${EXIT_STATUS} -eq 0 ]; then
	# Call the completion function for the file change check
	file_change_check_complete
	
	echo " * Build Completed Successfully, Exit=${EXIT_STATUS}"
else
	echo " * BUILD FAILED, Exit=${EXIT_STATUS}"
fi

echo
cat TIME_RESULTS && rm TIME_RESULTS

exit ${EXIT_STATUS}