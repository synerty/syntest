# QMake project file for boost libs
# This project file will add the configuration to the project to include
# boost support.
#
# Add the boost components you want included into the BOOST variable
#
# =============================================================================
# Version History 
#
# <version>	<yymmdd_hhmm>	<Name>
#	<comment ....>
# =============================================================================
#
# 0006		110704_2052		Jarrod Chesney
#	Added -p to copy commands
# 0005		110115_1031		Jarrod Chesney
#	* Added config for static build (tested under linux)
# 0004		110105_0850		Jarrod Chesney
#	* Fixed up lib names for 1.45.0
# 0003		101009_0113		Jarrod Chesney
#	* Fixed up regexp for boost version for lib names - again.
# 0002		100723_2322		Jarrod Chesney
#	* Fixed up regexp for boost version for lib names
# 0001		100504_1209		Jarrod Chesney
#	* Initial Writing
#
# =============================================================================

# Verify our environment, additional to that verified in qm.pro
DEVENV_BOOST_VERSION = $$(DEVENV_BOOST_VERSION)

isEmpty(DEVENV_BOOST_VERSION) {
	error(DEVENV_BOOST_VERSION has not been defined in the build environment)
}

# Include the boost libs
# BOOST_DIR = $$(MAKE_SH_DEVENV_DIR)/boost/$$(DEVENV_BOOST_VERSION)/$$(DEVENV_BUILD_TYPE)/$$(DEVENV_LINK_TYPE)
BOOST_DIR = $$(MAKE_SH_DEVENV_DIR)/boost/$$(DEVENV_BOOST_VERSION)/release/$$(DEVENV_LINK_TYPE)
BOOST_LIB_DIR = $${BOOST_DIR}/lib
BOOST_INCLUDE_DIR = $${BOOST_DIR}/include

# Get our boost version string, replace . with _
VER_STR = $${DEVENV_BOOST_VERSION}
VER_STR ~= s/\.[0-9]*$//
VER_STR ~= s/\./_/

# Not setup for static yet
# contains(DEVENV_LINK_TYPE, static) {
# 	contains(DEVENV_BUILD_TYPE, debug) {
# 		for(lib, BOOST) {
# 			LIBS += $${BOOST_LIB_DIR}/boost_iostreams-mgw44-mt-d-1_42.lib
# 		}
# 	}
# 	contains(DEVENV_BUILD_TYPE, release) {
# 		for(lib, BOOST) {
# 			LIBS += $${BOOST_LIB_DIR}/libwx_msw_$${lib}-2.8-$${DEVENV_BUILD_TARGET}.lib
# 		}
# 	}
# }

contains(DEVENV_LINK_TYPE, static) {
	for(lib, BOOST) {
		# Add the lib
		# Currently tested on linux
		LIBS += $${BOOST_LIB_DIR}/libboost_$${lib}.a
	}
} else {
	# Currently tested on windows
	contains(DEVENV_LINK_TYPE, shared) {
	
		contains(DEVENV_BUILD_TYPE, debug) {
			# VER_STR = d-$${VER_STR}
		}
		
		# Add the libs to the linker args, theses are import libs
		# and copy the dll into the build directory
		for(lib, BOOST) {
			# 1.42.0 has a different naming for the libs, 1.45.0 has an improved naming
			# standard
			contains(DEVENV_BOOST_VERSION, 1.42.0) {
				file = boost_$${lib}-mgw44-mt-$${VER_STR}
			
				contains(lib, regex) {
					file = lib$${file}
				} else {
					# Copy the DLL to the output dir
					system(cp -puv $${BOOST_LIB_DIR}/$${file}.dll $${MAKE_SH_OUTPUT_DIR})
				}
			
				# Add the lib
				LIBS += $${BOOST_LIB_DIR}/$${file}.lib
				
			} else { # Boost 1.45.0 and onwards
				file = libboost_$${lib}-mgw44-mt-$${VER_STR}
			
				# Copy the DLL to the output dir
				system(cp -puv $${BOOST_LIB_DIR}/$${file}.dll $${MAKE_SH_OUTPUT_DIR})
				
				# Add the lib
				LIBS += $${BOOST_LIB_DIR}/$${file}.dll.a
				
			}
		}
	}
}
	
INCLUDEPATH += $${BOOST_INCLUDE_DIR}

