# QMake project file for WX libs
# This project file will add the configuration to the project for the WX
# libs, Please append the component you want to add to WX
#
# =============================================================================
# Version History 
#
# <version>	<yymmdd_hhmm>	<Name>
#	<comment ....>
# =============================================================================
#
# 0002		110704_2052		Jarrod Chesney
#	Added -p to copy commands
# 0001		100504_1209		Jarrod Chesney
#	Initial Writing
#
# =============================================================================

# Verify our environment
DEVENV_WX_VERSION = $$(DEVENV_WX_VERSION)

isEmpty(DEVENV_WX_VERSION) {
	error(DEVENV_WX_VERSION has not been defined in the build environment)
}


# This assumes you are not using QT at all
# Remove the QT components, We're only using qmake, no QT libs or includes
DEFINES -= UNICODE QT_LARGEFILE_SUPPORT
QT -= core gui 

# Linker args, Setup for a standard wx windows application with threading
LIBS += -mthreads  -Wl,--subsystem,windows -mwindows

# Add the core module to the list of libs
WX += core

# Add in WX
WX_LIB_DIR = $$(MAKE_SH_DEVENV_DIR)/wx/$$(DEVENV_WX_VERSION)/$$(DEVENV_BUILD_TYPE)/$$(DEVENV_LINK_TYPE)/lib
contains(DEVENV_LINK_TYPE, static) {

	contains(DEVENV_BUILD_TYPE, debug) {
		DEFINES += __WXDEBUG__
		for(lib, WX) {
			LIBS += $${WX_LIB_DIR}/libwx_mswd_$${lib}-2.8-$${DEVENV_BUILD_TARGET}.a
		}
		LIBS += $${WX_LIB_DIR}/libwx_based-2.8-$${DEVENV_BUILD_TARGET}.a
	}
	
	contains(DEVENV_BUILD_TYPE, release) {
		for(lib, WX) {
			LIBS += $${WX_LIB_DIR}/libwx_msw_$${lib}-2.8-$${DEVENV_BUILD_TARGET}.a
		}
		LIBS += $${WX_LIB_DIR}/libwx_base-2.8-$${DEVENV_BUILD_TARGET}.a
	}
	
	# Add the libs required for wx static objects 
	LIBS += -lrpcrt4 -luuid -lole32 -loleaut32 -lwinspool -lwinmm -lshell32 -lcomctl32 -lcomdlg32 -lctl3d32 -ladvapi32 -lws2_32 -lgdi32 
}

contains(DEVENV_LINK_TYPE, shared) {
	DEFINES += WXUSINGDLL

	contains(DEVENV_BUILD_TYPE, debug) {
		DEFINES += __WXDEBUG__
		# Add the libs to the linker args, theses are import libs
		for(lib, WX) {
			# Add the lib
			LIBS += $${WX_LIB_DIR}/libwx_mswd_$${lib}-2.8-$${DEVENV_BUILD_TARGET}.dll.a
			# Copy the DLL to the output dir
			system(cp -puv $${WX_LIB_DIR}/wxmsw28d_$${lib}_gcc_custom.dll $${MAKE_SH_OUTPUT_DIR})
		}

		# Add the base lib
		LIBS += $${WX_LIB_DIR}/libwx_based-2.8-$${DEVENV_BUILD_TARGET}.dll.a

		# Copy the dlls to the output directory
		system(cp -puv $${WX_LIB_DIR}/wxbase28d_gcc_custom.dll $${MAKE_SH_OUTPUT_DIR})
	}
	
	contains(DEVENV_BUILD_TYPE, release) {
		for(lib, WX) {
			# Add the lib
			LIBS += $${WX_LIB_DIR}/libwx_msw_$${lib}-2.8-$${DEVENV_BUILD_TARGET}.dll.a
			# Copy the DLL to the output dir
			system(cp -puv $${WX_LIB_DIR}/wxmsw28_$${lib}_gcc_custom.dll $${MAKE_SH_OUTPUT_DIR})
		}

		# Add the base lib
		LIBS += $${WX_LIB_DIR}/libwx_base-2.8-$${DEVENV_BUILD_TARGET}.dll.a

		# Copy the dlls to the output directory
		system(cp -puv $${WX_LIB_DIR}/wxbase28_gcc_custom.dll $${MAKE_SH_OUTPUT_DIR})
	}

}

INCLUDEPATH += $${WX_LIB_DIR}/wx/include/$${DEVENV_BUILD_TARGET}-msw-ansi-$$(DEVENV_BUILD_TYPE)-2.8
INCLUDEPATH += $$(MAKE_SH_DEVENV_DIR)/wx/$$(DEVENV_WX_VERSION)/$$(DEVENV_BUILD_TYPE)/$$(DEVENV_LINK_TYPE)/include/wx-2.8
DEFINES += _IODBC_ __WXMSW__

