# Custom PRO file
# Allows project specific options such as libs and includes
# Includes notes from the manual
# For more info, See http://doc.trolltech.com/4.6/qmake-project-files.html
#
# =============================================================================
# Version History 
#
# <version>	<yymmdd_hhmm>	<Name>
#	<comment ....>
# =============================================================================
#
# 0003		100519_1344		Jarrod Chesney
#	Updated boost comments
# 0002		100504_1402		Jarrod Chesney
#	Converted stricture to the modular libs/???.pri format
# 0001		100226_2008		Jarrod Chesney
#	Initial Writing
#
# =============================================================================


# Declaring Other Libraries
# If you are using other libraries in your project in addition
# to those supplied with Qt, you need to specify them in your project file.

# The paths that qmake searches for libraries and the specific
# libraries to link against can be added to the list of values
# in the LIBS variable.
# The paths to the libraries themselves can be given, or the familiar
# Unix-style notation for specifying libraries and paths can be
# used if preferred.

# For example, the following lines show how a library can be specified:
# LIBS += -L/usr/local/lib -lmath

# The paths containing header files can also be specified in a similar
# way using the INCLUDEPATH variable.
# For example, it is possible to add several paths to be searched for
# header files:
#INCLUDEPATH = c:/msdev/include d:/stl/include

# NOTE : The libs for the development environment are added from their respective
# lib/xxx.pri files. You only need to add the additional libs you want and then
# include the pri files.
# This allows the individual lib project files to be improved with out the need for
# editing this file.
#
# Some Examples
#

# Include the WX libs
# NOTE : Adding WX removes all QT libs, headers and defines
# Add the advanced component for WX
# WX += adv
# include(../../build/libs/wx.pri)

# Include the BOOST libs and headers
# Add the libs to this variable
# BOOST += math_c99f date_time
# include(../../build/libs/boost.pri)

# Include the DTL lib
# include(../../build/libs/dtl.pri)


QT += sql
CONFIG -= console

BOOST += system thread filesystem date_time
DEFINES += BOOST_ENABLE_ASSERT_HANDLER

include(../../build/libs/boost.pri)
LIBS += -lws2_32
# -lwsock32 

INCLUDEPATH += "../../Source"
INCLUDEPATH += "../../Source/autotest"
INCLUDEPATH += "../../Source/autotest/gen_batch"
INCLUDEPATH += "../../Source/autotest/execute"
INCLUDEPATH += "../../Source/autotest/execute/summary"
INCLUDEPATH += "../../Source/autotest/execute/test_queue"
INCLUDEPATH += "../../Source/autotest/execute/processor"
INCLUDEPATH += "../../Source/autotest/execute/processor/initors"
INCLUDEPATH += "../../Source/autotest/execute/processor/resultors"
INCLUDEPATH += "../../Source/autotest/execute/processor/sim_dispatcher_proxy"
INCLUDEPATH += "../../Source/autotest/results"
INCLUDEPATH += "../../Source/autotest/export"
INCLUDEPATH += "../../Source/main_uc"
INCLUDEPATH += "../../Source/connectors"
INCLUDEPATH += "../../Source/connectors/sim_dll"
INCLUDEPATH += "../../Source/connectors/agent"
INCLUDEPATH += "../../Source/connectors/tcp_client"
INCLUDEPATH += "../../Source/point_control"
INCLUDEPATH += "../../Source/point_control/models"
INCLUDEPATH += "../../Source/point_control/sim_control"
INCLUDEPATH += "../../Source/util"
INCLUDEPATH += "../../Source/settings"
INCLUDEPATH += "../../Source/models"
INCLUDEPATH += "../../Source/models/syntest"
INCLUDEPATH += "../../Source/models/syntest/create"
INCLUDEPATH += "../../Source/models/syntest/initialise"

RESOURCES += "../../../icons/synerty.qrc"

DEFINES += USE_DB_ENMAC_DLL
INCLUDEPATH += "../../../DbEnmacLib/Source/"
LIBS += -L'../../../DbEnmacLib/Output/Release32Shared/'
LIBS += -lDbEnmacLib

DEFINES += USE_DB_SIMSCADA_DLL
INCLUDEPATH += "../../../DbSimScadaLib/Source/"
LIBS += -L'../../../DbSimScadaLib/Output/Release32Shared/'
LIBS += -lDbSimScadaLib

DEFINES += USE_DB_SYNTEST_DLL
INCLUDEPATH += "../../../DbSynTestLib/Source/"
LIBS += -L'../../../DbSynTestLib/Output/Release32Shared/'
LIBS += -lDbSynTestLib

INCLUDEPATH += "../../../RsOrm/Source/"
LIBS += -L'../../../RsOrm/Output/Release32Shared/'
LIBS += -lRsOrm

#DEFINES += USE_MOD_SYNTEST_LINK_DLL
#INCLUDEPATH += "../../../ModSynTestLink/Source/"
#LIBS += -L'../../../ModSynTestLink/Output/Release32Shared/'
#LIBS += -lModSynTestLink
			
# Copy the database schema from the project into the build folder
system(cp -puv db/SynTestDbSchema.sql $${MAKE_SH_OUTPUT_DIR})
